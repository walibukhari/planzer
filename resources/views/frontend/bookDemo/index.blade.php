@extends('layouts.default')
@section('title')
    Book Demo
@endsection
@push('css')
    <style>
        form input:not([type="checkbox"]), .greenhouse-form input:not([type="checkbox"]) {
            background:#fff !important;
        }
        .js-field{
            border: 1px solid #e5e5e5 !important;
        }
        .p-demo .shadowed-box {
            background: transparent !important;
            padding: 50px;
            box-shadow: none !important;
            z-index: 2;
        }
        .p-demo {
            background: #fff !important;
        }
        .p-demo .shadowed-box {
            width: 60% !important;
            margin: 0 auto !important;
        }
        .p-demo .hero .hero-copy {
            text-align:center;
        }
        #gridContainer{
            display: flex;
            margin-top:50px;
            flex-direction: column;
            align-items: center;
        }
        .p-demo .field-wrap-inline {
            margin-bottom:2px !important;
        }
        .form *, .greenhouse-form *{
            margin-bottom: 15px !important;
        }
    </style>
@endpush
@section('content')
<article class="p-demo js-demo-page" data-layout="control">

    <section class="hero">
        <div class="grid container" id="gridContainer">
            <div class="row" style="justify-content: center;">
                <div class="col col-small-12 col-medium-10 push-medium-1 demo-form-col" style="display:flex;justify-content: center;align-items: center;margin-left:0px;">
                    <div class="hero-copy mt-lg-10">
                        <h1>It’s demo time</h1>
                        <div>
                            <h4 class="h5 mt-10" >
                                This is your gateway to the magical land of socialday.io, where we help you improve relationships with your audience on social media. It’s pretty simple, just like our software. Sign up so you can see socialday.io in action.
                            </h4>
                            <ul class="layout-side-bullets layout-side-bullets-taller layout-side-all list-check-circle-fill mt-10">
                                <li>Let customer conversations flow securely across channels</li>
                                <li>Give agents access to the information they need</li>
                                <li>Configure your support and workflow</li>
                                <li>Get access to customer intelligence and benchmark data</li>
                            </ul>
                            <section class="layout-side-logos layout-side-all logo-module container">
                                <h3 class="logo-module-header">The companies you use, use Zendesk</h3>
                                <div class="grid">
                                    <div class="row">
                                        <div class="col col-4">
                                            <img src="https://web-assets.zendesk.com/images/p-demo/logo-venmo_kale-subdued.svg" alt="Venmo logo">
                                        </div>
                                        <div class="col col-4">
                                            <img src="https://web-assets.zendesk.com/images/p-demo/logo-slack_kale-subdued.svg" alt="Slack logo">
                                        </div>
                                        <div class="col col-4">
                                            <img src="https://web-assets.zendesk.com/images/p-demo/logo-uber_kale-subdued_xs.svg" alt="Uber logo">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col col-4">
                                            <img src="https://web-assets.zendesk.com/images/p-demo/logo-open-table_kale-subdued.svg" alt="OpenTable logo">
                                        </div>
                                        <div class="col col-4">
                                            <img src="https://web-assets.zendesk.com/images/p-demo/logo-black-and-decker_kale-subdued_cc.svg" alt="Stanley Black &amp; Decker logo">
                                        </div>
                                        <div class="col col-4">
                                            <img src="https://web-assets.zendesk.com/images/p-demo/logo-shopify_kale-subdued.svg" alt="Shopify logo">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col col-4">
                                            <img src="https://web-assets.zendesk.com/images/p-demo/logo-ingersoll-rand_kale-subdued.svg" alt="Ingersoll Rand logo">
                                        </div>
                                        <div class="col col-4">
                                            <img src="https://web-assets.zendesk.com/images/p-demo/logo-tesco_kale-subdued.svg" alt="Tesco logo">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
{{--                        <img src="https://web-assets.zendesk.com/images/p-demo/demo-233b-best.jpg" alt="Woman pointing">--}}
                    </div>
                </div>
                <style>
                    .inputround{
                        border-radius: 6px !important;
                    }
                </style>
                <div class="col col-small-12 col-medium-10 push-medium-1 demo-form-col" style="display:flex;justify-content: center;align-items: center;margin-left:0px;">
                    <div class="shadowed-box">
                        <form class="greenhouse-form demo-form js-demo-form" method="post">
                            <div class="field-container js-field-container js-step" data-step="1">
                                <h3 class="h5">Tell us a bit about yourself, and we’ll tell you a lot more about us.</h3>
                                <div class="abcdef field-wrap-inline">
                                    <div class="field-wrap js-field-wrap js-first-name-container">
                                        {{-- <label class="field-label" for="FirstName">First name</label> --}}
                                        <input type="text" class="first-name required js-field inputround" id="FirstName" name="FirstName" autocomplete="given-name" placeholder="FirstName" tabindex="0" /><div class="field-error js-field-error">Please enter your first name</div> </div>
                                    <div class="field-wrap js-field-wrap js-last-name-container">
                                        {{-- <label class="field-label" for="LastName">Last name</label> --}}
                                        <input type="text" class="last-name required js-field inputround" id="LastName" name="LastName" autocomplete="family-name" placeholder="LastName" tabindex="0" /><div class="field-error js-field-error">Please enter your last name</div> </div>
                                </div>
                                <div class="abcdef field-wrap js-field-wrap">
                                    {{-- <label class="field-label" for="owner[email]">Work email</label> --}}
                                    <input type="email" class="email required js-field inputround" id="owner[email]" name="owner[email]" autocomplete="email" placeholder="Email" activateFields="phone" tabindex="0" /><div class="field-error js-field-error">Please enter a valid email address
                                    </div>
                                </div>
                                <div class="abcdef field-wrap js-field-wrap">
                                    {{-- <label class="field-label" for="phone">Phone Number</label> --}}
                                    <input type="number" class="phone required js-field inputround" name="phone" placeholder="Phone Number" /><div class="field-error js-field-error">Please enter a valid phone number
                                    </div>
                                </div>
                                <div class="abcdef field-wrap">
                                    <button class="btn btn btn-sunset btn-medium rounded-5 font-size-3" type="submit">Watch now</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="pt-12 pb-9">
                        <!-- Company Section -->
                        <div class="container">
                            <div class="row justify-content-center">
                                <div class="col-xl-12">
                                    <!-- Brand Logos -->
                                    <div class="brand-logo-small d-flex align-items-center justify-content-center justify-content-lg-between flex-wrap">
                                        <!-- Single Brand -->
                                        <div class="single-brand-logo opacity-5 mx-5 my-6">
                                            <img class="light-version-logo aos-init aos-animate" src="http://localhost:8000/images/Client_1.png" style="width: auto;height: 50px;" alt="" data-aos="fade-in" data-aos-once="true">
                                            <img class="dark-version-logo default-logo aos-init aos-animate" src="http://localhost:8000/images/Client_1.png" style=" height: 50px;" alt="" data-aos="fade-in" data-aos-once="true">
                                        </div>
                                        <!-- Single Brand -->
                                        <div class="single-brand-logo opacity-5 mx-5 my-6">
                                            <img class="light-version-logo aos-init aos-animate" src="http://localhost:8000/images/Client_2.png" style="width: auto;height: 50px;" alt="" data-aos="fade-in" data-aos-duration="300" data-aos-delay="300" data-aos-once="true">
                                            <img class="dark-version-logo default-logo aos-init aos-animate" src="http://localhost:8000/images/Client_2.png" style="width: auto;height: 50px;" alt="" data-aos="fade-in" data-aos-duration="300" data-aos-delay="300" data-aos-once="true">
                                        </div>
                                        <!-- Single Brand -->
                                        <div class="single-brand-logo opacity-5 mx-5 my-6">
                                            <img class="light-version-logo aos-init aos-animate" src="http://localhost:8000/images/Client_3.png" style="width: auto;height: 50px;" alt="" data-aos="fade-in" data-aos-duration="600" data-aos-delay="500" data-aos-once="true">
                                            <img class="dark-version-logo default-logo aos-init aos-animate" src="http://localhost:8000/images/Client_3.png" style="width: auto;height: 50px;" alt="" data-aos="fade-in" data-aos-duration="600" data-aos-delay="500" data-aos-once="true">
                                        </div>
                                        <!-- Single Brand -->
                                        <div class="single-brand-logo opacity-5 mx-5 my-6">
                                            <img class="light-version-logo aos-init aos-animate" src="http://localhost:8000/images/Client_4.png" style="width: auto;height: 50px;" alt="" data-aos="fade-in" data-aos-duration="900" data-aos-delay="700" data-aos-once="true">
                                            <img class="dark-version-logo default-logo aos-init aos-animate" src="http://localhost:8000/images/Client_4.png" style="width: auto;height: 50px;" alt="" data-aos="fade-in" data-aos-duration="900" data-aos-delay="700" data-aos-once="true">
                                        </div>
                                        <!-- Single Brand -->
                                        <div class="single-brand-logo opacity-5 mx-5 my-6">
                                            <img class="light-version-logo aos-init aos-animate" src="http://localhost:8000/images/Client_5.png" style="width: auto;height: 50px;" alt="" data-aos="fade-in" data-aos-duration="1200" data-aos-delay="700" data-aos-once="true">
                                            <img class="dark-version-logo default-logo aos-init aos-animate" src="http://localhost:8000/images/Client_5.png" style="width: auto;height: 50px;" alt="" data-aos="fade-in" data-aos-duration="1200" data-aos-delay="700" data-aos-once="true">
                                        </div>
                                    </div>
                                    <!-- End Brand Logos -->
                                </div>
                            </div>
                        </div>
                        <!-- End Company Section -->
                    </div>
                </div>
            </div>
        </div>
        <div class="hide-small-up">
            <figure class="hero-image-mobile lazyload">
            </figure>
            <div class="hero-mobile-content">
                <h4 class="h5">This is your gateway to the magical land of Zendesk, where we help you improve relationships with your customers. It’s pretty simple, just like our software. Sign up so you can see Zendesk in action.</h4>
                <ul class="layout-side-bullets layout-side-bullets-taller layout-side-all list-check-circle-fill">
                    <li>Let customer conversations flow securely across channels</li>
                    <li>Give agents access to the information they need</li>
                    <li>Configure your support and workflow</li>
                    <li>Get access to customer intelligence and benchmark data</li>
                </ul>
                <section class="layout-side-logos layout-side-all logo-module container">
                    <h3 class="logo-module-header">The companies you use, use Zendesk</h3>
                    <div class="grid">
                        <div class="row">
                            <div class="col col-4">
                                <img src="https://web-assets.zendesk.com/images/p-demo/logo-venmo_kale-subdued.svg" alt="Venmo logo">
                            </div>
                            <div class="col col-4">
                                <img src="https://web-assets.zendesk.com/images/p-demo/logo-slack_kale-subdued.svg" alt="Slack logo">
                            </div>
                            <div class="col col-4">
                                <img src="https://web-assets.zendesk.com/images/p-demo/logo-uber_kale-subdued_xs.svg" alt="Uber logo">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col col-4">
                                <img src="https://web-assets.zendesk.com/images/p-demo/logo-open-table_kale-subdued.svg" alt="OpenTable logo">
                            </div>
                            <div class="col col-4">
                                <img src="https://web-assets.zendesk.com/images/p-demo/logo-black-and-decker_kale-subdued_cc.svg" alt="Stanley Black & Decker logo">
                            </div>
                            <div class="col col-4">
                                <img src="https://web-assets.zendesk.com/images/p-demo/logo-shopify_kale-subdued.svg" alt="Shopify logo">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col col-4">
                                <img src="https://web-assets.zendesk.com/images/p-demo/logo-ingersoll-rand_kale-subdued.svg" alt="Ingersoll Rand logo">
                            </div>
                            <div class="col col-4">
                                <img src="https://web-assets.zendesk.com/images/p-demo/logo-tesco_kale-subdued.svg" alt="Tesco logo">
                            </div>
                        </div>
                    </div>
                </section> </div>
        </div>
    </section>
</article>
@endsection
@push('js')
@endpush
