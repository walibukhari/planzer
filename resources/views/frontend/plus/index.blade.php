@extends('layouts.default')
@section('title')
    Plus
@endsection
@push('css')

@endpush

@section('content')
    @include('frontend.modal.getStarted')
    <div class="pt-10 pt-md-18 pt-lg-34 pb-13 pb-md-17 pb-lg-33">
        <div class="container">
            <div class="row justify-content-center justify-content-lg-start align-items-center">
                <!-- Hero Content -->
                <div class="col-xl-5 col-lg-6 col-md-8 col-xs-10 order-2 order-lg-1" data-aos="fade-right" data-aos-duration="500" data-aos-once="true">
                    <div class="pr-lg-10 pr-xl-0 pt-0 pt-lg-0">
                        <h1 class="font-size-12 mb-8 pr-xs-15 pr-sm-10 pr-md-0">Should we manage your social media? </h1>
                        <p class="font-size-7 mb-0 heading-default-color">When you choose a socialday plus solution, will we handle all aspects of your social media management. </p>
                        <div class=" pt-9 pt-lg-10">
                            <a class="btn btn-dodger-blue-2 rounded-5" data-toggle="modal" data-target="#myModal">Get Started</a>
                            <span class="d-block pt-5 pt-lg-9 heading-default-color">Still confused?
                        <a class="video-btn text-dodger-blue-2" href="https://www.youtube.com/watch?v=QZazlSLaFww">Check our 1 min video</a>
                        </span>
                        </div>
                    </div>
                </div>
                <!-- End Hero Content -->
                <!-- l2-hero-image-group -->
                <div class="col-lg-6 offset-xl-1 col-md-7 col-xs-10 order-1 order-lg-2" data-aos="fade-left" data-aos-duration="800" data-aos-once="true">
                    <div class="l2-hero-image mb-8 mb-lg-0 mt-14 mt-lg-0">
                        <img class="w-100" src="{{asset('images/Planzer plus feed mockup.png')}}" alt="">
                    </div>
                </div>
                <!-- End l2-hero-image-group -->
            </div>
        </div>
    </div>
    <!-- End Hero Area -->
    <!-- Counter Area -->
    <div class="pt-lg-5 pb-12 pb-md-16 pb-lg-27">
        <div class="container">
            <div class="row justify-content-center">
                <!-- Left -->
                <div class="col-xl-5 col-lg-6 col-md-9">
                    <div class="row">
                        <!-- Single Counter -->
{{--                        <div class="col-xs-6">--}}
{{--                            <div class="mb-11 mb-lg-0" data-aos="fade-left" data-aos-once="true">--}}
{{--                                <h2 class="font-size-12 mb-6">--}}
{{--                                    <span class="counter">12</span>K--}}
{{--                                </h2>--}}
{{--                                <div class="mt-5">--}}
{{--                                    <img class="mb-7" src="https://uxtheme.net/demos/finity/image/home-2/png/heart-icon-group.png" alt="">--}}
{{--                                    <h5 class="font-size-5 font-weight-normal mb-1">Active Downloads</h5>--}}
{{--                                    <p class="font-size-5 text-dodger-blue-2 mb-0">Plus Partners</p>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
                        <!-- End Single Counter -->
                        <!-- Single Counter -->
                        <div class="col-xs-6">
{{--                            <div class="mb-11 mb-lg-0" data-aos="fade-left" data-aos-once="true">--}}
{{--                                <h2 class="font-size-12 mb-6">--}}
{{--                                    <span class="counter">4.9</span>--}}
{{--                                </h2>--}}
{{--                                <div class="mt-5">--}}
{{--                                    <img class="mb-7" src="https://uxtheme.net/demos/finity/image/home-2/png/star-group.png" alt="">--}}
{{--                                    <h5 class="font-size-5 font-weight-normal mb-1">1,938 Rating</h5>--}}
{{--                                    <p class="font-size-5 text-dodger-blue-2 mb-0">Satisfaction</p>--}}
{{--                                </div>--}}
{{--                            </div>--}}
                        </div>
                        <!-- End Single Counter -->
                    </div>
                </div>
                <!-- Left End -->
                <!-- Right Start -->
                <div class="col-lg-12 offset-xl-1 col-md-9">
                    <div class="row">
                        <div class="col-lg-12 col-md-10 col-xs-9">
                            <div class="section-title" data-aos="flip-up" data-aos-once="true">
                                <h2 class="font-size-9 pr-xl-22 mb-7 mb-lg-11 pr-lg-0">Trysted by some of the best world leading brands.</h2>
                            </div>
                        </div>
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-12">
                            <h6 class="font-size-6 font-weight-normal text-stone mb-5 mb-lg-7">Also featured in</h6>
                            <div class="d-flex align-items-center flex-wrap">
                                <!-- Single Brand -->
                                <div class="single-brand-logo mr-10 my-6" data-aos="fade-in" data-aos-duration="400" data-aos-once="true">
                                    <img src="https://uxtheme.net/demos/finity/image/home-2/png/star-group.png" alt="">
                                </div>
                                <!-- Single Brand -->
                                <div class="single-brand-logo mr-10 my-6" data-aos="fade-in" data-aos-duration="600" data-aos-delay="300" data-aos-once="true">
                                    <img src="https://uxtheme.net/demos/finity/image/home-2/png/company-logo/company-logo-1.png" alt="">
                                </div>
                                <!-- Single Brand -->
                                <div class="single-brand-logo mr-10 my-6" data-aos="fade-in" data-aos-duration="800" data-aos-delay="500" data-aos-once="true">
                                    <img src="https://uxtheme.net/demos/finity/image/home-2/png/company-logo/company-logo-3.png" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Right End -->
            </div>
        </div>
    </div>
    <!-- End Counter Area -->
    <!-- Progress Area -->
    <div class="pt-13 pt-md-18 pt-lg-23 pb-md-9 pb-lg-21 border-top border-default-color-1">
        <div class="container">
            <!-- Section Title -->
            <div class="row justify-content-center">
                <div class="col-xl-7 col-lg-8 col-md-9 col-xs-10">
                    <div class="text-center pb-13 pb-lg-12 pr-lg-10 pr-xl-0">
                        <h2 class="font-size-11 mb-0">Working with a plus.</h2>
                    </div>
                </div>
            </div>
            <!-- Section Title -->
            <!-- Progress Items -->
            <div class="row justify-content-center">
                <div class="col-sm-12 col-xs-8">
                    <div class="timeline-area d-sm-flex justify-content-center justify-content-lg-between flex-wrap flex-lg-nowrap position-relative">
                        <!-- Image Group -->
                        <div class="image-group-3">
                            <div class="arrow-shape-1 d-none d-lg-block absolute-top-left" data-aos="zoom-in" data-aos-delay="400" data-aos-once="true">
                                <img src="https://uxtheme.net/demos/finity/image/home-2/png/arrow-1.png" alt="">
                            </div>
                            <div class="arrow-shape-2 d-none d-lg-block absolute-top-right" data-aos="zoom-in" data-aos-delay="600" data-aos-once="true">
                                <img src="https://uxtheme.net/demos/finity/image/home-2/png/arrow-2.png" alt="">
                            </div>
                        </div>
                        <!-- Single Progress -->
                        <div class="single-widgets pr-md-18 pr-lg-0 pl-lg-10 pl-xl-22 mb-10 mb-lg-0 text-center text-md-left" data-aos="zoom-in" data-aos-delay="300" data-aos-once="true">
                            <div class="square-97 bg-dodger-blue-2 rounded-10 mb-10 shadow-bg-dodger-blue-2 mx-auto mx-md-0">
                                <img src="https://uxtheme.net/demos/finity/image/home-2/png/search.png" alt="">
                            </div>
                            <div class="">
                                <h3 class="font-size-8 mb-6">Planning and brainstorming</h3>
                                <p class="font-size-5 line-height-28 mb-0">We plan ahead and find the perfect social strategy for there brand, in terms of growing the online audience.</p>
                            </div>
                        </div>
                        <!-- End Single Progress -->
                        <!-- Single Progress -->
                        <div class="single-widgets pr-md-18 pr-lg-0 pl-lg-10 pl-xl-22 mb-10 mb-lg-0 text-center text-md-left" data-aos="zoom-in" data-aos-delay="500" data-aos-once="true">
                            <div class="square-97 bg-dodger-blue-2 rounded-10 mb-10 shadow-bg-dodger-blue-2 mx-auto mx-md-0">
                                <img src="https://uxtheme.net/demos/finity/image/home-2/png/event-confirm.png" alt="">
                            </div>
                            <div class="">
                                <h3 class="font-size-8 mb-6">Professional execution</h3>
                                <p class="font-size-5 line-height-28 mb-0">We provide the perfect content for your profiles on all channels, and ensure a high amount of engagement with your customers.</p>
                            </div>
                        </div>
                        <!-- End Single Progress -->
                        <!-- Single Progress -->
                        <div class="single-widgets pr-md-18 pr-lg-0 pl-lg-10 pl-xl-22 mb-10 mb-lg-0 text-center text-md-left" data-aos="zoom-in" data-aos-delay="700" data-aos-once="true">
                            <div class="square-97 bg-dodger-blue-2 rounded-10 mb-10 shadow-bg-dodger-blue-2 mx-auto mx-md-0">
                                <img src="https://uxtheme.net/demos/finity/image/home-2/png/right-layer.png" alt="">
                            </div>
                            <div class="">
                                <h3 class="font-size-8 mb-6">Data driven reporting</h3>
                                <p class="font-size-5 line-height-28 mb-0">As a socialday plus partner you will receive a detailed report in the end of every month with detailed results of your investment.</p>
                            </div>
                        </div>
                        <!-- End Single Progress -->
                    </div>
                </div>
            </div>
            <!-- End Progress Items -->
        </div>
    </div>
    <!-- End Progress Area -->
    <!-- contentOne Area -->
    <div class="pb-lg-14">
        <div class="container">
            <!-- Section Title -->
            <div class="row justify-content-center">
                <div class="col-xl-6 col-lg-7 col-md-8">
                    <div class="text-center mb-16 mb-lg-22 pr-lg-10 pr-xl-0">
                        <h2 class="font-size-11 mb-0" style="padding-bottom:70px;">Weekly reports to show your performance.</h2>
                    </div>
                </div>
            </div>
            <!-- Section Title -->
            <div class="row justify-content-center">
                <!-- Image Section -->
                <div class="col-lg-9 col-md-10 ">
                    <div class="l2-content-image-group-1 w-100" data-aos="zoom-in" data-aos-once="true">
                        <!-- Dashboard Img -->
                        <img style="    width: 60% !important; margin-bottom:120px;" class="w-100 w-lg-auto" src="/images/Report.png" alt="">
                        <!-- content-card-img-1 Img -->
{{--                        <div class="img-2">--}}
{{--                            <img src="https://uxtheme.net/demos/finity/image/home-2/png/card-img-1.png" alt="">--}}
{{--                        </div>--}}
                        <!-- End content-card-img-1 Img -->
                        <!-- img-group-content -->
{{--                        <div class="img-group-content">--}}
{{--                            <img src="https://uxtheme.net/demos/finity/image/home-2/png/l2-content-1-card.png" alt="">--}}
{{--                        </div>--}}
                        <!-- End Img-group-content -->
                        <div class="img-3">
                            <img src="https://uxtheme.net/demos/finity/image/home-1/png/dot-bg.png" alt="">
                        </div>
                        <!-- End Dashboard Img -->
                    </div>
                </div>
            </div>
        </div>
        <div class="full-width-shape w-100">
            <img class="w-100" src="https://uxtheme.net/demos/finity/image/home-2/png/content-shape.png" alt="">
        </div>
    </div>
    <!-- End contentOne Area -->
    <!-- contentTwo Area -->
    <div class="pt-10 pt-md-0 pb-md-8 pb-lg-31">
        <div class="container">
            <div class="row align-items-center justify-content-center">
                <!-- Left -->
                <div class="col-xl-4 col-lg-5 col-md-6 col-xs-8 order-2 order-md-1" data-aos="fade-right" data-aos-duration="500" data-aos-once="true">
                    <h2 class="font-size-11">Grow with social media.</h2>
                    <ul class="list-unstyled mt-9 mt-lg-14">
                        <!-- Single List -->
                        <li class="media align-items-center mb-12">
                            <div class="border square-83 rounded-10 mr-9">
                                <img src="https://uxtheme.net/demos/finity/image/home-2/png/explore.png" alt="">
                            </div>
                            <div class="content">
                                <h5>Professional content</h5>
                                <p class="font-size-5 line-height-28 mb-0">Our team of content creators will provide fresh content for your channels.</p>
                            </div>
                        </li>
                        <!-- Single List -->
                        <!-- Single List -->
                        <li class="media align-items-center mb-12">
                            <div class="border square-83 rounded-10 mr-9">
                                <img src="https://uxtheme.net/demos/finity/image/home-2/png/interview.png" alt="">
                            </div>
                            <div class="content">
                                <h5 class="font-size-7">Customer engagement</h5>
                                <p class="font-size-5 line-height-28 mb-0">We engage and creates traffic across your social channels.</p>
                            </div>
                        </li>
                        <!-- Single List -->
                        <!-- Single List -->
                        <li class="media align-items-center mb-12">
                            <div class="border square-83 rounded-10 mr-9">
                                <img src="https://uxtheme.net/demos/finity/image/home-2/png/card-favorite.png" alt="">
                            </div>
                            <div class="content">
                                <h5 class="font-size-7">Effective lead generation</h5>
                                <p class="font-size-5 line-height-28 mb-0">The effort will generate leads and customers for your business.</p>
                            </div>
                        </li>
                        <!-- Single List -->
                    </ul>
                </div>
                <!-- Left End -->
                <!-- Right -->
                <div class="col-xl-6 offset-xl-2 col-lg-7 col-md-6 col-xs-8 order-1 order-md-2" data-aos="fade-left" data-aos-once="true">
                    <div class="l2-content-image-group-2 mb-10 mb-md-0 rounded-10">
                        <div class="img-1 position-relative text-right rounded-10">
                            <img class="w-100 w-lg-auto" src="https://uxtheme.net/demos/finity/image/home-2/png/content-2-img.png" alt="">
                            <div class="img-2">
                                <img class="w-100" src="https://uxtheme.net/demos/finity/image/home-2/png/patterns-dot-green.png" alt="">
                            </div>
                            <div class="img-3">
                                <img class="w-100 mix-blend-multiply" src="https://uxtheme.net/demos/finity/image/home-2/png/right-circlehalf-shape.png" alt="">
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Right -->
            </div>
        </div>
    </div>
    <!-- End contentTwo Area -->
    <!-- contentThree Area -->
    <div class="pb-12 pb-md-16 pb-lg-25">
        <div class="container">
            <div class="row justify-content-center justify-content-md-start">
                <div class="col-xl-6 col-lg-7 col-md-9 col-xs-11">
                    <div class="section-title" data-aos="fade-left" data-aos-duration="500" data-aos-once="true">
                        <h2 class="font-size-11 mb-10 mb-lg-20 text-center text-md-left">Get a personal social specialist.</h2>
                    </div>
                </div>
            </div>
            <div class="row align-items-lg-center justify-content-center">
                <!-- Right -->
                <div class="col-xl-6 col-lg-6 col-md-5 col-xs-8" data-aos="fade-right" data-aos-duration="500" data-aos-once="true">
                    <div class="content-image-group-3 mb-17 mb-lg-0 mr-xl-16">
                        <div class="img-1">
                            <img class="w-100" src="https://uxtheme.net/demos/finity/image/home-2/png/meeting-2.png" alt="">
                            <div class="img-2">
                                <img class="w-100" src="https://uxtheme.net/demos/finity/image/home-2/png/dot-pattern-black.png" alt="">
                            </div>
                            <div class="img-3 rounded-10">
                                <img class="w-100 mix-blend-multiply" src="https://uxtheme.net/demos/finity/image/home-2/png/left-circlehalf-shape.png" alt="">
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Right -->
                <!-- Left -->
                <div class="col-lg-6 col-md-7 col-xs-10" data-aos="fade-left" data-aos-duration="500" data-aos-once="true">
                    <div class="mr-lg-10 mr-xl-25">
                        <p class="heading-default-color font-size-6 mb-10">When you choose our plus solution, we handle all aspects of your social media. So you can focus on all the other important tasks you have when building your business.</p>
                        <p class="heading-default-color font-size-6 mb-11">
                            Specialized Social Specialist, who professionally manages your projects .
                            Professional content created for your channels .
                            We are responsible for coordination and communication between all parties .
                            Monthly performance reports, such as. a. illuminates your ROI, EPC & CPC .
                            Your brand achieves great and unique branding and exposure .
                            Tracking of engagement and turnover .
                        </p>
                        <div class="btn-group mb-12">
                            <a class="btn btn-dodger-blue-2 rounded-5" data-toggle="modal" data-target="#myModal">Get Started</a>
                        </div>
                    </div>
                </div>
                <!-- Left End -->
            </div>
        </div>
    </div>
    <!-- End contentThree Area -->
    <!-- TestimonialOne Area -->
    <div class="pt-lg-19 pb-12 pb-md-18 pb-lg-31">
        <!-- category section -->
        <div class="container">
            <!-- Section Title -->
            <div class="mb-8 mb-lg-5">
                <div class="row align-items-center justify-content-center justify-content-lg-start">
                    <div class="col-lg-7 col-md-9 col-xs-10">
                        <div class="mb-10 mb-lg-16 mb-md-0 text-center text-md-left" data-aos="fade-right" data-aos-duration="500" data-aos-once="true">
                <span class="bg-dodger-blue-2-op1 circle-84 font-size-9 mb-5 mb-lg-11 mx-auto mx-md-0">
                                <i
                                    class="fa fa-heart text-dodger-blue-2"></i>
                            </span>
                            <h2 class="font-size-11 mb-0">#Success stories with socialday plus.</h2>
                        </div>
                    </div>
                    <div class="col-lg-5 col-md-3" data-aos="fade-left" data-aos-duration="500" data-aos-once="true">
                        <div class="testimonial-one-button text-center text-md-right mt-3 mt-md-18"></div>
                    </div>
                </div>
            </div>
            <!-- End Section Title -->
            <!-- category slider -->
            <div class="row">
                <div class="col-12">
                    <div class="testimonial-one">
                        <div class="single-category focus-reset">
                            <div class="bg-white border rounded-10 pt-9 pl-10 pr-10 pb-8 mr-5 mr-lg-9">
                                <div class="circle-48 mb-8">
                                    <img src="https://uxtheme.net/demos/finity/image/home-2/png/testimonial-img-1.png" alt="">
                                </div>
                                <div class="">
                                    <p class="font-size-5 line-height-28 text-dark-cloud mr-xl-5">I owe these guys my life. Already used their landing page templates for my latest two projects.</p>
                                    <h4 class="font-size-6 mb-0 text-dark-cloud font-weight-medium">- @thepatwalls</h4>
                                </div>
                            </div>
                        </div>
                        <div class="single-category focus-reset">
                            <div class="bg-white border rounded-10 pt-9 pl-10 pr-10 pb-8 mr-5 mr-lg-9">
                                <div class="circle-48 mb-8">
                                    <img src="https://uxtheme.net/demos/finity/image/home-2/png/testimonial-img-2.png" alt="">
                                </div>
                                <div class="">
                                    <p class="font-size-5 line-height-28 text-dark-cloud mr-xl-5">Time is the most precious thing you have when running a business. You can&#39;t take time to ponder on design…</p>
                                    <h4 class="font-size-6 mb-0 text-dark-cloud font-weight-medium">- @thepatwalls</h4>
                                </div>
                            </div>
                        </div>
                        <div class="single-category focus-reset">
                            <div class="bg-white border rounded-10 pt-9 pl-10 pr-10 pb-8 mr-5 mr-lg-9">
                                <div class="circle-48 mb-8">
                                    <img src="https://uxtheme.net/demos/finity/image/home-2/png/testimonial-img-3.png" alt="">
                                </div>
                                <div class="">
                                    <p class="font-size-5 line-height-28 text-dark-cloud mr-xl-5">I owe these guys my life. Already used their landing page templates for my latest two projects. </p>
                                    <h4 class="font-size-6 mb-0 text-dark-cloud font-weight-medium">- @thepatwalls</h4>
                                </div>
                            </div>
                        </div>
                        <div class="single-category focus-reset">
                            <div class="bg-white border rounded-10 pt-9 pl-10 pr-10 pb-8 mr-5 mr-lg-9">
                                <div class="circle-48 mb-8">
                                    <img src="https://uxtheme.net/demos/finity/image/home-2/png/testimonial-img-4.png" alt="">
                                </div>
                                <div class="">
                                    <p class="font-size-5 line-height-28 text-dark-cloud mr-xl-5">I owe these guys my life. Already used their landing page templates for my latest two projects.</p>
                                    <h4 class="font-size-6 mb-0 text-dark-cloud font-weight-medium">- @thepatwalls</h4>
                                </div>
                            </div>
                        </div>
                        <div class="single-category focus-reset">
                            <div class="bg-white border rounded-10 pt-9 pl-10 pr-10 pb-8 mr-5 mr-lg-9">
                                <div class="circle-48 mb-8">
                                    <img src="https://uxtheme.net/demos/finity/image/home-2/png/testimonial-img-3.png" alt="">
                                </div>
                                <div class="">
                                    <p class="font-size-5 line-height-28 text-dark-cloud mr-xl-5">I owe these guys my life. Already used their landing page templates for my latest two projects.</p>
                                    <h4 class="font-size-6 mb-0 text-dark-cloud font-weight-medium">- @thepatwalls</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End category slider -->
        </div>
    </div>
    <!-- End category section -->
    <!-- End TestimonialOne Area -->
    <!-- promoContent Area -->
    <div class="bg-images" style="background-image:url(https://uxtheme.net/demos/finity/image/home-2/jpg/promo-bg-img.jpg)">
        <div class="container">
            <div class="row align-items-center justify-content-center">
                <div class="col-xl-8 col-lg-9 col-md-11">
                    <div class="text-center pt-14 pt-md-18 pb-15 pb-md-19 py-lg-31 dark-mode-texts" data-aos="zoom-in" data-aos-duration="500" data-aos-once="true">
                        <h2 class="font-size-11 mb-7" data-toggle="modal" data-target="#myModal">Get started with<br class="d-none d-sm-block"> socialday plus.</h2>
                        <p class="font-size-5 line-height-28 px-md-15 px-lg-23 px-xl-25 mb-0">Time is the most precious thing you have when running a business. You can't take time to ponder on time.</p>
                        <a class="btn btn-dodger-blue-2 rounded-5 mt-12" data-toggle="modal" data-target="#myModal">Get Started</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End promoContent Area -->

@endsection

@push('js')

@endpush
