<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SocialAccount extends Model
{
    use HasFactory;

    const INSTAGRAM = 'instagram';
    const FACEBOOK = 'facebook';
    const TWITTER = 'twitter';
    const LINK_DEN = 'link_den';
    const YOU_TUBE = 'youtube';

    protected $fillable = [
      'username' , 'social_type' , 'password'
    ];

    public static function saveUserCredentials($username , $password , $socialType){
        if($socialType == self::INSTAGRAM) {
            return self::create([
                'username' => $username,
                'password' => $password,
                'social_type' => self::INSTAGRAM
            ]);
        }
    }

    public static function updateUserCredentials($username , $password , $socialType){
        if($socialType == self::INSTAGRAM) {
            return self::where('social_type','=',self::INSTAGRAM)->update([
                'username' => $username,
                'password' => $password,
                'social_type' => self::INSTAGRAM
            ]);
        }
    }
}
