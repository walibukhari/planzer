@extends('userPortal.layout.app')
@section('title', 'Facebook')

@section('css')
@endsection

@push('style')
    <style>
        .panel-default{
            background: #fff;
            padding: 20px;
            margin-bottom: 30px;
            border-radius: 15px;
        }
        .panel-body > img {
            width:50px;
        }
        .items{
            border: 1px solid #b3b7bb;
            border-radius: 15px;
            padding: 15px;
            padding-bottom: 0px;
            margin-top: 15px;
        }
        .plan-btn{
            background-color: #0c0c0c !important;
            border: 0;
        }
        .plan-btn:hover{
            background-color: #1a202c !important;
        }
        .renew-btn{
            background-color: transparent !important;
            border: 1px solid #0c0c0c !important;
            margin-left: 10px;
            color: #1a202c !important;
        }
        .renew-btn:hover{
            background-color: #0c0c0c !important;
            color: #fff !important;

        }
        /*.renew-btn{*/
        /*    background-color: transparent !important;*/
        /*    border: 1px solid #fff !important;*/
        /*    margin-left: 10px;*/
        /*    color: #fff !important;*/
        /*}*/
        .panel-img{
            border-radius: 15px;
            margin-top: 20px;
        }
        .p-5 {
            padding: 20px !important;
        }
        .avatar-img{
            border-radius: 10px;
        }
    </style>
@endpush

@section('breadcrumb-title')
    <h3>Facebook Setup</h3>
@endsection

@section('breadcrumb-items')
    <li class="breadcrumb-item">Users</li>
    <li class="breadcrumb-item active">Add New User</li>
@endsection

@section('content')
    <div class="p-5">
        <div class="row">
            <div class="col-md-3 card panel-default">
                <h3>Plandzer</h3>
                <p>Home | Post Preview</p>
                <hr width="80%" style="margin: 0 auto; margin-bottom:20px;">
                <div class="col-md-12 m-auto">

                    <div class="accountlist">
                        <div class="listitem">
                            <div class="profile">
                                <img src="https://image.freepik.com/free-vector/profile-icon-male-avatar-hipster-man-wear-headphones_48369-8728.jpg" alt="">
                            </div>
                            <div class="detail">
                                <div class="name">Mr. Depp</div>
                                <div class="username">@jhonnydepp</div>
                            </div>
                        </div>
                        <div class="listitem">
                            <div class="profile">
                                <img src="https://image.freepik.com/free-vector/profile-icon-male-avatar-hipster-man-wear-headphones_48369-8728.jpg" alt="">
                            </div>
                            <div class="detail">
                                <div class="name">Mr. Depp</div>
                                <div class="username">@jhonnydepp</div>
                            </div>
                        </div>
                    </div>









                </div>
            </div>
            <div class="col-md-8 ml-5 card panel-default p-5">
                <div class="row" style="padding-left:30px;">
                    <h3 class="">Your Feed Images</h3>
                    <div class="row ml-auto" style="width: 30%">
                        <button class="btn btn-primary plan-btn">Change Plan</button>
                        <button class="btn btn-primary renew-btn">Renew</button>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-4 ">
                        <img class="panel-img" width="100%" src="https://i.pinimg.com/originals/b5/14/b9/b514b960869501961b425fb161175e49.jpg">
                    </div>
                    <div class="col-md-4 ">
                        <img class="panel-img" width="100%" src="https://i.pinimg.com/originals/b5/14/b9/b514b960869501961b425fb161175e49.jpg">
                    </div>
                    <div class="col-md-4 ">
                        <img class="panel-img" width="100%" src="https://i.pinimg.com/originals/b5/14/b9/b514b960869501961b425fb161175e49.jpg">
                    </div>
                    <div class="col-md-4 ">
                        <img class="panel-img" width="100%" src="https://i.pinimg.com/originals/b5/14/b9/b514b960869501961b425fb161175e49.jpg">
                    </div>
                    <div class="col-md-4 ">
                        <img class="panel-img" width="100%" src="https://i.pinimg.com/originals/b5/14/b9/b514b960869501961b425fb161175e49.jpg">
                    </div>
                    <div class="col-md-4 ">
                        <img class="panel-img" width="100%" src="https://i.pinimg.com/originals/b5/14/b9/b514b960869501961b425fb161175e49.jpg">
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection

@push('script')
    <script src="{{asset('assets/js/form-validation-custom.js')}}"></script>
@endpush
