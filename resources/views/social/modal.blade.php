<div class="modal fade" id="addnewset" ref="addnewset" tabindex="-1" role="dialog" aria-labelledby="addnewset" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle">Add New Set</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="">Name </label>
                    <input type="text" name="social_set_name" v-model="social_set_name" class="form-control" placeholder="Name of Social Set" >
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" v-if="add_new_set" @click="addNewSet()">Add</button>
                <button type="button" class="btn btn-primary" v-else @click="updateSets()">Update</button>
            </div>
        </div>
    </div>
</div>

{{-- select set modal --}}
<div class="modal fade" id="instagramCredit" ref="instagramCredit" tabindex="-1" role="dialog" aria-labelledby="addnewset" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle">Instagram Credential</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="Username">Username</label>
                    <input type="text" name="username" v-model="username" class="form-control" placeholder="Name of Social Set" >
                </div>
                <div class="form-group">
                    <label for="Password">Password</label>
                    <input type="text" name="username" v-model="password" class="form-control" placeholder="Name of Social Set" >
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" @click="saveInstagramCred()">Save</button>
            </div>
        </div>
    </div>
</div>
