<?php

namespace App\Http\Controllers\Engagement;

use App\Http\Controllers\Controller;
use App\Models\SocialAccountManager;
use App\Models\SocialSets;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class EngagementFeedController extends Controller
{
    //


    public function index(){
        $socialsets = SocialSets::where('user_id' , '=',Auth::user()->id)->get();
        // dd($socialsets);
        return view('social.engagement.index')->with('socialsets', $socialsets);
    }


    public function bysocialset(Request $request){
        $set_id = $request->setid;
        $account = $request->account;
        $account = SocialAccountManager::where(['set_id' => $set_id, 'social_network' => $account])->first();


        return view('social.engagement.feeds')->with('account', $account);

    }


}
