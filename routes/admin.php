<?php
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\AdminController;
use App\Http\Controllers\Admin\AuthController;
use App\Http\Controllers\Admin\PackageController;
use App\Http\Controllers\Admin\CompanyController;
use App\Http\Controllers\Admin\AccountProfileController;
use App\Http\Controllers\Admin\InboxController;
use App\Http\Controllers\Admin\TaskBoardController;
use App\Http\Controllers\Admin\SupportTicketController;
use App\Http\Controllers\HomeController;


Route::prefix('super-admin')->group(function () {
    Route::get('logout', [AuthController::class, 'logoutSuperAdmin'])->name('logoutSuperAdmin');
    Route::middleware('checkIsAdmin')->group(function () {
        Route::get('login', [AuthController::class, 'login'])->name('loginSuperAdmin');
        Route::post('login-post', [AuthController::class, 'postAdmin'])->name('postAdmin');
    });


    Route::middleware('admin_middleware')->group(function () {
        Route::view('index', 'dashboard.index')->name('index');
        Route::get('home', [HomeController::class, 'index'])->name('home');
        Route::get('add-user',[AdminController::class, 'add_new'])->name('add.new_user');
        Route::get('all-user',[AdminController::class, 'all_users'])->name('user.all');
        Route::post('save-user',[AdminController::class, 'save_user'])->name('save.user');
        Route::get('edit-user/{id}',[AdminController::class, 'edit_user'])->name('edit.user');
        Route::get('delete-user/{id}',[AdminController::class, 'delete_user'])->name('delete.user');

        /**  testimonial **/
        Route::get('testimonial',[AdminController::class , 'testimonial'])->name('testimonial');
        Route::post('post-testimonial',[AdminController::class , 'addTestimonial'])->name('addTestimonial');

        // packages routes //
        Route::get('create-package',[PackageController::class, 'create_new'])->name('add.new_package');
        Route::get('all-Packages',[PackageController::class, 'all_packages'])->name('packages.all');
        Route::post('save-package',[PackageController::class, 'store'])->name('save.package');
        Route::get('edit-package/{id}',[PackageController::class, 'edit_package'])->name('edit.package');
        Route::get('delete-package/{id}',[PackageController::class, 'delete_package'])->name('delete.package');

        // company routes //
        Route::get('company-detail',[CompanyController::class, 'companyDetail'])->name('company.detail');
        Route::get('create-company',[CompanyController::class, 'createCompany'])->name('create.company');
        Route::post('save-company',[CompanyController::class, 'saveCompany'])->name('save.company');
        Route::get('companies-list',[CompanyController::class, 'companiesList'])->name('companies.list');
        Route::get('edit-company/{id}',[CompanyController::class, 'editCompany'])->name('edit.company');
        Route::post('update-company/{id}',[CompanyController::class, 'updateCompany'])->name('update.company');
        Route::get('delete-company/{id}',[CompanyController::class, 'deleteCompany'])->name('delete.company');
        Route::get('company-users',[CompanyController::class, 'companyUsers'])->name('company.users');

        // account profile routes //
        Route::get('account-profile',[AccountProfileController::class, 'accountProfile'])->name('account.profile');
        Route::post('account-profile-update/{id}',[AccountProfileController::class, 'accountProfileUpdate'])->name('account.profile.update');
        Route::post('update-user-image',[AccountProfileController::class, 'updateUserImage'])->name('updateUserImage');



        // inbox routes //
        Route::get('inbox',[InboxController::class, 'inbox'])->name('inbox');

        // task board routes //
        Route::get('task-board',[TaskBoardController::class, 'taskBoard'])->name('taskBoard');

        // task board routes //
        Route::get('ticket-system',[SupportTicketController::class, 'ticket'])->name('ticket.system');
    });

});


