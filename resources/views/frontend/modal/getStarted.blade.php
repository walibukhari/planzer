<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog" style="max-width: 45% !important;">

        <!-- Modal content-->
        <div class="modal-content" >
                       <div class="modal-header">
                           <button type="button" class="close" data-dismiss="modal">&times;</button>
                       </div>
            <form data-aos="fade-up" data-aos-duration="800" data-aos-once="true" class="aos-init aos-animate">
                <div class="modal-body" >
                    @csrf
                    <div class="form-group mb-6 position-relative">
                        <label>Name: </label>
                        <input type="text" name="name" required class="form-control  form-control-lg bg-white rounded-4 text-dark-cloud text-placeholder-bali-gray pl-7 font-size-5" placeholder="Enter Name" id="email">
                    </div>
                    <div class="form-group mb-6 position-relative">
                        <label>Email: </label>
                        <input type="email" name="email" required class="form-control  form-control-lg bg-white rounded-4 text-dark-cloud text-placeholder-bali-gray pl-7 font-size-5" placeholder="Email Address" id="email">
                    </div>
                    <div class="form-group mb-6 position-relative">
                        <label>Phone Number: </label>
                        <input type="number" name="phone_number" required class="form-control  form-control-lg bg-white rounded-4 text-dark-cloud text-placeholder-bali-gray pl-7 font-size-5" placeholder="Phone Number" id="email">
                    </div>
                    <div class="form-group mb-6 position-relative">
                        <label>Company: </label>
                        <input type="text" name="company_name" required class="form-control  form-control-lg bg-white rounded-4 text-dark-cloud text-placeholder-bali-gray pl-7 font-size-5" placeholder="Enter Company Name" id="email">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn btn-sunset btn-medium rounded-5 font-size-3" data-dismiss="modal">Send</button>
                </div>
            </form>
        </div>

    </div>
</div>
