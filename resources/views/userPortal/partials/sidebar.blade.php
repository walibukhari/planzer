
<style>

    .activeSpanColor{
        color:#3f51DB !important;
    }

</style>

<div class="sidebar-wrapper">

    <div class="logo-wrapper" style="padding:6px 30px !important;padding-bottom: 12px;">
        <a href="{{route('/')}}" style="display:flex;justify-content:center;">
            <img class="img-fluid for-light " style="max-width:80%;margin-top:0px;position:relative;left:-30px;" src="{{asset('/SocialdayLogoColor1.png')}}" alt="" />
            <img class="img-fluid for-dark" style="max-width:80%;margin-top:0px;position:relative;left:-30px;" src="{{asset('/Socialday_Logo_Color _f7f7f9.png')}}" alt="" /></a>
        <div class="back-btn"><i class="fa fa-angle-left"></i></div>
        <div class="toggle-sidebar">
            <!-- <i class="status_toggle middle sidebar-toggle" data-feather="grid"> </i> -->
        </div>
    </div>
    <div class="logo-icon-wrapper">
        <a href="{{route('/')}}"><img class="img-fluid" src="{{asset('assets/images/logo/logo-icon.png')}}" alt="" /></a>
    </div>
    <nav class="sidebar-main">
        <div class="left-arrow" id="left-arrow"><i data-feather="arrow-left"></i></div>
        <div id="sidebar-menu">
            <ul class="sidebar-links custom-scrollbar">
                <li class="back-btn">
                    <a href="{{route('/')}}"><img class="img-fluid" src="{{asset('assets/images/logo/logo-icon.png')}}" alt="" /></a>
                    <div class="mobile-back text-right "><span>Back</span><i class="fa fa-angle-right pl-2 " aria-hidden="true"></i></div>
                </li>
            <!--  <li class="sidebar-main-title">
               <div>
                  <h6 class="lan-1">{{ trans('lang.General') }}  </h6>
                  <p class="lan-2">{{ trans('lang.Dashboards,widgets & layout.') }}</p>
               </div>
            </li> -->



                <li class=" sidebar-list ">
                    <label class="badge badge-success"></label>

                    <a class="sidebar-link sidebar-title custom_hover" href="{{url('/')}}/user/home"  onmouseleave="unHoverAble(1)"  onmouseover="hoverAble(1)"><span id="custom_hover-1"  class="lan-3 "><i class="fa fa-home"></i> &nbsp   Dashboard </span>
                        <div class="according-menu "><i class="fa fa-angle-{{request()->route()->getPrefix() == '/dashboard' ? 'down' : 'right' }}"></i></div>
                    </a>
                    <ul class="sidebar-submenu" style="display: {{ request()->route()->getPrefix() == '/dashboard' ? 'block;' : 'none;' }}">
                    <!--<li><a class="lan-4 {{ Route::currentRouteName()=='index' ? 'active' : '' }}" href="{{route('index')}}">{{ trans('lang.Default') }}</a></li>-->
                    <!--   <li><a class="lan-5 {{ Route::currentRouteName()=='dashboard-02' ? 'active' : '' }}" href="{{route('dashboard-02')}}">{{ trans('lang.Ecommerce') }}</a></li> -->
                        {{-- <li><a class="lan-4 " href="{{url('/user/home')}}">Facebook</a></li>
                        <li><a class="lan-4 " href="{{url('/user/home')}}">Instagram</a></li>
                        <li><a class="lan-4 " href="{{url('/user/home')}}">Twitter</a></li>
                        <li><a class="lan-4 " href="{{url('/user/home')}}">LinkedIn</a></li>
                        <li><a class="lan-4 " href="{{url('/user/home')}}">Tiktok</a></li>
                        <li><a class="lan-4 " href="{{url('/user/home')}}">Youtube</a></li> --}}


                    </ul>
                </li>
                <li class="sidebar-list">
                    <a class="sidebar-link sidebar-title custom_hover" href="{{route('socialMedia')}}" onmouseleave="unHoverAble(2)"  onmouseover="hoverAble(2)"><span id="custom_hover-2" class="lan-6 "> <i class="fa fa-hashtag"></i>&nbsp Social Sets</span>

                    </a>

                </li>






                <!--      <li class="sidebar-list">-->
                <!--   <a class="sidebar-link sidebar-title" href="#"><i data-feather="users"></i><span class="lan-6">Accounts</span>-->
                <!--<div class="according-menu"></div>-->
                <!--   </a>-->

                <!--</li>-->

                {{-- <li class="sidebar-list ">
                    <a class=" sidebar-link sidebar-title  custom_hover " href="#" onmouseleave="unHoverAble(3)"  onmouseover="hoverAble(3)"><span id="custom_hover-3" class="lan-6 "><i class="fa fa-calendar"></i> &nbsp   Posts</span>
                        <div class="according-menu "><i class="fa fa-angle-{{request()->route()->getPrefix() == '/users' ? 'down' : 'right' }}"></i></div>
                    </a>
                    <ul class="sidebar-submenu " style="display: {{ request()->route()->getPrefix() == '/widgets' ? 'block;' : 'none;' }}">
                        <li><a href="{{route('postToSocialMedia')}}" class="{{ Route::currentRouteName()=='general-widget' ? 'active' : '' }}">Instagram Post</a></li>
                        <li><a href="{{route('postToSocialMedia')}}" class="{{ Route::currentRouteName()=='users' ? 'active' : '' }}">Youtube Post</a></li>
                        <li><a href="{{route('postToSocialMedia')}}" class="{{ Route::currentRouteName()=='users' ? 'active' : '' }}">Linkdin Post</a></li>
                        <li><a href="{{route('postToSocialMedia')}}" class="{{ Route::currentRouteName()=='users' ? 'active' : '' }}">Twitter Post</a></li>
                    </ul>
                </li> --}}
                <li class="sidebar-list">
                    <a class=" sidebar-link sidebar-title  custom_hover " href="#" onmouseleave="unHoverAble(3)"  onmouseover="hoverAble(3)"><span id="custom_hover-3" class="lan-6 "><i class="fa fa-calendar"></i> &nbsp   Scheduling</span>
                        <div class="according-menu "><i class="fa fa-angle-{{request()->route()->getPrefix() == '/users' ? 'down' : 'right' }}"></i></div>
                    </a>
                    <ul class="sidebar-submenu " style="display: {{ request()->route()->getPrefix() == '/widgets' ? 'block;' : 'none;' }}">
                        <li><a href="{{route('postToSocialMedia')}}" class="{{ Route::currentRouteName()=='general-widget' ? 'active' : '' }}">New Post</a></li>
                        <li><a href="{{url('/')}}/user/scheduling/calender" class="{{ Route::currentRouteName()=='general-widget' ? 'active' : '' }}">Calender</a></li>
                        <li><a href="{{url('/')}}/user/scheduling/preview" class="{{ Route::currentRouteName()=='general-widget' ? 'active' : '' }}">Preview</a></li>
                        <li><a href="" class="{{ Route::currentRouteName()=='general-widget' ? 'active' : '' }}">Caption</a></li>

                    </ul>

                </li>
                <li class="sidebar-list">
                    <a class="sidebar-link sidebar-title custom_hover" href="#" onmouseleave="unHoverAble(7)"  onmouseover="hoverAble(7)"> <span id="custom_hover-7" class="lan-6 ">
                       <i class="fa fa-link" style="font-weight:bold;" aria-hidden="true"></i>
                            &nbsp  Engagement </span>
                        <div class="according-menu"><i class="fa fa-angle-{{request()->route()->getPrefix() == '/users' ? 'down' : 'right' }}"></i></div>
                    </a>
                    <ul class="sidebar-submenu" style="display: {{ request()->route()->getPrefix() == '/widgets' ? 'block;' : 'none;' }}">
                        <li><a href="{{url('/')}}/user/engagement/feeds" class="{{ Route::currentRouteName()=='general-widget' ? 'active' : '' }}">User feeds</a></li>


                    </ul>
                </li>

                <li class="sidebar-list">
                    <a class="sidebar-link sidebar-title custom_hover" href="#" onmouseleave="unHoverAble(7)"  onmouseover="hoverAble(7)"> <span id="custom_hover-7" class="lan-6 ">
                       <i class="fa fa-retweet" style="font-weight:bold;" aria-hidden="true"></i>
                            &nbsp  Automation </span>
                        <div class="according-menu"><i class="fa fa-angle-{{request()->route()->getPrefix() == '/users' ? 'down' : 'right' }}"></i></div>
                    </a>
                    <ul class="sidebar-submenu" style="display: {{ request()->route()->getPrefix() == '/widgets' ? 'block;' : 'none;' }}">
                        <li><a href="{{url('/')}}/user/automation/likes" class="{{ Route::currentRouteName()=='users' ? 'active' : '' }}">Like  </a></li>
                        <li><a href="#" class="{{ Route::currentRouteName()=='users' ? 'active' : '' }}">Comments </a></li>
                        <li><a href="#" class="{{ Route::currentRouteName()=='users' ? 'active' : '' }}">Follow</a></li>
                        <li><a href="#" class="{{ Route::currentRouteName()=='users' ? 'active' : '' }}">Message </a></li>
                        <li><a href="#" class="{{ Route::currentRouteName()=='users' ? 'active' : '' }}">Views  </a></li>

                    </ul>
                </li>
                <li class="sidebar-list">
                    <a class="sidebar-link sidebar-title custom_hover" href="#" onmouseleave="unHoverAble(7)"  onmouseover="hoverAble(7)"> <span id="custom_hover-7" class="lan-6 ">
                       <i class="fa fa-list-ul" style="font-weight:bold;" aria-hidden="true"></i>
                            &nbsp  Reports </span>
                        <div class="according-menu"><i class="fa fa-angle-{{request()->route()->getPrefix() == '/users' ? 'down' : 'right' }}"></i></div>
                    </a>
                    <ul class="sidebar-submenu" style="display: {{ request()->route()->getPrefix() == '/widgets' ? 'block;' : 'none;' }}">
                        <li><a href="#" class="{{ Route::currentRouteName()=='users' ? 'active' : '' }}">New Report  </a></li>
                        <li><a href="#" class="{{ Route::currentRouteName()=='users' ? 'active' : '' }}">Current Report </a></li>


                    </ul>
                </li>
            </ul>
        </div>
        <div class="right-arrow" id="right-arrow"><i data-feather="arrow-right"></i></div>
    </nav>
</div>


<script>
    function hoverAble(id){
        $('#custom_hover-'+id).addClass('activeSpanColor');
    }
    function unHoverAble(id){
        $('#custom_hover-'+id).removeClass('activeSpanColor');
    }
</script>
