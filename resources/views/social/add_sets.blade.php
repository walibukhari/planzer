@extends('userPortal.layout.app')
@push('style')

<link rel="stylesheet" href="{{asset('emoji/css/reset.css')}}">
<link rel="stylesheet" href="{{asset('emoji/css/style.css')}}">
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
<script src="{{asset('emoji/js/jquery.emojiarea.js')}}"></script>
<script src="{{asset('emoji/js/main.js')}}"></script>
<link rel="stylesheet" href="{{asset('dropZone/dist/image-uploader.min.css')}}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css" integrity="sha512-vKMx8UnXk60zUwyUnUPM3HbQo8QfmNx7+ltw8Pm5zLusl1XIfwcxo8DbWCqMGKaWeNxWA8yrx5v3SaVpMvR3CA==" crossorigin="anonymous" />
<link rel="stylesheet" href="{{url('/')}}/assets/css/override-addset.css">
@endpush

@section('content')
    <br>
    <br>
    <div class="container" id="customContainerClass">
        @if(session()->has('success_message'))
            <div class="row">
                <div class="col-md-12">
                    <div class="alert alert-success">
                        {{session()->get('success_message')}}
                    </div>
                </div>
            </div>
        @endif
        <h3>Add Social Sets</h3>
        <br>
        <br>
            @if(isset($socialSets))
                <form action="{{route('updatePostSets')}}" method="POST">
                    <input type="hidden" name="id" value="{{$socialSets->id}}" />
            @else
                <form action="{{route('addPostSets')}}" method="POST">
            @endif
            @csrf
            <div class="form-group">
                <div class="row">
                    <div class="col-md-12">
                        <label>Set Name</label>
                        <input type="text" value="{{@$socialSets->name}}" name="name" class="form-control" placeholder="Enter Set Name" />
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <div class="col-md-12">
                        <label>Social Account</label>
                        <select name="social_accounts[]" multiple class="form-control">
                            @foreach(\App\Models\SocialSets::accounts() as $k => $d)
                                <option value="{{$d}}" @if(isset($accounts[$k]) && $accounts[$k] == $d) selected @endif>{{str_replace('_','',$d)}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-success">Submit</button>
            </div>
        </form>
    </div>
    <br>
    <br>
    <div class="container" id="customContainerClass">
        <h2>All Sets</h2>
        <br>
        <br>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Accounts</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach($allSocialSets as $sets)
                    <tr>
                        <td>{{$sets->name}}</td>
                        <td>
                            @foreach(json_decode($sets->social_account) as $account)
                                {{str_replace('_','',$account)}},
                            @endforeach
                        </td>
                        <td>
                            <a href="{{route('socialMedia',[$sets->id])}}" class="btn btn-success">Connect</a> /
                            <a href="{{route('socialMediaDelete',[$sets->id])}}" class="btn btn-danger">Delete</a>
                            <a href="{{route('editSocialMedia',[$sets->id])}}" class="btn btn-info">Edit</a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <br>
    <br>
    <br>
@endsection
@push('js')

@endpush
