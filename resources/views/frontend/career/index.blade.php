@extends('layouts.default')
@section('title')
    Career
@endsection
@push('css')

@endpush

@section('content')

{{--    <div class="pt-23 pt-md-25 pt-lg-30">--}}
{{--        <div class="container">--}}
{{--            <!-- Section Padding -->--}}
{{--            <div class="row justify-content-center">--}}
{{--                <div class="col-xl-7 col-lg-8 col-md-11">--}}
{{--                    <div class="text-center mb-11 mb-lg-24" data-aos="fade-up" data-aos-duration="500" data-aos-once="true">--}}
{{--                        <h2 class="font-size-11 mb-5">Career</h2>--}}
{{--                        <p class="font-size-7 mb-0">Our focus is always on finding the--}}
{{--                            best people to work with. Our bar is high, but you look ready--}}
{{--                            to take on the challenge.</p>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--            <!-- End Section Padding -->--}}
{{--        </div>--}}
{{--        <div class="container-fluid">--}}
{{--            <div class="row">--}}
{{--                <div class="col-12">--}}
{{--                    <div class="rounded-8" data-aos="fade-up" data-aos-duration="800" data-aos-once="true">--}}
{{--                        <img class="w-100" src="https://uxtheme.net/demos/finity/image/inner-page/jpg/career-banner-img.jpg" alt="">--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
    <!-- End breadCrumbs Area -->
    <!-- videoOne Area -->
    <div class="pt-14 pt-md-19 pt-lg-27 pb-15 pb-md-18 pb-lg-25 mb-lg-1">
        <div class="container">
            <div class="row align-items-center justify-content-center">
                <div class="col-xl-6 col-lg-6 col-md-10 col-sm-11" data-aos="fade-right" data-aos-duration="500" data-aos-once="true">
                    <div class="bg-images d-flex align-items-center justify-content-center max-w-540 py-18 py-sm-28 rounded-10 mx-auto" style="background-image: url(https://uxtheme.net/demos/finity/image/inner-page/png/video-bg.png);">
                        <a class="video-btn sonar bg-blue-3-op5 text-white circle-98 font-size-8"
                           href="https://www.youtube.com/watch?v=QZazlSLaFww&t=9s"><i class="fa fa-play"></i></a>
                    </div>
                </div>
                <div class="col-lg-6 col-md-10 col-sm-11">
                    <div class="pt-9 pt-lg-0 pr-xs-10 pr-sm-5 pr-md-25 pr-lg-0 pl-lg-10 pl-xl-16">
                        <h2 class="font-size-10 mb-8 letter-spacing-n83" data-aos="fade-up" data-aos-duration="600" data-aos-once="true">Working with us</h2>
                        <p class="font-size-6 mb-0 pr-xs-15 pr-sm-10 pr-xl-15" data-aos="fade-up" data-aos-duration="900" data-aos-once="true">These companies release their own versions of the operating systems with minor changes, and yet always with the same bottom line. </p>
                        <div class="mt-11" data-aos="fade-up" data-aos-duration="1200" data-aos-once="true">
                            <a class="btn btn-blue-3 btn-2 font-weight-medium rounded-5" href="https://www.linkedin.com/company/42782060/">See Job Openings</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End videoOne Area -->
    <!-- featureOne Area -->
    <!-- Section Title -->
    <div class="pb-9 pb-md-13 pb-lg-15">
        <div class="container">
            <!-- section title -->
            <div class="row justify-content-center">
                <div class="col-xl-5 col-lg-6 col-md-7 col-xs-8">
                    <div class="text-center mb-12 mb-lg-17" data-aos="fade-up" data-aos-duration="600" data-aos-once="true">
                        <h2 class="font-size-10 letter-spacing-n83">Why you’d love to work with us</h2>
                    </div>
                </div>
            </div>
            <!-- End Section title -->
            <div class="row justify-content-center">
                <div class="col-xl-4 col-lg-5 col-md-6 col-sm-9 col-xs-11 mb-7 mb-lg-15" data-aos="fade-up" data-aos-duration="500" data-aos-once="true">
                    <!-- Career Service Widget Starts-->
                    <div class="d-xs-flex">
                        <div class="square-83 d-flex rounded-10 border mt-2">
                            <img src="https://uxtheme.net/demos/finity/image/inner-page/png/explore.png" alt="">
                        </div>
                        <div class="mt-5 mt-xs-0 pl-xs-6">
                            <h3 class="font-size-7 mb-1">Work from anywhere</h3>
                            <p class="font-size-5 line-height-28 mb-0 pr-sm-10 pr-md-0 pr-xs-17 pr-lg-8 pr-xl-5">Many of us are able to work from home, a café, or a park whenever we want. Yay freedom!</p>
                        </div>
                    </div>
                    <!-- Career Service Widget Ends-->
                </div>
                <div class="col-xl-4 col-lg-5 col-md-6 col-sm-9 col-xs-11 mb-7 mb-lg-15" data-aos="fade-up" data-aos-duration="500" data-aos-once="true">
                    <!-- Career Service Widget Starts-->
                    <div class="d-xs-flex">
                        <div class="square-83 d-flex rounded-10 border mt-2">
                            <img src="https://uxtheme.net/demos/finity/image/inner-page/png/explore.png" alt="">
                        </div>
                        <div class="mt-5 mt-xs-0 pl-xs-6">
                            <h3 class="font-size-7 mb-1">Flexible hours</h3>
                            <p class="font-size-5 line-height-28 mb-0 pr-sm-10 pr-md-0 pr-xs-17 pr-lg-8 pr-xl-5">Need to work around school drop-offs and pick-ups? Just not a morning person? No worries.</p>
                        </div>
                    </div>
                    <!-- Career Service Widget Ends-->
                </div>
                <div class="col-xl-4 col-lg-5 col-md-6 col-sm-9 col-xs-11 mb-7 mb-lg-15" data-aos="fade-up" data-aos-duration="500" data-aos-once="true">
                    <!-- Career Service Widget Starts-->
                    <div class="d-xs-flex">
                        <div class="square-83 d-flex rounded-10 border mt-2">
                            <img src="https://uxtheme.net/demos/finity/image/inner-page/png/explore.png" alt="">
                        </div>
                        <div class="mt-5 mt-xs-0 pl-xs-6">
                            <h3 class="font-size-7 mb-1">Work and travel</h3>
                            <p class="font-size-5 line-height-28 mb-0 pr-sm-10 pr-md-0 pr-xs-17 pr-lg-8 pr-xl-5">You can work and travel around the world for up to three months every year!</p>
                        </div>
                    </div>
                    <!-- Career Service Widget Ends-->
                </div>
                <div class="col-xl-4 col-lg-5 col-md-6 col-sm-9 col-xs-11 mb-7 mb-lg-15" data-aos="fade-up" data-aos-duration="500" data-aos-once="true">
                    <!-- Career Service Widget Starts-->
                    <div class="d-xs-flex">
                        <div class="square-83 d-flex rounded-10 border mt-2">
                            <img src="https://uxtheme.net/demos/finity/image/inner-page/png/explore.png" alt="">
                        </div>
                        <div class="mt-5 mt-xs-0 pl-xs-6">
                            <h3 class="font-size-7 mb-1">Paid parental leave</h3>
                            <p class="font-size-5 line-height-28 mb-0 pr-sm-10 pr-md-0 pr-xs-17 pr-lg-8 pr-xl-5">Many of us are able to work from home, a café, or a park whenever we want. Yay freedom!</p>
                        </div>
                    </div>
                    <!-- Career Service Widget Ends-->
                </div>
                <div class="col-xl-4 col-lg-5 col-md-6 col-sm-9 col-xs-11 mb-7 mb-lg-15" data-aos="fade-up" data-aos-duration="500" data-aos-once="true">
                    <!-- Career Service Widget Starts-->
                    <div class="d-xs-flex">
                        <div class="square-83 d-flex rounded-10 border mt-2">
                            <img src="https://uxtheme.net/demos/finity/image/inner-page/png/explore.png" alt="">
                        </div>
                        <div class="mt-5 mt-xs-0 pl-xs-6">
                            <h3 class="font-size-7 mb-1">Yearly bonuses</h3>
                            <p class="font-size-5 line-height-28 mb-0 pr-sm-10 pr-md-0 pr-xs-17 pr-lg-8 pr-xl-5">Need to work around school drop-offs and pick-ups? Just not a morning person? No worries.</p>
                        </div>
                    </div>
                    <!-- Career Service Widget Ends-->
                </div>
                <div class="col-xl-4 col-lg-5 col-md-6 col-sm-9 col-xs-11 mb-7 mb-lg-15" data-aos="fade-up" data-aos-duration="500" data-aos-once="true">
                    <!-- Career Service Widget Starts-->
                    <div class="d-xs-flex">
                        <div class="square-83 d-flex rounded-10 border mt-2">
                            <img src="https://uxtheme.net/demos/finity/image/inner-page/png/explore.png" alt="">
                        </div>
                        <div class="mt-5 mt-xs-0 pl-xs-6">
                            <h3 class="font-size-7 mb-1">Smart salary</h3>
                            <p class="font-size-5 line-height-28 mb-0 pr-sm-10 pr-md-0 pr-xs-17 pr-lg-8 pr-xl-5">You can work and travel around the world for up to three months every year!</p>
                        </div>
                    </div>
                    <!-- Career Service Widget Ends-->
                </div>
            </div>
        </div>
    </div>
    <!-- End featureOne Area -->
    <!-- jobCardtwoSection Area -->
    <!-- Job section -->
    <div class="bg-default-3 pt-12 pt-md-18 pb-md-19 pb-15 pt-lg-22 pb-lg-26">
        <div class="container">
            <!-- section title -->
            <div class="row justify-content-center">
                <div class="col-xl-6 col-lg-7 col-md-10">
                    <div class="text-center mb-7" data-aos="fade-up" data-aos-duration="600" data-aos-once="true">
                        <h2 class="font-size-10 mb-7 letter-spacing-n83">Open roles</h2>
                        <p class="font-size-6 font-weight-light mb-0 px-md-5 px-lg-0">These companies release their own versions of the operating
                            systems
                            with minor changes, and yet always.</p>
                    </div>
                </div>
            </div>
            <!-- career roles -->
            <div class="row mt-lg-9 justify-content-center">
                <div class="col-lg-4 col-md-6 col-xs-8 mt-9" data-aos="fade-up" data-aos-duration="900" data-aos-once="true">
                    <a class="pt-9 pb-9 pl-11 pr-10 bg-white d-block rounded-0 shadow-2 gr-hover-1" href="https://www.linkedin.com/company/42782060/" target="__blank">
                        <div class="d-flex justify-content-between align-items-center">
                            <div class="d-flex align-items-center mb-5 mr-5 w-70">
                                <i class="icon icon-pin-3 mr-2 text-bali-gray position-relative"></i>
                                <span class="text-bali-gray font-size-5 line-height-normal">Marbella</span>
                            </div>
                            <span
                                class="badge bg-dark-green-op1 d-inline-block text-dark-green py-4 px-2 min-w-76 d-block line-height-reset mb-5 rounded-5 font-size-3 font-weight-normal w-30">
        Full-time
    </span>
                        </div>
                        <h5 class="pt-6 text-dark-cloud font-size-7 font-weight-medium line-height-reset mb-5">Client Success Manager</h5>
                    </a>
                </div>
                <div class="col-lg-4 col-md-6 col-xs-8 mt-9" data-aos="fade-up" data-aos-duration="900" data-aos-once="true">
                    <a class="pt-9 pb-9 pl-11 pr-10 bg-white d-block rounded-0 shadow-2 gr-hover-1" href="https://www.linkedin.com/company/42782060/" target="__blank">
                        <div class="d-flex justify-content-between align-items-center">
                            <div class="d-flex align-items-center mb-5 mr-5 w-70">
                                <i class="icon icon-pin-3 mr-2 text-bali-gray position-relative"></i>
                                <span class="text-bali-gray font-size-5 line-height-normal">Marbella</span>
                            </div>
                            <span
                                class="badge bg-dark-green-op1 d-inline-block text-dark-green py-4 px-2 min-w-76 d-block line-height-reset mb-5 rounded-5 font-size-3 font-weight-normal w-30">
        Full-time
    </span>
                        </div>
                        <h5 class="pt-6 text-dark-cloud font-size-7 font-weight-medium line-height-reset mb-5">Social Specialist</h5>
                    </a>
                </div>
                <div class="col-lg-4 col-md-6 col-xs-8 mt-9" data-aos="fade-up" data-aos-duration="900" data-aos-once="true">
                    <a class="pt-9 pb-9 pl-11 pr-10 bg-white d-block rounded-0 shadow-2 gr-hover-1" href="https://www.linkedin.com/company/42782060/" target="__blank">
                        <div class="d-flex justify-content-between align-items-center">
                            <div class="d-flex align-items-center mb-5 mr-5 w-70">
                                <i class="icon icon-pin-3 mr-2 text-bali-gray position-relative"></i>
                                <span class="text-bali-gray font-size-5 line-height-normal">Marbella</span>
                            </div>
                            <span
                                class="badge bg-dark-green-op1 d-inline-block text-dark-green py-4 px-2 min-w-76 d-block line-height-reset mb-5 rounded-5 font-size-3 font-weight-normal w-30">
        Full-time
    </span>
                        </div>
                        <h5 class="pt-6 text-dark-cloud font-size-7 font-weight-medium line-height-reset mb-5">
                            Marketing Manager</h5>
                    </a>
                </div>
{{--                <div class="col-lg-4 col-md-6 col-xs-8 mt-9" data-aos="fade-up" data-aos-duration="900" data-aos-once="true">--}}
{{--                    <a class="pt-9 pb-9 pl-11 pr-10 bg-white d-block rounded-0 shadow-2 gr-hover-1" href="#">--}}
{{--                        <div class="d-flex justify-content-between align-items-center">--}}
{{--                            <div class="d-flex align-items-center mb-5 mr-5 w-70">--}}
{{--                                <i class="icon icon-pin-3 mr-2 text-bali-gray position-relative"></i>--}}
{{--                                <span class="text-bali-gray font-size-5 line-height-normal">Mooreville, Canada</span>--}}
{{--                            </div>--}}
{{--                            <span--}}
{{--                                class="badge bg-buttercup-op1 d-inline-block text-buttercup py-4 px-2 min-w-76 d-block line-height-reset mb-5 rounded-5 font-size-3 font-weight-normal w-30">--}}
{{--        Part-time--}}
{{--    </span>--}}
{{--                        </div>--}}
{{--                        <h5 class="pt-6 text-dark-cloud font-size-7 font-weight-medium line-height-reset mb-5">Project Manager</h5>--}}
{{--                    </a>--}}
{{--                </div>--}}
{{--                <div class="col-lg-4 col-md-6 col-xs-8 mt-9" data-aos="fade-up" data-aos-duration="900" data-aos-once="true">--}}
{{--                    <a class="pt-9 pb-9 pl-11 pr-10 bg-white d-block rounded-0 shadow-2 gr-hover-1" href="#">--}}
{{--                        <div class="d-flex justify-content-between align-items-center">--}}
{{--                            <div class="d-flex align-items-center mb-5 mr-5 w-70">--}}
{{--                                <i class="icon icon-pin-3 mr-2 text-bali-gray position-relative"></i>--}}
{{--                                <span class="text-bali-gray font-size-5 line-height-normal">Anywhere</span>--}}
{{--                            </div>--}}
{{--                            <span--}}
{{--                                class="badge bg-blue-2-op1 d-inline-block text-blue-2 py-4 px-2 min-w-76 d-block line-height-reset mb-5 rounded-5 font-size-3 font-weight-normal w-30">--}}
{{--        Remote--}}
{{--    </span>--}}
{{--                        </div>--}}
{{--                        <h5 class="pt-6 text-dark-cloud font-size-7 font-weight-medium line-height-reset mb-5">Senior Software Engineer</h5>--}}
{{--                    </a>--}}
{{--                </div>--}}
{{--                <div class="col-lg-4 col-md-6 col-xs-8 mt-9" data-aos="fade-up" data-aos-duration="900" data-aos-once="true">--}}
{{--                    <a class="pt-9 pb-9 pl-11 pr-10 bg-white d-block rounded-0 shadow-2 gr-hover-1" href="#">--}}
{{--                        <div class="d-flex justify-content-between align-items-center">--}}
{{--                            <div class="d-flex align-items-center mb-5 mr-5 w-70">--}}
{{--                                <i class="icon icon-pin-3 mr-2 text-bali-gray position-relative"></i>--}}
{{--                                <span class="text-bali-gray font-size-5 line-height-normal">Vonstad, Spain</span>--}}
{{--                            </div>--}}
{{--                            <span--}}
{{--                                class="badge bg-dark-green-op1 d-inline-block text-dark-green py-4 px-2 min-w-76 d-block line-height-reset mb-5 rounded-5 font-size-3 font-weight-normal w-30">--}}
{{--        Full-time--}}
{{--    </span>--}}
{{--                        </div>--}}
{{--                        <h5 class="pt-6 text-dark-cloud font-size-7 font-weight-medium line-height-reset mb-5">Marketing Director</h5>--}}
{{--                    </a>--}}
{{--                </div>--}}
            </div>
        </div>
    </div>
    <!-- End jobCardtwoSection Area -->

@endsection

@push('js')

@endpush
