@extends('layouts.simple.master')
@section('title', 'Companies List')

@section('css')
@endsection

@section('style')
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/vendors/datatables.css')}}">
@endsection

@section('breadcrumb-title')
    <h3>All Companies</h3>
@endsection

@section('breadcrumb-items')
    <li class="breadcrumb-item">Companies</li>
    <li class="breadcrumb-item active">All Companies</li>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <!-- Zero Configuration  Starts-->
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        @if (session('success_message'))
                            <div class="alert alert-success" role="alert">
                                {{ session('success_message') }}
                            </div>
                        @endif
                        <div class="row" style="display:flex; align-items:center; justify-content: space-between;">
                            <h3>Companies List</h3>
                            <a href="{{route('create.company')}}" class="btn btn-primary">Add New Company</a>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="display" id="basic-1">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>City</th>
                                    <th>State</th>
                                    <th>Country</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php
                                    $counter = 1;
                                @endphp
                                @foreach($companies as $company)
                                    <tr>
                                        <td>{{$counter++}}</td>
                                        <td>{{$company->company_name}}</td>
                                        <td>{{$company->company_email}}</td>
                                        <td>{{$company->city}}</td>
                                        <td>{{$company->state}}</td>
                                        <td>{{$company->country}}</td>
                                        <td>
                                            <a href="{{route('edit.company',[$company->id])}}" class="btn btn-primary">Edit</a>
                                            <a href="{{route('delete.company',[$company->id])}}" class="btn btn-danger" onclick="return confirm('Are you sure you want to delete this user?');">Delete</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>City</th>
                                    <th>State</th>
                                    <th>Country</th>
                                    <th>Actions</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="{{asset('assets/js/datatable/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('assets/js/datatable/datatables/datatable.custom.js')}}"></script>
@endsection
