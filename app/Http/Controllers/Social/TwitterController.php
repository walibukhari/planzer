<?php

namespace App\Http\Controllers\Social;

use Abraham\TwitterOAuth\TwitterOAuth;
use App\Http\Controllers\Controller;
use App\Models\SocialAccount;
use App\Models\SocialAccountManager;
use App\Models\User;
use App\Traits\General;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;

class TwitterController extends Controller
{

    use General;
    public $twitter;
    public $consumer_key;
    public $consumer_secret;
    public $twitter_access_token;
    public $twitter_access_token_secret;

    public function __construct(){
        $this->consumer_key = "YGiTFOsjohZUDHixwWREztG5E";
        $this->consumer_secret = "EDvFyHY6edv5FxSFsVsMBTxCiCMTYMu5Cx5iToU97f2lsCeKAs";
        $this->twitter_access_token = "1135435572451975168-1ykSP4Z76qQP3CCQRG9jjnXcJHr6UR";
        $this->twitter_access_token_secret = "K0CcF5vG4ex41JdT3GSTg5pH7W4JfEZPPCWvAcgNsKr1f";
    }

    public function refreshAccount(){
        return back();
    }

    public function connectToTwitter(){
        $twitter = SocialAccountManager::where('social_network','=',SocialAccount::TWITTER)->first();
        return view('social.twitter.create' , [
            'twitter' => $twitter
        ]);
    }

    public function login(Request $request) {
         if(!$this->consumer_key || !$this->consumer_secret || !$this->twitter_access_token || !$this->twitter_access_token_secret) {
            return collect([
                'code' => 400,
                'status' => true,
                'message' => 'Please add your twitter credentials to the .env file.'
            ]);
        }
        $connection = new TwitterOAuth($this->consumer_key, $this->consumer_secret);
        $request_token = $connection->oauth('oauth/request_token', array('oauth_callback' => route('twitter.callback')));
        if (isset($request_token['oauth_token_secret'])) {
            $url = $connection->url('oauth/authorize', array('oauth_token' => $request_token['oauth_token']));
            Session::put('oauth_state', 'start');
            Session::put('set_id',$request->set_id);
            Session::put('twitter_oauth_token', $request_token['oauth_token']);
            Session::put('twitter_oauth_token_secret', $request_token['oauth_token_secret']);
            return collect([
                'status' => true,
                'redirect_url' => $url,
                'set_id' => $request->set_id
            ]);
        }
        return collect([
            'code' => 400,
            'status' => false,
            'message' => 'some thing went wrong please check your env and twitter developer account app settings and redirect url'
        ]);
    }

    public function twitterCallback(Request $request) {
        $oauth_token = Session::get("twitter_oauth_token");
        $set_id = Session::get('set_id');
        $oauth_token_secret = Session::get("twitter_oauth_token_secret");
        $connection = new TwitterOAuth($this->consumer_key, $this->consumer_secret, $oauth_token, $oauth_token_secret);
        $access_token = $connection->oauth("oauth/access_token", ["oauth_verifier" => $request->get('oauth_verifier')]);
        $access_token = (object)$access_token;
        $connection->setOauthToken($access_token->oauth_token, $access_token->oauth_token_secret);
        $profile = $connection->get("account/verify_credentials");
        if (!isset($access_token->oauth_token_secret)) {
            return redirect()->route('connectToTwitter')->with('error_message','Oops! We could not log you in on Twitter.');
        }
        if (Session::has('twitter_oauth_token')) {
            if (is_object($profile) && !isset($profile->error))
            {
                $user_id = Auth::user();
                $request->request->add([
                   'set_id' => $set_id,
                   'ids' => $profile->id_str,
                   'social_network' => 'twitter',
                   'category' => 'profile',
                   'team_id' => NULL,
                   'login_type' => NULL,
                   'can_post' => 1,
                   'pid' => $profile->id,
                   'name' => $profile->name,
                   'username' => $profile->screen_name,
                   'token' => $access_token->oauth_token,
                   'avatar' => $profile->profile_image_url,
                   'url' => NULL,
                   'data' => NULL,
                   'proxy' => NULL,
                   'status' => 1,
                   'access_token' => json_encode($access_token),
                   'user_id' => $user_id->id
                ]);
                $check = SocialAccountManager::where('social_network','=',SocialAccount::TWITTER)->where('set_id','=',$set_id)->first();
                if(is_null($check)) {
                    SocialAccountManager::createAccount($request->all());
                } else {
                    SocialAccountManager::updateAccount($request->all());
                }
                #$this->syncAccountToClientGroup($social->id, auth()->user()->id);
                Session::put('access_token', $access_token);
                return redirect()->route("socialMedia")->with('success_message','Congrats! Your Twitter account was added!');
            }
            return redirect()->route("socialMedia")->with('error_message','Oops! Something went wrong while adding your twitter account!');
        }
    }

    public function syncAccountToClientGroup($social_account_id, $user_id) {
            dd('sync account from client group');
    }

    public function postToTwitterViw() {
        $account = SocialAccountManager::where('social_network','=',SocialAccount::TWITTER)->where('user_id','=',Auth::user()->id)->first();
        return view('social.twitter.twitter',[
            'account' => $account
        ]);
    }

    public function postToTwitter($request) {
        $post_type = 'photo';
        $counter = $request['counter'];
        $platform = SocialAccountManager::where('set_id','=',$request['account_set_id'])->where('social_network','=',SocialAccount::TWITTER)->first();
        $platform = json_decode($platform->access_token);
        $twitter = new TwitterOAuth($this->consumer_key, $this->consumer_secret);
        $caption = $this->cut_text($this->spintax($request['description']));
        $twitter->setOauthToken($platform->oauth_token, $platform->oauth_token_secret);
        $endpoint = "statuses/update";
        $params = [];
        switch ($post_type)
        {
            case 'photo':
                $twitter->setTimeouts(120,60);
                $medias = array();
                for($i = 0; $i < $counter; $i++) {
                    if(isset($request['schedule_type']) && $request['schedule_type'] == 1) {
                        $media = $request["files"];
                    } else {
                        $media = $request["files-".$i];
                    }
                    $media = $this->uploadFile('uploads/twitter',$media);
                    $upload = $twitter->upload( 'media/upload', array('media' => public_path($media)));
                    $medias[] = isset($upload->media_id_string) ? $upload->media_id_string: '';
                }
                $params = [
                    'status' => $caption,
                    'media_ids' => implode(',', $medias)
                ];
                break;

            case 'text':
                $params = ['status' => $caption];
                break;

        }
        if($post_type == 'photo') {
            $response = $twitter->post('statuses/update', $params);
        } else {
            $response = $twitter->post($endpoint, $params);
        }
        return collect([
            'status' => true,
            'data' => $response,
            'message' => 'Post Published To Twitter Successfully ..... !'
        ]);
    }

    public function cut_text($text, $n = 280){
        if(strlen($text) <= $n){
            return $text;
        }

        $text= substr($text, 0, $n);
        if($text[$n-1] == ' '){
            return trim($text)."...";
        }

        $x  = explode(" ", $text);
        $sz = sizeof($x);

        if($sz <= 1){
            return $text."...";
        }

        $x[$sz-1] = '';

        return trim(implode(" ", $x))."...";
    }

    public function process($text)
    {
        return preg_replace_callback(
            '/\{(((?>[^\{\}]+)|(?R))*?)\}/x',
            array($this, 'replace'),
            $text
        );
    }
    public function spintax($caption){
        return $this->process($caption);
    }
}
