<?php

namespace App\Console\Commands;

use App\Jobs\SocialMediaPostScheduler;
use App\Models\SchedulePost;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Auth;

class PostSchedule extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'run:post_schedule';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        \Log::info('post scheduler task running .... !');
        dispatch(new SocialMediaPostScheduler(SchedulePost::where('status','=',SchedulePost::POST_SCHEDULE)->get()));
    }
}
