@extends('layouts.simple.master')
@section('title', 'Company Users')

@section('css')
@endsection

@section('style')
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/vendors/datatables.css')}}">
    <style>
        .customColor{
            color:#1092af;
            font-weight: bold;
        }
    </style>
@endsection

@section('breadcrumb-title')
    <h3>Company Users</h3>
@endsection

@section('breadcrumb-items')
    <li class="breadcrumb-item">Company</li>
    <li class="breadcrumb-item active">Company Users</li>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <!-- Zero Configuration  Starts-->
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        @if (session('success_message'))
                            <div class="alert alert-success" role="alert">
                                {{ session('success_message') }}
                            </div>
                        @endif
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="display" id="basic-1">
                                <thead>
                                <tr>
                                    <th><input type="checkbox" /></th>
                                    <th>Name</th>
                                    <th>Team</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td><input type="checkbox" /></td>
                                    <td>
                                        <div class="row" style="display: flex;align-items: center;margin-left:3px;">
                                            <img src="{{asset('assets/images/user/1.jpg')}}" style="border-radius:30px;width:60px;height:60px;" />
                                            <div class="row" style="margin-left: 10px;display: flex;flex-direction: column;">
                                                <span class="customColor">John Deo</span>
                                                <span>johndeo@gmail.com</span>
                                            </div>
                                        </div>
                                    </td>
                                    <td></td>
                                    <td>Super Admin</td>
                                </tr>
                                <tr>
                                    <td><input type="checkbox" /></td>
                                    <td>
                                        <div class="row" style="display: flex;align-items: center;margin-left:3px;">
                                            <img src="{{asset('assets/images/user/1.jpg')}}" style="border-radius:30px;width:60px;height:60px;" />
                                            <div class="row" style="margin-left: 10px;display: flex;flex-direction: column;">
                                                <span class="customColor">John Deo</span>
                                                <span>johndeo@gmail.com</span>
                                            </div>
                                        </div>
                                    </td>
                                    <td></td>
                                    <td>Super Admin</td>
                                </tr>
                                <tr>
                                    <td><input type="checkbox" /></td>
                                    <td>
                                        <div class="row" style="display: flex;align-items: center;margin-left:3px;">
                                            <img src="{{asset('assets/images/user/1.jpg')}}" style="border-radius:30px;width:60px;height:60px;" />
                                            <div class="row" style="margin-left: 10px;display: flex;flex-direction: column;">
                                                <span class="customColor">John Deo</span>
                                                <span>johndeo@gmail.com</span>
                                            </div>
                                        </div>
                                    </td>
                                    <td></td>
                                    <td>Super Admin</td>
                                </tr>
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th><input type="checkbox" /></th>
                                    <th>Name</th>
                                    <th>Team</th>
                                    <th>Actions</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="{{asset('assets/js/datatable/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('assets/js/datatable/datatables/datatable.custom.js')}}"></script>
    <script>
        $('<a href="{{route('add.new_user')}}" class="btn btn-orange" style="background-color:#ff7959;color:#fff">Create User</a>').appendTo('div.dataTables_filter');
    </script>
@endsection
