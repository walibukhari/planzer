@extends('layouts.simple.master')
@section('title', 'Default')

@section('style')
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/vendors/animate.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/vendors/chartist.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/vendors/date-picker.css')}}">
    <style>
        .earning-card.card .card-body .border-top {
            border-top: 1px solid #ecf3fa !important;
            padding: 20px;
        }
        .page-wrapper.compact-wrapper .page-body-wrapper.sidebar-icon .page-body{
            padding: 30px;
        }
        .dashboard-sec{
            padding-top: 20px;
        }
        .earning-card.card .card-body .media .media-left {
            margin-left: 3px;
        }
        .custombox{
            padding: 10px;
            background: #1d1e26;
            display: flex;
            border-radius: 8px;

        }
        .custombox2{
            padding: 15px;
            background: #F7F8F8;
            width:100%;
            display: flex;
            border-radius: 8px;
        }
        .earning-card.card .card-body .media .media-body h6 {
            margin-bottom: 2px;
            font-size: 20px;
        }
        .earning-card.card .card-body .media .media-left i {
            font-size: 25px;
            -webkit-transition: 0.3s all linear;
            transition: 0.3s all linear;
        }
        .earning-card.card .card-body .media .media-left {
            width: 47px;
            height: 47px;
        }
        .earning-card.card .card-body .media .media-left {
            width: 47px;
            height: 47px;
            border-radius: 4px;
        }
        .select-box{
            position: absolute;
            right: 20px;
            width: 200px;
            display: flex;
            align-items: center;
        }
        .faFacalender{
            font-size: 21px;
            margin-right: 10px;
            position: relative;
            top: 1px;
        }
        .three-dot > i {
            margin-right: 2px;
            position: relative;
            top:2px;
        }
        .three-dotDark > i {
            margin-right: 2px;
            position: relative;
            top: 10px;
        }
        .three-dotDark{
            position: relative;
            border: 1px solid #1d1e26;
            height: 40px;
            border-radius: 3px;
            display: flex;
            align-items: center;
            margin-left: 3px;
        }
        .three-dot{
            position: relative;
            border: 1px solid #aaa;
            height: 40px;
            border-radius: 3px;
            display: flex;
            align-items: center;
            margin-left: 3px;
        }


        @media only screen and (max-width: 1291px) {
      .earning-card.card .card-body .media .media-body h6{

         font-size: 17px;
        }
    }
    </style>
@endsection

@section('breadcrumb-title')
    <!--<h3>Default</h3>-->
@endsection

@section('breadcrumb-items')
    <!--<li class="breadcrumb-item">Dashboard</li>-->
    <!--<li class="breadcrumb-item active">Default</li>-->
@endsection

@section('content')
    <div class="">
        <div class="row second-chart-list third-news-update">

            <div class="col-xl-12 xl-100 dashboard-sec box-col-12">
                <div class="card earning-card m-padding" >
                    <div class="card-body p-0">
                        <div class="row m-0">

                            <div class="col-xl-12 p-0">
                                <div class="row" style="display: flex;align-items: center;">
                                    <h5 style="padding-top:25px;padding-left:25px;padding-bottom:20px;">Analytics Overview</h5>
                                    <div class="select-box">
                                        <select name="cmbIdioma" id="cmbIdioma" class="form-control" style="width: 100%">
                                            <option selected>Last six month</option>
                                            <option value="">Last three month</option>
                                            <option value="">Last two month</option>
                                            <option value="">Last one month</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row border-top m-0">
                                    <!--<h5>Analytics Overview</h5>-->
                                    <div class="col-xl-3 pl-0 col-md-6 col-sm-6">

                                        <div class="media p-0 for-light">
                                            <div class="custombox2">
                                                <div class="media-left" style="background-color:blue"><i class="icofont icofont-users"></i></div>
                                                <div class="media-body" >
                                                    <h6>Followers</h6>
                                                    <p>23.4K</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="media p-0 for-dark">
                                            <div class="custombox">
                                                <div class="media-left" style="background-color:blue;"><i class="icofont icofont-users"></i></div>
                                                <div class="media-body">
                                                    <h6>Followers</h6>
                                                    <p>23.4K</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xl-3 col-md-6 col-sm-6">
                                        <div class="media p-0 for-light">
                                            <div class="custombox2">
                                                <div class="media-left bg-secondary"><i class="icofont icofont-hand-drag1"></i></div>
                                                <div class="media-body">
                                                    <h6>Impressions</h6>
                                                    <p>45.8K</p>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="media p-0 for-dark">
                                            <div class="custombox" >
                                                <div class="media-left bg-secondary" style="padding: 8px;"><i style="" class="icofont icofont-hand-drag1"></i></div>
                                                <div class="media-body">
                                                    <h6>Impressions</h6>
                                                    <p>45.8K</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xl-3 col-md-12 pr-0">
                                        <div class="media p-0 for-light">
                                            <div class="custombox2">
                                                <div class="media-left"><i class="icofont icofont-paper-plane"></i></div>
                                                <div class="media-body">
                                                    <h6>Reach</h6>
                                                    <p>600</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="media p-0 for-dark">
                                            <div class="custombox">
                                                <div class="media-left"><i class="icofont icofont-paper-plane"></i></div>
                                                <div class="media-body">
                                                    <h6>Reach</h6>
                                                    <p>600</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xl-3 col-md-12 pr-0">
                                        <div class="media p-0 for-light">
                                            <div class="custombox2">
                                                <div class="media-left" style="background-color:#24EDC3 ;" ><img style="border-radius: 180px;" width="30" height="30" src="{{asset('assets/images/logo/connect.png')}}"></i></div>
                                                <div class="media-body">
                                                    <h6>Engagement</h6>
                                                    <p>18.2%</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="media p-0 for-dark">
                                            <div class="custombox" style=" " >
                                                <div class="media-left" style="background-color:#24EDC3 ;" style="padding: 8px;"><img style="border-radius: 180px; "  width="30" height="30" src="{{asset('assets/images/logo/connect.png')}}"></i></div>
                                                <div class="media-body">
                                                    <h6 >Engagement</h6>
                                                    <p>18.2%</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row second-chart-list third-news-update">
        <div class="col-xl-6 xl-50 col-md-6" style="height:200px;">
            <div class="card" style="height:550px;">
                <div class="card-header">
                    <h5>Get more likes this summer</h5>
                </div>
                <div class="card-body">
                    <div class="knob-block text-center">
                        <input class="knob"  data-width="200" data-thickness=".1" data-angleoffset="90" data-fgcolor="#7366ff" data-linecap="round" value="35">
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-6 xl-50 col-md-6">
            <div class="card">
                <div class="card-header">
                    <div class="row" style="display: flex;align-items: center;">
                        <h5>Flowers growth </h5>
                        <div class="select-box">
                            <select name="cmbIdioma2" id="cmbIdioma2" class="form-control" style="width: 100%">
                                <option selected>This month</option>
                                <option value="">Last month</option>
                                <option value="">Last two month</option>
                                <option value="">Last three month</option>
                            </select>

                            <div class="three-dot for-light">
                                <i class="fa fa-ellipsis-h" style="transform:rotate(90deg);"></i>
                            </div>
                            <div class="three-dotDark for-dark">
                                <i class="fa fa-ellipsis-h" style="transform:rotate(90deg);"></i>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div id="column-chart"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-12 col-xl-12 box-col-6" style="padding: 0px;">
        <div class="card">
            <div class="card-header">
                <div class="row" style="display: flex;align-items: center;">
                    <h5>Total Statistics</h5>
                    <div class="select-box">
                        <select name="cmbIdioma2" id="cmbIdioma3" class="form-control" style="width: 100%">
                            <option selected>Last six month</option>
                            <option value="">Last month</option>
                            <option value="">Last two month</option>
                            <option value="">Last three month</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <div id="annotationchart"></div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="{{asset('assets/js/chart/chartist/chartist.js')}}"></script>
    <script src="{{asset('assets/js/chart/chartist/chartist-plugin-tooltip.js')}}"></script>
    <script src="{{asset('assets/js/chart/knob/knob.min.js')}}"></script>
    <script src="{{asset('assets/js/chart/knob/knob-chart.js')}}"></script>
    <script src="{{asset('assets/js/chart/apex-chart/apex-chart.js')}}"></script>
    <script src="{{asset('assets/js/chart/apex-chart/stock-prices.js')}}"></script>
    <script src="{{asset('assets/js/notify/bootstrap-notify.min.js')}}"></script>
    <script src="{{asset('assets/js/dashboard/default.js')}}"></script>
    <script src="{{asset('assets/js/notify/index.js')}}"></script>
    <script src="{{asset('assets/js/datepicker/date-picker/datepicker.js')}}"></script>
    <script src="{{asset('assets/js/datepicker/date-picker/datepicker.en.js')}}"></script>
    <script src="{{asset('assets/js/datepicker/date-picker/datepicker.custom.js')}}"></script>
    <script src="{{asset('assets/js/chart/apex-chart/apex-chart.js')}}"></script>
    <script src="{{asset('assets/js/chart/apex-chart/stock-prices.js')}}"></script>
    <script src="{{asset('assets/js/chart/apex-chart/chart-custom.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet"/>
    <script>
        $("#cmbIdioma").select2({
            templateResult: function (idioma) {
                var $span = $("<span>" + idioma.text + "</span>");
                return $span;
            },
            templateSelection: function (idioma) {
                var $span = $("<span><i class=\"fa fa-calendar faFacalender\"></i>" + idioma.text + "</span>");
                return $span;
            }
        });
        $("#cmbIdioma2").select2({
            templateResult: function (idioma) {
                var $span = $("<span>" + idioma.text + "</span>");
                return $span;
            },
            templateSelection: function (idioma) {
                var $span = $("<span><i class=\"fa fa-calendar faFacalender\"></i>" + idioma.text + "</span>");
                return $span;
            }
        });
        $("#cmbIdioma3").select2({
            templateResult: function (idioma) {
                var $span = $("<span>" + idioma.text + "</span>");
                return $span;
            },
            templateSelection: function (idioma) {
                var $span = $("<span><i class=\"fa fa-calendar faFacalender\"></i>" + idioma.text + "</span>");
                return $span;
            }
        });
    </script>
@endsection

