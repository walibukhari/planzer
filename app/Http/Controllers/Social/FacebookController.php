<?php

namespace App\Http\Controllers\Social;

use App\Http\Controllers\Controller;
use App\Models\Admin;
use App\Models\User;
use Facebook\Facebook;
use Facebook\Exceptions\FacebookResponseException;
use Facebook\Exceptions\FacebookSDKException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class FacebookController extends Controller
{
    public function profile() {
        $appId         = env('FACEBOOK_CLIENT_ID'); //Facebook App ID
        $appSecret     = env('FACEBOOK_CLIENT_SECRET'); //Facebook App Secret
        $redirectURL   = env('FACEBOOK_REDIRECT'); //Callback URL
        $fbPermissions = array('publish_actions'); //Facebook permission

        $fb = new Facebook(array(
            'app_id' => $appId,
            'app_secret' => $appSecret,
            'default_graph_version' => 'v2.6',
        ));

// Get redirect login helper
        $helper = $fb->getRedirectLoginHelper();
        $accessToken = $helper->getAccessToken();
        dd($accessToken);
// Try to get access token
//        try {
            if(isset($_SESSION['facebook_access_token'])){
                $accessToken = $_SESSION['facebook_access_token'];
            }else{
                $accessToken = $helper->getAccessToken();
            }
//        } catch(FacebookResponseException $e) {
//            echo 'Graph returned an error: ' . $e->getMessage();
//            exit;
//        } catch(FacebookSDKException $e) {
//            echo 'Facebook SDK returned an error: ' . $e->getMessage();
//            exit;
//        }
    }
}
