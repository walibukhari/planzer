
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>SocialDay - Register Page</title>
    <link rel="shortcut icon" href="/favicon.png" type="image/x-icon">
    <!-- Bootstrap , fonts & icons  -->
    <link rel="stylesheet" href="https://uxtheme.net/demos/finity/css/bootstrap.css">
    <link rel="stylesheet" href="https://uxtheme.net/demos/finity/fonts/icon-font/css/style.css">
    <link rel="stylesheet" href="https://uxtheme.net/demos/finity/fonts/typography-font/typo.css">
    <link rel="stylesheet" href="https://uxtheme.net/demos/finity/fonts/fontawesome-5/css/all.css">
    <!-- Plugin'stylesheets  -->
    <link rel="stylesheet" href="https://uxtheme.net/demos/finity/plugins/aos/aos.min.css">
    <link rel="stylesheet" href="https://uxtheme.net/demos/finity/plugins/fancybox/jquery.fancybox.min.css">
    <link rel="stylesheet" href="https://uxtheme.net/demos/finity/plugins/nice-select/nice-select.min.css">
    <link rel="stylesheet" href="https://uxtheme.net/demos/finity/plugins/slick/slick.min.css">
    <!-- Vendor stylesheets  -->
    <link rel="stylesheet" href="https://uxtheme.net/demos/finity/css/main.css">
    <!-- Custom stylesheet -->
    <style>
        .nice-select {
            -webkit-tap-highlight-color: transparent;
            background-color: #fff;
            border-radius: 5px;
            border: solid 1px #e8e8e8;
            box-sizing: border-box;
            clear: both;
            cursor: pointer;
            padding-left: 12px !important;
            display: block;
             float: unset !important;
            font-family: inherit;
            font-size: 14px;
            font-weight: normal;
             height: auto !important;
            line-height: 40px;
            outline: 0;
        }
        .md-6-l{
            /*padding-left:3px;*/
        }
        .md-6-r{
            /*padding-right:3px;*/
        }
        #row_row{
            margin-bottom:30px;
        }
    </style>
</head>

<body data-theme="light">
<div class="site-wrapper overflow-hidden ">
    <div id="loading">
        <img src="https://uxtheme.net/demos/finity/image/preloader.gif" alt="">
    </div>
    <!-- Header Area -->
    <header class="site-header site-header--absolute 3 pt-lg-11">
        <div class="container">
            <div class="d-flex justify-content-between align-items-center flex-wrap">
                <!-- Brand Logo-->
                <div class="brand-logo mx-auto text-center"  style="z-index:99999;">
                    <a class="" href="/home" style="z-index:99999;">
                        <img style="width:225px;position:relative;top:30px;" src="{{asset('/SocialdayLogoColor1.png')}}" alt="" class="light-version-logo">
                        <!-- Dark version logo (logo must be White)-->
                        <img style="width:225px;position:relative;top:30px;" src="{{asset('/SocialdayLogoColor1.png')}}" alt="" class="dark-version-logo">
                    </a>
                </div>
                <div class="header-social-share d-none ">
                    <ul class="list-unstyled d-flex align-items-center text-right mb-0">
                        <li>
                            <a class="heading-default-color ml-10 gr-text-blue-3" href="#">
                                <i class="fab fa-twitter"></i>
                            </a>
                        </li>
                        <li>
                            <a class="heading-default-color ml-10" href="#">
                                <i class="fab fa-facebook-f"></i>
                            </a>
                        </li>
                        <li>
                            <a class="heading-default-color ml-10" href="#">
                                <i class="fab fa-google"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </header>
    <!-- End Header Area -->
    <div class="min-height-100vh d-flex align-items-center pt-15 pb-13 pt-lg-32 pb-lg-27 bg-default-3">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-6 col-lg-7 col-md-8 col-xs-10">
                    <div class="">
                        <div class="mb-10 text-center" data-aos="fade-up" data-aos-duration="500" data-aos-once="true">
                            <h2 class="mb-1 font-size-10 letter-spacing-n83">Create account</h2>
                            <p class="text-bali-gray font-size-7 mb-0">No credit card required</p>
                        </div>
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul style="list-style:none;">
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <form id="submitFormTest" action="{{route('postRegister')}}" data-aos="fade-up" data-aos-duration="800" data-aos-once="true">
                            @csrf
                            <!-- First Name -->
                            <div class="row" id="row_row">
                                <div class="col-md-6 md-6-r">
                                    <input type="text" name="first_name" style="padding-left:12px !important;" class="form-control form-control-lg bg-white rounded-4 text-dark-cloud text-placeholder-bali-gray pl-18 font-size-5" placeholder="First Name" id="email">
                                </div>
                                <!-- Last Name -->
                                <div class="col-md-6 md-6-l">
                                    <input type="text" name="last_name" style="padding-left:12px !important;" class="form-control form-control-lg bg-white rounded-4 text-dark-cloud text-placeholder-bali-gray pl-18 font-size-5" placeholder="Last Name" id="email">
                                </div>
                            </div>
                            <div class="row" id="row_row">
                                <!-- Company Name -->
                                <div class="col-md-6 md-6-r">
                                    <input type="text" name="address" style="padding-left:12px !important;" class="form-control form-control-lg bg-white rounded-4 text-dark-cloud text-placeholder-bali-gray pl-18 font-size-5" placeholder="Company name" id="company">
                                </div>
                                <!-- Vat Number -->
                                <div class="col-md-6 md-6-l">
                                    <input type="text" name="vat_number" style="padding-left:12px !important;" class="form-control form-control-lg bg-white rounded-4 text-dark-cloud text-placeholder-bali-gray pl-18 font-size-5" placeholder="Vat Number" id="vat_number">
                                </div>
                            </div>
                            <div class="row" id="row_row">
                                <!-- Phone Number -->
                                <div class="col-md-6 md-6-r">
                                    <input type="number" name="phone_number" style="padding-left:12px !important;" class="form-control form-control-lg bg-white rounded-4 text-dark-cloud text-placeholder-bali-gray pl-18 font-size-5" placeholder="Phone Number" id="phone_number">
                                </div>
                                <!-- Email -->
                                <div class="col-md-6 md-6-l">
                                    <input type="email" name="email" style="padding-left:12px !important;" class="form-control form-control-lg bg-white rounded-4 text-dark-cloud text-placeholder-bali-gray pl-18 font-size-5" placeholder="Email Address" id="email">
                                </div>
                            </div>
                           <div class="row" id="row_row">
                               <!-- Password -->
                               <div class="col-md-6 md-6-r">
                                   <input type="password" name="password" style="padding-left:12px !important;" class="form-control form-control-lg bg-white rounded-4 text-dark-cloud text-placeholder-bali-gray pl-18 font-size-5" placeholder="Password" id="password">
                               </div>
                               <!-- Confirm Password -->
                               <div class="col-md-6 md-6-l">
                                   <input type="password" name="confirm_password" style="padding-left:12px !important;" class="form-control form-control-lg bg-white rounded-4 text-dark-cloud text-placeholder-bali-gray pl-18 font-size-5" placeholder="Confirm Password" id="c_password">
                               </div>
                           </div>
                            <!-- UTC -->
                            <div class="row" id="row_row">
                                <div class="col-md-12">
                                    <select style="padding-left:12px !important;" class="form-control form-control-lg bg-white rounded-4 text-dark-cloud text-placeholder-bali-gray pl-18 font-size-5" name="timezone">
                                        <option value="" selected="" disabled="">Select One</option>
                                        @foreach($tzlist as $tzone )
                                            <option>{{$tzone}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group mt-8 mb-7">
                                <label for="terms-check" class="check-input-control d-flex align-items-center">
                                    <input class="d-none" required type="checkbox" id="terms-check">
                                    <span class="checkbox mr-5"></span>
                                    <span class="mb-0 font-size-5 text-bali-gray">I agree to the  <a
                                            href="/terms" class="text-blue-3 ">Terms & Conditions</a>
                  </span>
                                </label>
                            </div>
                            <div class="button">
                                <button type="button" onclick="submitForm()" class="btn btn-blue-3  w-100 rounded-4">Sign up</button>
                                <p class="text-center font-size-5 mt-8 text-bali-gray" data-aos="fade-up" data-aos-duration="1100" data-aos-once="true">Already have an account? <a href="{{route('login')}}" class="text-blue-3 ">Sign in</a>
                                </p>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://uxtheme.net/demos/finity/js/vendor.min.js"></script>
<!-- Plugin's Scripts -->
<script src="https://uxtheme.net/demos/finity/plugins/fancybox/jquery.fancybox.min.js"></script>
<script src="https://uxtheme.net/demos/finity/plugins/nice-select/jquery.nice-select.min.js"></script>
<script src="https://uxtheme.net/demos/finity/plugins/aos/aos.min.js"></script>
<script src="https://uxtheme.net/demos/finity/plugins/slick/slick.min.js"></script>
<script src="https://uxtheme.net/demos/finity/plugins/counter-up/jquery.counterup.min.js"></script>
<script src="https://uxtheme.net/demos/finity/plugins/counter-up/jquery.waypoints.min.js"></script>
<!-- Activation Script -->
<script src="https://uxtheme.net/demos/finity/js/custom.js"></script>
<script>
    function submitForm(){
        let password = $('#password').val();
        let c_password = $('#c_password').val();
        if(password == c_password) {
            $('#submitFormTest').submit();
        } else {
            alert('password and confirm password not match');
        }
    }
</script>
</body>

</html>
