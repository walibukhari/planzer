<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class NewRegisterUser implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    public $user = null;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($user)
    {
        \Log::info('queue send email to user started');
        $this->user = $user;
        \Log::info($this->user->email);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            $senderEmail = env('MAIL_USERNAME');
            $senderName  ='planzer.com';
            $UserEmail = $this->user->email;
            Mail::send('emails.RegisterEmail', array('data' => $this->user), function ($message) use ($senderEmail, $senderName , $UserEmail) {
                $message->from($senderEmail, $senderName);
                $message->to($UserEmail)
                    ->subject('Registration Successfully .... !');
            });
            \Log::info('email send to user success ... !');
        } catch (\Exception $e) {
            \Log::info('some thing went wrong');
            \Log::info($e->getMessage());
        }
    }
}
