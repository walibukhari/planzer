<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\FormRequest;
use App\Http\Requests\PostFormRequest;
use App\Jobs\NewRegisterUser;
use App\Models\Profile;
use App\Models\User;
use App\Traits\General;
use DateTimeZone;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class AuthController extends Controller
{
    use ThrottlesLogins , General;
    protected $maxAttempts = 3;
    protected $decayMinutes = 2;

    public function index(){
        return view('auth.login');
    }

    public function newRegister(){
        $data['tzlist'] = DateTimeZone::listIdentifiers(DateTimeZone::ALL);
        return view('auth.new_register',$data);
    }

    public function postRegister(PostFormRequest  $request){
        $user = User::createUser($request->all());
        Profile::createUserProfile($user,$request,Profile::TYPE_NORMAL_ADMIN);
        NewRegisterUser::dispatch($user)->delay(now()->addMinutes(1));
        return redirect('/login')->with('success_message','successfully registered');
    }

    public function resetPassword(){
        return view('auth.resetPassword');
    }

    public function updateResetPassword() {
        $email = \Session::get('email_address');
        if($email) {
            $user = User::where('email','=',$email)->first();
            return view('auth.updatePassword',[
                'email' => $email,
                'user' => $user
            ]);
        } else {
            return redirect()->route('resetPassword');
        }
    }

    public function postUpdatePassword(Request $request){
        if($request->password == $request->c_password) {
            User::where('id', '=', $request->user_id)->update([
                'password' => bcrypt($request->password)
            ]);
            return redirect()->route('login');
        } else {
            return back()->with('error','Password Confirm Password Not Match....!');
        }
    }

    public function sendEmailToUserEmailAddress($email){
        $userEmail = $email;
        \Session::put('email_address',$email);
        $array = [
            'user_email' => $userEmail,
            'link' => route('updateResetPassword')
        ];
        $view = 'mails.reset_password';
        $msg = 'Reset Your Password';
        return $this->sendEmail($userEmail,$array,$view,$msg);
    }

    public function postResetPassword(Request $request){
        $check = User::where('email','=',$request->email)->first();
        if(is_null($check)) {
            return back()->with('error','Email not exist our record...!');
        } else {
            if($this->sendEmailToUserEmailAddress($request->email)) {
                return back()->with('success','Please check your email address if you cannot receive email in 2 minutes then resend email');
            } else {
                return back()->with('error','Something went wrong please try again later');
            }
        }
    }

    public function loginPost(Request $request) {
        if(!$this->checkUserExists($request)){
            return back()->with('error','email not exists in our record ....');
        }
        $cred = [
            'email' => $request->email,
            'password' => $request->password
        ];
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);
            return $this->sendLockoutResponse($request);
        }
        if(!$this->checkPasswordEmailMatch($request)) {
            return back()->with('error','email and password not match ....');
        }
        if(Auth::attempt($cred)){
            $this->clearLoginAttempts($request);
            return redirect('/user/home');
        } else {
            return redirect('/login');
        }
        $this->incrementLoginAttempts($request);
        return back()->with('error','Too Many Return....');
    }

    public function checkPasswordEmailMatch($request){
        $user = User::where('email','=',$request->email)->first();
        if(!\Hash::check($request->password , $user->password)) {
            return false;
        } else {
            return true;
        }
    }

    public function username() {
        return 'email';
    }

    public function checkUserExists($request){
        $user = User::where('email','=',$request->email)->first();
        if($user) {
            $request->request->add([
                'username' => $user->name
            ]);
            return true;
        } else {
            return false;
        }
    }

    public function register(Request $request) {
        dd($request->all());
    }
    public function forgotPassword(Request $request){
        dd($request->all());
    }
    public function logoutUser() {
        Auth::logout();
        if(!Auth::check()) {
            return redirect('/login');
        }
    }
}
