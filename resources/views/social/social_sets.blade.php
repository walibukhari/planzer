<div class="col-sm-4">
    <div class="panel panel-default for-light">
        <div class="panel-body text-center">
            <div class="cardtitle"><strong>Twitter</strong></div>
            <br> <img src="{{url('/')}}/assets/icons/twitter.png" class="social-icon"><br><br>

            <br>
            <a href="javascript:;" @click="connectToTwitter()" class="btn btn-primary btn-block actionbtn">connect</a>
        </div>
    </div>
    <div class="panel panel-default for-dark" style="background-color:rgb(22, 22, 22);">
        <div class="panel-body text-center">
            <div class="cardtitle"><strong>Twitter</strong></div>
            <br> <img src="{{url('/')}}/assets/icons/twitter.png" class="social-icon"><br><br>

            <br>
            <a href="{{route('twitter.login')}}" class="btn btn-primary btn-block actionbtn">
                @{{ twitter_connect }}
            </a>
        </div>
    </div>
</div>
<div class="col-sm-4">
    <div class="panel panel-default for-light">
        <div class="panel-body text-center">
            <div class="cardtitle"><strong>Facebook</strong></div>
            <br> <img src="{{url('/')}}/assets/icons/facebook.png " class="social-icon"><br><br>

            <br> <a href="{{url('super-admin/facebook/profile')}}" class="btn btn-primary btn-block actionbtn">connect</a>
        </div>
    </div>
    <div class="panel panel-default for-dark" style="background-color:rgb(22, 22, 22);">
        <div class="panel-body text-center">
            <div class="cardtitle"><strong>Facebook</strong></div>
            <br> <img src="{{url('/')}}/assets/icons/facebook.png " class="social-icon"><br><br>

            <br> <a href="{{url('super-admin/facebook/profile')}}" class="btn btn-primary btn-block actionbtn">connect</a>
        </div>
    </div>
</div>
<div class="col-sm-4">
    <div class="panel panel-default for-light">
        <div class="panel-body text-center">
            <div class="cardtitle"><strong>Instagram</strong></div>
            <br> <img src="{{url('/')}}/assets/icons/instagram.png" class="social-icon"><br><br>

            <br> <a href="{{route('connectMedia',['insta'])}}" class="btn btn-primary btn-block actionbtn">connect</a>
        </div>
    </div>
    <div class="panel panel-default for-dark" style="background-color:rgb(22, 22, 22);">
        <div class="panel-body text-center">
            <div class="cardtitle"><strong>Instagram</strong></div>
            <br> <img src="{{url('/')}}/assets/icons/instagram.png" class="social-icon"><br><br>

            <br> <a href="{{route('connectMedia',['insta'])}}" class="btn btn-primary btn-block actionbtn">connect</a>
        </div>
    </div>
</div>
<div class="col-sm-4">
    <div class="panel panel-default for-light">
        <div class="panel-body text-center">
            <div class="cardtitle"><strong>LinkedIn</strong></div>
            <br> <img class="social-icon" src="{{url('/')}}/assets/icons/linkedin.png"><br><br>

            <br> <a href="{{route('link_dinLogin')}}" class="btn btn-primary btn-block actionbtn">connect</a>
        </div>
    </div>
    <div class="panel panel-default for-dark" style="background-color:rgb(22, 22, 22);">
        <div class="panel-body text-center">
            <div class="cardtitle"><strong>LinkedIn</strong></div>
            <br> <img class="social-icon" src="{{url('/')}}/assets/icons/linkedin.png"><br><br>

            <br> <a href="{{route('link_dinLogin')}}" class="btn btn-primary btn-block actionbtn">connect</a>
        </div>
    </div>
</div>
<div class="col-sm-4">
    <div class="panel panel-default for-light">
        <div class="panel-body text-center">
            <div class="cardtitle"><strong>Youtube</strong></div>
            <br> <img
                src="{{url('/')}}/assets/icons/youtube.png"
                class="social-icon"><br><br>

            <br> <a href="{{route('you_tube')}}" class="btn btn-primary btn-block actionbtn">connect</a>
        </div>
    </div>
    <div class="panel panel-default for-dark" style="background-color:rgb(22, 22, 22);">
        <div class="panel-body text-center">
            <div class="cardtitle"><strong>Youtube</strong></div>
            <br> <img
                src="{{url('/')}}/assets/icons/youtube.png"
                class="social-icon"><br><br>

            <br> <a href="{{route('you_tube')}}" class="btn btn-primary btn-block actionbtn">connect</a>
        </div>
    </div>
</div>
<div class="col-sm-4">
    <div class="panel panel-default for-light">
        <div class="panel-body text-center">
            <div class="cardtitle"><strong>Tiktok</strong></div>
            <br> <img src="{{url('/')}}/assets/icons/tiktok.png" class="social-icon"><br><br>

            <br> <a href="{{route('you_tube')}}" class="btn btn-primary btn-block actionbtn">connect</a>
        </div>
    </div>
    <div class="panel panel-default for-dark" style="background-color:rgb(22, 22, 22);" >
        <div class="panel-body text-center">
            <div class="cardtitle"><strong>Tiktok</strong></div>
            <br> <img src="{{url('/')}}/assets/icons/tiktok.png" class="social-icon"><br><br>

            <br> <a href="{{route('you_tube')}}" class="btn btn-primary btn-block actionbtn">connect</a>
        </div>
    </div>
</div>
