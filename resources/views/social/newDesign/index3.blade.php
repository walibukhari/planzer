

@extends('userPortal.layout.app')
@section('title', 'Facebook')

@section('css')
@endsection

@push('style')
    <style>
        .panel-default{
            background: #fff;
            padding: 20px;
            margin-bottom: 30px;
            border-radius: 15px;
        }
        .form-tabs li a{
            color: #b3b7bb !important;
            font-size: 15px;
            padding-left: 20px;

        }
        .form-tabs .active{
            border-bottom: 3px solid #1dc116 !important;
        }
        .form-tabs .active a{
            color: #1dc116 !important;
        }
        .avatar-img{
            margin-top: 5px;
            padding: 5px;
            margin-left: 18px;
            width: 15%;
            border-radius: 15px;
        }
        .card-heading{
            padding-top: 15px;
            padding-left: 5px;
        }
        .card-img-top{
            border-radius: 15px;
            margin: 15px;
            width: 90%;
            height: 275px;
        }
        .card-body-link{
            padding-left: 15%;
            font-size: 15px;
        }
        .card .card-body{
            padding: 10px !important;
            padding-bottom: 20px !important;
        }
        .card-body-link p{
            padding-left: 5px;
        }
    </style>
@endpush

@section('breadcrumb-title')
    <h3>Facebook Setup</h3>
@endsection

@section('breadcrumb-items')
    <li class="breadcrumb-item">Users</li>
    <li class="breadcrumb-item active">Add New User</li>
@endsection

@section('content')
    <div class="m-5">
        <div class="row card panel-default" style="margin-top: 7%">
            <ul class="form-tabs row">
                <li class="active"><a href="#">Home</a></li>
                <li><a href="#">Feed</a></li>
                <li><a href="#">Followers</a></li>
                <li><a href="#">Following</a></li>
                <li><a href="#">Tagged</a></li>
                <li><a href="#">Saved</a></li>
                <li><a href="#">Stories</a></li>
                <li><a href="#">Archived Stories</a></li>
                <li><a href="#">Search</a></li>
            </ul>
        </div>
        <div class="row ml-auto mr-auto">
            <div class="col-md-3">
                <div class="card">
                    <div class="row">
                        <img class="avatar-img" src="https://image.freepik.com/free-vector/profile-icon-male-avatar-hipster-man-wear-headphones_48369-8728.jpg">
                        <h5 class="card-heading">Breathembrella</h5>
                    </div>
                    <img class="card-img-top" src="https://graphberry.imgix.net/t/leon-psd-agency-template-thumb.png?auto=compress,format&w=360&q=80" alt="Card image cap">
                    <div class="card-body row">
                        <a class="row card-body-link">
                            <i class="fa fa-heart-o" aria-hidden="true"></i>
                            <p>1.2K</p>
                        </a>
                        <a class="row card-body-link">
                            <i class="fa fa-commenting-o" aria-hidden="true"></i>
                            <p>425</p>
                        </a>
                        <a class="row card-body-link">
                            <i class="fa fa-bookmark-o" aria-hidden="true"></i>
                            <p>13</p>
                        </a>
                        <a class="row card-body-link">
                            <i class="fa fa-paper-plane-o" aria-hidden="true"></i>
                            <p>35</p>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card">
                    <div class="row">
                        <img class="avatar-img" src="https://image.freepik.com/free-vector/profile-icon-male-avatar-hipster-man-wear-headphones_48369-8728.jpg">
                        <h5 class="card-heading">Breathembrella</h5>
                    </div>
                    <img class="card-img-top" src="https://graphberry.imgix.net/t/leon-psd-agency-template-thumb.png?auto=compress,format&w=360&q=80" alt="Card image cap">
                    <div class="card-body row">
                        <a class="row card-body-link">
                            <i class="fa fa-heart-o" aria-hidden="true"></i>
                            <p>1.2K</p>
                        </a>
                        <a class="row card-body-link">
                            <i class="fa fa-commenting-o" aria-hidden="true"></i>
                            <p>425</p>
                        </a>
                        <a class="row card-body-link">
                            <i class="fa fa-bookmark-o" aria-hidden="true"></i>
                            <p>13</p>
                        </a>
                        <a class="row card-body-link">
                            <i class="fa fa-paper-plane-o" aria-hidden="true"></i>
                            <p>35</p>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card">
                    <div class="row">
                        <img class="avatar-img" src="https://image.freepik.com/free-vector/profile-icon-male-avatar-hipster-man-wear-headphones_48369-8728.jpg">
                        <h5 class="card-heading">Breathembrella</h5>
                    </div>
                    <img class="card-img-top" src="https://graphberry.imgix.net/t/leon-psd-agency-template-thumb.png?auto=compress,format&w=360&q=80" alt="Card image cap">
                    <div class="card-body row">
                        <a class="row card-body-link">
                            <i class="fa fa-heart-o" aria-hidden="true"></i>
                            <p>1.2K</p>
                        </a>
                        <a class="row card-body-link">
                            <i class="fa fa-commenting-o" aria-hidden="true"></i>
                            <p>425</p>
                        </a>
                        <a class="row card-body-link">
                            <i class="fa fa-bookmark-o" aria-hidden="true"></i>
                            <p>13</p>
                        </a>
                        <a class="row card-body-link">
                            <i class="fa fa-paper-plane-o" aria-hidden="true"></i>
                            <p>35</p>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card">
                    <div class="row">
                        <img class="avatar-img" src="https://image.freepik.com/free-vector/profile-icon-male-avatar-hipster-man-wear-headphones_48369-8728.jpg">
                        <h5 class="card-heading">Breathembrella</h5>
                    </div>
                    <img class="card-img-top" src="https://graphberry.imgix.net/t/leon-psd-agency-template-thumb.png?auto=compress,format&w=360&q=80" alt="Card image cap">
                    <div class="card-body row">
                        <a class="row card-body-link">
                            <i class="fa fa-heart-o" aria-hidden="true"></i>
                            <p>1.2K</p>
                        </a>
                        <a class="row card-body-link">
                            <i class="fa fa-commenting-o" aria-hidden="true"></i>
                            <p>425</p>
                        </a>
                        <a class="row card-body-link">
                            <i class="fa fa-bookmark-o" aria-hidden="true"></i>
                            <p>13</p>
                        </a>
                        <a class="row card-body-link">
                            <i class="fa fa-paper-plane-o" aria-hidden="true"></i>
                            <p>35</p>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card">
                    <div class="row">
                        <img class="avatar-img" src="https://image.freepik.com/free-vector/profile-icon-male-avatar-hipster-man-wear-headphones_48369-8728.jpg">
                        <h5 class="card-heading">Breathembrella</h5>
                    </div>
                    <img class="card-img-top" src="https://graphberry.imgix.net/t/leon-psd-agency-template-thumb.png?auto=compress,format&w=360&q=80" alt="Card image cap">
                    <div class="card-body row">
                        <a class="row card-body-link">
                            <i class="fa fa-heart-o" aria-hidden="true"></i>
                            <p>1.2K</p>
                        </a>
                        <a class="row card-body-link">
                            <i class="fa fa-commenting-o" aria-hidden="true"></i>
                            <p>425</p>
                        </a>
                        <a class="row card-body-link">
                            <i class="fa fa-bookmark-o" aria-hidden="true"></i>
                            <p>13</p>
                        </a>
                        <a class="row card-body-link">
                            <i class="fa fa-paper-plane-o" aria-hidden="true"></i>
                            <p>35</p>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card">
                    <div class="row">
                        <img class="avatar-img" src="https://image.freepik.com/free-vector/profile-icon-male-avatar-hipster-man-wear-headphones_48369-8728.jpg">
                        <h5 class="card-heading">Breathembrella</h5>
                    </div>
                    <img class="card-img-top" src="https://graphberry.imgix.net/t/leon-psd-agency-template-thumb.png?auto=compress,format&w=360&q=80" alt="Card image cap">
                    <div class="card-body row">
                        <a class="row card-body-link">
                            <i class="fa fa-heart-o" aria-hidden="true"></i>
                            <p>1.2K</p>
                        </a>
                        <a class="row card-body-link">
                            <i class="fa fa-commenting-o" aria-hidden="true"></i>
                            <p>425</p>
                        </a>
                        <a class="row card-body-link">
                            <i class="fa fa-bookmark-o" aria-hidden="true"></i>
                            <p>13</p>
                        </a>
                        <a class="row card-body-link">
                            <i class="fa fa-paper-plane-o" aria-hidden="true"></i>
                            <p>35</p>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card">
                    <div class="row">
                        <img class="avatar-img" src="https://image.freepik.com/free-vector/profile-icon-male-avatar-hipster-man-wear-headphones_48369-8728.jpg">
                        <h5 class="card-heading">Breathembrella</h5>
                    </div>
                    <img class="card-img-top" src="https://graphberry.imgix.net/t/leon-psd-agency-template-thumb.png?auto=compress,format&w=360&q=80" alt="Card image cap">
                    <div class="card-body row">
                        <a class="row card-body-link">
                            <i class="fa fa-heart-o" aria-hidden="true"></i>
                            <p>1.2K</p>
                        </a>
                        <a class="row card-body-link">
                            <i class="fa fa-commenting-o" aria-hidden="true"></i>
                            <p>425</p>
                        </a>
                        <a class="row card-body-link">
                            <i class="fa fa-bookmark-o" aria-hidden="true"></i>
                            <p>13</p>
                        </a>
                        <a class="row card-body-link">
                            <i class="fa fa-paper-plane-o" aria-hidden="true"></i>
                            <p>35</p>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card">
                    <div class="row">
                        <img class="avatar-img" src="https://image.freepik.com/free-vector/profile-icon-male-avatar-hipster-man-wear-headphones_48369-8728.jpg">
                        <h5 class="card-heading">Breathembrella</h5>
                    </div>
                    <img class="card-img-top" src="https://graphberry.imgix.net/t/leon-psd-agency-template-thumb.png?auto=compress,format&w=360&q=80" alt="Card image cap">
                    <div class="card-body row">
                        <a class="row card-body-link">
                            <i class="fa fa-heart-o" aria-hidden="true"></i>
                            <p>1.2K</p>
                        </a>
                        <a class="row card-body-link">
                            <i class="fa fa-commenting-o" aria-hidden="true"></i>
                            <p>425</p>
                        </a>
                        <a class="row card-body-link">
                            <i class="fa fa-bookmark-o" aria-hidden="true"></i>
                            <p>13</p>
                        </a>
                        <a class="row card-body-link">
                            <i class="fa fa-paper-plane-o" aria-hidden="true"></i>
                            <p>35</p>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card">
                    <div class="row">
                        <img class="avatar-img" src="https://image.freepik.com/free-vector/profile-icon-male-avatar-hipster-man-wear-headphones_48369-8728.jpg">
                        <h5 class="card-heading">Breathembrella</h5>
                    </div>
                    <img class="card-img-top" src="https://graphberry.imgix.net/t/leon-psd-agency-template-thumb.png?auto=compress,format&w=360&q=80" alt="Card image cap">
                    <div class="card-body row">
                        <a class="row card-body-link">
                            <i class="fa fa-heart-o" aria-hidden="true"></i>
                            <p>1.2K</p>
                        </a>
                        <a class="row card-body-link">
                            <i class="fa fa-commenting-o" aria-hidden="true"></i>
                            <p>425</p>
                        </a>
                        <a class="row card-body-link">
                            <i class="fa fa-bookmark-o" aria-hidden="true"></i>
                            <p>13</p>
                        </a>
                        <a class="row card-body-link">
                            <i class="fa fa-paper-plane-o" aria-hidden="true"></i>
                            <p>35</p>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('script')
    <script src="https://use.fontawesome.com/0e55b1dfb4.js"></script>
    <script src="{{asset('assets/js/form-validation-custom.js')}}"></script>
@endpush
