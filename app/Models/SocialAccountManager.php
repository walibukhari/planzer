<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SocialAccountManager extends Model
{
    protected $fillable = [
        'ids',
        'set_id',
        'social_network',
        'category',
        'team_id',
        'login_type',
        'can_post',
        'pid',
        'name',
        'username',
        'token',
        'avatar',
        'url',
        'data',
        'proxy',
        'status',
        'access_token',
        'user_id'
    ];

    public static function createAccount($data){
        return self::create([
            'set_id' => isset($data['set_id']) ? $data['set_id'] : '',
            'ids' => isset($data['ids']) ? $data['ids'] : '',
            'social_network' => isset($data['social_network']) ? $data['social_network'] : '',
            'category' => isset($data['category']) ? $data['category'] : '',
            'team_id' => isset($data['team_id']) ? $data['team_id'] : '',
            'login_type' => isset($data['login_type']) ? $data['login_type'] : '',
            'can_post' => isset($data['can_post']) ? $data['can_post'] : '',
            'pid' => isset($data['pid']) ? $data['pid'] : '',
            'name' => isset($data['name']) ? $data['name'] : '',
            'username' => isset($data['username']) ? $data['username'] : '',
            'token' => isset($data['token']) ? $data['token'] : '',
            'avatar' => isset($data['avatar']) ? $data['avatar'] : '',
            'url' => isset($data['url']) ? $data['url'] : '',
            'data' => isset($data['data']) ? $data['data'] : '',
            'proxy' => isset($data['proxy']) ? $data['proxy'] : '',
            'status' => isset($data['status']) ? $data['status'] : '',
            'access_token' => isset($data['access_token']) ? $data['access_token'] : '',
            'user_id' => isset($data['user_id']) ? $data['user_id'] : ''
        ]);
    }

    public static function updateAccount($data){
        return self::where('user_id', '=', $data['user_id'])->where('social_network','=',$data['social_network'])->update([
            'set_id' => isset($data['set_id']) ? $data['set_id'] : '',
            'ids' => isset($data['ids']) ? $data['ids'] : '',
            'social_network' => isset($data['social_network']) ? $data['social_network'] : '',
            'category' => isset($data['category']) ? $data['category'] : '',
            'team_id' => isset($data['team_id']) ? $data['team_id'] : '',
            'login_type' => isset($data['login_type']) ? $data['login_type'] : '',
            'can_post' => isset($data['can_post']) ? $data['can_post'] : '',
            'pid' => isset($data['pid']) ? $data['pid'] : '',
            'name' => isset($data['name']) ? $data['name'] : '',
            'username' => isset($data['username']) ? $data['username'] : '',
            'token' => isset($data['token']) ? $data['token'] : '',
            'avatar' => isset($data['avatar']) ? $data['avatar'] : '',
            'url' => isset($data['url']) ? $data['url'] : '',
            'data' => isset($data['data']) ? $data['data'] : '',
            'proxy' => isset($data['proxy']) ? $data['proxy'] : '',
            'status' => isset($data['status']) ? $data['status'] : '',
            'access_token' => isset($data['access_token']) ? $data['access_token'] : '',
            'user_id' => isset($data['user_id']) ? $data['user_id'] : ''
        ]);
    }

    public static function checkInstagramAccountExistsAgainstComingUser($pid , $set_id){
        $check = self::where('pid','=',$pid)->where('set_id','=',$set_id)->exists();
        return $check;
    }
}
