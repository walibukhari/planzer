@extends('userPortal.layout.app')
@push('style')
    <style>
        .fa-plus {
            border-radius: 100px;
            font-size: 17px;
            border: 1px solid #000;
            padding: 2px 6px;
        }
        .fa-camera{
            font-size:23px;
            margin: 0px !important;
        }
        .types input {
            display: none;
        }
        .clearfix:after, .clearfix:before {
            display: table;
            line-height: 0;
            content: "";
        }
        input[type=checkbox], input[type=radio] {
            box-sizing: border-box;
            padding: 0;
        }
        .types .icon {
            margin-right: 15px;
            font-size: 34px;
            line-height: 1;
        }
        /*.types input+div:hover {*/
        /*    color: #212121;*/
        /*}*/
        .types {
            width:100%;
            margin-bottom: 30px;
        }
        .sli:before {
            display: inline-block;
            font: normal normal normal 24px/1 simple-line-icons;
            font-size: inherit;
            -moz-osx-font-smoothing: grayscale;
            -webkit-font-smoothing: antialiased;
            line-height: inherit;
            text-rendering: auto;
        }
        .sli-camera:before {
            content: "\e07f";
        }
        .types .type {
            display: inline-block;
            vertical-align: -1px;
            margin-left:20px;
        }
        label {
            display: block;
            float: left;
            width: 33.33%;
        }
        label {
            font-weight: 500;
        }
        label {
            vertical-align: middle;
        }
        .types .name {
            font-size: 18px;
            font-weight: 500;
            line-height: 24px;
            white-space: nowrap;
            overflow: hidden;
            text-overflow: ellipsis;
        }
        .types input+div {
            display:flex;
            align-items: center;
            position: relative;
            padding: 18px 34px;
            font-size: 14px;
            line-height: 16px;
            cursor: pointer;
            color: #fff;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            -webkit-transition: all ease 0.2s;
            -moz-transition: all ease 0.2s;
            transition: all ease 0.2s;
        }
        .types input:checked+div {
            color: #fff;
            border-color: #333333;
            background-color: #3C3F4E;
        }
        .fa {
            margin-right: 20px;
        }
        #styleImage {
            margin-right: 20px;
        }
        #styleImage2 {
            margin-right: 20px;
        }
    </style>
    <style>
        .panel-default{
            background: #fff;
            padding: 20px;
        }
        .box-search-one {
            margin-bottom: 15px;
        }
        .input-group {
            position: relative;
            display: -ms-flexbox;
            display: flex;
            -ms-flex-wrap: wrap;
            flex-wrap: wrap;
            -ms-flex-align: stretch;
            align-items: stretch;
            width: 100%;
        }
        .box-search-one input {
            border: none;
            background-color: #f2f3f7!important;
        }
        #faSearch {
            color:#000;
        }
        .input-group-append > button {
            background:#f2f3f7!important;
        }
        .input-group-append {
            margin-left: 0px;
            border: 1px solid #c0c0c0;
        }
        .card .card-body {
            padding: 15px 40px;
            background-color: transparent;
        }
        .subheader-main > h3{
            margin:0px;
        }
        .wrap {
            height: 50px;
            display: flex;
            align-items: center;
            justify-content: flex-start;
        }
        .widget-list .widget-item {
            padding: 7px 0;
            position: relative;
            height: 55px;
            display: flex;
            align-items: center;
        }
        .widget-list .widget-item.widget-item-3 .content {
            margin-left: 50px;
            margin-right: 23px;
        }
        .widget-list .widget-item .content.content-2 {
            padding-top: 0;
        }
        .widget-list .widget-item .content {
            color: #7d7e80;
            text-overflow: ellipsis;
            white-space: nowrap;
            overflow: hidden;
            padding-top: 8px;
        }
        .widget-list .widget-item .content .desc {
            color: #a3a3a3;
            font-size: 10px;
            text-overflow: ellipsis;
            white-space: nowrap;
            overflow: hidden;
        }
        .widget-list .widget-item .content .title {
            padding-top: 2px;
            font-weight: 600;
            text-overflow: ellipsis;
            white-space: nowrap;
            overflow: hidden;
            width: 85px;
        }
        .widget-list .widget-option {
            margin-left: 12px;
            margin-right: 12px;
            display: flex;
            align-items: center;
        }
        .search-list{
            margin-bottom:0px;
        }
        .widget {
            display: flex;
            align-items: center;
            justify-content: center;
            margin-top: 30px;
        }
        .widget-list .widget-item .icon img {
            width: 100%;
            border-radius: 100px;
            height: 100%;
            position: relative;
            top: -2px;
        }
        .widget-list .widget-item .icon {
            display: inline-block;
            height: 40px;
            width: 40px;
            border-radius: 4px;
            text-align: center;
            line-height: 40px;
            font-size: 18px;
            position: absolute;
            padding: 0;
        }
        .i-radio > input {
            position: absolute;
            z-index: -1;
            opacity: 0;
        }
        .i-radio {
            display: inline-block;
            position: relative;
            padding-left: 30px;
            text-align: left;
            margin-bottom: 10px;
            cursor: pointer;
            -webkit-transition: all 0.3s ease;
            transition: all 0.3s ease;
        }
        .post .post-type .item.active a {
            background: #5578eb;
            color: #fff;
        }
        .post .post-type .item:first-child a {
            border-left: none;
        }
        .item {
            cursor: pointer;
            height: 40px;
            display: flex;
            align-items: center;
            border: 1px solid #efefef;
        }
        .post .post-type .item a {
            border-left: 1px solid #f4f4f4;
            border-top: 1px solid #f4f4f4;
            border-bottom: 1px solid #f4f4f4;
            padding: 12px 15px;
            color: #74788d;
            display: block;
        }
        .fs-16{
            color: #17a2b8 !important;
            margin-bottom: 20px;
        }
        .post .post-preview .tab {
            text-align: center;
            margin-bottom: 0;
            background: #fff;
            padding-top: 6px;
            padding-bottom:12px;
            display: block;
            border-bottom: 1px solid #f4f4f4;
        }
        .m-t-30 {
            margin-top: 0px!important;
        }
        .tab-content>.active {
            display: block;
        }
        .preview-instagram-photo {
            position: relative;
            border: 1px solid #f4f4f4;
        }
        .preview-instagram-photo .preview-content .user-info {
            padding: 10px 20px;
        }
        .preview-instagram-photo .preview-content .preview-media {
            height: 368px;
            background-size: cover;
            background-position: center center;
            background-color: #f4f4f4;
            overflow: hidden;
            position: relative;
        }
        .preview-instagram-photo .preview-content .post-info, .preview-instagram-photo .preview-content .caption {
            padding: 10px 20px;
            word-break: break-all;
        }
        .preview-instagram-photo .preview-content .post-info, .preview-instagram-photo .preview-content .caption {
            padding: 10px 20px;
            word-break: break-all;
        }
        .preview-instagram-photo .preview-content .preview-comment {
            position: relative;
            border-top: 1px solid #eee;
            padding: 15px 50px;
            color: #999;
            background: url(/heart-icon.png) no-repeat 20px;
        }
        .img-circle{
            width: 30px;
        }
        .preview-instagram-photo .preview-content .line-no-text {
            margin-bottom: 3px;
            height: 13px;
            background: #f4f4f4;
        }
        .border-0{
            border-radius: 0px !important;
        }
        .activeClass{
            background:#7366ff;
            color:#fff;
        }
        .tab-content:focus {
            outline-color: transparent !important;
        }
        .m-t-30:focus{
            outline-color: transparent !important;
        }
    </style>
    <style>

        #input1{
            width:100%;
            width: 100%;
            padding: 6px 20px;
            background: #1B1C21;
            border: 1px solid rgb(60, 63, 78);
            color: #fff;
            border-top: 0px;
        }
        .input-field{
            width:100%;
        }
        .image-uploader .uploaded .uploaded-image {
            width: 77px;
            height: 50px;
        }
        .image-uploader {
            min-height: 12rem !important;
            border: 1px solid #29C4AC !important;
            position: relative !important;
            border-style: dashed !important;
            border-radius: 12px;
        }
        .upload-text{
            cursor: pointer;
        }
        .selectedActivated{
            background: #29C4AC;
        }
        #caption-area{
            border-top-left-radius:16px;
            padding:12px;
            border: 1px solid rgb(60, 63, 78);
            cursor: pointer;
            color:#fff;
            text-align: center;
        }
        #comment-area{
            border: 1px solid rgb(60, 63, 78);
            cursor: pointer;
            text-align: center;
            border-top-right-radius:16px;
            padding:12px;
            color:#fff;
        }
    </style>
    <link rel="stylesheet" href="{{asset('emoji/css/reset.css')}}">
    <link rel="stylesheet" href="{{asset('emoji/css/style.css')}}">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
    <script src="{{asset('emoji/js/jquery.emojiarea.js')}}"></script>
    <script src="{{asset('emoji/js/main.js')}}"></script>
    <link rel="stylesheet" href="{{asset('dropZone/dist/image-uploader.min.css')}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css" integrity="sha512-vKMx8UnXk60zUwyUnUPM3HbQo8QfmNx7+ltw8Pm5zLusl1XIfwcxo8DbWCqMGKaWeNxWA8yrx5v3SaVpMvR3CA==" crossorigin="anonymous" />
    <style>

        .image-uploader .upload-text span {
            color: #29C4AC;
        }
        .image-uploader .upload-text {
            background-color: rgba(51, 163, 110, 0.5);
            color: rgba(51, 163, 110, 0.5);
        }
        .addClassActiveClass{
            background: #38585F !important;
            color: #29C4AC !important;
            display: flex !important;
            align-items: center !important;
            justify-content: center !important;
            border-radius: 6px !important;
        }
        .changeBgColor{
            background: #38585F;
        }
        .location-section{
            width: 100%;
        }
        .comment-post-section{
            width: 100%;
            margin-top: 0px;
        }
        .image-uploader .uploaded .uploaded-image .delete-image {
            width:32px !important;
        }
        #inner-section {
            border-radius: 16px;
            border-radius: 16px;
        }
        #camera-view {
            background: #1B1C21;
            color: #E5E6E8;
            padding: 10px;
            display: flex;
            align-items: center;
            justify-content: center;
            border-radius: 6px;
        }
        #photo-video-area {
            color:#838894;
        }
        .same-bg {
            background: #21242B;
            padding: 12px 10px;
            border-top-left-radius: 16px;
            border-bottom-left-radius: 16px;
            border-right: 1px solid rgb(60, 63, 78);
        }
        .section-a {
            background: #1B1C21;
        }
        .section-c {
            background: #1B1C21;
        }
        .section-b{
            background: #1B1C21;
        }
        .mid-bg{
            background: #21242B;
            padding: 12px 10px;
            border-right: 1px solid rgb(60, 63, 78);
        }
        .activeSections {
            background: #3C3F4E;
            color: #fff;
        }
        .hide-on-small-only:hover{
            color: #9b9b9b !important;
        }
        .right-bg{
            border-right: 1px solid rgb(60, 63, 78);
            background: #21242B;
            padding: 12px 10px;
            border-top-right-radius: 16px;
            border-bottom-right-radius: 16px;
        }
        #plus-view{
            background:#1B1C21;
            color: #E5E6E8;
            padding: 10px;
            display: flex;
            align-items: center;
            justify-content: center;
            border-radius: 6px;
        }
        #file-view{
            background: #1B1C21;
            color: #E5E6E8;
            padding: 10px;
            display: flex;
            align-items: center;
            justify-content: center;
            border-radius: 6px;
        }
        #container {
            padding: 0px;
        }
        .fa-desktop {
            font-size:20px;
            margin-right:5px !important;
        }
        .fa-link{
            font-size:20px;
            margin-right:5px !important;
        }
        .section-heading{
            display: flex;
            align-items: center;
            justify-content: space-between;
            padding: 12px 12px;
            background: #21242B;
            color: #fff;
            border-top-right-radius: 12px;
            border-top-left-radius: 12px;
            border-bottom:1px solid #3C3F4E;
        }
        .section-body{
            background: #21242B;
            padding:25px;
            border-bottom-right-radius: 15px;
            border-bottom-left-radius: 15px;
        }
        #customImages {
            padding: 8px;
        }
        #customImages > img {
            height: 170px;
            object-fit: cover;
            border-radius: 12px;
        }
        #imagesMedia{
            padding-bottom: 100px;
        }
        .customH3{
            font-size:18px;
        }
        .fa-check {
            padding:6px;
            border-radius:12px;
            color:#fff;
            background-color:#32D55A;
        }
        .fa-map-marker{
            color: #fff;
            position: absolute;
            margin-top: 22px;
            margin-left: 13px;
            font-size: 17px;
        }
        .whiteClass{
            color:#fff;
        }
        .user-profile-section{
            display: flex;
            align-items: center;
            padding: 20px;
        }
        .flex-box-pro{
            display: flex;
            flex-direction: column;
            padding-left: 12px;
        }
        .pac-target-input:active{
            background: rgb(60, 63, 78) !important;
        }
        .pac-target-input{
            background: rgb(60, 63, 78);
            border: 1px solid rgb(60, 63, 78);
            height: 60px;
            padding-left: 35px;
        }
        .post-now-btn{
            background: #29C4AC;
            color:#fff;
            padding:15px;
            border: 0px;
            border-radius:12px;
            width: 100%;
        }
        .fa-ellipsis-h{
            transform: rotate(
                -90deg
            );
            position: absolute;
            right: -12px;
            top: 20px;
            cursor: pointer;
            color: #fff;
            background: rgb(60, 63, 78);
            padding: 0px 10px;
            border-radius: 4px;
        }
        .reaction-section {
            margin-top: 25px;
            color: #fff;
            margin-left: 10px;
            margin-right: 10px;
            display: flex;
            align-items: center;
            justify-content: space-between;
        }
        @media (max-width:991px) {
            .container{
                width: 100%;
                max-width: 100%;
                min-width: 100%;
            }
        }
        @media (max-width:800px) {
            .container{
                width: 100%;
                max-width: 100%;
                min-width: 100%;
            }
            #mid-section-1 {
                width:100%;
                max-width:100%;
                min-width:100%;
            }
            #mid-section-3 {
                width:100%;
                max-width:100%;
                min-width:100%;
            }
            #mid-section{
                padding: 0px;
            }
            .types > label {
                width: 100%;
                border-radius: 12px;
            }
        }
        @media (max-width:500px) {
            .types > label {
                width: 100%;
                border-radius: 12px;
            }
            #mid-section{
                padding: 0px;
            }
        }
    </style>
    <style>
        .image-uploader .uploaded {
            height: 200px;
        }
        .lightClass {
            background: #efefef;
            padding: 12px 10px;
            border-top-left-radius: 16px;
            border-bottom-left-radius: 16px;
            border-right: 1px solid #fff;
        }
        .lightMidClass {
            background: #efefef;
            padding: 12px 10px;
            border-right: 1px solid #fff;
        }
        .lightRightClass {
            border-right: 1px solid #fff;
            background: #efefef;
            padding: 12px 10px;
            border-top-right-radius: 16px;
            border-bottom-right-radius: 16px;
        }

        .SecionLightRightClass{
            background: #c0c0c0 !important;
        }

        .SecionLightMidClass{
            background: #c0c0c0 !important;
        }

        .SecionLightClass{
            background: #c0c0c0 !important;
        }
        .changeTextColor {
            color:#000 !important;
        }
        .sectionHeadingLight{
            background: #ffffff !important;
            color: #000 !important;
        }
        .blackClass {
            color: #000 !important;
        }
        .textAreaClass{
            background: #efefef !important;
            border: 1px solid #fff !important;
            color:#000 !important;
        }
        .custom-control-label::after {
            position: absolute;
            top: 0.25rem;
            left: -1.5rem;
            display: block;
            width: 1rem;
            height: 1rem;
            content: "";
            background: no-repeat 50% / 50% 50%;
        }
        .custom-checkbox .custom-control-input:checked ~ .custom-control-label::after {
            background-image: url(data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' width='8' height='8' viewBox='0 0 8 8'%3e%3cpath fill='%23fff' d='M6.564.75l-3.59 3.612-1.538-1.55L0 4.26l2.974 2.99L8 2.193z'/%3e%3c/svg%3e);
        }
        .custom-checkbox .custom-control-label::before {
            border-radius: 25px;
        }
        .custom-control-input:checked ~ .custom-control-label::before{
            border: 0px;
            background-color: #1dc116;
        }
        #imagesMedia {
            padding-bottom: 100px;
            height: 500px;
            /* overflow-y: scroll; */
        }
        #div1 > img {
            width:100px !important;
            height:60px;
        }
        .image-uploader .uploaded .uploaded-image img {
            object-fit: cover;
            position: absolute;
        }
        #setul {
            list-style:none;
            display: flex;
        }
        #setul li {
            margin-right: 12px;
            padding: 10px 5px;
            border: 1px solid #efefef;
            border-radius: 6px;
            cursor: pointer;
        }
        .activated {
            background:#29C4AC;
            color:#fff;
        }

        #cover-spin {
            position:fixed;
            width:100%;
            left:0;right:0;top:0;bottom:0;
            background-color: rgba(255,255,255,0.7);
            z-index:9999;
            display:none;
        }

        @-webkit-keyframes spin {
            from {-webkit-transform:rotate(0deg);}
            to {-webkit-transform:rotate(360deg);}
        }

        @keyframes spin {
            from {transform:rotate(0deg);}
            to {transform:rotate(360deg);}
        }

        #cover-spin::after {
            content:'';
            display:block;
            position:absolute;
            left:48%;top:40%;
            width:40px;height:40px;
            border-style:solid;
            border-color:black;
            border-top-color:transparent;
            border-width: 4px;
            border-radius:50%;
            -webkit-animation: spin .8s linear infinite;
            animation: spin .8s linear infinite;
        }
    </style>
    <link
        rel="stylesheet"
        href="https://cdn.jsdelivr.net/npm/simplebar@latest/dist/simplebar.css"
    />
    <script src="https://cdn.jsdelivr.net/npm/simplebar@latest/dist/simplebar.min.js"></script>
@endpush
@section('content')
    <br>
    <br>
    <div id="cover-spin"></div>
    <div class="container">
        <div class="row">
            <div class="types clearfix">

            <label class="same-bg"  socialtype="post">
                <input name="type" value="timeline" type="radio">
                <div id="inner-section" class="section-a">
                    <div id="camera-view">
                        <i class="fa fa-camera" aria-hidden="true"></i>
                    </div>

                    <div class="type" >
                        <div class="name">
                            <span class="hide-on-small-only">Add</span>
                            <span class="hide-on-medium-and-up">Post</span>
                        </div>
                        <div id="photo-video-area">
                            Photo / Video
                        </div>
                    </div>
                </div>
            </label>

            <label class="mid-bg"  socialtype="story">
                <input name="type" type="radio" value="story">
                <div id="inner-section" class="section-b">
                    <div id="plus-view">
                        <i class="fa fa-plus" style="margin:0px;" aria-hidden="true"></i>
                    </div>
                    <div class="type">
                        <div class="name">
                            <span class="hide-on-small-only">Add</span>
                            <span class="hide-on-medium-and-up">Story</span>
                        </div>
                        <div id="photo-video-area">
                            Photo / Video
                        </div>
                    </div>
                </div>
            </label>

            <label class="right-bg"  socialtype="album">
                <input name="type" type="radio" value="album">
                <div id="inner-section" class="section-c">
                    <div id="file-view">
                        <i class="fa fa-file-image-o" style="margin:0px;font-size:19px;padding:2px;" aria-hidden="true"></i>
                    </div>
                    <div class="type">
                        <div class="name">
                            <span class="hide-on-small-only">Add</span>
                            <span class="hide-on-medium-and-up">Album</span>
                        </div>

                        <div>
                            Photo / Video
                        </div>
                    </div>
                </div>
            </label>
        </div>
        </div>
        <input type="hidden" name="post_type" />
        <input type="hidden" name="social_type" />
        @include('social.newMainDesign')
    </div>
@endsection
@push('js')
    <script
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCIasatwSA8nyOeN8Cobzil6yO1jsT13rE&libraries=places&callback=initMap"
        defer>
    </script>
    <script>
        let orgLat = '';
        let orgLong = '';
        // This example requires the Places library. Include the libraries=places
        // parameter when you first load the API. For example:
        // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

        function initMap(id) {
            var card = document.getElementById('pac-card');
            if(id == undefined){
                var map = new google.maps.Map(document.getElementById('map'), {
                    zoom: 13
                });
                var input = document.getElementById('pac-input');
            } else {
                var map = new google.maps.Map(document.getElementById('map'+id), {
                    zoom: 13
                });
                var input = document.getElementById('pac-input-'+id);
            }
            var types = document.getElementById('type-selector');
            var strictBounds = document.getElementById('strict-bounds-selector');

            map.controls[google.maps.ControlPosition.TOP_RIGHT].push(card);

            var autocomplete = new google.maps.places.Autocomplete(input);

            // Bind the map's bounds (viewport) property to the autocomplete object,
            // so that the autocomplete requests use the current map bounds for the
            // bounds option in the request.
            autocomplete.bindTo('bounds', map);

            // Set the data fields to return when the user selects a place.
            autocomplete.setFields(
                ['address_components', 'geometry', 'icon', 'name']);

            var infowindow = new google.maps.InfoWindow();
            var infowindowContent = document.getElementById('infowindow-content');
            infowindow.setContent(infowindowContent);
            var marker = new google.maps.Marker({
                map: map,
                anchorPoint: new google.maps.Point(0, -29),
                draggable: true
            });

            marker.setVisible(false);
            marker.setVisible(true);


            autocomplete.addListener('place_changed', function () {

                infowindow.close();
                marker.setVisible(false);
                var place = autocomplete.getPlace();

                var lat = place.geometry.location.lat();
                var lng = place.geometry.location.lng();


                setSelectedLatLng(lat, lng)
                if (!place.geometry) {
                    // User entered the name of a Place that was not suggested and
                    // pressed the Enter key, or the Place Details request failed.
                    window.alert("No details available for input: '" + place.name + "'");
                    return;
                }
                if(id == undefined){
                    $('#row-map-enter-0').hide();
                    $('#row-map-show-0').show();
                    $('#row-label-0').hide();
                } else {
                    $('#row-map-enter-'+id).hide();
                    $('#row-map-show-'+id).show();
                    $('#row-label-'+id).hide();
                }

                // If the place has a geometry, then present it on a map.
                if (place.geometry.viewport) {
                    map.fitBounds(place.geometry.viewport);
                } else {
                    map.setCenter(place.geometry.location);
                    map.setZoom(17);  // Why 17? Because it looks good.
                }
                marker.setPosition(place.geometry.location);
                marker.setVisible(true);

                var address = '';
                if (place.address_components) {
                    address = [
                        (place.address_components[0] && place.address_components[0].short_name || ''),
                        (place.address_components[1] && place.address_components[1].short_name || ''),
                        (place.address_components[2] && place.address_components[2].short_name || '')
                    ].join(' ');
                }

                infowindowContent.children['place-icon'].src = place.icon;
                infowindowContent.children['place-name'].textContent = place.name;
                infowindowContent.children['place-address'].textContent = address;
                infowindow.open(map, marker);
            });


            google.maps.event.addListener(marker, 'dragend', function (marker) {

                var latLng = marker.latLng;
                currentLatitude = latLng.lat();
                currentLongitude = latLng.lng();


                setSelectedLatLng(currentLatitude, currentLongitude)
            });
        }

        function setSelectedLatLng(lat, lng) {
            $('#lat').val(lat);
            $('#lng').val(lng);
        }

    </script>
    <script>
        function scheduleTime(){
            $('#scheduleTime').toggle();
        }
        /*! Image Uploader  - 15/02/2021 3:21 PM Monday */
        (function ($) {

            $.fn.imageUploader = function (options) {

                // Default settings
                let defaults = {
                    preloaded: [],
                    imagesInputName: 'images',
                    preloadedInputName: 'preloaded',
                    label: 'Drag & Drop files here or click to browse'
                };

                // Get instance
                let plugin = this;

                // Set empty settings
                plugin.settings = {};

                // Plugin constructor
                plugin.init = function () {

                    // Define settings
                    plugin.settings = $.extend(plugin.settings, defaults, options);

                    // Run through the elements
                    plugin.each(function (i, wrapper) {

                        // Create the container
                        let $container = createContainer();

                        // Append the container to the wrapper
                        $(wrapper).append($container);

                        // Set some bindings
                        $container.on("dragover", fileDragHover.bind($container));
                        $container.on("dragleave", fileDragHover.bind($container));
                        $container.on("drop", fileSelectHandler.bind($container));

                        // If there are preloaded images
                        if (plugin.settings.preloaded.length) {

                            // Change style
                            $container.addClass('has-files');

                            // Get the upload images container
                            let $uploadedContainer = $container.find('.uploaded');

                            // Set preloaded images preview
                            for (let i = 0; i < plugin.settings.preloaded.length; i++) {
                                $uploadedContainer.append(createImg(plugin.settings.preloaded[i].src, plugin.settings.preloaded[i].id, true));
                            }

                        }

                    });

                };


                let dataTransfer = new DataTransfer();

                let createContainer = function () {

                    // Create the image uploader container
                    let $container = $('<div>', {class: 'image-uploader'}),

                        // Create the input type file and append it to the container
                        $input = $('<input>', {
                            type: 'file',
                            id: plugin.settings.imagesInputName + '-' + random(),
                            name: plugin.settings.imagesInputName + '[]',
                            multiple: ''
                        }).appendTo($container),

                        // Create the uploaded images container and append it to the container
                        $uploadedContainer = $('<div id="div1" ondrop="drop(event)" ondragover="allowDrop(event)" class="uploaded">', {class: 'uploaded'}).appendTo($container),

                        // Create the text container and append it to the container
                        $textContainer = $('<div>', {
                            class: 'upload-text'
                        }).appendTo($container),

                        // Create the icon and append it to the text container
                        $i = $('<i>', {class: ''}).appendTo($textContainer),

                        // Create the text and append it to the text container
                        $span = $('<span>', {text: plugin.settings.label}).appendTo($textContainer);


                    // Listen to container click and trigger input file click
                    $container.on('click', function (e) {
                        // Prevent browser default event and stop propagation
                        prevent(e);

                        // Trigger input click
                        $input.trigger('click');
                    });

                    // Stop propagation on input click
                    $input.on("click", function (e) {
                        e.stopPropagation();
                    });

                    // Listen to input files changed
                    $input.on('change', fileSelectHandler.bind($container));

                    return $container;
                };


                let prevent = function (e) {
                    // Prevent browser default event and stop propagation
                    e.preventDefault();
                    e.stopPropagation();
                };

                let createImg = function (src, id) {

                    // Create the upladed image container
                    let $container = $('<div>', {class: 'uploaded-image'}),

                        // Create the img tag
                        $img = $('<img>', {src: src}).appendTo($container),

                        // Create the delete button
                        $button = $('<button>', {class: 'delete-image'}).appendTo($container),

                        // Create the delete icon
                        $i = $('<i>', {class: 'fa fa-times'}).appendTo($button);

                    // If the images are preloaded
                    if (plugin.settings.preloaded.length) {

                        // Set a identifier
                        $container.attr('data-preloaded', true);

                        // Create the preloaded input and append it to the container
                        let $preloaded = $('<input>', {
                            type: 'hidden',
                            name: plugin.settings.preloadedInputName + '[]',
                            value: id
                        }).appendTo($container)

                    } else {

                        // Set the identifier
                        $container.attr('data-index', id);

                    }

                    // Stop propagation on click
                    $container.on("click", function (e) {
                        // Prevent browser default event and stop propagation
                        prevent(e);
                    });

                    // Set delete action
                    $button.on("click", function (e) {
                        // Prevent browser default event and stop propagation
                        prevent(e);

                        // If is not a preloaded image
                        if ($container.data('index')) {

                            // Get the image index
                            let index = parseInt($container.data('index'));

                            // Update other indexes
                            $container.find('.uploaded-image[data-index]').each(function (i, cont) {
                                if (i > index) {
                                    $(cont).attr('data-index', i - 1);
                                }
                            });

                            // Remove the file from input
                            dataTransfer.items.remove(index);
                        }

                        // Remove this image from the container
                        $container.remove();

                        // If there is no more uploaded files
                        if (!$container.find('.uploaded-image').length) {

                            // Remove the 'has-files' class
                            $container.removeClass('has-files');

                        }

                    });

                    return $container;
                };

                let fileDragHover = function (e) {

                    // Prevent browser default event and stop propagation
                    prevent(e);

                    // Change the container style
                    if (e.type === "dragover") {
                        $(this).addClass('drag-over');
                    } else {
                        $(this).removeClass('drag-over');
                    }
                };

                let fileSelectHandler = function (e) {

                    // Prevent browser default event and stop propagation
                    prevent(e);

                    // Get the jQuery element instance
                    let $container = $(this);

                    // Change the container style
                    $container.removeClass('drag-over');

                    // Get the files
                    let files = e.target.files || e.originalEvent.dataTransfer.files;

                    // Makes the upload
                    setPreview($container, files);
                };

                let setPreview = function ($container, files) {

                    // Add the 'has-files' class
                    $container.addClass('has-files');

                    // Get the upload images container
                    let $uploadedContainer = $container.find('.uploaded'),

                        // Get the files input
                        $input = $container.find('input[type="file"]');

                    // Run through the files
                    $(files).each(function (i, file) {
                        console.log('file');
                        console.log(i);
                        if(i == 0) {
                            var reader = new FileReader();
                            reader.onload = function(e) {
                                $('#displayImage').attr('src', e.target.result);
                                $('#displayImage2').attr('src', e.target.result);
                                setsocialpreviewimage(e.target.result);
                                console.log('file is loaded');

                            }
                            console.log(file);
                            console.log('this is new file preview');

                            reader.readAsDataURL(file);

                            let fileType = file.type;
                            let type = fileType.split('/');
                            console.log('fileType');
                            if(type[0] == 'video') {
                                $.ajaxSetup({
                                    headers: {
                                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                    }
                                });
                                let form = new FormData();
                                form.append('video',file);
                                $.ajax({
                                    url:'{{route('uploadVideo')}}',
                                    method:'POST',
                                    data:form,
                                    contentType:false,
                                    processData:false,
                                    cache:true,
                                    success: function (response) {
                                        console.log('response');
                                        console.log(response);
                                    },
                                    error: function (error) {
                                        console.log('error');
                                        console.log(error);
                                    }
                                })
                            } else {
                                let media = $('#post-a').hasClass('activeClass');
                                let story = $('#post-b').hasClass('activeClass');
                                let album = $('#post-c').hasClass('activeClass');

                                if(media) {
                                    reader.readAsDataURL(file);
                                    $('#displayImage').show();
                                    $('#displayImage').css('object-fit','contain');
                                    $('#displayImage').css('width','100%');
                                    $('#displayImage').css('height','100%');
                                } else if(story) {
                                    reader.readAsDataURL(file);
                                    $('#displayImage2').show();
                                    $('#displayImage2').css('object-fit','contain');
                                    $('.preview-content').css('height','100%');
                                    $('#displayImage2').css('width','100%');
                                    $('#displayImage2').css('height','100%');
                                } else {
                                    // reader.readAsDataURL(file);
                                }
                            }
                        }
                        // Add it to data transfer
                        dataTransfer.items.add(file);

                        // Set preview
                        $uploadedContainer.append(createImg(URL.createObjectURL(file), dataTransfer.items.length - 1));

                    });

                    // Update input files
                    $input.prop('files', dataTransfer.files);

                };

                // Generate a random id
                let random = function () {
                    return Date.now() + Math.floor((Math.random() * 100) + 1);
                };

                this.init();

                // Return the instance
                return this;
            };

        }(jQuery));
    </script>
    <script>

        function setsocialpreviewimage(image){
            $('.image-src-container').attr('src', image);
            $('.image-bg-container').css('background-image', `url(${image})`);
        }
        function selectedSets(){
            let account_set_id = $('select[name=account_set_id]').val();
            console.log('account_set_id');
            console.log(account_set_id);
            let route = '{{route('socialSetsData',':id')}}';
            route = route.replace(':id' , account_set_id);
            $('#setul').html();
            $('#setul').append();
            $.ajax({
               url:route,
               method:'GET',

               success: function (response) {
                   console.log('response');
                   console.log(response.data);
                   $('#setul').html('');
                   $('#setul').append('');
                   response.data.map((dt) => {
                       console.log('dt');
                       console.log(dt);
                       let html = '<li onclick="selectedSetName('+ dt.id +')" id="active_set_name_'+ dt.id +'">' + setName(dt.social_network) + '</li>';
                       $('#setul').append(html);
                   });
               },
               error: function (error) {
                   console.log('error');
                   console.log(error);
               }
            });
        }
        function setName(name) {
            if(name == 'link_den') {
                return 'linkedIn';
            } else {
                return name;
            }
        }
        function selectedSetName(id) {
            console.log('id');
            console.log(id);
            $('#active_set_name_'+id).toggleClass('activated');
        }
        function socialpreview(){
            var socialtype = $('select[name=social-switch-select]').val();
            var posttype = $('select[name=posttype]').val();
            console.log('socialtype');
            console.log(socialtype);
            console.log('posttype');
            console.log(posttype);
            $("input[name=social_type]").val(socialtype);
            $("input[name=post_type]").val(posttype);
            $('.socialpreview').hide();
            $(`.${socialtype}-${posttype}`).show();
        }
    </script>
    <script>
        function submitForm(){
            $('#customFormSubmit').submit();
        }
        function allowDrop(ev) {
            ev.preventDefault();
        }
        function drag(ev) {
            console.log('ev.target.id');
            let dataId = ev.target.getAttribute('data-id');
            sessionStorage.setItem('removeId',dataId);
            let ids = ev.target.id;
            let img = ev.target;
            sessionStorage.setItem('dimage' , $(ev.target).attr('src'));


            ev.dataTransfer.setData("files", ev.target);
        }
        function drop(ev) {
            let dataId = sessionStorage.getItem('removeId');
            $('.remove-'+dataId).remove();
            console.log('dimage');
            $dimage = sessionStorage.getItem('dimage');
            setsocialpreviewimage($dimage);
            // console.log($dimage);
            ev.preventDefault();
            console.log('listen object whats coming inside');
            let html = '<div class="uploaded-image" data-index="'+ dataId +'">' +
                `<img src="${$dimage}">` +
                '<button class="delete-image"><i class="fa fa-times"></i></button>' +
                ' </div>';

            $('#gallery').append(`<img src="${$dimage}" />`);

            // let get_images = document.getElementById('#drag1');
            // console.log('get_images');
            // console.log(get_images);
            var data = ev.dataTransfer.getData("text");
            ev.target.appendChild(document.getElementById(data));
            $('#div1').append(html);
        }
        function uploadImages(){
            $('#selectFiles').click();
        }
        function getImages(event) {
            console.log('event.target.value');
            let files = $('#selectFiles').prop('files');
            var form = new FormData();
            let arr = [];
            form.append('_token','{{csrf_token()}}');
            form.append('length' , files.length);
            for (var i = 0; i < files.length; i++) {
                form.append(`file`+i , files[i]);
                arr.push(files[i]);
            }
            console.log(form);
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url:'{{route('postMediaImages')}}',
                method:'POST',
                data:form,
                contentType:false,
                cache:false,
                processData:false,

                success: function (response) {
                    console.log('response');
                    console.log(response);
                    $('#customImages').html('');
                    $('#customImages').append('');
                    response.data.map((d) => {
                        let html = ' <div class="col-md-6" id="customImages">' +
                            '<i class="fa fa-trash" style="color:red;" aria-hidden="true"></i>' +
                            '<img src="' + d.file + '" style="width:100%;" />' +
                            '</div>';
                        $('#customImages').append(html);
                    });
                    window.location.reload();
                },
                error: function (error) {
                    console.log('error');
                    console.log(error);
                }

            });
        }
        function sectionSelected(val) {
            if(val == 'a') {
                $('#input1').attr('placeholder','write a caption ...');
                $('.section-selected-b').removeClass('selectedActivated');
                $('.section-selected-a').addClass('selectedActivated');
            } else {
                $('#input1').attr('placeholder','write a first comment ...');
                $('.section-selected-a').removeClass('selectedActivated');
                $('.section-selected-b').addClass('selectedActivated');
            }
        }
        function getTextData() {
            let text = $('#postCaptionText').val();
            let desc = $('#input1').val();
            console.log('text');
            let html = text+' '+desc;
            console.log(html);
            $('#captionPT0').html(html);
        }
        function removeWords() {
            let text = $('#postCaptionText').val();
            let desc = $('#input1').val();
            console.log('text');
            let html = text+' '+desc;
            console.log(html);
            $('#captionPT0').html(html);
        }
        function postToSocialMedia() {
            @if(count($allSocialSets) == 0)
                toastr.error('Please Connect Or Add Your Instagram Account ......!');
                return;
            @endif
            let $form = $('#media-section-card');
            let $inputImages = $form.find('input[name^="images"]');
            if (!$inputImages.length) {
                $inputImages = $form.find('input[name^="photos"]');
                // console.log($inputImages);
                // social preview load
                // SetSocialPreview
                $srcfile = $form.find('input[name^="photos"]')[0];


                // reader.addEventListener("load", function () {
                //     // convert image file to base64 string
                //     // preview.src = reader.result;
                //     $('.image-src-container').attr('src', reader.result);

                // }, false);

                // if (file) {
                //     reader.readAsDataURL($srcfile);
                // }
                // console.log('image found');
            }
            let schedule = $('#schedule_at').val();
            let post_type = $("input[name=post_type]").val();
            let social_type = $("input[name=social_type]").val();
            let url = '{{route('postSendSocialMedia')}}';
            let formData = new FormData();
            let files = GetPostFiles();
            formData.append('_token','{{csrf_token()}}');
            formData.append('description',$('#input1').val());
            formData.append('location',$('#pac-input').val());
            formData.append('lat', $('#lat').val());
            formData.append('lng', $('#lng').val());
            formData.append('account_set_id', $('#account_set_id').val());
            for (var i = 0; i < files.length; i++) {
                console.log(files[i]);
                formData.append('images-' + i, files[i]);
                formData.append('files-' + i, files[i]);
            }
            formData.append('file_type', 'image');
            formData.append('counter',files.length);
            formData.append('type',post_type);
            formData.append('schedule_at',schedule);
            formData.append('social_type',social_type);

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $('#cover-spin').show(0);
            $.ajax({
                url: url,
                method: 'POST',
                data: formData,
                contentType: false,
                processData: false,
                cache: false,
                success: function (response) {
                    console.log('response back response');
                    console.log(response);
                    if (response.status == true) {
                        $('#cover-spin').hide(0);
                        toastr.success('Post Posted Successfully ... !');
                        setTimeout(() => {
                            // window.location.reload();
                        },1000);
                    } else {
                        toastr.success(response.data);
                        setTimeout(() => {
                            // window.location.reload();
                        },1000);
                    }
                },
                error: function (error) {
                    $('#cover-spin').hide(0);
                    console.log('error');
                    console.log(error);
                    toastr.error('Something Went Wrong ... !');
                }
            });
        }
        function checkFunction(val = null){
            if(val == 1) {
                $('.same-bg').addClass('lightClass');
                // $('.preview-holder').addClass('dark-preview-holder');

                $('.mid-bg').addClass('lightMidClass');
                $('.right-bg').addClass('lightRightClass');
                $('.section-a').addClass('SecionLightClass');
                $('.section-b').addClass('SecionLightMidClass');
                $('.section-c').addClass('SecionLightRightClass');
                $('.hide-on-small-only').addClass('changeTextColor');
                $('.hide-on-medium-and-up').addClass('changeTextColor');
                $('.section-heading').addClass('sectionHeadingLight');
                $('.section-body').addClass('sectionHeadingLight');
                $('.whiteClass').addClass('blackClass');
                $('#comment-area').addClass('blackClass');
                $('#caption-area').addClass('blackClass');
                $('#input1').addClass('textAreaClass');
                $('#start_time').addClass('textAreaClass');
                $('#end_time').addClass('textAreaClass');
                $('.pac-target-input').addClass('textAreaClass');
            } else {
                let toggle = $('#modeChecker');
                console.log('toggle');
                if (toggle[0].className == 'fa fa-lightbulb-o') {
                    console.log(toggle[0].className);
                    $('.same-bg').addClass('lightClass');
                    $('.preview-holder').removeClass('dark-preview-holder');

                    $('.mid-bg').addClass('lightMidClass');
                    $('.right-bg').addClass('lightRightClass');
                    $('.section-a').addClass('SecionLightClass');
                    $('.section-b').addClass('SecionLightMidClass');
                    $('.section-c').addClass('SecionLightRightClass');
                    $('.hide-on-small-only').addClass('changeTextColor');
                    $('.hide-on-medium-and-up').addClass('changeTextColor');
                    $('.section-heading').addClass('sectionHeadingLight');
                    $('.section-body').addClass('sectionHeadingLight');
                    $('.whiteClass').addClass('blackClass');
                    $('#comment-area').addClass('blackClass');
                    $('#caption-area').addClass('blackClass');
                    $('#input1').addClass('textAreaClass');
                    $('#start_time').addClass('textAreaClass');
                    $('#end_time').addClass('textAreaClass');
                    $('.pac-target-input').addClass('textAreaClass');
                } else {
                    console.log(toggle[0].className);
                    $('.preview-holder').addClass('dark-preview-holder');

                    $('.same-bg').removeClass('lightClass');
                    $('.mid-bg').removeClass('lightMidClass');
                    $('.right-bg').removeClass('lightRightClass');
                    $('.section-a').removeClass('SecionLightClass');
                    $('.section-b').removeClass('SecionLightMidClass');
                    $('.section-c').removeClass('SecionLightRightClass');
                    $('.hide-on-small-only').removeClass('changeTextColor');
                    $('.hide-on-medium-and-up').removeClass('changeTextColor');
                    $('.section-heading').removeClass('sectionHeadingLight');
                    $('.section-body').removeClass('sectionHeadingLight');
                    $('.whiteClass').removeClass('blackClass');
                    $('#comment-area').removeClass('blackClass');
                    $('#caption-area').removeClass('blackClass');
                    $('#input1').removeClass('textAreaClass');
                    $('#start_time').removeClass('textAreaClass');
                    $('#end_time').removeClass('textAreaClass');
                    $('.pac-target-input').removeClass('textAreaClass');
                }
            }
        }
        $(document).ready(function () {
            socialpreview();
            checkFunction(1);
            changeTabs('a');
            sectionSelected('a');
            uploadDragDropFiles();
            uploadDragDropFiles2();



            $('label[socialtype]').click(function(){
                var socialtype = $(this).attr('socialtype');
                // console.log(socialtype);
                $('select[name=posttype]').val(socialtype);
                socialpreview();
            });


        });
        function uploadDragDropFiles() {
            var maxFilesUpload;
            let media = $('#post-a').hasClass('activeClass');
            let story = $('#post-b').hasClass('activeClass');
            let album = $('#post-c').hasClass('activeClass');
            if(media) {
                maxFilesUpload = 1;
            } else if(story) {
                maxFilesUpload = 3;
            } else {
                maxFilesUpload = 6;
            }
            $('.input-images-2').imageUploader({
                // preloaded: preloaded,
                extensions: ['.jpg', '.jpeg', '.png', '.gif', '.svg'],
                mimes: ['image/jpeg', 'image/png', 'image/gif', 'image/svg+xml'],
                imagesInputName: 'photos',
                preloadedInputName: 'old',
                maxSize: 2 * 1024 * 1024,
                maxFiles: maxFilesUpload
            });
        }
        function uploadDragDropFiles2() {
            var maxFilesUpload;
            let media = $('#post-a').hasClass('activeClass');
            let story = $('#post-b').hasClass('activeClass');
            let album = $('#post-c').hasClass('activeClass');
            if(media) {
                maxFilesUpload = 1;
            } else if(story) {
                maxFilesUpload = 3;
            } else {
                maxFilesUpload = 6;
            }
            $('.input-images-3').imageUploader({
                // preloaded: preloaded,
                extensions: ['.jpg', '.jpeg', '.png', '.gif', '.svg'],
                mimes: ['image/jpeg', 'image/png', 'image/gif', 'image/svg+xml'],
                imagesInputName: 'photos',
                preloadedInputName: 'old',
                maxSize: 2 * 1024 * 1024,
                maxFiles: maxFilesUpload
            });
        }
        function allCheckbox(val) {
            let allAccounts = $('#account-'+val).prop('checked');
            let Array = [{id:'a'},{id:'b'},{id:'c'},{id:'d'},{id:'e'}];
            Array.map((data) => {
                if(!allAccounts) {
                    $('#account-' + data.id).prop('checked', false);
                } else {
                    $('#account-' + data.id).prop('checked', true);
                }
            });
            $('#selectedAccount').val(0);
        }
        $(document).ready(function () {
            $('#socialImage').attr('src','');
            changeTabs('a');
        });
        function updateCheckBox(val){
            let Array = [{id:'a'},{id:'b'},{id:'c'},{id:'d'},{id:'e'}];
            let match = '';
            Array.map((data) => {
                $('#account-' + data.id).prop('checked', false);
                if (data.id == val) {
                    $('#account-0').prop('checked',false);
                    match = data.id;
                }
            });
            let social , src;
            if(val == 'a') {
                social = 'twitter';
                src = 'https://openvisualfx.com/wp-content/uploads/2019/10/pnglot.com-twitter-bird-logo-png-139932.png';
            }
            if(val == 'e') {
                social = 'Youtube';
                src = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAQ0AAAC7CAMAAABIKDvmAAAAkFBMVEX/AAD/////OTn/5OT/9vb/ysr/dnb/19f/0tL/ior/nJz/kpL/39//tbX/TEz/Vlb/wcH/paX/oKD/8PD/6en/u7v/fHz/YGD/jo7/qqr/2dn/xsb/SEj/NDT/hYX/9PT/HBz/sLD/Kir/bGz/gID/EBD/XV3/QkL/Ly//UVH/cnL/IyP/ZWX/Pz//FRX/ICClOSYmAAAG6klEQVR4nO2da3eqOhBAMwqCgg8evq2i0ndP7///dzcItqiA0OrM0LC/nrVOJrtIhmSSCEBGi/H8diab5N817LgOiJv9T55vG6apj8b9uWt112E46eynw8Hzwy54FL8i2O0eVsPh034SrtfrmeXO+yPdNBe2791Y2k9teLLr46jb4X6w+2Vvf8vjrvU6fQu7PaevS0cekg3fWDqzyVOLuPclCFZva2urG+1qD08pG565tSbP1D38IZ+rTndr2DexsdGtJ+r+3Ib3D0u/9iMqtOG7dX0g8ghC/Yc2+n9NRcJkUd2GQx30HVnlPSA5Nvr8h41f8WyUt7H4Rx3t/ZmUtTGjjhSFwCxjw29Rx4mFe93GkjpGRPbXbMypI0RlWmyjRx0fMk9FNizq6ND5yLfhUsdGwCzPxpg6MhJG2TZs6riI8DJtvFOHRcQ0y0aHOioyRpc2TOqY6Hi/tPFAHRMhzrkNNceThM9zGy/UEZEyPrWhU8dDy+DUxpQ6HmL8ExvU0VDjpG0o/kMRYpW2ocbcXxFayobKyUaM/m1Do46FHuvbhsJZ+ZHVtw3V5v+y+Laxpw6FAfaXjU/qUBgwOtpoXqIieY1GNhbUkXBgerSh9Nf8F0cb6q2iZKElNpohJWKR2NhRB8KCUWKDOg4e9GIbHnUcPAhjGwZ1HDwYxDZUql8pIrbxl2shqxDb6FKHwQT/YEPdBdhTjIONFXUYTFgebPzxsuHSOAcbeO29vOK1VZ1ZZANxdqMFW7zGKhNGNtp47bUAtDe85ioyjGwgpqKtwwQ914/EXWQDcfmgFa/vcZ2ij2wgznwlNsDjuVUusoGYmB9tAIw4DuuetIE4D/htA2CN12xZbGkDMay0DbDZbSI0pY0JXnMnNoBd8qFLGx94zZ3ZgA2v5KMvbSB+tJ3bkMkHp1pER9pATIYubbBKPixpA7G5LBvgs0k+1gxsyOQjQIyhgAkLG1wmIz+Y2AB7gBhGHisQmMUb+TZYJB87ED5ic0U2YEM/Ww0CcbKn2IZMPqjLVkFg7u27YoN8CyoIzDKnqzbAJ90MAQKzcva6DYBlgBjQGRo7G5RbAHyGNuiSj7bArFcoaQOgjxhUCltglkeWtgEaSfJhCMw/Q3kbNMmHKTAz4io2KJIPU2AeTVPNBnjYyYcuMOucKtqQyQfu7oiRwHweK9tATj7G3G1AGzH52ArMWdqf2MBMPuY1sAEa1gKYIzC3Y/zQBoCBk3y49bCBVOLbq4sN2CCskNbHBoB+9+TDqpGN+ycf9bIB7fsuoVt1GGHT3LXGocs+Fz3Bv29i2q3Vs3HvX3WdbNx/TKmPDYx8Y1YXGyi56Kweb1Gk7xSrDjbQvmHrYANvfqPHe14UcOe+XM5z5hGo86IObxvIc+ZbxqtL+OspfbYrjxRrbSOmq9I067BL6QMP7mv0psA8WpR7/YbZ1PakMBjaoKv7stnZoKwJbDf1oin8ppY4hdbUmado9iCkAbFBbI35/hTR7F1K8cjEBo99bS8sbHDZ8/jKwAaf/bB7aSPAa475XulQ2kC8GZf5PvqutDHEa475GQs9aQMx6WF+/sZc2gjxmmN+NstI2kAc65mf2xOdVIP4Umd+ptNC2kD89R5t+Ihv7gr40gbiNHENzoLDPhnPZHvzamQDcfIrOkOS7zUDQWQD8XD3FusL3gfN2bMpOsg2eNM92OD04USJe7Ch+v2fR+ITvBE/VFgTn+7eXDQUE5/8T1QewA442GhueTzwGNvAXIllzBCae5e+WTc2UriJDdb5Mhp6YoPhpBwBdmKjuXgpAhIbyt+/HrE72sAs72FL52ijGVREPKTENpp7qKLFlKMNHsUktHhfNprLg8UnfNloXqPxSxSSW+mVZ5uy0Ux/2SkbTf4FKRvK/1QmJzboq5pp0U9sKH6bcgAnNjArAxnintlQ+z0KZzaUngCbX9hQOB99gAsbCi+5mRk2oEUdFRFryLKh6G8lXcSasgGYlwDwwc+xwa7wGwMD8mxQ70clYAn5NpRbWhlBkQ3FijlMKLahUjVH0D7v/IUN8FRZT3jSLvp+aUOVrNTJ6HmWDdTz14gY+lkdz7QBMGa7UeAm7EbZ3c6xIQeXv1ti/DDO63SuDQCdzSbmmzI9H1bL2ZDDi8tuX+IvWTleUYcLbUg2S+uvzIpNLX1zpbfXbBxo6053+h91b37MamL1jcJnopKNBG+hb3vdyZD6eJ1SPLaewpmjG5kj6S1spND8hbmUasLJx2uLxWj8+DLYh13LnY9Nwy71INzOxiUb316Y5rK/ddyeNeuuw850KBk8P+yCm/Q22LWeB8PpvhPKTvfc+Xasm6axsG3/2tugPDezUQnN89t5+J4WQxDX/6+3UVUbGgLqAAAAAElFTkSuQmCC';
            }
            if(val == 'd') {
                social = 'LinkedIn';
                src = 'https://icons-for-free.com/iconfiles/png/512/linked+linkedin+logo+social+icon-1320191784782940875.png';
            }
            if(val == 'b') {
                social = 'Facebook';
                src = 'https://image.flaticon.com/icons/png/512/124/124010.png';
            }
            if(val == 'c') {
                social = 'Instagram';
                src = 'https://www.brandchannel.com/wp-content/uploads/2016/05/instagram-new-logo-may-2016.jpg';
            }
            $('#socialImage').attr('src',src);
            $('#selectedAccount').val(social);
            $('#account-' + match).prop('checked', true);
        }
        function changeImage(){
            $('#styleImage').hide();
            $('#styleImage2').show();
        }
        function changeImage2(){
            $('#styleImage').show();
            $('#styleImage2').hide();
        }
        function advancedOptions(){
            let checbox = $('#advanced_option').prop('checked');
            if(checbox) {
                $('#showAdvacnedOptions').toggle();
            } else {
                $('#pac-input').val('');
                $('#input2').val('');
                $('#showAdvacnedOptions').toggle();
            }
        }
        function advancedOptions12(){
            let checbox = $('#advanced_option12').prop('checked');
            if(checbox) {
                $('#showAdvacnedOptions12').toggle();
            } else {
                $('#pac-input1').val('');
                $('#input212').val('');
                $('#showAdvacnedOptions12').toggle();
            }
        }
        function changeTabs(val){
            if(val == 'a') {
                $('.section-a').css('background','#3C3F4E');
                $('#file-view').removeClass('addClassActiveClass');
                $('#file-view').css('background','rgb(33, 36, 43)');
                $('.fa-plus').removeClass('addClassActiveClass');
                $('#plus-view').css('background','rgb(33, 36, 43)');
                $('.fa-camera').addClass('addClassActiveClass');
                $('#camera-view').css('background','rgb(56, 88, 95)');
                $('#postType').val('media');
                $('#postMedia').show();
                $('#storyMedia').hide();
            }
            if(val == 'b') {
                $('.section-a').css('background','#1B1C21');
                $('#file-view').removeClass('addClassActiveClass');
                $('.fa-camera').removeClass('addClassActiveClass');
                $('#camera-view').css('background','#21242B');
                $('.fa-plus').addClass('addClassActiveClass');
                $('#plus-view').css('background','#38585F');
                $('#postType').val('story');
                $('#postMedia').hide();
                $('#storyMedia').show();
            }
            if(val == 'c') {
                $('.section-a').css('background','#1B1C21');
                $('.fa-camera').removeClass('addClassActiveClass');
                $('#camera-view').css('background','#21242B');
                $('.fa-plus').removeClass('addClassActiveClass');
                $('#plus-view').css('background','rgb(33, 36, 43)');
                $('#file-view').addClass('addClassActiveClass');
                $('#postType').val('album');
                $('#postMedia').show();
                $('#storyMedia').hide();
            }
        }
    </script>
@endpush
