<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin;
use App\Models\Profile;
use App\Models\User;
use App\Traits\General;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DateTimeZone;
use Illuminate\Support\Facades\Hash;


class AccountProfileController extends Controller
{
    use General;

    public function accountProfile() {
        if(Auth::guard('admin')->check()) {
            $profile = Auth::guard('admin')->user()->with('profile')->first();
        } else {
            $profile = Auth::user()->with('profile')->first();
        }
        $tzlist = DateTimeZone::listIdentifiers(DateTimeZone::ALL);
        return view('admin.account.profile',[
            'profile' => $profile,
            'tzlist' => $tzlist
        ]);
    }

    public function updateUserImage(Request $request){
        if($request->hasFile('image')) {
            if(Auth::guard('admin')->user()) {
                $type = Profile::TYPE_SUPER_ADMIN;
                $user_id = Auth::guard('admin')->user()->id;
            } else {
                $type = Profile::TYPE_NORMAL_ADMIN;
                $user_id = Auth::user()->id;
            }
            $image = $this->uploadFile('uploads/users',$request->image);
            Profile::where('user_id','=',$user_id)->where('type','=',$type)->update([
                'image' => $image
            ]);
            return collect([
                'status' => true,
                'message' => 'Image Upload Successfully'
            ]);
        } else {
            return collect([
                'status' => false,
                'message' => 'some thing went wrong'
            ]);
        }
    }


    public function accountProfileUpdate(Request $request,$id)
    {
        if(Auth::guard('admin')->check()) {
            $user_id = Auth::guard('admin')->user()->id;
            $type = Profile::TYPE_SUPER_ADMIN;
            Admin::where('id','=',Auth::guard('admin')->user()->id)->update([
                'name' => $request->name,
                'email' => $request->email,
                'password' => isset($request->password) ? Hash::make($request->password) : Auth::guard('admin')->user()->password
            ]);
        } else {
            $user_id = Auth::user()->id;
            $type = Profile::TYPE_SUPER_ADMIN;
            User::where('id','=',Auth::user()->id)->update([
                'name' => $request->name,
                'email' => $request->email,
                'password' => isset($request->password) ? Hash::make($request->password) : Auth::user()->password
            ]);
        }
        Profile::updateUserProfile($user_id,$request->all(),$type);
        return redirect()->back()->withStatus(__('Account Profile Updated Successfully.'));
    }



}
