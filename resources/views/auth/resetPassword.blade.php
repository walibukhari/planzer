
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>SocialDay - Reset Password</title>
    <link rel="shortcut icon" href="/favicon.png" type="image/x-icon">
    <!-- Bootstrap , fonts & icons  -->
    <link rel="stylesheet" href="https://uxtheme.net/demos/finity/css/bootstrap.css">
    <link rel="stylesheet" href="https://uxtheme.net/demos/finity/fonts/icon-font/css/style.css">
    <link rel="stylesheet" href="https://uxtheme.net/demos/finity/fonts/typography-font/typo.css">
    <link rel="stylesheet" href="https://uxtheme.net/demos/finity/fonts/fontawesome-5/css/all.css">
    <!-- Plugin'stylesheets  -->
    <link rel="stylesheet" href="https://uxtheme.net/demos/finity/plugins/aos/aos.min.css">
    <link rel="stylesheet" href="https://uxtheme.net/demos/finity/plugins/fancybox/jquery.fancybox.min.css">
    <link rel="stylesheet" href="https://uxtheme.net/demos/finity/plugins/nice-select/nice-select.min.css">
    <link rel="stylesheet" href="https://uxtheme.net/demos/finity/plugins/slick/slick.min.css">
    <!-- Vendor stylesheets  -->
    <link rel="stylesheet" href="https://uxtheme.net/demos/finity/css/main.css">
    <!-- Custom stylesheet -->
</head>

<body data-theme="light">
<div class="site-wrapper overflow-hidden ">
    <div id="loading">
        <img src="https://uxtheme.net/demos/finity/image/preloader.gif" alt="">
    </div>
    <!-- Header Area -->
    <div class="position-absolute w-100">
        <div class="container-fluid px-0">
            <div class="row no-gutters">
                <div class="col-xl-5 col-lg-6">
                    <div class="brand-logo pt-12 max-w-413 mx-auto  text-center text-lg-left"  style="z-index:99999;">
                        <a class="" href="{{route('plus')}}" style="z-index:999;">
                            <img style="width:225px;position:relative;top:15px;" src="{{asset('/SocialdayLogoColor1.png')}}" alt="" class="light-version-logo">
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Header Area -->
    <div class="min-height-100vh d-flex align-items-center bg-default-3">
        <div class="container-fluid h-100 px-0">
            <div class="row no-gutters align-items-center justify-content-center h-100">
                <div class="col-xl-5 col-lg-6 col-md-10">
                    <div class="pt-26 pt-md-17 pt-lg-18 pb-md-4 pb-lg-10 max-w-413 mx-auto" data-aos="fade-up" data-aos-duration="500" data-aos-once="true">
                        <div class="mb-10 text-center text-lg-left">
                            <h2 class="mb-1 font-size-10 letter-spacing-n83">Reset Password</h2>
                            <p class="text-bali-gray font-size-7 mb-0">Enter your email address below</p>
                            @if(session()->has('error'))
                                <div class="row">
                                    <div class="col-12">
                                        <div class="alert alert-danger">
                                            {{session()->get('error')}}
                                        </div>
                                    </div>
                                </div>
                            @endif
                            @if(session()->has('success'))
                                <div class="row">
                                    <div class="col-12">
                                        <div class="alert alert-success">
                                            {{session()->get('success')}}
                                        </div>
                                    </div>
                                </div>
                            @endif
                        </div>
                        <form method="post" action="{{ route('postResetPassword') }}" data-aos="fade-up" data-aos-duration="800" data-aos-once="true">
                            @csrf
                            <!-- Email -->
                            <div class="form-group mb-6 position-relative">
                                <input type="email" name="email" class="form-control @error('email') is-invalid @enderror form-control-lg bg-white rounded-4 text-dark-cloud text-placeholder-bali-gray pl-7 font-size-5" placeholder="Email Address" id="email">
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="button">
                                <button type="submit" class="btn btn-blue-3  w-100 rounded-4">Reset Password</button>
                                <p class="font-size-5 mt-8 text-bali-gray" data-aos="fade-up" data-aos-duration="1100" data-aos-once="true">
                                    <a href="{{route('login')}}" class="text-blue-3 ">Sign In</a>
                                </p>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- Right Image -->
                <div class="col-xl-7 col-lg-6 col-md-10 min-height-lg-100vh">
                    <div class="bg-images min-height-100vh d-none d-lg-block" style="background-image: url(/images/Login.png);"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Vendor Scripts -->
<script src="https://uxtheme.net/demos/finity/js/vendor.min.js"></script>
<!-- Plugin's Scripts -->
<script src="https://uxtheme.net/demos/finity/plugins/fancybox/jquery.fancybox.min.js"></script>
<script src="https://uxtheme.net/demos/finity/plugins/nice-select/jquery.nice-select.min.js"></script>
<script src="https://uxtheme.net/demos/finity/plugins/aos/aos.min.js"></script>
<script src="https://uxtheme.net/demos/finity/plugins/slick/slick.min.js"></script>
<script src="https://uxtheme.net/demos/finity/plugins/counter-up/jquery.counterup.min.js"></script>
<script src="https://uxtheme.net/demos/finity/plugins/counter-up/jquery.waypoints.min.js"></script>
<!-- Activation Script -->
<script src="https://uxtheme.net/demos/finity/js/custom.js"></script>
</body>

</html>
