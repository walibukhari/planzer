<?php

namespace Database\Seeders;

use App\Models\Admin;
use App\Models\Profile;
use App\Models\User;
use Illuminate\Database\Seeder;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Admin::truncate();
        $admin = Admin::create([
            'name' => 'super_admin',
            'email' => 'super_admin@gmail.com',
            'password' => bcrypt('password')
        ]);
        Profile::createUserProfile($admin,'',Profile::TYPE_SUPER_ADMIN);
    }
}
