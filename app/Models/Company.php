<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    use HasFactory;

    protected $fillable = [
        'company_name',
        'company_type',
        'company_address',
        'postal_code',
        'city',
        'state',
        'country',
        'email',
        'phone_number',
        'website'
    ];

    public static function createCompany($request){
        return self::create([
            'company_name' => $request['company_name'],
            'company_type' => $request['company_type'],
            'company_address' => $request['company_address'],
            'postal_code' => $request['postal_code'],
            'city' => $request['city'],
            'state' => $request['state'],
            'country' => $request['country'],
            'email' => $request['email'],
            'phone_number' => $request['phone_number'],
            'website' => $request['website']
        ]);
    }

    public static function updateCompany($id , $request){
        return self::where('id','=',$id)->update([
            'company_name' => $request['company_name'],
            'company_type' => $request['company_type'],
            'company_address' => $request['company_address'],
            'postal_code' => $request['postal_code'],
            'city' => $request['city'],
            'state' => $request['state'],
            'country' => $request['country'],
            'email' => $request['email'],
            'phone_number' => $request['phone_number'],
            'website' => $request['website']
        ]);
    }
}
