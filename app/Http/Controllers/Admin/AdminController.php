<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Testimonial;
use App\Traits\General;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Profile;
use DateTimeZone;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AdminController extends Controller
{
    use General;

    public function testimonial() {
        return view('admin.testimonial.create');
    }

    public function addTestimonial(Request  $request) {
        if($request->hasFile('file')) {
            $file = $request->file('file');
            $name = $file->getClientOriginalName();
            $path = 'uploads/';
            $saveFile = $path.''.$name;
            $file->move($path , $saveFile);
        }
        Testimonial::create([
            'name'  => $request['name'],
            'title'  => $request['title'],
            'image'  => $saveFile,
            'description' => $request['description'],
        ]);
        return back()->with('success_message','Testimonial Added Successfully');
    }

    public function add_new()
    {
        $data['tzlist'] = DateTimeZone::listIdentifiers(DateTimeZone::ALL);
        return view('user.new_user',$data);
    }
    public function save_user(Request $request)
    {
        $type = Profile::TYPE_NORMAL_ADMIN;
        if(Auth::guard('admin')->check()) {
            $type = Profile::TYPE_SUPER_ADMIN;
        }
        $validated = $request->validate([
            'email' => 'required|unique:users|max:255',
        ]);
        $user = User::createUser($request->all());
        Profile::createUserProfile($user,$request,$type);
        return back()->withStatus(__('User registered successfully.'));
    }

    public function all_users()
    {
        $data['counter'] = 1;
        $data['user']    = User::all()->where('type',0);
        return view('user.all_user',$data);
    }

    public function admins()
    {
        return view('auth.admin.login');
    }
    public function edit_user($id)
    {
        $data['user']    = User::find($id);
        $data['profile'] = Profile::where('user_id',$id)->first();
        $data['tzlist'] = DateTimeZone::listIdentifiers(DateTimeZone::ALL);
        return view('user.edit_user',$data);
    }

    public function delete_user($id)
    {
        User::where('id',$id)->delete();
        return back()->withStatus(__('User Deleted'));
    }
}
