<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSchedulePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('schedule_posts', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id')->nullable();
            $table->longText('minutes')->nullable();
            $table->longText('description')->nullable();
            $table->string('location')->nullable();
            $table->string('lat')->nullable();
            $table->string('lng')->nullable();
            $table->integer('account_set_id')->nullable();
            $table->string('file_type')->nullable();
            $table->integer('counter')->nullable();
            $table->string('type')->nullable();
            $table->string('schedule_at')->nullable();
            $table->string('social_type')->nullable();
            $table->longText('images')->nullable();
            $table->longText('files')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('schedule_posts');
    }
}
