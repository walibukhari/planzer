@extends('layouts.simple.master')
@section('title', 'Update Company')

@section('css')
@endsection

@section('style')
@endsection

@section('breadcrumb-title')
    <h3>Update Company</h3>
@endsection

@section('breadcrumb-items')
    <li class="breadcrumb-item">Company</li>
    <li class="breadcrumb-item active">Update Company</li>
@endsection

@section('content')
    <br>
    <div>
        @if (session('success_message'))
            <div class="alert alert-success" role="alert">
                {{ session('success_message') }}
            </div>
        @endif
        @if (Session::has('error'))
            <div class="alert alert-danger alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>{!! Session('error') !!}</strong>
            </div>
        @endif
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">

                    <div class="card-body">
                        <div class="row" style="display:flex; align-items:center; justify-content: space-between;">
                            <h3>Create Company</h3>
                            <a href="{{route('companies.list')}}" class="btn btn-primary">All Companies</a>
                        </div>
                        <br>
                        <form class="needs-validation" novalidate="" method="POST" action="{{route('update.company',[$company->id])}}">
                            @csrf
                            <div class="row">
                                <div class="col-md-6 mb-3">
                                    <label for="first_name"> Company name</label>
                                    <input  class="form-control"  name="company_name" value="{{$company->company_name}}" type="text" placeholder="Company Name" required="">
                                </div>
                                <div class="col-md-6 mb-3">
                                    <label for="last_name">Company Type</label>
                                    <input  class="form-control" value="{{$company->company_type}}" id="" name="company_type" type="text" placeholder="Company Type" required="">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 mb-3">
                                    <label for="validationCustomUsername">Email</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend"><span class="input-group-text" id="inputGroupPrepend">@</span></div>
                                        <input  id="email" type="email" class="form-control" placeholder="Company Email" name="email" value="{{$company->email}}" required autocomplete="email">
                                    </div>
                                </div>
                                <div class="col-md-6 mb-3">
                                    <label for="validationCustom01">Company Address</label>
                                    <input  id="name" type="text" class="form-control" name="company_address" placeholder="Company Address" value="{{$company->company_address}}" required autofocus>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 mb-3">
                                    <label for="password">Postal code</label>
                                    <input  id="postal_code" type="text" class="form-control" placeholder="Postal Code" value="{{$company->postal_code}}" name="postal_code" required>
                                </div>

                                <div class="col-md-6 mb-3">
                                    <label for="address">City</label>
                                    <input  class="form-control" value="{{$company->city}}"  id="" name="city" type="text" placeholder="City" required="">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 mb-3">
                                    <label for="city">State</label>
                                    <input  class="form-control" name="state" id="" type="text" placeholder="State" value="{{$company->state}}" required="">
                                </div>

                                <div class="col-md-6 mb-3">
                                    <label for="postal_code">Country</label>
                                    <input  class="form-control" name="country" id="" type="text" placeholder="Country" value="{{$company->country}}" required="">
                                </div>

                            </div>

                            <div class="row">
                                <div class="col-md-6 mb-3">
                                    <label for="state">Phone number</label>
                                    <input  class="form-control" value="{{$company->phone_number}}" name="phone_number" id="" type="text" placeholder="Phone Number" required="">
                                </div>
                                <div class="col-md-6 mb-3">
                                    <label for="country">Website</label>
                                    <input  class="form-control" value="{{$company->website}}" id="" name="website" type="text" placeholder="Website Url" required="">
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <button class="btn btn-primary">Save</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="{{asset('assets/js/form-validation-custom.js')}}"></script>
@endsection
