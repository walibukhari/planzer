<div class="body ">
    {{-- instagram post --}}
    <div class="socialpreview instagram-post">
        <div class="post-header">

            <div class="left">
                <div class="circle"></div>
            </div>
            <div class="right">
                <div class="placebar-full"></div>
                <div class="placebar-half"></div>
            </div>

        </div>
        <div class="post-body">
            <div class="post-image">
                <img src="{{url('/')}}/assets/images/placeholderimage.png" alt="" class="img image-src-container">
            </div>
        </div>
        <div class="post-footer caption">
            <div class="placebar-full"></div>
            <div class="placebar-half"></div>
        </div>
    </div>

    {{-- instagram story --}}
    <div class="socialpreview instagram-story">
        <div class="post-container image-bg-container" style="background-image:url('{{url('/')}}/assets/images/placeholderimage.png');">
            <div class="story-strip">
                <div class="strip"></div>
                <div class="strip"></div>
                <div class="strip"></div>
            </div>
            <div class="post-header">

                <div class="left">
                    <div class="circle"></div>
                </div>
                <div class="right">
                    <div class="placebar-full"></div>
                    <div class="placebar-half"></div>
                </div>

            </div>
            {{-- <img src="{{url('/')}}/assets/images/placeholderimage.png" alt="" class="post-image"> --}}
            <div class="post-footer">
                <div class="replybox"></div>
                <div class="replybtn"></div>
            </div>
        </div>



    </div>

    {{-- instagram album --}}
    <div class="socialpreview instagram-album">
        <div class="post-header">

            <div class="left">
                <div class="circle"></div>
            </div>
            <div class="right">
                <div class="placebar-full"></div>
                <div class="placebar-half"></div>
            </div>

        </div>
        <div class="post-body">
            <div class="post-image">
                <img src="{{url('/')}}/assets/images/placeholderimage.png" alt="" class="img image-src-container">
            </div>
        </div>
        <div class="post-footer caption">
            <div class="placebar-full"></div>
            <div class="placebar-half"></div>
        </div>
    </div>



    {{-- twitter post --}}
    <div class="socialpreview twitter-post">
        <div class="post-header">

            <div class="left">
                <div class="circle"></div>
            </div>
            <div class="right">
                <div class="placebar-full"></div>
                <div class="placebar-half"></div>
            </div>

        </div>
        <div class="post-footer caption">
            <div class="placebar-full"></div>
            <div class="placebar-half"></div>
        </div>
        <div class="post-body">
            <div class="post-image">
                <img src="{{url('/')}}/assets/images/placeholderimage.png" alt="" class="img image-src-container">
            </div>
        </div>
        <div class="post-footer">
            <div class="placebar-full"></div>

        </div>

    </div>

    {{-- twitter story --}}
    <div class="socialpreview twitter-story">
        <div class="post-container image-bg-container" style="background-image:url('{{url('/')}}/assets/images/placeholdertall.png');">
            <div class="story-strip">
                <div class="strip"></div>
                <div class="strip"></div>
                <div class="strip"></div>
            </div>
            <div class="post-header">

                <div class="left">
                    <div class="circle"></div>
                </div>
                <div class="right">
                    <div class="placebar-full"></div>
                    <div class="placebar-half"></div>
                </div>

            </div>
            {{-- <img src="{{url('/')}}/assets/images/placeholderimage.png" alt="" class="post-image"> --}}

        </div>



    </div>



    {{-- linkedin post --}}
    <div class="socialpreview linkedin-post">
        <div class="post-header">

            <div class="left">
                <div class="circle"></div>
            </div>
            <div class="right">
                <div class="placebar-full"></div>
                <div class="placebar-half"></div>
                <div class="placebar-half" style="width:25%;"></div>
            </div>

        </div>
        <div class="post-footer caption">
            <div class="placebar-full"></div>
            <div class="placebar-full"></div>
            <div class="placebar-half"></div>
        </div>
        <div class="post-body">
            <div class="post-image">
                <img src="{{url('/')}}/assets/images/placeholderimage.png" alt="" class="img image-src-container">
            </div>
        </div>
        <div class="post-footer">
            <div class="placebar-full"></div>

        </div>

    </div>


    {{-- facebook post --}}
    <div class="socialpreview facebook-post">
        <div class="post-header">

            <div class="left">
                <div class="circle"></div>
            </div>
            <div class="right">
                <div class="placebar-full"></div>
                <div class="placebar-half"></div>
                <div class="placebar-half" style="width:25%;"></div>
            </div>

        </div>
        <div class="post-footer caption">
            <div class="placebar-full"></div>
            <div class="placebar-full"></div>
            <div class="placebar-half"></div>
        </div>
        <div class="post-body">
            <div class="post-image">
                <img src="{{url('/')}}/assets/images/placeholderimage.png" alt="" class="img image-src-container">
            </div>
        </div>
        <div class="post-footer">
            <div class="actionbar">
                <div class="action">
                    <div class="icon">
                        <img src="{{url('/')}}/assets/icons/facebook-like.png" alt="" class="img">
                    </div>
                    <div class="label">Like</div>
                </div>
                <div class="action">
                    <div class="icon">
                        <img src="{{url('/')}}/assets/icons/facebook-comment.png" alt="" class="img">
                    </div>
                    <div class="label">Comment</div>
                </div>
            </div>

        </div>

    </div>

    {{-- facebook story --}}
    <div class="socialpreview facebook-story">
        <div class="post-container image-bg-container" style="background-image:url('{{url('/')}}/assets/images/placeholdertall.png');">
            <div class="story-strip">
                <div class="strip"></div>
                <div class="strip"></div>
                <div class="strip"></div>
            </div>
            <div class="post-header">

                <div class="left">
                    <div class="circle"></div>
                </div>
                <div class="right">
                    <div class="placebar-full"></div>
                    <div class="placebar-half"></div>
                </div>

            </div>
            {{-- <img src="{{url('/')}}/assets/images/placeholderimage.png" alt="" class="post-image"> --}}
            <div class="post-footer">
                <div class="replybox"></div>
                <div class="replybtn"></div>
                <div class="replybtn"></div>
                <div class="replybtn"></div>


            </div>
        </div>



    </div>



    {{-- youtube post --}}
    <div class="socialpreview youtube-post">
        <div class="video-container">
            <img src="{{url('/')}}/assets/images/placeholderimage.png" alt="" class="img image-src-container">
        </div>
        <div class="title">
            <div class="placebar-full" ></div>
            <div class="placebar-half"></div>
        </div>

        <hr>

        <div class="profile">
            <div class="left">
                <div class="circle"></div>
            </div>
            <div class="right">
                <div class="placebar-full"></div>
                <div class="placebar-half"></div>
            </div>
        </div>

        <hr>

        <div class="videolist">
            <div class="video">
                <div class="image">

                </div>
                <div class="detail">
                    <div class="left">
                        <div class="circle"></div>
                    </div>
                    <div class="right">
                        <div class="placebar-full"></div>
                        <div class="placebar-half"></div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    {{-- youtube story --}}
    <div class="socialpreview youtube-story">
        <div class="post-container image-bg-container" style="background-image:url('{{url('/')}}/assets/images/placeholderimage.png');">
            <div class="story-strip">
                <div class="strip"></div>
                <div class="strip"></div>
                <div class="strip"></div>
            </div>
            <div class="post-header">

                <div class="left">
                    <div class="circle"></div>
                </div>
                <div class="right">
                    <div class="placebar-full"></div>
                    <div class="placebar-half"></div>
                </div>

            </div>
            {{-- <img src="{{url('/')}}/assets/images/placeholderimage.png" alt="" class="post-image"> --}}
            <div class="post-footer">
                <div class="replybox"></div>
                <div class="replybtn"></div>
            </div>
        </div>



    </div>

    {{-- tiktok post --}}
    <div class="socialpreview tiktok-post">
        <div class="post-container image-bg-container" style="background-image:url('{{url('/')}}/assets/images/placeholdertall.png');">
            <div class="top-header">
                Following | <span>For You</span>
            </div>

            <div class="sideaction">
                <div class="circle"></div>
                <div class="icons">
                    <img src="{{url('/')}}/assets/icons/heart-icon.png" alt="" style="">
                </div>
                <div class="icons">
                    <img src="{{url('/')}}/assets/icons/comment-icon-black.png" alt="" style="filter : invert(100%) ; width: 60%;">
                </div>
                <div class="icons">
                    <img src="{{url('/')}}/assets/icons/forward-icon.png" alt="" style="filter : invert(100%) ;width: 60%;">
                </div>

            </div>
            <div class="caption">
                <div class="placebar-full"></div>
                <div class="placebar-half"></div>
            </div>

        </div>



    </div>




</div>
