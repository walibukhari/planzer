@extends('layouts.simple.master')
@section('title', 'New User')

@section('css')
@endsection

@section('style')
@endsection

@section('breadcrumb-title')
<h3>Add New User</h3>
@endsection

@section('breadcrumb-items')
<li class="breadcrumb-item">Users</li>
<li class="breadcrumb-item active">Add New User</li>
@endsection

@section('content')
<div>
	  @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
        @endif
        @if (session('password_status'))
            <div class="alert alert-success" role="alert">
                {{ session('password_status') }}
            </div>
        @endif
        @if (Session::has('error'))
            <div class="alert alert-danger alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>{!! Session('error') !!}</strong>
            </div>
        @endif
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
</div>
<div class="container-fluid">
	<div class="row">
		<div class="col-sm-12">
			<div class="card">
				
				<div class="card-body">
					<form class="needs-validation" novalidate="" method="POSt" action="{{route('save.user')}}">
						@csrf

							<div class="row">
							
							<div class="col-md-6 mb-3">
								<label for="first_name">First Name</label>
								<input class="form-control" value="{{ old('first_name') }}" name="first_name" id="" type="text" placeholder="First Name" required="">
								<div class="invalid-feedback">Please provide a valid state.</div>
							</div>
							<div class="col-md-6 mb-3">
								<label for="last_name">Last Name</label>
								<input class="form-control" value="{{ old('last_name') }}" id="" name="last_name" type="text" placeholder="Last Name" required="">
								<div class="invalid-feedback">Please provide a Country</div>
							</div>
						</div>


						<div class="row">
							<div class="col-md-6 mb-3">
								<label for="validationCustomUsername">Email</label>
								<div class="input-group">
									<div class="input-group-prepend"><span class="input-group-text" id="inputGroupPrepend">@</span></div>
								   <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">
									<div class="invalid-feedback">Please Enter a Email.</div>
								</div>
							</div>
							<div class="col-md-6 mb-3">
								<label for="validationCustom01">Username</label>
								 <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
								<div class="valid-feedback">Looks good!</div>
							</div>
						<!-- 	<div class="col-md-4 mb-3">
								<label for="validationCustom02">Last name</label>
								<input class="form-control" id="validationCustom02" type="text" placeholder="Last name" required="">
								<div class="valid-feedback">Looks good!</div>
							</div> -->
							
						</div>



	                 <div class="row">
							<div class="col-md-6 mb-3">
								<label for="password">Password</label>
								   <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
								<div class="valid-feedback">Looks good!</div>
							</div>

							<div class="col-md-6 mb-3">
								<label for="address">Address</label>
								<input class="form-control" value="{{ old('address') }}" id="" name="address" type="text" placeholder="Address" required="">
								<div class="invalid-feedback">Please provide a Address.</div>
							</div>
						
							<!-- <div class="col-md-6 mb-3">
								<label for="password-confirm" >Confirm Password</label>
								<div class="input-group">
									<div class="input-group-prepend"></div>
								    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
									<div class="invalid-feedback">Please Enter Same Passowrd.</div>
								</div>
							</div> -->
						</div>








						<div class="row">
							<div class="col-md-6 mb-3">
								<label for="city">City</label>
								<input class="form-control" name="city" id="" type="text" placeholder="City" value="{{ old('city') }}" required="">
								<div class="invalid-feedback">Please provide a valid city.</div>
							</div>

							<div class="col-md-6 mb-3">
								<label for="postal_code">Postal Code</label>
								<input class="form-control" name="postal_code" id="" type="text" placeholder="City" value="{{ old('postal_code') }}" required="">
								<div class="invalid-feedback">Please provide a valid Postal Code.</div>
							</div>
						
						</div>

						<div class="row">
								<div class="col-md-6 mb-3">
								<label for="state">State</label>
								<input class="form-control" value="{{ old('state') }}" name="state" id="" type="text" placeholder="State" required="">
								<div class="invalid-feedback">Please provide a valid state.</div>
							</div>
							<div class="col-md-6 mb-3">
								<label for="country">Country</label>
								<input class="form-control" value="{{ old('country') }}" id="" name="country" type="text" placeholder="Country" required="">
								<div class="invalid-feedback">Please provide a Country</div>
							</div>
						</div>
						<div class="row">
							
							
								<div class="col-md-6 mb-3">
								<label for="mobile">Mobile Nnumber</label>
								<input class="form-control" value="{{ old('mobile') }}" name="mobile" id="" type="number" placeholder="Mobile" required="">
								<div class="invalid-feedback">Please provide a Mobile No.</div>
							</div>

							<div class="col-md-6 mb-3">
								<label for="company">Company Name</label>
								<input class="form-control" value="{{ old('company') }}" name="company" id="" type="text" placeholder="Company Name" required="">
								<div class="invalid-feedback">Please provide a company name</div>
							</div>
						</div>


						<div class="row">
						
							<div class="col-md-6 mb-3">
								<label for="vat_no">VAT Number</label>
								<input class="form-control" name="vat_no" value="{{old('vat_no')}}" id="" type="text" placeholder="VAT Number" required="">
								<div class="invalid-feedback">Please provide a VAT</div>
							</div>

								<div class="col-md-6 mb-3">
								<label for="vat_no">Timezone</label>
								<select class="form-control" name="timezone">
									<option value="" selected="" disabled="">Select One</option>
									@foreach($tzlist as $tzone )
									<option>{{$tzone}}</option>
									@endforeach
								</select>
								<div class="invalid-feedback">Please provide a Timezone</div>
							</div>
							
						</div>




						<!-- <div class="form-group">
							<div class="form-check">
								<div class="checkbox p-0">
									<input class="form-check-input" id="invalidCheck" type="checkbox" required="">
									<label class="form-check-label" for="invalidCheck">Agree to terms and conditions</label>
								</div>
								<div class="invalid-feedback">You must agree before submitting.</div>
							</div>
						</div> -->
						<button class="btn btn-primary" type="submit">save</button>
					</form>
				</div>
			</div>
		
			
		
		</div>
	</div>
</div>
@endsection

@section('script')
<script src="{{asset('assets/js/form-validation-custom.js')}}"></script>
@endsection