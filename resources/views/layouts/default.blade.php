<!DOCTYPE html>
<html lang="en">

<head>
    @php
        $url = \Request::fullUrl();
        $match = str_contains($url,'book-demo');
    @endphp
    @if($match)
        <link href="https://web-assets.zendesk.com/css/p-demo.min.a75d6bbc.css" media="all" rel="stylesheet" type="text/css" />
    @endif
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Socialday - @yield('title') Page</title>
    <link rel="shortcut icon" href="/favicon.png" type="image/x-icon">
    <!-- Bootstrap , fonts & icons  -->
    <link rel="stylesheet" href="/assets/bootstrap.css">
    <link rel="stylesheet" href="/assets/style.css">
    <link rel="stylesheet" href="/assets/typo.css">
    <link rel="stylesheet" href="/assets/all.css">
    <!-- Plugin'stylesheets  -->
    <link rel="stylesheet" href="/assets/aos.min.css">
    <link rel="stylesheet" href="/assets/fancy.min.css">
    <link rel="stylesheet" href="/assets/select.min.css">
    <link rel="stylesheet" href="/assets/slick.min.css">
    <!-- Vendor stylesheets  -->
    <link rel="stylesheet" href="/assets/main.css">
    <!-- Custom stylesheet -->
    <style>
        .btn.btn-medium {
            min-width: 158px;
            height: 45px;
            font-size: 13px;
            background: #235AF6;
            border-color: #235AF6;
        }
        .btn.btn-xl {
            background: #235AF6;
            border-color: #235AF6;
        }
        .footer-yellow-shape-img {
            position: absolute;
            left: 0;
            bottom: 0;
            transform: translate(-70%, 50%) scale(0.5);
        }
        .btn:hover {
            color: #fff;
            background-color: #0544de;
            border-color: #0440d2;
        }
        .btn.btn-medium:hover {
            color: #fff;
            background-color: #0544de;
            border-color: #0440d2;
        }
        .btn.btn-sunset:hover {
            color: #fff;
            background-color: #0544de;
            border-color: #0440d2;
        }
    </style>
    @stack('css')
</head>

<body data-theme="light">
    <div class="site-wrapper overflow-hidden ">
        <div id="loading">
            <img src="/assets/preloader.gif" alt="">
        </div>
        <!-- Header Area -->
        <!-- Header Area -->
        <header class="site-header site-header--menu-right px-7 px-lg-10 z-index-99 dynamic-sticky-bg site-header--absolute site-header--sticky">
            <div class="container">
                <nav class="navbar site-navbar offcanvas-active navbar-expand-lg  px-0" style="height:80px;">
                    <!-- Brand Logo-->
                    <div class="brand-logo mt-3 mt-md-0"><a  href="{{route('homePage')}}">
                            <!-- light version logo (logo must be black)-->
                            <img style="width:225px;" src="{{asset('/SocialdayLogoColor1.png')}}" alt="" class="light-version-logo">
                            <!-- Dark version logo (logo must be White)-->
                            <img style="width:225px;" src="{{asset('/SocialdayLogoColor1.png')}}" alt="" class="dark-version-logo">
                        </a></div>
                    <div class="collapse navbar-collapse" id="mobile-menu">
                        <div class="navbar-nav-wrapper">
                            <ul class="navbar-nav main-menu">
                                <li class="nav-item dropdown">
                                    <a class="nav-link" href="{{route('homePage')}}" role="button" aria-expanded="false">Home</a>
                                </li>
                                <li class="nav-item dropdown">
                                    <a class="nav-link" href="{{route('about')}}" role="button" aria-expanded="false">About</a>
                                </li>
                                <li class="nav-item dropdown">
                                    <a class="nav-link" href="{{route('pricingP')}}" role="button" aria-expanded="false">Pricing</a>
                                </li>
                                <li class="nav-item dropdown">
                                    <a class="nav-link" href="{{route('contactUs')}}" role="button" aria-expanded="false">Contact</a>
                                </li>
                                <li class="nav-item dropdown">
                                    <a class="nav-link" href="{{route('login')}}" role="button" aria-expanded="false">Login</a>
                                </li>
                                {{--                            <li class="nav-item">--}}
                                {{--                                <a class="nav-link" href=" https://grayic.com/html-support/" role="button" aria-expanded="false">Support</a>--}}
                                {{--                            </li>--}}
                            </ul>
                        </div>
{{--                        <button class="d-block d-lg-none offcanvas-btn-close" type="button" data-toggle="collapse" data-target="#mobile-menu" aria-controls="mobile-menu" aria-expanded="true" aria-label="Toggle navigation">--}}
{{--                            <i class="gr-cross-icon"></i>--}}
{{--                        </button>--}}
                    </div>
                    <div class="header-btn ml-auto ml-lg-6 d-none d-sm-block font-size-3">
                        <a class="btn btn btn-sunset btn-medium rounded-5 font-size-3" href="{{route('newRegister')}}">
                            Start 14 Days trial
                        </a>
                    </div>
                    <!-- Mobile Menu Hamburger-->
                    <button class="navbar-toggler btn-close-off-canvas  hamburger-icon border-0" type="button" data-toggle="collapse" data-target="#mobile-menu" aria-controls="mobile-menu" aria-expanded="false" aria-label="Toggle navigation">
                        <!-- <i class="icon icon-simple-remove icon-close"></i> -->
                        <span class="hamburger hamburger--squeeze js-hamburger">
              <span class="hamburger-box">
                <span class="hamburger-inner"></span>
                </span>
                </span>
                    </button>
                    <!--/.Mobile Menu Hamburger Ends-->
                </nav>
            </div>
        </header>
        <!-- navbar- -->

        @yield('content')

        <!-- Footer Area -->
        <footer>
            <div class="bg-dark-cloud pt-13 pt-lg-27 pb-7 dark-mode-texts position-relative">
            <div class="container">
                <div class="row pb-lg-25">
                    <div class="col-lg-4 col-md-5 col-md-3 col-xs-8">
                        <div class="pr-xl-20 mb-11 mb-lg-0 mt-lg-5">
                            <div class="brand-logo mb-5">
                                <a href="javascript:">
                                    <img style="width:235px;position:relative;left:-35px;" class="mx-auto mx-0 light-version-logo default-logo"
                                         src="/Socialday_Logo_Color _f7f7f9.png" alt="">
                                    <img style="width:235px;position:relative;left:-35px;" src="/Socialday_Logo_Color _f7f7f9.png" alt="" class="dark-version-logo mx-auto mx-0">
                                </a>
                            </div>
                            <p class="font-size-5 mb-0 text-bali-gray pr-sm-10 pr-md-0">Your ultimate social media solution to grow
                                business and engaging with future customers and clients.
                            </p>
                            <div class="mt-8">
                                <a class="btn btn-blue-3 btn-sm rounded-5 text-lily-white font-size-3" href="{{route('login')}}">Get Started</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-7 col-md-12 offset-xl-1">
                        <div class="row">
                            <!-- Single Widgets -->
                            <div class="col-md-3 col-xs-6">
                                <div class="mb-10 mb-lg-0">
                                    <h4 class="font-size-6 font-weight-normal mb-8 text-bali-gray">Community</h4>
                                    <ul class="list-unstyled">
                                        <li class="mb-6"><a class="font-size-5 text-lily-white gr-hover-text-blue-3" href="{{route('bookDemo')}}">Book demo</a></li>
                                        <li class="mb-6"><a class="font-size-5 text-lily-white gr-hover-text-blue-3" href="{{route('terms')}}">Terms</a></li>
                                        <li class="mb-6"><a class="font-size-5 text-lily-white gr-hover-text-blue-3" href="{{route('policies')}}">Privacy Policy</a></li>
                                    </ul>
                                </div>
                            </div>
                            <!-- End Single Widgets -->
                            <!-- Single Widgets -->
                            <div class="col-md-3 col-xs-6">
                                <div class="mb-10 mb-lg-0">
                                    <h4 class="font-size-6 font-weight-normal mb-8 text-bali-gray">About us</h4>
                                    <ul class="list-unstyled">
                                        <li class="mb-6"><a class="font-size-5 text-lily-white gr-hover-text-blue-3" href="{{route('meetTeams')}}">Meet the team</a></li>
                                        <li class="mb-6"><a class="font-size-5 text-lily-white gr-hover-text-blue-3" href="{{route('career')}}">Career</a></li>
                                        <li class="mb-6"><a class="font-size-5 text-lily-white gr-hover-text-blue-3" href="{{route('plus')}}">Socialday Plus</a></li>
                                    </ul>
                                </div>
                            </div>
                            <!-- End Single Widgets -->
                            <!-- Single Widgets -->
                            <div class="col-lg-6 col-md-5 col-xs-8">
                                <div class="mb-10 mb-lg-0 mr-xl-12">
                                    <h4 class="font-size-6 font-weight-normal mb-8 text-bali-gray">Contacts</h4>
                                    <p class="font-size-5 mb-0 text-lily-white">Feel free to get in touch with us
                                        via phone or send us a message.</p>
                                    <div class="mt-7">
                                        <a class="font-size-5 d-block text-golden-yellow mb-0" href="tel:+45 93 91 21 25">+45 93 91 21 25</a>
                                        <a class="font-size-5 d-block text-golden-yellow mb-0" href="mailto:info@finity.com">support@socialday.io </a>
                                    </div>
                                </div>
                            </div>
                            <!-- End Single Widgets -->
                        </div>
                    </div>
                </div>
                <div class="row align-items-center pt-10 border-top">
                    <div class="col-lg-6 text-center text-lg-left px-0">
                        <div class="copyright">
                            <p class="mb-0 font-size-3 text-bali-gray">Socialday.io © 2021, All Rights Reserved</p>
                        </div>
                    </div>
                    <div class="col-lg-6 text-center text-lg-right px-0">
                        <div class="footer-right mt-5 mt-lg-0">
                            <ul class="list-unstyled d-flex align-items-center justify-content-center justify-content-lg-end mb-0">
                                <li><a class="text-white gr-hover-text-blue-3 ml-7" href="https://www.linkedin.com/company/42782060/"><i class="fab fa-linkedin"></i></a></li>
                                <li><a class="text-white gr-hover-text-blue-3 ml-7" href="https://www.facebook.com/planzer.io"><i class="fab fa-facebook-f"></i></a></li>
                                <li><a class="text-white gr-hover-text-blue-3 ml-7" href="https://www.instagram.com/planzer.io/"><i class="fab fa-instagram"></i></a></li>
                                <li><a class="text-white gr-hover-text-blue-3 ml-7" href="https://www.youtube.com/watch?v=QZazlSLaFww&amp;t=2s"><i class="fab fa-youtube"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer-yellow-shape-img">
                <img src="/images/yellow-footer-shape.png" alt="">
            </div>
        </div>
        </footer>
        <!-- End Footer Area -->
    </div>
    <!-- Vendor Scripts -->
    <script src="/assets/vendor.min.js"></script>
    <!-- Plugin's Scripts -->
    <script src="/assets/fancybox.min.js"></script>
    <script src="/assets/select.min.js"></script>
    <script src="/assets/aos.min.js"></script>
    <script src="/assets/slick.min.js"></script>
    <script src="/assets/counterup.min.js"></script>
    <script src="/assets/waypoints.min.js"></script>
    <!-- Activation Script -->
    <script src="/assets/custom.js"></script>

    @stack('js')
</body>
