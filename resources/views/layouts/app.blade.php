
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Finity - Landing Page Template </title>
    <link rel="shortcut icon" href="image/favicon.png" type="image/x-icon">
    <!-- Bootstrap , fonts & icons  -->
    <link rel="stylesheet" href="https://uxtheme.net/demos/finity/css/bootstrap.css">
    <link rel="stylesheet" href="https://uxtheme.net/demos/finity/fonts/icon-font/css/style.css">
    <link rel="stylesheet" href="https://uxtheme.net/demos/finity/fonts/typography-font/typo.css">
    <link rel="stylesheet" href="https://uxtheme.net/demos/finity/fonts/fontawesome-5/css/all.css">
    <!-- Plugin'stylesheets  -->
    <link rel="stylesheet" href="https://uxtheme.net/demos/finity/plugins/aos/aos.min.css">
    <link rel="stylesheet" href="https://uxtheme.net/demos/finity/plugins/fancybox/jquery.fancybox.min.css">
    <link rel="stylesheet" href="https://uxtheme.net/demos/finity/plugins/nice-select/nice-select.min.css">
    <link rel="stylesheet" href="https://uxtheme.net/demos/finity/plugins/slick/slick.min.css">
    <!-- Vendor stylesheets  -->
    <link rel="stylesheet" href="https://uxtheme.net/demos/finity/css/main.css">

    <!-- Custom stylesheet -->
</head>
