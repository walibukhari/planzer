@extends('layouts.default')
@section('title')
Landing
@endsection
@push('css')
<style type="text/css">
    .play-btn {
        cursor: pointer;
        width: 300px;
        height: 300px;
        background: radial-gradient(rgba(38, 44, 244, 0.8) 60%, rgba(38, 44, 244, 1) 62%);
        border-radius: 50%;
        position: relative;
        display: block;
        margin: 100px auto;
        box-shadow: 0px 0px 25px 3px rgba(38, 44, 244, 0.8);
    }

    /* triangle */
    .play-btn::after {
        content: "";
        position: absolute;
        left: 50%;
        top: 50%;
        -webkit-transform: translateX(-30%) translateY(-40%);
        transform: translateX(-30%) translateY(-40%);
        transform-origin: center center;
        width: 80px;
        height: 80px;
        border-top: 40px solid transparent;
        border-bottom: 40px solid transparent;
        border-left: 65px solid #fff;
        z-index: 100;
        -webkit-transition: all 400ms cubic-bezier(0.55, 0.055, 0.675, 0.19);
        transition: all 400ms cubic-bezier(0.55, 0.055, 0.675, 0.19);
    }

    /* pulse wave */
    .play-btn:before {
        content: "";
        position: absolute;
        width: 160%;
        height: 160%;
        -webkit-animation-delay: 0s;
        animation-delay: 0s;
        -webkit-animation: pulsate1 2s;
        animation: pulsate1 2s;
        -webkit-animation-direction: forwards;
        animation-direction: forwards;
        -webkit-animation-iteration-count: infinite;
        animation-iteration-count: infinite;
        -webkit-animation-timing-function: steps;
        animation-timing-function: steps;
        opacity: 1;
        border-radius: 50%;
        border: 5px solid rgba(255, 255, 255, .75);
        top: -30%;
        left: -30%;
        background: rgba(198, 16, 0, 0);
    }

    @-webkit-keyframes pulsate1 {
        0% {
            -webkit-transform: scale(0.6);
            transform: scale(0.6);
            opacity: 1;
            box-shadow: inset 0px 0px 25px 3px rgba(255, 255, 255, 0.75), 0px 0px 25px 10px rgba(255, 255, 255, 0.75);
        }

        100% {
            -webkit-transform: scale(1);
            transform: scale(1);
            opacity: 0;
            box-shadow: none;

        }
    }

    @keyframes pulsate1 {
        0% {
            -webkit-transform: scale(0.6);
            transform: scale(0.6);
            opacity: 1;
            box-shadow: inset 0px 0px 25px 3px rgba(255, 255, 255, 0.75), 0px 0px 25px 10px rgba(255, 255, 255, 0.75);
        }

        100% {
            -webkit-transform: scale(1, 1);
            transform: scale(1);
            opacity: 0;
            box-shadow: none;

        }
    }

</style>
<style type="text/css">
    @font-face {
        font-weight: 400;
        font-style: normal;
        font-family: 'Circular-Loom';

        src: url('https://cdn.loom.com/assets/fonts/circular/CircularXXWeb-Book-cd7d2bcec649b1243839a15d5eb8f0a3.woff2') format('woff2');
    }

    @font-face {
        font-weight: 500;
        font-style: normal;
        font-family: 'Circular-Loom';

        src: url('https://cdn.loom.com/assets/fonts/circular/CircularXXWeb-Medium-d74eac43c78bd5852478998ce63dceb3.woff2') format('woff2');
    }

    @font-face {
        font-weight: 700;
        font-style: normal;
        font-family: 'Circular-Loom';

        src: url('https://cdn.loom.com/assets/fonts/circular/CircularXXWeb-Bold-83b8ceaf77f49c7cffa44107561909e4.woff2') format('woff2');
    }

    @font-face {
        font-weight: 900;
        font-style: normal;
        font-family: 'Circular-Loom';

        src: url('https://cdn.loom.com/assets/fonts/circular/CircularXXWeb-Black-bf067ecb8aa777ceb6df7d72226febca.woff2') format('woff2');
    }

</style>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
@endpush

@section('content')
<div class="bg-default-3 pt-25 pt-md-25 pt-lg-29">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-9 col-lg-7 col-md-9">
                <div class="text-center mb-11 mb-lg-24" data-aos="zoom-in" data-aos-duration="800" data-aos-once="true">
                    <h5 class="font-size-11 mb-9 mx-xs-16 mx-sm-12 mx-md-9 mx-lg-0"> Social Media Automation</h5>
                    <p class="font-size-7 px-lg-5 mb-0">We helped more than 200 individuals and companies to grow their
                        business successfully with automated social media</p>
                    <div class="btn-group d-inline-block mt-10">
                        <a class="btn btn-blue-3 btn-2 rounded-5" href="{{route('newRegister')}}">Start 14 Days Free
                            Trial</a>
                        <p class="d-block mt-6 font-size-3 mb-0 text-bali-gray letter-spacing-normal">No credit card
                            required</p>
                    </div>
                </div>
            </div>
        </div>
        <!-- Hero Image Group -->
        <div class="row justify-content-center">
            <div class="col-md-10 col-xs-11">
                <div class="l3-hero-image-group w-100">
                    <!-- Image One -->
                    <div class="img-1 shadow-13" data-aos="fade-up" data-aos-duration="800" data-aos-delay="300"
                        data-aos-once="true">
                        <img class="w-100" src="{{asset('/images/Front_picture_1.png')}}" alt="">
                    </div>
                </div>
            </div>
        </div>
        <!-- End Hero Image Group -->
    </div>
</div>
<!-- End Header Area -->
<!-- BrandLogo Area -->
<!-- Band Logo -->
<div class="pt-12 pb-9">
    <!-- Company Section -->
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-10">
                <!-- Brand Logos -->
                <div
                    class="brand-logo-small d-flex align-items-center justify-content-center justify-content-lg-between flex-wrap">
                    <!-- Single Brand -->
                    <div class="single-brand-logo opacity-5 mx-5 my-6">
                        <img class="light-version-logo" src="{{asset('images/Client_1.png')}}"
                            style="width: auto;height: 50px;" alt="" data-aos="fade-in" data-aos-once="true">
                        <img class="dark-version-logo default-logo" src="{{asset('images/Client_1.png')}}"
                            style=" height: 50px;" alt="" data-aos="fade-in" data-aos-once="true">
                    </div>
                    <!-- Single Brand -->
                    <div class="single-brand-logo opacity-5 mx-5 my-6">
                        <img class="light-version-logo" src="{{asset('images/Client_2.png')}}"
                            style="width: auto;height: 50px;" alt="" data-aos="fade-in" data-aos-duration="300"
                            data-aos-delay="300" data-aos-once="true">
                        <img class="dark-version-logo default-logo" src="{{asset('images/Client_2.png')}}"
                            style="width: auto;height: 50px;" alt="" data-aos="fade-in" data-aos-duration="300"
                            data-aos-delay="300" data-aos-once="true">
                    </div>
                    <!-- Single Brand -->
                    <div class="single-brand-logo opacity-5 mx-5 my-6">
                        <img class="light-version-logo" src="{{asset('images/Client_3.png')}}"
                            style="width: auto;height: 50px;" alt="" data-aos="fade-in" data-aos-duration="600"
                            data-aos-delay="500" data-aos-once="true">
                        <img class="dark-version-logo default-logo" src="{{asset('images/Client_3.png')}}"
                            style="width: auto;height: 50px;" alt="" data-aos="fade-in" data-aos-duration="600"
                            data-aos-delay="500" data-aos-once="true">
                    </div>
                    <!-- Single Brand -->
                    <div class="single-brand-logo opacity-5 mx-5 my-6">
                        <img class="light-version-logo" src="{{asset('images/Client_4.png')}}"
                            style="width: auto;height: 50px;" alt="" data-aos="fade-in" data-aos-duration="900"
                            data-aos-delay="700" data-aos-once="true">
                        <img class="dark-version-logo default-logo" src="{{asset('images/Client_4.png')}}"
                            style="width: auto;height: 50px;" alt="" data-aos="fade-in" data-aos-duration="900"
                            data-aos-delay="700" data-aos-once="true">
                    </div>
                    <!-- Single Brand -->
                    <div class="single-brand-logo opacity-5 mx-5 my-6">
                        <img class="light-version-logo" src="{{asset('images/Client_5.png')}}"
                            style="width: auto;height: 50px;" alt="" data-aos="fade-in" data-aos-duration="1200"
                            data-aos-delay="700" data-aos-once="true">
                        <img class="dark-version-logo default-logo" src="{{asset('images/Client_5.png')}}"
                            style="width: auto;height: 50px;" alt="" data-aos="fade-in" data-aos-duration="1200"
                            data-aos-delay="700" data-aos-once="true">
                    </div>
                </div>
                <!-- End Brand Logos -->
            </div>
        </div>
    </div>
    <!-- End Company Section -->
</div>
<!-- End Band Logo -->
<!-- End BrandLogo Area -->
<!-- featureOne Area -->
<div class="pt-4 pt-md-9 pt-lg-18 pb-15 pb-md-19 pb-lg-25 border-bottom border-default-color-3">
    <!-- feature section -->
    <div class="container">
        <div class="">
            <!-- Section Title -->
            <div class="row justify-content-center">
                <div class="col-xl-6 col-lg-7 col-md-9">
                    <div class="text-center mb-8 mb-lg-14" data-aos="zoom-in" data-aos-duration="800"
                        data-aos-once="true">
                        <h2 class="font-size-10 mb-6 letter-spacing-n83" style="cursor:pointer;"
                            onclick="window.location.href='/register'">Features for you</h2>
                        <p class="font-size-6 mb-0">We have the best features related to social media automation.</p>
                    </div>
                </div>
            </div>
            <!-- End Section Title -->
        </div>
    </div>
    <!-- feature slider -->
    <div class="row">
        <div class="col-12" data-aos="zoom-in" data-aos-duration="500" data-aos-delay="300" data-aos-once="true">
            <div class="l3-feature-slider my-4 my-lg-7">
                <!--             <a href="#" javascript:;="single-feature mx-5 focus-reset">
                                  <div class="d-flex bg-gray-3 rounded-10 pt-7 pl-7 pr-5 pb-7">
                                    <div class="mb-8 mr-8 bg-white circle-55">
                                      <img src="image/svg/node.svg" alt="">
                                    </div>
                                    <div class="">
                                      <h4 class="font-size-7 mb-0 text-dark-cloud">Find Leads</h4>
                                      <p class="font-size-5 line-height-28 text-stone mr-xl-7 mb-0">We’ve helped over 2,500 job seekers to get into the most popular tech teams.</p>
                                    </div>
                                  </div>
                                </a> -->
                <a href="javascript:;" class="single-feature mx-5 focus-reset">
                    <div class="d-flex bg-gray-3 rounded-10 pt-7 pl-7 pr-5 pb-7">
                        <div class="mb-8 mr-8 bg-white circle-55">
                            <i class="fa fa-hourglass-half" style="font-size:48px;" aria-hidden="true"></i>
                        </div>
                        <div class="">
                            <h4 class="font-size-7 mb-0 text-dark-cloud">Save Time</h4>
                            <p class="font-size-5 line-height-28 text-stone mr-xl-7 mb-0">Save time using the regular
                                instagram app, with a optimized online platform.</p>
                        </div>
                    </div>
                </a>
                <a href="javascript:;" class="single-feature mx-5 focus-reset">
                    <div class="d-flex bg-gray-3 rounded-10 pt-7 pl-7 pr-5 pb-7">
                        <div class="mb-8 mr-8 bg-white circle-55">
                            <i class='fas fa-seedling' style='font-size:48px'></i>
                        </div>
                        <div class="">
                            <h4 class="font-size-7 mb-0 text-dark-cloud">Organic Growth</h4>
                            <p class="font-size-5 line-height-28 text-stone mr-xl-7 mb-0">Grow organically with our
                                instagram engagement tools for your online business.</p>
                        </div>
                    </div>
                </a>
                <a href="javascript:;" class="single-feature mx-5 focus-reset">
                    <div class="d-flex bg-gray-3 rounded-10 pt-7 pl-7 pr-5 pb-7">
                        <div class="mb-8 mr-8 bg-white circle-55">
                            <i class='fas fa-user-friends' style='font-size:38px'></i>
                        </div>
                        <div class="">
                            <h4 class="font-size-7 mb-0 text-dark-cloud">Multiple Accounts</h4>
                            <p class="font-size-5 line-height-28 text-stone mr-xl-7 mb-0">Schedule, manage and engage
                                multiple accounts at once with one single platform.</p>
                        </div>
                    </div>
                </a>
                <a href="javascript:;" class="single-feature mx-5 focus-reset">
                    <div class="d-flex bg-gray-3 rounded-10 pt-7 pl-7 pr-5 pb-7">
                        <div class="mb-8 mr-8 bg-white circle-55">
                            <i class="fa fa-life-ring" style="font-size:48px;" aria-hidden="true"></i>
                        </div>
                        <div class="">
                            <h4 class="font-size-7 mb-0 text-dark-cloud">Fast Support</h4>
                            <p class="font-size-5 line-height-28 text-stone mr-xl-7 mb-0">Effective support regarding
                                everything from creating posts to generating leads.</p>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </div>
    <!-- End feature slider -->
    <div class="row justify-content-center">
        <div class="col-12">
            <div class="mt-10 mt-lg-12 text-center" data-aos="zoom-in" data-aos-duration="800" data-aos-once="true">
                <a class="video-btn btn btn-blue-3 btn-2 rounded-5" href="{{route('newRegister')}}">
                    Start 14 days free trial</a>
            </div>
        </div>
    </div>
</div>
<!-- End feature section -->
<!-- End featureOne Area -->
<!-- contentOne Area -->
<div class="pt-17 pt-sm-27 pt-md-25 pt-lg-35 pb-md-8 pb-lg-34 position-relative">
    <div class="container position-static">
        <div class="row position-static justify-content-center justify-content-lg-start">
            <div class="col-xl-5 col-lg-7 col-md-10 col-sm-10 col-xs-11 order-2 order-lg-1">
                <div class="pr-md-20 pr-xl-5">
                    <!-- Section Title -->
                    <div class="mb-5 mb-lg-13 pr-md-15 pr-lg-10 pr-xl-0" data-aos="fade-right" data-aos-duration="800"
                        data-aos-delay="300" data-aos-once="true">
                        <h2 class="font-size-10 mb-8 pr-xs-22 pr-sm-20 pr-md-13 pr-lg-0">Scheduling have never been
                            easier</h2>
                        <p class="font-size-6 mb-0">Plan a week of instagram posts in 20 minutes.spend less time posting
                            to Instagram and more time growing your business. </p>
                    </div>
                    <!-- End Section Title -->
                    <div class="list" data-aos="fade-right" data-aos-duration="800" data-aos-once="true">
                        <div class="d-flex align-items-center mb-6">
                            <div class="circle-59 bg-gray-3 mr-6">
                                <i class="fa fa-calendar-check-o" style="font-size:28px;"></i>
                            </div>
                            <p class="font-size-8 heading-default-color font-weight-medium mb-0">Scheduling posts in
                                minutes</p>
                        </div>
                        <div class="d-flex align-items-center mb-6">
                            <div class="circle-59 bg-gray-3 mr-6">
                                <i class="fa fa-line-chart" style="font-size:28px;"></i>
                            </div>
                            <p class="font-size-8 heading-default-color font-weight-medium mb-0">Increase chance to
                                engage</p>
                        </div>
                        <div class="d-flex align-items-center mb-6">
                            <div class="circle-59 bg-gray-3 mr-6">
                                <i class="fa fa-user-plus" style="font-size:28px;" aria-hidden="true"></i>
                            </div>
                            <p class="font-size-8 heading-default-color font-weight-medium mb-0">Advanced lead
                                generation </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-7 col-lg-6 col-xs-10 position-static order-1 order-lg-2">
                <div class="l3-content-image-group-4 position-lg-absolute mb-13 mb-lg-0">
                    <div class="img-1 shadow-5">
                        <img class="w-100" src="{{asset('images/Front_picture_2edit.png')}}" alt="">
                        <div class="img-2 rounded-10">
                            <img class="w-100" src="" alt="">
                        </div>
                        <div class="img-3">
                            <img class="w-100 w-lg-auto" src="{{asset('images/yellow-card.png')}}" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End contentOne Area -->
<!-- contentTwo Area -->
<div class="pb-13 pb-md-16 pb-lg-28">
    <div class="container">
        <div class="row align-items-center justify-content-center">
            <div class="col-xl-6 col-lg-6 col-md-7 col-xs-9"
                style="border-radius: 15px;background-image: url(/backImage.jpeg);background-repeat:no-repeat;"
                data-aos="fade-right" data-aos-duration="800" data-aos-once="true">
                <div class="l3-content-image-5 mt-5 mt-lg-0">
                    <a class="play-btn" target="_blank" data-toggle="modal" data-target="#myModal"></a>
                </div>
            </div>
            <div class="col-xl-5 col-lg-6 col-md-7 col-xs-10" data-aos="fade-left" data-aos-duration="800"
                data-aos-once="true">
                <div class="pl-lg-15 pt-11 pt-lg-0">
                    <h2 class="font-size-10 mb-8">Automated Engagement</h2>
                    <p class="font-size-6 mb-0">socialday is a social media tool that handles all of your social media
                        activities on complete autopilot. This great tool will handel your likes, comments, follows,
                        unfollows and much more. </p>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End contentTwo Area -->
<!-- contentTwo Area -->
<div class="pt-lg-19 pb-12 pb-md-18 pb-lg-31">
    <!-- category section -->
    <div class="container">
        <!-- Section Title -->
        <div class="mb-8 mb-lg-5">
            <div class="row align-items-center justify-content-center justify-content-lg-start">
                <div class="col-lg-7 col-md-9 col-xs-10">
                    <div class="mb-10 mb-lg-16 mb-md-0 text-center text-md-left" data-aos="fade-right" data-aos-duration="500" data-aos-once="true">
                <span class="bg-dodger-blue-2-op1 circle-84 font-size-9 mb-5 mb-lg-11 mx-auto mx-md-0">
                                <i
                                    class="fa fa-heart text-dodger-blue-2"></i>
                            </span>
                        <h2 class="font-size-11 mb-0">#Success stories with socialday plus.</h2>
                    </div>
                </div>
                <div class="col-lg-5 col-md-3" data-aos="fade-left" data-aos-duration="500" data-aos-once="true">
                    <div class="testimonial-one-button text-center text-md-right mt-3 mt-md-18"></div>
                </div>
            </div>
        </div>
        <!-- End Section Title -->
        <!-- category slider -->
        <div class="row">
            <div class="col-12">
                <div class="testimonial-one">
                    <div class="single-category focus-reset">
                        <div class="bg-white border rounded-10 pt-9 pl-10 pr-10 pb-8 mr-5 mr-lg-9">
                            <div class="circle-48 mb-8">
                                <img src="https://uxtheme.net/demos/finity/image/home-2/png/testimonial-img-1.png" alt="">
                            </div>
                            <div class="">
                                <p class="font-size-5 line-height-28 text-dark-cloud mr-xl-5">I owe these guys my life. Already used their landing page templates for my latest two projects.</p>
                                <h4 class="font-size-6 mb-0 text-dark-cloud font-weight-medium">- @thepatwalls</h4>
                            </div>
                        </div>
                    </div>
                    <div class="single-category focus-reset">
                        <div class="bg-white border rounded-10 pt-9 pl-10 pr-10 pb-8 mr-5 mr-lg-9">
                            <div class="circle-48 mb-8">
                                <img src="https://uxtheme.net/demos/finity/image/home-2/png/testimonial-img-2.png" alt="">
                            </div>
                            <div class="">
                                <p class="font-size-5 line-height-28 text-dark-cloud mr-xl-5">Time is the most precious thing you have when running a business. You can&#39;t take time to ponder on design…</p>
                                <h4 class="font-size-6 mb-0 text-dark-cloud font-weight-medium">- @thepatwalls</h4>
                            </div>
                        </div>
                    </div>
                    <div class="single-category focus-reset">
                        <div class="bg-white border rounded-10 pt-9 pl-10 pr-10 pb-8 mr-5 mr-lg-9">
                            <div class="circle-48 mb-8">
                                <img src="https://uxtheme.net/demos/finity/image/home-2/png/testimonial-img-3.png" alt="">
                            </div>
                            <div class="">
                                <p class="font-size-5 line-height-28 text-dark-cloud mr-xl-5">I owe these guys my life. Already used their landing page templates for my latest two projects. </p>
                                <h4 class="font-size-6 mb-0 text-dark-cloud font-weight-medium">- @thepatwalls</h4>
                            </div>
                        </div>
                    </div>
                    <div class="single-category focus-reset">
                        <div class="bg-white border rounded-10 pt-9 pl-10 pr-10 pb-8 mr-5 mr-lg-9">
                            <div class="circle-48 mb-8">
                                <img src="https://uxtheme.net/demos/finity/image/home-2/png/testimonial-img-4.png" alt="">
                            </div>
                            <div class="">
                                <p class="font-size-5 line-height-28 text-dark-cloud mr-xl-5">I owe these guys my life. Already used their landing page templates for my latest two projects.</p>
                                <h4 class="font-size-6 mb-0 text-dark-cloud font-weight-medium">- @thepatwalls</h4>
                            </div>
                        </div>
                    </div>
                    <div class="single-category focus-reset">
                        <div class="bg-white border rounded-10 pt-9 pl-10 pr-10 pb-8 mr-5 mr-lg-9">
                            <div class="circle-48 mb-8">
                                <img src="https://uxtheme.net/demos/finity/image/home-2/png/testimonial-img-3.png" alt="">
                            </div>
                            <div class="">
                                <p class="font-size-5 line-height-28 text-dark-cloud mr-xl-5">I owe these guys my life. Already used their landing page templates for my latest two projects.</p>
                                <h4 class="font-size-6 mb-0 text-dark-cloud font-weight-medium">- @thepatwalls</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End category slider -->
    </div>
</div>
<!-- End contentTwo Area -->
<!-- End pricingOne Area -->
<div class="pt-13 pt-md-17 pt-lg-25 pb-8 pb-md-11 pb-lg-22 position-relative">
    <div class="container">
        <!-- Section Title -->
        <div class="mb-13 mb-lg-13">
            <div class="row align-items-center justify-content-center">
                <div class="col-xl-5 col-lg-6 col-md-10">
                    <div class="mb-10 mb-lg-0 text-center text-lg-left" data-aos-duration="800" data-aos="fade-right"
                        data-aos-duration="500" data-aos-once="true">
                        <h2 class="font-size-10 pr-md-10 pr-xl-0 mb-0 letter-spacing-n83">Choose the right plan for your
                            biz</h2>
                    </div>
                </div>
                <div class="col-xl-7 col-lg-6 col-md-6 text-center text-md-right">
                    <div class="btn-section" data-aos="fade-left" data-aos-duration="500" data-aos-once="true">
                        <a href="javascript:"
                            class="btn-toggle-square active mx-3 price-deck-trigger rounded-10 bg-golden-yellow">
                            <span data-pricing-trigger="" data-target="#table-price-value" data-value="yearly"
                                class="active text-break">Yearly</span>
                            <span data-pricing-trigger="" data-target="#table-price-value" data-value="monthly"
                                class="text-break">Monthly</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Section Title -->
        <!-- Table Main Body -->
        <div class="table-body" id="table-price-value" data-pricing-dynamic="" data-value-active="yearly">
            {{-- <div class="row justify-content-center">
                <!-- Single Table -->
                <div class="col-lg-12 col-sm-6">
                    <div class="border-top pt-10 pb-8" data-aos="fade-up" data-aos-duration="800" data-aos-delay="500"
                        data-aos-once="true">
                        <div class="row">
                            <div class="col-lg-3">
                                <div class="mb-5 mb-lg-0">
                                    <h3 class="font-size-7">Free</h3>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="mb-5 mb-lg-0">
                                    <ul class="list-unstyled font-size-6">
                                        <li class="heading-default-color mb-4">1 Social set</li>
                                        <li class="heading-default-color mb-4">1 user</li>
                                        <li class="heading-default-color mb-4">Simple features</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="pr-lg-15 pr-xl-10 mb-5 mb-lg-0">
                                    <h2 class="mb-0 font-size-11 font-weight-medium">€ <span class="dynamic-value"
                                            data-active="0" data-monthly="0" data-yearly="0"></span></h2>
                                    <p class="mb-0 font-size-5 pr-xl-22">Free</p>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="mb-5 mb-lg-0 mt-4 text-left text-lg-right">
                                    <a class="btn btn-outline-gray-1 btn-2 border-width-2 rounded-5 gr-hover-bg-blue-3 heading-default-color"
                                        href="{{route('newRegister')}}">Sign
                                        Up
                                        For
                                        Free</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Single Table End -->
                <!-- Single Table -->
                <div class="col-lg-12 col-sm-6">
                    <div class="border-top pt-10 pb-8" data-aos="fade-up" data-aos-duration="800" data-aos-delay="500"
                        data-aos-once="true">
                        <div class="row">
                            <div class="col-lg-3">
                                <div class="mb-5 mb-lg-0">
                                    <h3 class="font-size-7">Basic</h3>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="">
                                    <ul class="list-unstyled font-size-6">
                                        <li class="heading-default-color mb-4">1 Social set</li>
                                        <li class="heading-default-color mb-4">3 users</li>
                                        <li class="heading-default-color mb-4">Basic features</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="pr-lg-15 pr-xl-10 mb-5 mb-lg-0">
                                    <h2 class="mb-0 font-size-11 font-weight-medium">€ <span class="dynamic-value"
                                            data-active="15" data-monthly="15" data-yearly="12,5"></span></h2>
                                    <p class="mb-0 font-size-5 pr-xl-22"> Pr. social set pr. month</p>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="mb-5 mb-lg-0 mt-4 text-left text-lg-right">
                                    <a class="btn btn-outline-gray-1 btn-2 border-width-2 rounded-5 gr-hover-bg-blue-3 heading-default-color"
                                        href="{{route('newRegister')}}">Start
                                        14 Days
                                        Free
                                        Trial</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Single Table End -->
                <!-- Single Table -->
                <div class="col-lg-12 col-sm-6">
                    <div class="border-top pt-10 pb-8" data-aos="fade-up" data-aos-duration="800" data-aos-delay="800"
                        data-aos-once="true">
                        <div class="row">
                            <div class="col-lg-3">
                                <div class="mb-5 mb-lg-0">
                                    <h3 class="font-size-7">Pro</h3>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="">
                                    <ul class="list-unstyled font-size-6">
                                        <li class="heading-default-color mb-4">1 Social set</li>
                                        <li class="heading-default-color mb-4">5 users</li>
                                        <li class="heading-default-color mb-4">Unlimited features</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="pr-lg-15 pr-xl-10 mb-5 mb-lg-0">
                                    <h2 class="mb-0 font-size-11 font-weight-medium">€ <span class="dynamic-value"
                                            data-active="40" data-monthly="40" data-yearly="30"></span></h2>
                                    <p class="mb-0 font-size-5 pr-xl-22">
                                        <span></span>Pr. social set pr. month
                                    </p>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="mb-5 mb-lg-0 mt-4 text-left text-lg-right">
                                    <a class="btn btn-outline-gray-1 btn-2 border-width-2 rounded-5 gr-hover-bg-blue-3 heading-default-color"
                                        href="{{route('newRegister')}}">
                                        Start 14 Days Free Trial
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Single Table End -->
            </div> --}}

            <div class="pb-5 pb-md-11 pb-lg-19">
                <div class="container">
                    <div class="row justify-content-center">
                        <!-- Single Table -->
                        <div class="col-xl-4 col-lg-5 col-md-6 col-sm-8">
                            <div class="border rounded-10 text-center px-7 px-lg-16 pt-10 pb-13 gr-hover-2 mb-9 aos-init aos-animate"
                                data-aos="fade-up" data-aos-duration="300" data-aos-once="true">
                                <p class="text-dodger-blue-1 font-size-5 mb-9">Free</p>
                                <h2 class="font-size-11 mb-1">€
                                    <span class="dynamic-value"
                                                        data-active="0" data-monthly="0" data-yearly="0"></span>
                                </h2>
                                <span class="font-size-5 mb-2">free forever</span>
                                <ul class="list-unstyled font-size-5 line-height-50 heading-default-color pt-8">
                                    <li class="heading-default-color mb-7">1 Social set</li>
                                    <li class="heading-default-color mb-7">1 user</li>
                                    <li class="heading-default-color mb-7">Simple features</li>
                                </ul>
                                <div class="pt-7 pt-lg-17">
                                    <a class="btn btn-blue-3 btn-2 rounded-5" href="{{route('newRegister')}}">Start 14 days free trial</a>
                                </div>
                            </div>
                        </div>
                        <!-- End Single Table -->
                        <!-- Single Table -->
                        <div class="col-xl-4 col-lg-5 col-md-6 col-sm-8">
                            <div class="border rounded-10 text-center px-7 px-lg-16 pt-10 pb-13 gr-hover-2 mb-9 aos-init aos-animate"
                                data-aos="fade-up" data-aos-duration="600" data-aos-once="true">
                                <p class="text-dodger-blue-1 font-size-5 mb-9">Basic</p>
                                <h2 class="font-size-11 mb-1">€
                                    <span class="dynamic-value"
                                                        data-active="15" data-monthly="15" data-yearly="12,5"></span>
                                </h2>
                                <span class="font-size-5 mb-2">first 14 days free</span>
                                <ul class="list-unstyled font-size-5 line-height-50 heading-default-color pt-8">
                                    <li class="heading-default-color mb-7">1 Social set</li>
                                    <li class="heading-default-color mb-7">3 users</li>
                                    <li class="heading-default-color mb-7">Basic features</li>
                                </ul>
                                <div class="pt-7 pt-lg-17">
                                    <a class="btn btn-blue-3 btn-2 rounded-5" href="{{route('newRegister')}}">Start 14 days free trial</a>
                                </div>
                            </div>
                        </div>
                        <!-- End Single Table -->
                        <!-- Single Table -->
                        <div class="col-xl-4 col-lg-5 col-md-6 col-sm-8">
                            <div class="border rounded-10 text-center px-7 px-lg-16 pt-10 pb-13 gr-hover-2 mb-9 aos-init aos-animate"
                                data-aos="fade-up" data-aos-duration="900" data-aos-once="true">
                                <p class="text-dodger-blue-1 font-size-5 mb-9">Pro</p>
                                <h2 class="font-size-11 mb-1">€
                                    <span class="dynamic-value"
                                                        data-active="40" data-monthly="40" data-yearly="30"></span>
                                </h2>
                                <span class="font-size-5 mb-2">first 14 days free</span>
                                <ul class="list-unstyled font-size-5 line-height-50 heading-default-color pt-8">
                                    <li class="heading-default-color mb-7">1 Social set</li>
                                    <li class="heading-default-color mb-7">5 users</li>
                                    <li class="heading-default-color mb-7">Unlimited features</li>
                                </ul>
                                <div class="pt-7 pt-lg-17">
                                    <a class="btn btn-blue-3 btn-2 rounded-5" href="{{route('newRegister')}}">Start 14 days free trial</a>
                                </div>
                            </div>
                        </div>
                        <!-- End Single Table -->
                    </div>
                </div>
            </div>
            <div class="pricing-bottom-shape d-none d-sm-block">
                <img src="http://planzer.io/image/svg/footer-shape.svg" alt="" data-aos="fade-left" data-aos-delay="500"
                    data-aos-once="true">
            </div>
        </div>
        <!-- Table Main Body -->
    </div>

</div>
<!-- End pricingOne Area -->



<!-- end pricing area about page -->
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog" style="max-width:90%;width:100%;">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-body" style="padding:0px;height:450px;">
                <iframe width="100%" style="padding:0px;border-radius:8px;border:0px;" height="450"
                    src="https://www.youtube.com/embed/QZazlSLaFww">
                </iframe>
            </div>
        </div>

    </div>
</div>
@endsection
@push('js')
@endpush
