@extends('layouts.simple.master')
@section('title', 'Company Detail')

@section('css')
@endsection

@section('style')
@endsection

@section('breadcrumb-title')
    <h3>Company Detail</h3>
@endsection

@section('breadcrumb-items')
    <li class="breadcrumb-item">Company</li>
    <li class="breadcrumb-item active">Company Detail's</li>
@endsection

@section('content')
    <div>
        @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
        @endif
        @if (Session::has('error'))
            <div class="alert alert-danger alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>{!! Session('error') !!}</strong>
            </div>
        @endif
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">

                    <div class="card-body">
                        <div class="row" style="display:flex; align-items:center; justify-content: space-between;">
                            <h3>Company Detail</h3>
                            <a href="{{route('create.company')}}" class="btn btn-primary">Add New Company</a>
                        </div>
                        <br>
                        <form class="needs-validation" novalidate="" method="POST">
                            @csrf

                            <div class="row">
                                <div class="col-md-6 mb-3">
                                    <label for="first_name"> Company name</label>
                                    <input readonly class="form-control" readonly name="first_name" id="" type="text" placeholder="First Name" required="">
                                    <div class="invalid-feedback">Please provide a valid state.</div>
                                </div>
                                <div class="col-md-6 mb-3">
                                    <label for="last_name">Company Type</label>
                                    <input readonly class="form-control" value="{{ old('last_name') }}" id="" name="last_name" type="text" placeholder="Last Name" required="">
                                    <div class="invalid-feedback">Please provide a Country</div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 mb-3">
                                    <label for="validationCustomUsername">Email</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend"><span class="input-group-text" id="inputGroupPrepend">@</span></div>
                                        <input readonly id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">
                                        <div class="invalid-feedback">Please Enter a Email.</div>
                                    </div>
                                </div>
                                <div class="col-md-6 mb-3">
                                    <label for="validationCustom01">Company Address</label>
                                    <input readonly id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                                    <div class="valid-feedback">Looks good!</div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 mb-3">
                                    <label for="password">Postal code</label>
                                    <input readonly id="password" type="text" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
                                    <div class="valid-feedback">Looks good!</div>
                                </div>

                                <div class="col-md-6 mb-3">
                                    <label for="address">City</label>
                                    <input readonly class="form-control" value="{{ old('address') }}" id="" name="address" type="text" placeholder="Address" required="">
                                    <div class="invalid-feedback">Please provide a Address.</div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 mb-3">
                                    <label for="city">State</label>
                                    <input readonly class="form-control" name="city" id="" type="text" placeholder="City" value="{{ old('city') }}" required="">
                                    <div class="invalid-feedback">Please provide a valid city.</div>
                                </div>

                                <div class="col-md-6 mb-3">
                                    <label for="postal_code">Country</label>
                                    <input readonly class="form-control" name="postal_code" id="" type="text" placeholder="City" value="{{ old('postal_code') }}" required="">
                                    <div class="invalid-feedback">Please provide a valid Postal Code.</div>
                                </div>

                            </div>

                            <div class="row">
                                <div class="col-md-6 mb-3">
                                    <label for="state">Phone number</label>
                                    <input readonly class="form-control" value="{{ old('state') }}" name="state" id="" type="text" placeholder="State" required="">
                                    <div class="invalid-feedback">Please provide a valid state.</div>
                                </div>
                                <div class="col-md-6 mb-3">
                                    <label for="country">Website</label>
                                    <input readonly class="form-control" value="{{ old('country') }}" id="" name="country" type="text" placeholder="Country" required="">
                                    <div class="invalid-feedback">Please provide a Country</div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="{{asset('assets/js/form-validation-custom.js')}}"></script>
@endsection
