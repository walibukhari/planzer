<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Planzer User Portal.">
    <meta name="keywords" content="User Portal">
    <meta name="author" content="pixelstrap">
    <link rel="icon" href="{{asset('assets/images/favicon.png')}}" type="image/x-icon">
    <link rel="shortcut icon" href="{{asset('assets/images/favicon.png')}}" type="image/x-icon">
    <title>@yield('title')</title>
    <!-- Google font-->
    <link href="https://fonts.googleapis.com/css?family=Rubik:400,400i,500,500i,700,700i&amp;display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i,900&amp;display=swap" rel="stylesheet">
    @include('userPortal.partials.headerScript')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css" integrity="sha512-vKMx8UnXk60zUwyUnUPM3HbQo8QfmNx7+ltw8Pm5zLusl1XIfwcxo8DbWCqMGKaWeNxWA8yrx5v3SaVpMvR3CA==" crossorigin="anonymous" />
    @stack('style')
    <style>
        .page-wrapper.compact-wrapper .page-body-wrapper.sidebar-icon div.sidebar-wrapper .sidebar-main .sidebar-links > li .sidebar-link.active span {
            color: #3f51DB;
        }
        .page-wrapper.compact-wrapper .page-body-wrapper.sidebar-icon div.sidebar-wrapper .sidebar-main .sidebar-links > li .sidebar-link.active {
            -webkit-transition: all 0.5s ease;
            transition: all 0.5s ease;
            position: relative;
            margin-bottom: 10px;
            color: #3f51DB;
            background-color: #7366FF1f;
        }
    </style>
    <style>
        .page-wrapper.compact-wrapper .page-body-wrapper.sidebar-icon div.sidebar-wrapper .sidebar-main .sidebar-links > li .sidebar-link.active span {
            color: #3f51DB;
        }
        .page-wrapper.compact-wrapper .page-body-wrapper.sidebar-icon div.sidebar-wrapper .sidebar-main .sidebar-links > li .sidebar-link.active {
            -webkit-transition: all 0.5s ease;
            transition: all 0.5s ease;
            position: relative;
            margin-bottom: 10px;
            color: #3f51DB;
            background-color: #7366FF1f;
        }
    </style>
    <link rel="stylesheet" href="{{url('/')}}/assets/css/globalfix.css">
</head>
<body>
<!-- tap on top starts-->
<div class="tap-top"><i data-feather="chevrons-up"></i></div>
<!-- tap on tap ends-->
<!-- page-wrapper Start-->
<div class="page-wrapper compact-wrapper" id="pageWrapper">
    <!-- Page Header Start-->
@include('userPortal.partials.header')
<!-- Page Header Ends   -->
    <!-- Page Body Start-->
    <div class="page-body-wrapper sidebar-icon">
        <!-- Page Sidebar Start-->
    @include('userPortal.partials.sidebar')
    <!-- Page Sidebar Ends-->
        <div class="page-body">
        @yield('content')
        </div>
        <!-- footer start-->
        @include('userPortal.partials.footer')
    </div>
</div>
<!-- latest jquery-->
@include('userPortal.partials.footerScript')
@stack('js')
</body>
</html>
