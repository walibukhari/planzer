@extends('userPortal.layout.app')
@section('title', 'Facebook')

@section('css')
@endsection

@push('style')
    <style>
        .custom_ul{
            display:flex;
            justify-content: space-between;
        }
        .custom_ul li{
            cursor:pointer;
            border: 1px solid #efefef;
            padding:10px 12px;
        }
        .panel-default{
            background: rgb(255, 255, 255);
            box-shadow: 1px 1px 20px 2px rgba(0, 0, 0, 0.055);

            /* padding-top:20px; */
            /* padding: 20px; */
            margin-bottom: 30px;
            /* height:240px; */
            border-radius:11px;
            padding-bottom: 0px;
            overflow: hidden;
        }
        .panel-default:hover{
            box-shadow: 1px 1px 8px 2px rgba(0, 0, 0, 0.137);
            transform: scale(1.03);
        }

        /* .panel-body > img {
            width:50px;
        } */
        .social-icon{
            height: 80px;
        }

        .actionbtn{
            width: 100%;
            border-top-right-radius:0px;
            border-top-left-radius:0px;

        }
        .cardtitle{
            border-bottom: 1px solid lightgray;
            width:80%;
            margin: 0 auto ;
            padding: 10px 0px;
            font-size:18px;
        }
        .page-head{
            display: flex;
            justify-content: space-between;
            margin-bottom: 50px;
        }
        .activeted_item {
            background:#7366ff !important;
            color:#fff !important;
        }
        [v-cloak] {
            display: none;
        }

        .lds-roller {
            display: inline-block;
            position: relative;
            width: 80px;
            height: 80px;
        }
        .lds-roller div {
            animation: lds-roller 1.2s cubic-bezier(0.5, 0, 0.5, 1) infinite;
            transform-origin: 40px 40px;
        }
        .lds-roller div:after {
            content: " ";
            display: block;
            position: absolute;
            width: 7px;
            height: 7px;
            border-radius: 50%;
            background: #3F51DB;
            margin: -4px 0 0 -4px;
        }
        .lds-roller div:nth-child(1) {
            animation-delay: -0.036s;
        }
        .lds-roller div:nth-child(1):after {
            top: 63px;
            left: 63px;
        }
        .lds-roller div:nth-child(2) {
            animation-delay: -0.072s;
        }
        .lds-roller div:nth-child(2):after {
            top: 68px;
            left: 56px;
        }
        .lds-roller div:nth-child(3) {
            animation-delay: -0.108s;
        }
        .lds-roller div:nth-child(3):after {
            top: 71px;
            left: 48px;
        }
        .lds-roller div:nth-child(4) {
            animation-delay: -0.144s;
        }
        .lds-roller div:nth-child(4):after {
            top: 72px;
            left: 40px;
        }
        .lds-roller div:nth-child(5) {
            animation-delay: -0.18s;
        }
        .lds-roller div:nth-child(5):after {
            top: 71px;
            left: 32px;
        }
        .lds-roller div:nth-child(6) {
            animation-delay: -0.216s;
        }
        .lds-roller div:nth-child(6):after {
            top: 68px;
            left: 24px;
        }
        .lds-roller div:nth-child(7) {
            animation-delay: -0.252s;
        }
        .lds-roller div:nth-child(7):after {
            top: 63px;
            left: 17px;
        }
        .lds-roller div:nth-child(8) {
            animation-delay: -0.288s;
        }
        .lds-roller div:nth-child(8):after {
            top: 56px;
            left: 12px;
        }
        @keyframes lds-roller {
            0% {
                transform: rotate(0deg);
            }
            100% {
                transform: rotate(360deg);
            }
        }
        .all_sets{
            display: flex;
        }
        .all_sets > li {
            margin-right:10px;
            padding: 10px 15px;
            border: 1px solid #efefef;
            border-radius: 6px;
            cursor:pointer;
        }
        .activatedClass {
            background:#3F51DB;
            color:#fff;
        }
    </style>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css" rel="stylesheet" />
@endpush

@section('breadcrumb-title')
    <h3>Facebook Setup</h3>
@endsection

@section('breadcrumb-items')
    <li class="breadcrumb-item">Users</li>
    <li class="breadcrumb-item active">Add New User</li>
@endsection

@section('content')
    <br>
    <div id="socialSets" v-cloak>
        <div>
            @if (session('status'))
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
            @endif
            @if (session('password_status'))
                <div class="alert alert-success" role="alert">
                    {{ session('password_status') }}
                </div>
            @endif
            @if (Session::has('error'))
                <div class="alert alert-danger alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{!! Session('error') !!}</strong>
                </div>
            @endif
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </div>
        <div v-if="ifLoader" style="display:flex;justify-content: center;height:auto;">
            <div class="lds-roller"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
        </div>
        <div v-else class="container-fluid">
            <div class="page-head" >
                <div class=" set-select">
                    <select class="form-control" name="" @change="changeSocialSets()" v-model="set_id" style="width:100%;">
                        <option value="">Select Set</option>
                        <option value="all">All</option>
                        <option v-for="sets in social_sets" :value="sets.id">@{{ sets.name }}</option>
                    </select>
                </div>
                <div class="btn btn-primary addnew" data-toggle="modal" data-target="#addnewset">
                    <span v-if="add_new_set">Add New Set</span>
                    <span v-else>Update Set</span>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4" v-for="social in social_set_data">
                    <div class="panel panel-default for-light">
                        <div class="panel-body text-center">
                            <div class="cardtitle"><strong>@{{ social.name }}</strong></div>
                            <br>
                            <img :src="social.img" class="social-icon" />
                            <br><br>
                            <br>
                            <a href="javascript:;" @click="connectToSocialMedia(social.code)" class="btn btn-primary btn-block actionbtn" :style="{backgroundColor:social.color}">@{{ social.text }}</a>
                        </div>
                    </div>
                    <div class="panel panel-default for-dark" style="background-color:rgb(22, 22, 22);">
                        <div class="panel-body text-center">
                            <div class="cardtitle"><strong>@{{ social.name }}</strong></div>
                            <br>  <img :src="social.img" class="social-icon" /><br><br>
                            <br>
                            <a href="javascript:;" @click="connectToSocialMedia(social.code)" :style="{backgroundColor:social.color}" class="btn btn-block actionbtn">@{{social.text}}</a>
                        </div>
                    </div>
                </div>
                {{--@include('social.social_sets')--}}
            </div>
        </div>
        @include('social.modal')
    </div>
@endsection

@push('js')
    <script src="{{asset('assets/js/form-validation-custom.js')}}"></script>
    <script src="https://cdn.jsdelivr.net/npm/vue@2.6.12"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/vue-resource/0.6.1/vue-resource.min.js" integrity="sha512-NuUIe6TWdEivPTcxnihx2e6r2xQFEFPrJfpdZWoBwZF6G51Rphcf5r/1ZU/ytj4lyHwLd/YGMix4a5LqAN15XA==" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
    <script>
        var success_message = '{{session()->has('success_message')}}';
        var show_success_message = '{{session()->get('success_message')}}';
        var error_message = '{{session()->has('error_message')}}';
        var show_error_message = '{{session()->get('error_message')}}';
        new Vue({
            el: '#socialSets',
            data: {
                add_new_set:true,
                isActiveClass:false,
                ifLoader:true,
                twitter_connect:'connect',
                message: 'Hello Vue!',
                social_set_name:'',
                user_id:'',
                social_sets:[],
                social_set_data:[
                    {
                        code:'twitter',
                        img:'/assets/icons/twitter.png',
                        name:'Twitter',
                        text:'connect',
                        url:'',
                        color:'#4433ff'
                    },
                    {
                        code:'facebook',
                        img:'/assets/icons/facebook.png',
                        name:'Facebook',
                        text:'connect',
                        url:'',
                        color:'#4433ff'
                    },
                    {
                        code:'instagram',
                        img:'/assets/icons/instagram.png',
                        name:'Instagram',
                        text:'connect',
                        url:'',
                        color:'#4433ff'
                    },
                    {
                        code:'youtube',
                        img:'/assets/icons/youtube.png',
                        name:'Youtube',
                        text:'connect',
                        url:'',
                        color:'#4433ff'
                    },
                    {
                        code:'link_den',
                        img:'/assets/icons/linkedin.png',
                        name:'LinkedIn',
                        text:'connect',
                        url:'',
                        color:'#4433ff'
                    },
                    {
                        code:'tiktok',
                        img:'/assets/icons/tiktok.png',
                        name:'Tiktok',
                        text:'connect',
                        color:'#4433ff'
                    }
                ],
                set_id:'',
                username:'',
                password:''
            },
            created() {
                console.log('success_message');
                console.log(success_message);
                console.log('error_message');
                console.log(error_message);
                if(success_message) {
                    this.twitter_connect = 'Connected';
                    toastr.success(show_success_message);
                }
                if(error_message) {
                    toastr.error(show_error_message);
                }
            },
            methods: {
                saveInstagramCred: function () {
                    console.log('this.username && this.password');
                    console.log(this.username);
                    console.log(this.password);
                    $(this.$refs.instagramCredit).modal('hide');
                    toastr.success('Instagram Username Password Set Click Connect Button To Proceed ... !');
                },
                connectToSocialMedia: function (code) {
                    if(!this.set_id) {
                        toastr.warning('Please select social set and then continue ... !');
                    } else {
                        if(code == 'instagram') {
                            if(this.username && this.password) {
                                this.ifLoader = true;
                                let url = '{{route('connectToInsta')}}';
                                url = url + '?set_id=' + this.set_id + '&username=' + this.username + '&password=' + this.password;
                                this.$http.get(url).then((response) => {
                                    console.log('response');
                                    console.log(response);
                                    if (response.data.status) {
                                        this.ifLoader = false;
                                        // window.location.href = response.data.redirect_url
                                        toastr.success(response.data.message)
                                    }
                                    if (!response.data.status) {
                                        this.ifLoader = false;
                                        toastr.error(response.data.message)
                                    }
                                }).catch((error) => {
                                    this.ifLoader = false;
                                    console.log('error');
                                    console.log(error);
                                });
                            } else {
                                $(this.$refs.instagramCredit).modal('show');
                            }
                        } else {
                            toastr.success('Social set selected ... !');
                            let url;
                            if(code == 'twitter'){
                                url = '{{route('twitter.login')}}';
                            }
                            if(code == 'link_den'){
                                url = '{{route('link_dinLogin')}}';
                            }
                            url = url+'?set_id='+this.set_id;
                            this.$http.get(url).then((response) => {
                                console.log('response');
                                console.log(response);
                                if(response.data.status) {
                                    window.location.href=response.data.redirect_url
                                }
                            }).catch((error) => {
                                console.log('error');
                                console.log(error);
                            });
                        }
                    }
                },
                changeSocialSets: function () {
                    this.ifLoader = true;
                    if(this.set_id == 'all') {
                        this.social_set_data = []
                        this.social_set_data.push(
                            {
                                code:'twitter',
                                img:'/assets/icons/twitter.png',
                                name:'Twitter',
                                text:'connect',
                                url:'',
                                color:'#4433ff'
                            },
                            {
                                code:'facebook',
                                img:'/assets/icons/facebook.png',
                                name:'Facebook',
                                text:'connect',
                                url:'',
                                color:'#4433ff'
                            },
                            {
                                code:'instagram',
                                img:'/assets/icons/instagram.png',
                                name:'Instagram',
                                text:'connect',
                                url:'',
                                color:'#4433ff'
                            },
                            {
                                code:'youtube',
                                img:'/assets/icons/youtube.png',
                                name:'Youtube',
                                text:'connect',
                                url:'',
                                color:'#4433ff'
                            },
                            {
                                code:'link_den',
                                img:'/assets/icons/linkedin.png',
                                name:'LinkedIn',
                                text:'connect',
                                url:'',
                                color:'#4433ff'
                            },
                            {
                                code:'tiktok',
                                img:'/assets/icons/tiktok.png',
                                name:'Tiktok',
                                text:'connect',
                                url:'',
                                color:'#4433ff'
                            }
                        );
                        this.ifLoader = false;
                    } else {
                        this.getSocialSetAgainstId(this.set_id);
                    }
                },
                getSocialSetAgainstId: function (id) {
                    console.log('social set id');
                    console.log(id);
                    this.add_new_set = false;
                    this.getConnectedSocialAccount(id);
                },
                getConnectedSocialAccount: function (id) {
                    let url = '{{route('connectedAccounts',':id')}}';
                    url = url.replace(':id',id);
                    let img , name , text;
                    let arr = [];
                    this.$http.get(url).then((response) => {
                        console.log('response.data.data');
                        console.log(response.data.social_set);
                        this.social_set_name = response.data.social_set.name;
                        console.log(response.data.data.length);
                        if(response.data.data.length == 0) {
                            console.log('no data found ..')
                            this.social_set_data = [];
                            this.social_set_data.push(
                                {
                                    code:'twitter',
                                    img:'/assets/icons/twitter.png',
                                    name:'Twitter',
                                    text:'connect',
                                    url:'',
                                    color:'#4433ff'
                                },
                                {
                                    code:'facebook',
                                    img:'/assets/icons/facebook.png',
                                    name:'Facebook',
                                    text:'connect',
                                    url:'',
                                    color:'#4433ff'
                                },
                                {
                                    code:'instagram',
                                    img:'/assets/icons/instagram.png',
                                    name:'Instagram',
                                    text:'connect',
                                    url:'',
                                    color:'#4433ff'
                                },
                                {
                                    code:'youtube',
                                    img:'/assets/icons/youtube.png',
                                    name:'Youtube',
                                    text:'connect',
                                    url:'',
                                    color:'#4433ff'
                                },
                                {
                                    code:'link_den',
                                    img:'/assets/icons/linkedin.png',
                                    name:'LinkedIn',
                                    text:'connect',
                                    url:'',
                                    color:'#4433ff'
                                },
                                {
                                    code:'tiktok',
                                    img:'/assets/icons/tiktok.png',
                                    name:'Tiktok',
                                    text:'connect',
                                    url:'',
                                    color:'#4433ff'
                                }
                            );
                        } else {
                            console.log('data found ..');
                            arr = [];
                            response.data.data.map((data) => {
                                this.social_set_data.map((d) => {
                                    if (d.code === data.social_network) {
                                        arr.push({code: d.code});
                                    }
                                });
                            });
                            this.social_set_data.map((data) => {
                                data.text = 'connect';
                                data.color = '#4433ff'
                                arr.map((d) => {
                                    if(data.code == d.code) {
                                        data.text = 'Connected';
                                        data.color = '#3f901d !important'
                                    }
                                });
                            });
                        }
                        this.ifLoader = false;
                    }).catch((error) => {
                        console.log('error');
                        console.log(error);
                    });
                },
                getSocialSets: function () {
                    let url = '{{route('getSocialSets')}}';
                    this.$http.get(url).then((response) => {
                        console.log('response');
                        console.log(response.data);
                        this.social_sets = response.data;
                    }).catch((error) => {
                        console.log('error');
                        console.log(error);
                    });
                },
                addNewSet: function () {
                    let _token = '{{csrf_token()}}';
                    let set_name = this.social_set_name;
                    let formData = new FormData();
                    console.log('this.social_items');
                    console.log(this.social_items);
                    formData.append('_token',_token);
                    formData.append('name',set_name);
                    let url = '{{route('addPostSets')}}';
                    this.$http.post(url , formData).then((response) => {
                        console.log('response');
                        console.log(response.data);
                        if(response.data.status) {
                            toastr.success(response.data.message);
                            $(this.$refs.addnewset).modal('hide');
                            this.social_set_name = '';
                            this.getSocialSets();
                        }
                    }).catch((error) => {
                        console.log('error');
                        console.log(error);
                        if(error.data.errors) {
                            toastr.error(error.data.errors.name);
                            $(this.$refs.addnewset).modal('hide');
                            this.social_set_name = '';
                        }
                    });
                },
                updateSets: function () {
                    let _token = '{{csrf_token()}}';
                    let set_name = this.social_set_name;
                    let formData = new FormData();
                    console.log('this.social_items');
                    console.log(this.social_items);
                    formData.append('_token',_token);
                    formData.append('id',this.set_id);
                    formData.append('name',set_name);
                    let url = '{{route('updateSets')}}';
                    this.$http.post(url , formData).then((response) => {
                        console.log('response');
                        console.log(response.data);
                        if(response.data.status) {
                            toastr.success(response.data.message);
                            $(this.$refs.addnewset).modal('hide');
                            this.social_set_name = '';
                            this.getSocialSets();
                        }
                    }).catch((error) => {
                        console.log('error');
                        console.log(error);
                        if(error.data.errors) {
                            toastr.error(error.data.errors.name);
                            $(this.$refs.addnewset).modal('hide');
                            this.social_set_name = '';
                        }
                    });
                }
            },
            mounted() {
                console.log('social sets mounted started ... ');
                this.getSocialSets();
                let that = this;
                setTimeout(() => {
                    that.ifLoader = false
                },1000);
            }
        });
    </script>
@endpush
