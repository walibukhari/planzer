@extends('layouts.simple.master')
@section('title', 'All Users')

@section('css')
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/vendors/datatables.css')}}">
@endsection

@section('style')
@endsection

@section('breadcrumb-title')
<h3>All Users</h3>
@endsection

@section('breadcrumb-items')
<li class="breadcrumb-item">Users</li>
<li class="breadcrumb-item active">All Users</li>
@endsection

@section('content')
<div class="container-fluid">
	<div class="row">
		<!-- Zero Configuration  Starts-->
		<div class="col-sm-12">
			<div class="card">
				<div class="card-header">
				<!-- 	<h5>Zero Configuration</h5>
					<span>DataTables has most features enabled by default, so all you need to do to use it with your own tables is to call the construction function:<code>$().DataTable();</code>.</span><span>Searching, ordering and paging goodness will be immediately added to the table, as shown in this example.</span> -->
				</div>
				<div class="card-body">
					<div class="table-responsive">
						<table class="display" id="basic-1">
							<thead>
								<tr>
									<th>Name</th>
									<th>Description</th>
									<th>Status</th>
									<th>Default</th>									
									<th>Actions</th>
								</tr>
							</thead>
							<tbody>
								@foreach($package as $packages)
									<tr>
										<td>{{$packages->name}}</td>
										<td>{{$packages->description}}</td>
										@if($packages->status==1)
										<td > <font style="color: green"> Active</font></td>
										@else
										<td><font style="color: red">Inactive</font></td>
										@endif
												
										@if($packages->is_default==1)
										<td > <font style="color: green"> Yes</font></td>
										@else
										<td><font style="color: red">No</font></td>
										@endif										
										<td>
											<a href="{{route('edit.package',[$packages->id])}}" class="btn btn-primary">Edit</a>
											<a href="{{route('delete.package',[$packages->id])}}" class="btn btn-danger" onclick="return confirm('Are you sure you want to delete this package?');">Delete</a>
										</td>
									</tr>
								@endforeach
							</tbody>
							<tfoot>
								<tr>
									<th>Name</th>
									<th>Description</th>
									<th>Status</th>
									<th>Default</th>									
									<th>Actions</th>
								</tr>
							</tfoot>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('script')
<script src="{{asset('assets/js/datatable/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/js/datatable/datatables/datatable.custom.js')}}"></script>
@endsection