<?php

namespace App\Http\Controllers;

use App\Models\SchedulePost;
use Illuminate\Http\Request;

class SchedulePostController extends Controller
{
    public function schedulePost($request,$social_network) {
        return SchedulePost::schedulePostCreate($request->all() , $social_network);
    }
}
