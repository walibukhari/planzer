<?php

namespace App\Http\Controllers\Social;

use App\Http\Controllers\Controller;
use App\Models\SocialAccount;
use App\Models\SocialAccountManager;
use App\Traits\General;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Happyr\LinkedIn\LinkedIn;
use Illuminate\Support\Facades\Auth;

class LinkDinController extends Controller
{

    use General;
    private $linkedin;
    private $accountId;
    public $linkedin_secret;
    public $linkedin_key;

    public function __construct(){
        $this->linkedin_secret = "P0PZl3eGx8eI185X";
        $this->linkedin_key = "77kar07itxs573";
        if(!$this->linkedin_key || !$this->linkedin_secret) {
            abort(400, 'Please add your twitter credentials to the .env file.');
        }
        $checkSession = session()->get('scheduled');
        if($checkSession != 1) {
            $this->linkedin = new \Phillipsdata\LinkedIn\LinkedIn($this->linkedin_key, $this->linkedin_secret, route('link_din.callback'));
        }
    }

    public function postToLinkedIn(){
        $link_din = SocialAccountManager::where('social_network','=',SocialAccount::LINK_DEN)->first();
        return view('social.link_din.link_din' , [
            'account' => $link_din
        ]);
    }

    public function connectToLinkDin(){
        $link_din = SocialAccountManager::where('social_network','=',SocialAccount::LINK_DEN)->first();
        return view('social.link_din.create' , [
            'link_din' => $link_din
        ]);
    }

    public function login(Request $request){
        session()->put('set_id',$request->set_id);
        $scope = "r_emailaddress r_liteprofile w_member_social";
        $liLoginUrl = $this->linkedin->getPermissionUrl( $scope );
        return collect([
           'status' => true,
           'redirect_url' => $liLoginUrl
        ]);
    }

    public function getToken($request){
        try {
            if($code = $request->code) {
                $tokenResponse = $this->linkedin->getAccessToken($code);
                if($tokenResponse->status() == 200) {
                    $tokenResponse = $tokenResponse->response();
                    return $tokenResponse->access_token;
                } else {
                    return redirect(route("connectToLinkDin", array('auth' => true)));
                }
            } else {
                return redirect(route("connectToLinkDin", array('auth' => true)));
            }
        } catch (\Exception $e) {
            return redirect(route("connectToLinkDin", array('auth' => true)));
        }
    }

    public function setToken($token){
        $token = (object)array(
            "access_token" => $token
        );
        $this->linkedin->setAccessToken($token);
    }

    public function getCurrentUser(){
        try {
            $profile = $this->linkedin->getUser();
            if($profile->status() == 200){
                return $profile->response();
            }else{
                return false;
            }
        } catch (\Exception $e) {
            return false;
        }
    }

    public function getCompanies(){
        try {
            $companies = $this->linkedin->getCompanies();
            if($companies->status() == 200){
                return $companies->response();
            } else {
                return false;
            }
        } catch (\Exception $e) {
            return false;
        }
    }

    public function getAvatar($user) {
        if (isset($user->profilePicture)) {
            $picture = json_decode(json_encode($user->profilePicture), true);
            if (isset($picture['displayImage~']['elements'][0]['identifiers']['0']['identifier'])) {
                $avatar = $picture['displayImage~']['elements'][0]['identifiers']['0']['identifier'];
                return $avatar;
            }
        }
        return 'assets/images/linkedin.png';
    }

    public function linkDinCallback(Request $request) {
        $set_id = session()->get('set_id');
        $token = $this->getToken($request);
        $this->setToken($token);
        $user = (object)$this->getCurrentUser();
        $firstName_param = (array)$user->firstName->localized;
        $lastName_param = (array)$user->lastName->localized;

        $firstName = reset($firstName_param);
        $lastName = reset($lastName_param);
        $fullname = $firstName." ".$lastName;
        $user_image = $this->getAvatar($user);
        $data = [
            'set_id' => $set_id,
            'ids' => ids(),
            'social_network' => SocialAccount::LINK_DEN,
            'category' => 'profile',
            'pid' => $user->id,
            'user_id' => isset(Auth::user()->id) ? Auth::user()->id : Auth::guard('admin')->user()->id,
            'can_post' => 1,
            'name' => $firstName,
            'username' => $fullname,
            'token' => $token,
            'avatar' => $user_image,
            'url' => NULL,
            'data' => NULL,
            'proxy' => NULL,
            'status' => 200,
            'access_token' => $token,
        ];
        $checkLinkedIn = SocialAccountManager::where('social_network','=',SocialAccount::LINK_DEN)->where('set_id','=',$set_id)->exists();
        if($checkLinkedIn) {
            SocialAccountManager::updateAccount($data);
            $message = 'Your LinkedIn Account Already Connected.......';
        } else {
            SocialAccountManager::createAccount($data);
            $message = 'LinkedIn Connected Successfully.......';
        }
        return redirect()->route('socialMedia')->with('success_message',$message);
    }

    public function uploadLinkedInFile($path,$file){
        if($file){
            $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
            $name = $this->generateRandomString().'-'.$timestamp.'.png';
            $file->move($path,$name);
            return $path.'/'.$name;
        }
    }

    public function postLinkedIn($request) {
        \Log::info('media area upload image area .... !');
        $account = SocialAccountManager::where('set_id','=',$request['account_set_id'])->where('social_network','=',SocialAccount::LINK_DEN)->first();
        $this->accountId = $account->pid;
        $post_type = 'media';
        $this->setToken($account->access_token);
        $author = ($account->category == 'profile') ? "urn:li:person:".$account->pid : "urn:li:organization:".$account->pid;
        $caption = $request['description'];
        $counter = $request['counter'];
        switch($post_type) {
            case 'media':
                $medias = array();
                for($i = 0; $i < $counter; $i++) {
                    if(isset($request['schedule_type']) && $request['schedule_type'] == 1) {
                        $media = $request["images"];
                    } else {
                        $media = $request["images-".$i];
                    }
                    $media = $this->uploadLinkedInFile('uploads/linkedin', $media);
                    $media = $this->linkedin->upload(public_path($media), $author);
                    $medias[] = $media;
                }
                $content = [
                    'lifecycleState' => 'PUBLISHED',
                    'specificContent' => [
                        'com.linkedin.ugc.ShareContent' => [
                            'shareCommentary' => [
                                'text' => $caption
                            ],
                            "shareMediaCategory" => "IMAGE",
                            "media" => [
                                [
                                    "status" => "READY",
                                    "description"=> [
                                        "text" => $caption
                                    ],
                                    "media" => $media,
                                    "title" => [
                                        "text" => $caption
                                    ]
                                ]
                            ]
                        ]
                    ],
                    'visibility' => ['com.linkedin.ugc.MemberNetworkVisibility' => 'PUBLIC']
                ];
                break;
            case 'text':
                $content = [
                    'lifecycleState' => 'PUBLISHED',
                    'specificContent' => [
                        'com.linkedin.ugc.ShareContent' => [
                            'shareCommentary' => [
                                'text' => $caption
                            ],
                            "shareMediaCategory" => "NONE"
                        ]
                    ],
                    'visibility' => ['com.linkedin.ugc.MemberNetworkVisibility' => 'PUBLIC']
                ];
                break;
        }
        $response = (object)$this->linkedin->share($content, $author);
        $response = $response->response();
        if(isset($response->id)) {
            return collect([
                'status' => true,
                'data' => $response
            ]);
        }
    }
}
