

@extends('userPortal.layout.app')
@section('title', 'Facebook')

@section('css')
@endsection

@push('style')
    <style>
        .panel-default{
            background: #fff;
            padding: 20px;
            margin-bottom: 30px;
            border-radius: 15px;
        }
        .panel-body > img {
            width:50px;
        }
        .items{
            border: 1px solid #b3b7bb;
            border-radius: 15px;
            padding: 15px;
            padding-bottom: 0px;
            margin-top: 15px;
        }
        .form-tabs li a{
            color: #b3b7bb !important;
            font-size: 18px;

        }
        .btn-primary:focus{
            background-color: #1a202c !important;
            color: #fff !important;
            border: 0 !important;
        }
        .form-tabs li{
            display: inline;
            padding: 15px;
        }
        .plan-btn{
            background-color: #0c0c0c !important;
            border: 0;
        }
        .plan-btn:hover{
            background-color: #1a202c !important;
            border: 0px !important;
        }
        .renew-btn{
            background-color: transparent !important;
            border: 1px solid #0c0c0c !important;
            margin-left: 10px;
            color: #1a202c !important;
        }
        .renew-btn:hover{
            background-color: #0c0c0c !important;
            color: #fff !important;
        }
        /*.renew-btn{*!*/
        /*!*    background-color: transparent !important;*!*/
        /*!*    border: 1px solid #fff !important;*!*/
        /*!*    margin-left: 10px;*!*/
        /*!*    color: #fff !important;*!*/
        /*!*}*/
        .custom-checkbox .custom-control-label::before {
            border-radius: 25px;
        }
        .custom-control-input:checked ~ .custom-control-label::before{
            border: 0px;
            background-color: #1dc116;
        }
        #right-panel{
            height: 615px;
        }
        .form-tabs .active{
            border-bottom: 3px solid #1dc116 !important;
        }
        .save-btn{
            padding-left: 65px;
            padding-right: 65px;
            padding-top:10px;
            padding-bottom: 10px;
            border: 0px !important;
            margin-top: 20px;
            background-color: #1dc116 !important;
        }
        .save-btn:hover{
            background-color: #1dc116 !important;
        }
        .left-panel{
            margin-right: 3%;
        }
        @media only screen and (max-width: 900px) {
            .form-tabs li a {
                font-size: 14px;
            }
            .username{
                font-size: 12px;
                padding-left: 5px;
            }
            .profile{
                font-size: 10px;
                padding-left: 5px;
            }
        }
        @media only screen and (max-width: 768px) {
            .form-tabs li a {
                font-size: 14px;
            }
            .username{
                font-size: 15px;
                padding-left: 0px;
            }
            .profile{
                font-size: 13px;
                padding-left: 0px;
            }
            .items{
                margin-right: 45%;
            }
            .item-inner{
                margin: auto;
            }
        }
    </style>
@endpush

@section('breadcrumb-title')
    <h3>Facebook Setup</h3>
@endsection

@section('breadcrumb-items')
    <li class="breadcrumb-item">Users</li>
    <li class="breadcrumb-item active">Add New User</li>
@endsection

@section('content')

    <div class="p-5">
        <div class="row">
            <div class="col-md-3 card left-panel panel-default">
                <h3>Plandzer</h3>
                <p>Home | Post Preview</p>
                <hr width="80%" style="float: left">
                <br>
                <div class="col-md-11">
                    <div class="items row">
                        <div class="item-inner">
                            <img width="40px" src="https://www.pngkey.com/png/full/114-1149878_setting-user-avatar-in-specific-size-without-breaking.png">
                        </div>
                        <div class="col-md-8 col-sm-6">
                            <h6 class="username">Username</h6>
                            <p class="profile">Instagram user</p>
                        </div>
                    </div>
                    <div class="items row">
                        <div class="item-inner">
                            <img width="40px" src="https://www.pngkey.com/png/full/114-1149878_setting-user-avatar-in-specific-size-without-breaking.png">
                        </div>
                        <div class="col-md-8 col-sm-6">
                            <h6 class="username">Username</h6>
                            <p class="profile">Instagram user</p>
                        </div>
                    </div>
                    <div class="items row">
                        <div class="item-inner">
                            <img width="40px" src="https://www.pngkey.com/png/full/114-1149878_setting-user-avatar-in-specific-size-without-breaking.png">
                        </div>
                        <div class="col-md-8 col-sm-6">
                            <h6 class="username">Username</h6>
                            <p class="profile">Instagram user</p>
                        </div>
                    </div>
                    <div class="items row">
                        <div class="item-inner">
                            <img width="40px" src="https://www.pngkey.com/png/full/114-1149878_setting-user-avatar-in-specific-size-without-breaking.png">
                        </div>
                        <div class="col-md-8 col-sm-6">
                            <h6 class="username">Username</h6>
                            <p class="profile">Instagram user</p>
                        </div>
                    </div>
                    <div class="items row">
                        <div class="item-inner">
                            <img width="40px" src="https://www.pngkey.com/png/full/114-1149878_setting-user-avatar-in-specific-size-without-breaking.png">
                        </div>
                        <div class="col-md-8 col-sm-6">
                            <h6 class="username">Username</h6>
                            <p class="profile">Instagram user</p>
                        </div>
                    </div>
                    <div class="items row">
                        <div class="item-inner">
                            <img width="40px" src="https://www.pngkey.com/png/full/114-1149878_setting-user-avatar-in-specific-size-without-breaking.png">
                        </div>
                        <div class="col-md-8 col-sm-6">
                            <h6 class="username">Username</h6>
                            <p class="profile">Instagram user</p>
                        </div>
                    </div>
                </div>
            </div>
            <div id="right-panel" class="card col-md-8 panel-default pl-5">
                <div class="row">
                    <h3 class="">Frefeltdmedia</h3>
                </div>
                <div class="row">
                    <ul class="list-inline form-tabs">
                        <li class="active"><a href="#">Target & Settings</a></li>
                        <li><a href="#">Comments</a></li>
                        <li><a href="#">Activity Log</a></li>
                    </ul>
                </div>
                <hr style="margin-top: 14px">
                <div class="row mt-3">
                    <button class="btn btn-primary plan-btn"># Hashtags</button>
                    <button class="btn btn-primary renew-btn">Places</button>
                    <button class="btn btn-primary renew-btn">People</button>
                </div>
                <form class="pt-5">
                    <div class="form-group col-md-7 p-0">
                        <label for="exampleInputEmail1">Search</label>
                        <input type="text" class="form-control" id="" aria-describedby="" placeholder="Type here something...">
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="exampleFormControlSelect1">Search</label>
                            <select class="form-control" id="exampleFormControlSelect1">
                                <option>Very Slow</option>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="exampleFormControlSelect1">Status</label>
                            <select class="form-control" id="exampleFormControlSelect1">
                                <option>Disable</option>
                            </select>
                        </div>
                    </div>
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" id="customCheck1">
                        <label class="custom-control-label" for="customCheck1">Pause actions everyday</label>
                    </div>
                    <div class="row pt-4">
                        <div class="form-group col-md-2">
                            <label for="exampleFormControlSelect1">From</label>
                            <input type="time" class="form-control" id="" aria-describedby="" placeholder="">
                        </div>
                        <div class="form-group col-md-2">
                            <label for="exampleFormControlSelect1">To</label>
                            <input type="time" class="form-control" id="" aria-describedby="" placeholder="">
                        </div>
                    </div>
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" id="customCheck1">
                        <label class="custom-control-label" for="customCheck1">Pause actions everyday</label>
                    </div>
                    <button type="submit" class="btn btn-primary save-btn">Save</button>
                </form>
            </div>

        </div>
    </div>
@endsection

@push('script')
    <script src="{{asset('assets/js/form-validation-custom.js')}}"></script>
@endpush
