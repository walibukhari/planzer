<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class SocialSets extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'user_id',
        'social_account'
    ];

    public static  function accounts() {
        $arr = [
            '0' => 'twitter',
            '1' => 'linked_in',
            '2' => 'instagram',
            '3' => 'youtube',
            '4' => 'facebook',
        ];
        return $arr;
    }

    public static function createSocialSets($request){
        return self::create([
            'name' => $request['name'],
            'user_id' => Auth::user()->id
        ]);
    }

    public static function updateSocialSets($request){
        return self::where('id','=',$request['id'])->update([
            'name' => $request['name']
        ]);
    }
}
