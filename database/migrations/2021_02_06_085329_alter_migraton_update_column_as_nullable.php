<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterMigratonUpdateColumnAsNullable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('social_account_managers', function (Blueprint $table) {
            $table->string('social_network')->nullable()->change();
            $table->string('category')->nullable()->change();
            $table->string('team_id')->nullable()->change();
            $table->string('login_type')->nullable()->change();
            $table->string('can_post')->nullable()->change();
            $table->longText('pid')->nullable()->change();
            $table->longText('name')->nullable()->change();
            $table->longText('username')->nullable()->change();
            $table->longText('token')->nullable()->change();
            $table->longText('avatar')->nullable()->change();
            $table->longText('url')->nullable()->change();
            $table->longText('data')->nullable()->change();
            $table->longText('proxy')->nullable()->change();
            $table->longText('status')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('social_account_managers', function (Blueprint $table) {
            //
        });
    }
}
