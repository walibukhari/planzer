<div class="row">
    <div class="col-md-3">
        <div class="card">
            <div class="card-body" style="padding-left:0px;padding-right:0px;">
                <div class="column-one nicescroll" tabindex="1" style="overflow-y: hidden; outline: none;">
                    <div class="input-group box-search-one">
                        <h4 style="width:100%;text-align:center;">
                            Accounts
                            <input type="checkbox" id="account-0" onchange="allCheckbox('0')" />
                        </h4>
                    </div>
                    <hr>
                    @if(count($all_accounts) > 0)
                        <input type="hidden" name="selectedAccount" id="selectedAccount" />
                        <input type="hidden" name="postType" id="postType" />
                        @foreach($all_accounts as $accounts)
                            @if($accounts->social_network == \App\Models\SocialAccount::TWITTER)
                                <div class="widget">
                                    <div class="widget-list search-list">
                                        <div class="widget-item widget-item-3 search-list" data-pid="45670875463">
                                            <a href="#" data-original-title="" title="">
                                                <input type="hidden" id="userPid" value="" data-original-title="" title="">
                                                <div class="icon"><img src="{{$accounts->avatar}}" onerror="this.src='http://localhost:8000/dummyImage.jpg'" data-original-title="" title=""></div>
                                                <div class="content content-2">
                                                    <div class="title fw-4">{{$accounts->username}}</div>
                                                    <div class="desc">Twitter profile</div>
                                                </div>
                                            </a>
                                            <input type="checkbox" id="account-a" onchange="updateCheckBox('a')" name="select_account" />
                                        </div>
                                    </div>
                                </div>
                            @elseif($accounts->social_network == \App\Models\SocialAccount::FACEBOOK)
                                <div class="widget">
                                    <div class="widget-list search-list">
                                        <div class="widget-item widget-item-3 search-list" data-pid="45670875463">
                                            <a href="#" data-original-title="" title="">
                                                <input type="hidden" id="userPid" value="" data-original-title="" title="">
                                                <div class="icon"><img src="{{$accounts->avatar}}" onerror="this.src='http://localhost:8000/dummyImage.jpg'" data-original-title="" title=""></div>
                                                <div class="content content-2">
                                                    <div class="title fw-4">{{$accounts->username}}</div>
                                                    <div class="desc">Facebook profile</div>
                                                </div>
                                            </a>
                                            <input type="checkbox" id="account-b" onchange="updateCheckBox('b')" name="select_account" />
                                        </div>
                                    </div>
                                </div>
                            @elseif($accounts->social_network == \App\Models\SocialAccount::INSTAGRAM)
                                <div class="widget">
                                    <div class="widget-list search-list">
                                        <div class="widget-item widget-item-3 search-list" data-pid="45670875463">
                                            <a href="#" data-original-title="" title="">
                                                <input type="hidden" id="userPid" value="" data-original-title="" title="">
                                                <div class="icon"><img src="{{$accounts->avatar}}" onerror="this.src='http://localhost:8000/dummyImage.jpg'" data-original-title="" title=""></div>
                                                <div class="content content-2">
                                                    <div class="title fw-4">{{$accounts->username}}</div>
                                                    <div class="desc">Instagram profile</div>
                                                </div>
                                            </a>
                                            <input type="checkbox" id="account-c" onchange="updateCheckBox('c')" name="select_account" />
                                        </div>
                                    </div>
                                </div>
                            @elseif($accounts->social_network == \App\Models\SocialAccount::LINK_DEN)
                                <div class="widget">
                                    <div class="widget-list search-list">
                                        <div class="widget-item widget-item-3 search-list" data-pid="45670875463">
                                            <a href="#" data-original-title="" title="">
                                                <input type="hidden" id="userPid" value="" data-original-title="" title="">
                                                <div class="icon"><img src="{{$accounts->avatar}}" onerror="this.src='http://localhost:8000/dummyImage.jpg'" data-original-title="" title=""></div>
                                                <div class="content content-2">
                                                    <div class="title fw-4">{{$accounts->username}}</div>
                                                    <div class="desc">LinkedIn profile</div>
                                                </div>
                                            </a>
                                            <input type="checkbox" id="account-d" onchange="updateCheckBox('d')" name="select_account" />
                                        </div>
                                    </div>
                                </div>
                            @elseif($accounts->social_network == \App\Models\SocialAccount::YOU_TUBE)
                                <div class="widget">
                                    <div class="widget-list search-list">
                                        <div class="widget-item widget-item-3 search-list" data-pid="45670875463">
                                            <a href="#" data-original-title="" title="">
                                                <input type="hidden" id="userPid" value="" data-original-title="" title="">
                                                <div class="icon"><img src="{{$accounts->avatar}}" onerror="this.src='http://localhost:8000/dummyImage.jpg'" data-original-title="" title=""></div>
                                                <div class="content content-2">
                                                    <div class="title fw-4">{{$accounts->username}}</div>
                                                    <div class="desc">Youtube profile</div>
                                                </div>
                                            </a>
                                            <input type="checkbox" id="account-e" onchange="updateCheckBox('e')" name="select_account" />
                                        </div>
                                    </div>
                                </div>
                            @endif
                        @endforeach
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-5">
        <div class="card">
            <div class="card-body">
                <div class="column-two">
                    <div class="title fs-16 text-info">
                        <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                        New post
                    </div>
                    <div id="media-section-card" style="">

                        <div class="row lg post-type">
                            <div class="input-field">
                                <div class="input-images-2" style="padding-top: .5rem;">
                                </div>
                            </div>
                        </div>
                        <div class="row lg post-type">
                            <div class="input-field">
                                <label class="active">Caption Text</label>
                                <input type="text" name="" id="postCaptionText" onkeydown="removeWords()" onkeypress="getTextData()" class="form-control" placeholder="Enter Caption Text...." data-original-title="" title="">
                            </div>
                        </div>
                        <br>
                        <div class="row lg post-type" style="display: flex;align-items:center;">
                            Advanced Options &nbsp;&nbsp;&nbsp;
                            <input type="checkbox" name="advanced_option" onchange="advancedOptions()" id="advanced_option" data-original-title="" title="">
                        </div>
                        <br>
                        <div class="row lg  post-type" style="display:none;" id="showAdvacnedOptions">
                            <div class="location-section">
                                <input type="text" class="form-control pac-target-input" id="pac-input" name="location" placeholder="Add Location...." data-original-title="" title="" autocomplete="off">
                                <div id="map" style="display: none; height: 300px; position: relative; overflow: hidden;"><div style="height: 100%; width: 100%; position: absolute; top: 0px; left: 0px; background-color: rgb(229, 227, 223);"><div style="overflow: hidden;"></div><div class="gm-style" style="position: absolute; z-index: 0; left: 0px; top: 0px; height: 100%; width: 100%; padding: 0px; border-width: 0px; margin: 0px;"><div tabindex="0" aria-label="Map" aria-roledescription="map" role="group" style="position: absolute; z-index: 0; left: 0px; top: 0px; height: 100%; width: 100%; padding: 0px; border-width: 0px; margin: 0px; cursor: url(&quot;https://maps.gstatic.com/mapfiles/openhand_8_8.cur&quot;), default; touch-action: pan-x pan-y;"><div style="z-index: 1; position: absolute; left: 50%; top: 50%; width: 100%;"><div style="position: absolute; left: 0px; top: 0px; z-index: 100; width: 100%;"><div style="position: absolute; left: 0px; top: 0px; z-index: 0;"></div></div><div style="position: absolute; left: 0px; top: 0px; z-index: 101; width: 100%;"></div><div style="position: absolute; left: 0px; top: 0px; z-index: 102; width: 100%;"></div><div style="position: absolute; left: 0px; top: 0px; z-index: 103; width: 100%;"><div style="position: absolute; left: 0px; top: 0px; z-index: -1;"></div></div><div style="position: absolute; left: 0px; top: 0px; z-index: 0;"></div></div><div class="gm-style-pbc" style="z-index: 2; position: absolute; height: 100%; width: 100%; padding: 0px; border-width: 0px; margin: 0px; left: 0px; top: 0px; opacity: 0;"><p class="gm-style-pbt"></p></div><div style="z-index: 3; position: absolute; height: 100%; width: 100%; padding: 0px; border-width: 0px; margin: 0px; left: 0px; top: 0px; touch-action: pan-x pan-y;"><div style="z-index: 4; position: absolute; left: 50%; top: 50%; width: 100%;"><div style="position: absolute; left: 0px; top: 0px; z-index: 104; width: 100%;"></div><div style="position: absolute; left: 0px; top: 0px; z-index: 105; width: 100%;"></div><div style="position: absolute; left: 0px; top: 0px; z-index: 106; width: 100%;"></div><div style="position: absolute; left: 0px; top: 0px; z-index: 107; width: 100%;"></div></div></div></div><iframe aria-hidden="true" frameborder="0" tabindex="-1" style="z-index: -1; position: absolute; width: 100%; height: 100%; top: 0px; left: 0px; border: none;"></iframe><div style="pointer-events: none; width: 100%; height: 100%; box-sizing: border-box; position: absolute; z-index: 1000002; opacity: 0; border: 2px solid rgb(26, 115, 232);"></div></div></div></div>
                                <input type="hidden" id="lat" name="lat" value="" data-original-title="" title="">
                                <input type="hidden" id="lng" name="lng" value="" data-original-title="" title="">
                            </div>
                            <br>
                            <div class="comment-post-section">
                                <div data-emojiarea="" style="width:100%;" data-type="unicode" data-global-picker="false">
                                    <div class="emoji-button">😄</div>
                                    <textarea class="form-control" id="input1" onkeydown="removeWords()" onkeypress="getTextData()" rows="5"></textarea>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row lg post-type">
                            <button onclick="postToSocialMedia()" class="btn btn-success" data-original-title="" title="">
                                <i class="fa fa-refresh" id="faRefresh2"></i>
                                Add Post
                            </button>
                        </div>

                    </div>
                    <div id="story-section-card" style="display:none;">

                        <div class="row lg post-type">
                            <div class="input-field">
                                <div class="input-images-3" style="padding-top: .5rem;"><div class="image-uploader"><input type="file" id="photos-1614602749277" name="photos[]" multiple="multiple"><div class="uploaded"></div><div class="upload-text"><i class="fa fa-cloud-upload"></i><span>Drag &amp; Drop files here or click to browse</span></div></div></div>
                            </div>
                        </div>
                        <br>
                        <div class="row lg post-type" style="display: flex;align-items:center;">
                            Advanced Options &nbsp;&nbsp;&nbsp;
                            <input type="checkbox" name="advanced_option" onchange="advancedOptions12()" id="advanced_option12" data-original-title="" title="">
                        </div>
                        <br>
                        <div class="row lg  post-type" style="display:none;" id="showAdvacnedOptions12">
                            <div class="location-section">
                                <input type="text" class="form-control" id="pac-input1" name="location" placeholder="Add Location...." data-original-title="" title="">
                                <div id="map12" style="display:none;height: 300px;"></div>
                                <input type="hidden" id="lat" name="lat" value="" data-original-title="" title="">
                                <input type="hidden" id="lng" name="lng" value="" data-original-title="" title="">
                            </div>
                            <br>
                            <div class="comment-post-section">
                                <div data-emojiarea="" style="width:100%;" data-type="unicode" data-global-picker="false">
                                    <div class="emoji-button">😄</div>
                                    <textarea class="form-control" id="input212" onkeydown="removeWords()" onkeypress="getTextData()" rows="5"></textarea>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row lg post-type">
                            <button onclick="postToSocialMedia()" class="btn btn-success" data-original-title="" title="">
                                <i class="fa fa-refresh" id="faRefresh2"></i>
                                Add Post
                            </button>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="column-three nicescroll" tabindex="3">
            <div class="post">
                <div class="row lg">
                    <div class="col p-0">
                        <div class="post-preview">
                            <div class="tab nav nav-tabs">
                                <a href="#preview-tab-0" class="active" data-toggle="tab" aria-expanded="true" data-original-title="" title="">
                                    <img style="width:50px;"
                                         src="https://www.brandchannel.com/wp-content/uploads/2016/05/instagram-new-logo-may-2016.jpg"
                                         data-original-title=""
                                         title=""
                                         id="socialImage"
                                    />
                                </a>
                            </div>
                            <div class="row justify-content-md-center m-t-30">
                                <div class="col-md-12 col-sm-12 tab-content">
                                    <div style="display: block !important;">
                                        <div style="" id="postMedia" class="preview-instagram">
                                            <div class="card border-0">
                                                <div class="card-block p-0 border-0">
                                                    <div class="preview-instagram preview-instagram-photo item-post-type" data-type="media" style="">
                                                        <div class="preview-content">
                                                            <div class="user-info">
                                                                <img class="img-circle" src="http://localhost:8006/inc/themes/backend/default/assets/img/avatar.jpg" data-original-title="" title="">
                                                                <span>Anonymous</span>
                                                            </div>
                                                            <div class="preview-media" data-max-image="1">
                                                                <img src="" style="display:none;" id="displayImage" data-original-title="" title="">
                                                            </div>
                                                            <div class="post-info">
                                                                <div class="info-active pull-left"> Be the first to Like this</div>
                                                                <div class="pull-right">1s</div>
                                                                <div class="clearfix"></div>
                                                            </div>
                                                            <div class="caption pt0" id="captionPT0">
                                                                <div class="line-no-text"></div>
                                                                <div class="line-no-text"></div>
                                                                <div class="line-no-text w50"></div>
                                                            </div>
                                                            <div class="preview-comment">
                                                                Add a comment
                                                                <div class="icon-3dot"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div style="display:none;" id="storyMedia" class="preview-instagram">
                                            <div class="card border-0">
                                                <div class="card-block p-0 border-0">
                                                    <div class="preview-instagram preview-instagram-photo item-post-type" data-type="story" style="    background: #000 url(/instagram/instagram-logo.png) no-repeat center center;height: 420px;overflow: hidden;position: relative;z-index: 5;max-width: 350px;margin: auto;">
                                                        <div class="preview-content">
                                                            <img src="" style="display:none;" id="displayImage2" data-original-title="" title="">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
