

@extends('userPortal.layout.app')
<link href='https://cdn.jsdelivr.net/npm/fullcalendar@5.6.0/main.css' rel='stylesheet' />
<script src='https://cdn.jsdelivr.net/npm/fullcalendar@5.6.0/main.js'></script>
<script>
    document.addEventListener('DOMContentLoaded', function() {
        $.ajax({
            url:'{{route('getScheduleDates')}}',
            method:'GET',

            success: function (response) {
                var eventData = [];
                let color = '';
                console.log('response schedule url\'s');
                console.log('scheduleArray');
                console.log(response);
                response.map((data) => {
                   console.log(data);
                   console.log(data.status);
                   if(data.status == 2) {
                       color = 'green';
                   } else {
                       color = 'orange';
                   }
                    eventData.push({
                        id:data.id,
                        title: data.description,
                        start: data.schedule_at,
                        // end: new Date('2021-04-3'),
                        color: color,
                        className : "Schedule Post"
                    })
                });
                var calendarEl = document.getElementById('calendar');
                var calendar = new FullCalendar.Calendar(calendarEl, {
                    initialView: 'dayGridMonth',
                    events: eventData,
                });
                calendar.render();
            },
            error: function (error) {
                console.log('error');
                console.log(error);
            }

        });
    });

</script>
@section('title', 'Facebook')

@section('css')
@endsection

@push('style')
    <style>
        body.dark-only .compact-wrapper .page-body-wrapper .page-body .card:not(.email-body) {
            border: 0px !important;
        }
        .fc .fc-button-group{
            position: absolute;
            display: inline-flex;
            vertical-align: middle;
            left: 0px;
            width: 35%;
        }
        .card {
            margin-bottom: 30px;
            border: none;
            -webkit-transition: all 0.3s ease;
            transition: all 0.3s ease;
            letter-spacing: 0.5px;
            border-radius: 15px;
            /* -webkit-box-shadow: 0 0 20px rgb(8 21 66 / 5%); */
            box-shadow: none !important;
        }
        .fc .fc-toolbar-title {
            font-size: 20px !important;
            margin: 0;
            position: relative;
            width: 176px;
            text-align: center;
        }
        .fc-direction-ltr .fc-button-group > .fc-button:not(:last-child) {
            border-top-right-radius: 0;
            border-bottom-right-radius: 0;
            position: relative;
            margin-right: 220px;
            background: #8074FF !important;
            border: 0px !important;
            border-radius: 7px !important;
        }
        button.fc-today-button.fc-button.fc-button-primary {
            visibility: hidden !important;
        }
        th.fc-col-header-cell {
            padding: 25px;
            font-weight: 200;
        }
        .fc .fc-daygrid-day-number {
            position: relative;
            z-index: 4;
            padding: 4px;
            top: 5px;
            right: 5px;
        }
        .fc-direction-ltr .fc-button-group > .fc-button:not(:first-child) {
            margin-left: -1px;
            border-top-left-radius: 0;
            border-bottom-left-radius: 0;
            position: relative;
            background: #8074FF !important;
            border: 0px !important;
            border-radius: 7px !important;
        }
        .fc .fc-toolbar-title {
            font-size: 1.75em;
            margin: 0;
            position: relative;
            left: 75px;
        }
    </style>
@endpush

@section('breadcrumb-title')
    <h3>Facebook Setup</h3>
@endsection

@section('breadcrumb-items')
    <li class="breadcrumb-item">Users</li>
    <li class="breadcrumb-item active">Add New User</li>
@endsection

@section('content')

    <div class="p-5">
        <div class="card p-5">
            <div class="card" id='calendar'></div>
        </div>
    </div>
@endsection

@push('script')
    <script src="{{asset('assets/js/form-validation-custom.js')}}"></script>


@endpush
