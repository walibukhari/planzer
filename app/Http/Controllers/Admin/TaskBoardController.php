<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class TaskBoardController extends Controller
{
    public function taskBoard(){
        return view('admin.taskBoard.task');
    }
}
