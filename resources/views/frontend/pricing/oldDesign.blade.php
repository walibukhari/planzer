<div class="pt-23 pt-md-26 pt-lg-28">
    <div class="container">
        <!-- Section Padding -->
        <div class="row justify-content-center">
            <div class="col-xl-7 col-lg-8 col-md-11">
                <div class="text-center mb-10 mb-lg-23" data-aos="fade-up" data-aos-duration="500" data-aos-once="true">
                    <h2 class="font-size-11 mb-7">Pricing & Plan</h2>
                    <p class="font-size-7 mb-0">Our focus is always on finding the
                        best people to work with. Our bar is high, but you look ready
                        to take on the challenge.</p>
                </div>
            </div>
        </div>
        <!-- End Section Padding -->
    </div>
</div>
<!-- End breadCrumbs Area -->
<div class="pb-5 pb-md-11 pb-lg-19">
    <div class="container">
        <div class="row justify-content-center">
            <a href="javascript:" class="btn-toggle-square active mx-3 price-deck-trigger rounded-10 bg-golden-yellow">
                <span data-pricing-trigger="" onclick="changeValue('y')" data-target="#table-price-value" data-value="yearly" class="text-break active">Yearly</span>
                <span data-pricing-trigger="" onclick="changeValue('m')" data-target="#table-price-value" data-value="monthly" class="text-break">Monthly</span>
            </a>
        </div>
        <br>
        <br>
        <br>
        <h5 style="text-align:center;">Additional Social Sets</h5>
        <div class="row justify-content-center">
            <div class="tPF--counter">
                <a class="tPF--counter__minus tPF--counter__minus--yearlySocialSets" onclick="decrementData()"></a>
                <input class="tPF--counter__input tPF--counter__input--yearlySocialSets" id="tPF--counter__input--yearlySocialSets" type="number" value="0" max="10000" min="0">
                <a class="tPF--counter__plus tPF--counter__plus--yearlySocialSets" onclick="incrementData()"></a>
            </div>
        </div>
    </div>
</div>
<!-- pricingTable Area -->
<div class="pb-5 pb-md-11 pb-lg-19">
    <div class="container">
        <div class="row justify-content-center">
            <!-- Single Table -->
            <div class="col-xl-4 col-lg-5 col-md-6 col-sm-8">
                <div class="customHeight border rounded-10 text-center px-7 px-lg-16 pt-10 pb-13 gr-hover-2 mb-9" data-aos="fade-up" data-aos-duration="300" data-aos-once="true">
                    <p class="text-dodger-blue-1 font-size-5 mb-9">Basic</p>
                    <h2 class="font-size-11 mb-1">
                        <input type="hidden" id="py1" value="0" />
                        <span id="y1">0€</span>
                        <span id="m1" style="display:none;">0€</span></h2>
                    <span class="font-size-5 mb-2">free forever</span>
                    <ul class="list-unstyled font-size-5 line-height-50 heading-default-color pt-8">
                        <li class="mb-7">1 social set</li>
                        <li class="mb-7">1 user</li>
                        <li class="mb-7">Basic features</li>
                        <li class="mb-7 text-stone"><del>No ad ons</del></li>
                    </ul>
                    <div class="customSet pt-7 pt-lg-17">
                        <a class="btn btn-blue-3 btn-2 rounded-5" href="{{route('newRegister')}}">Start free</a>
                    </div>
                </div>
            </div>
            <!-- End Single Table -->
            <!-- Single Table -->
            <div class="col-xl-4 col-lg-5 col-md-6 col-sm-8">
                <div class="customHeight border rounded-10 text-center px-7 px-lg-16 pt-10 pb-13 gr-hover-2 mb-9" data-aos="fade-up" data-aos-duration="600" data-aos-once="true">
                    <p class="text-dodger-blue-1 font-size-5 mb-9">Standard</p>
                    <h2 class="font-size-11 mb-1">
                        <input type="hidden" id="py2" value="12.5" />
                        <span id="y2">12.5€</span>
                        <span id="m2" style="display:none;">15€</span></h2>
                    <span class="font-size-5 mb-2">Pr. social set pr. month</span>
                    <ul class="list-unstyled font-size-5 line-height-50 heading-default-color pt-8">
                        <li class="mb-7">1 social set</li>
                        <li class="mb-7">3 users</li>
                        <li class="mb-7">Standard features</li>
                        <li class="mb-7">Ad ons<br>
                            {{--                                -Social sets (<span class="yP3">12.5€ Pr. user</span><span class="mP3">15€ Pr. user</span>)<br>--}}
                            {{--                                -Users (<span class="yP3">12.5€ Pr. user</span><span class="mP3">15€ Pr. user</span>)--}}
                        </li>
                    </ul>
                    <div class="customSet pt-7 pt-lg-17">
                        <a class="btn btn-blue-3 btn-2 rounded-5" href="{{route('newRegister')}}">Start 14 days free trial</a>
                    </div>
                </div>
            </div>
            <!-- End Single Table -->
            <!-- Single Table -->
            <div class="col-xl-4 col-lg-5 col-md-6 col-sm-8">
                <div class="customHeight border rounded-10 text-center px-7 px-lg-16 pt-10 pb-13 gr-hover-2 mb-9" data-aos="fade-up" data-aos-duration="900" data-aos-once="true">
                    <p class="text-dodger-blue-1 font-size-5 mb-9">Premium</p>
                    <h2 class="font-size-11 mb-1">
                        <input type="hidden" id="py3" value="30" />
                        <span id="y3">30€</span>
                        <span id="m3" style="display:none;">40€</span>
                    </h2>
                    <span class="font-size-5 mb-2">Pr. social set pr. month</span>
                    <ul class="list-unstyled font-size-5 line-height-50 heading-default-color pt-8">
                        <li class="mb-7">1 social set</li>
                        <li class="mb-7">5 users</li>
                        <li class="mb-7">Premium features</li>
                        <li class="mb-7">Ad ons<br>
                            {{--                                -Social sets (<span class="yP3">30€ Pr. user</span><span class="mP3">40€ Pr. user</span>)--}}
                            {{--                                <br>--}}
                            {{--                                -Users (<span class="yP3">30€ Pr. user</span><span class="mP3">40€ Pr. user</span>)--}}
                        </li>
                    </ul>
                    <div class="customSet pt-7 pt-lg-17">
                        <a class="btn btn-blue-3 btn-2 rounded-5" href="{{route('newRegister')}}">Start 14 days free trial</a>
                    </div>
                </div>
            </div>
            <!-- End Single Table -->
        </div>
    </div>
</div>
