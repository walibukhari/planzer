<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function login(){
        return view('auth.admin.login');
    }

    public function postAdmin(Request $request){
        if(Auth::guard('admin')->attempt(['email' => $request->email , 'password' => $request->password])) {
            return redirect('/super-admin/home');
        } else {
            return back()->with('error_message','Credtional\'s Not Match');
        }
    }

    public function logoutSuperAdmin(){
        Auth::guard('admin')->logout();
        session()->flush();
        return redirect()->route('loginSuperAdmin');
    }
}
