@extends('layouts.simple.master')
@section('title', 'New Package')

@section('css')
@endsection

@section('style')
@endsection

@section('breadcrumb-title')
<h3>Create Package</h3>
@endsection

@section('breadcrumb-items')
<li class="breadcrumb-item">Packages</li>
<li class="breadcrumb-item active">Create Package</li>
@endsection

@section('content')
<div>
	  @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
        @endif
        @if (session('password_status'))
            <div class="alert alert-success" role="alert">
                {{ session('password_status') }}
            </div>
        @endif
        @if (Session::has('error'))
            <div class="alert alert-danger alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>{!! Session('error') !!}</strong>
            </div>
        @endif
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
</div>
<div class="container-fluid">
	<div class="row">
		<div class="col-sm-12">
			<div class="card">
				
				<div class="card-body">
					<form class="needs-validation" novalidate="" method="POSt" action="{{route('save.package')}}">
						@csrf

						<div class="row">
							<div class="col-md-6 mb-3">
								
						
						 <div class="row">
							<div class="col-md-12 mb-3">
								<label for="name" class="">Name</label>
								   <input id="" type="text" class="form-control " name="name" required value="{{old('name')}}">
								<div class="valid-feedback">Looks good!</div>
							</div>
					
							
						</div>

							 <div class="row">
							<div class="col-md-12 mb-3">
								<label for="name">Description</label>
								   <input id="" type="te
								   " class="form-control " name="description" required value="{{old('description')}}">
								<div class="valid-feedback">Looks good!</div>
							</div>
					
							
						</div>








	                 <div class="row">
							<div class="col-md-6 mb-3">
								<label for="storage">Max Storage Size (MB)</label>
								   <input id="" type="number" class="form-control " name="storage" required value="{{old('storage')}}">
								<div class="valid-feedback">Looks good!</div>
							</div>
						<div class="col-md-6 mb-3">
								<label for="city">Max File Size (MB)</label>
								<input class="form-control" name="file_size" id="" type="number" placeholder="" value="{{ old('file_size') }}" required="">
								<div class="invalid-feedback">Please provide a File Size.</div>
							</div>
							
						</div>








						<div class="row">
							
							<div class="col-md-6 mb-3">
								<label for="Monthly Price">Price Monthly</label>
								<input class="form-control" value="{{ old('price_monthly') }}" name="price_monthly" id="" type="text" placeholder="" required="">
								<div class="invalid-feedback">Please Provide a Price Monthly.</div>
							</div>
							<div class="col-md-6 mb-3">
								<label for="price_anually">Price Annually</label>
								<input class="form-control" value="{{ old('price_anually') }}" id="" name="price_anually" type="text" placeholder="" required="">
								<div class="invalid-feedback">Please Provide a Price Annually</div>
							</div>
						</div>


						<div class="row">
							
							<div class="col-md-12 mb-3">
								<label for="no_of_account">Number of account per social</label>
								<input class="form-control" value="{{ old('no_of_account') }}" id="" name="no_of_account" type="number" placeholder="" required="">
								<div class="invalid-feedback">Please provide a number of account per social.</div>
							</div>
							
						</div>



							<div class="row">
							
								<div class="col-md-12 mb-3">
								<label for="sort">Sort</label>
								<input class="form-control" value="{{ old('sort') }}" name="sort" id="" type="number" placeholder="" required="">
								<div class="invalid-feedback">Please provide a sort.</div>
							</div>
							
						</div>







						




						<div class="row">
						
							<div class="col-md-12 mb-3">
								<label for="team_users">How many team users(0 for disabled / -1 for Unlimited)</label>
								<input class="form-control" id="" value="{{old('team_users')}}" name="team_users" type="number" placeholder="" required="">
								<div class="invalid-feedback">Please provide a team users.</div>
							</div>
							


							
							
						</div>


						<div class="row">
							<div class="col-md-12 mb-3" >
								
						

						<h6>File Pickers</h6>
						

                        <div class="row mb-3">
                            <div class="col-md-4">
                                <div class="custom-control custom-checkbox ">
                                    <input type="hidden" name="google_drive" value="0"/>
                                    <input type="checkbox"  value="1" name="google_drive"  class="custom-control-input" id="customCheck1googleDrive">
                                    <label class="custom-control-label" for="customCheck1googleDrive">Google drive</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="custom-control custom-checkbox">
                                    <input type="hidden" name="dropbox" value="0"/>
                                    <input type="checkbox"  value="1" name="dropbox"  class="custom-control-input" id="customCheck2">
                                    <label class="custom-control-label" for="customCheck2">Dropbox</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="custom-control custom-checkbox">
                                    <input type="hidden" name="one_drive" value="0"/>
                                    <input type="checkbox"  value="1" name="one_drive"  class="custom-control-input" id="customCheck3Onedrive">
                                    <label class="custom-control-label" for="customCheck3Onedrive">OneDrive</label>
                                </div>
                            </div>
                        </div>

                        <h6>Allowed Media Type</h6>
                 
                        <div class="row mb-3">
                            <div class="col-md-6">
                                <div class="custom-control custom-checkbox ">
                                    <input type="hidden" name="photo" value="0"/>
                                    <input type="checkbox"  value="1" name="photo"  class="custom-control-input" id="customCheckphoto">
                                    <label class="custom-control-label" for="customCheckphoto">Photo</label>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="custom-control custom-checkbox">
                                    <input type="hidden" name="video" value="0"/>
                                    <input type="checkbox"  value="1" name="video"  class="custom-control-input" id="customCheckvideo">
                                    <label class="custom-control-label" for="customCheckvideo">Video</label>
                                </div>
                            </div>

                        </div>
                  

                        <h6>Advance Options</h6>
                    
                        <div class="row mb-3">
                            <div class="col-md-4">
                                <div class="custom-control custom-checkbox ">
                                    <input type="hidden" name="watermark" value="0"/>
                                    <input type="checkbox"  value="1" name="watermark"  class="custom-control-input" id="customCheck6">
                                    <label class="custom-control-label" for="customCheck6">Watermark</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="custom-control custom-checkbox">
                                    <input type="hidden" name="image_editor" value="0"/>
                                    <input type="checkbox"  value="1" name="image_editor"  class="custom-control-input" id="customCheck7">
                                    <label class="custom-control-label" for="customCheck7">Image Editor</label>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="custom-control custom-checkbox">
                                    <input type="hidden" name="drafts" value="0"/>
                                    <input type="checkbox"  value="1" name="drafts"  class="custom-control-input" id="customCheckDrafts">
                                    <label class="custom-control-label" for="customCheckDrafts">Draft Posts</label>
                                </div>
                            </div>

                        </div>
                 

                        <h6>RSS</h6>
                      
                        <div class="row mb-3">
                            <div class="col-md-6">
                                <div class="custom-control custom-checkbox ">
                                    <input type="hidden" name="rss" value="0"/>
                                    <input type="checkbox"  value="1" name="rss"  class="custom-control-input" id="customCheckrss">
                                    <label class="custom-control-label" for="customCheckrss">Enable RSS</label>
                                </div>
                            </div>


                        </div>

                        <h6>Automation Bots</h6>
                        
<div class="row mb-3">
    <div class="col-md-6">
        <div class="custom-control custom-checkbox ">
            <input type="hidden" name="automations" value="0"/>
            <input type="checkbox"  value="1" name="automations"   class="custom-control-input" id="customCheckautomations">
            <label class="custom-control-label" for="customCheckautomations">Enable</label>
        </div>
    </div>


</div>

<h6>Bulk Schedule</h6>

<div class="row mb-3">

    <div class="col-md-6">
        <div class="custom-control custom-checkbox ">
            <input type="hidden" name="bulk_schedule" value="0"/>
            <input type="checkbox"  value="1" name="bulk_schedule"   class="custom-control-input" id="customCheckbulk-schedule">
            <label class="custom-control-label" for="customCheckbulk-schedule">Enable</label>
        </div>
    </div>


</div>    
   

	</div>	
	</div>     

	</div>	



	      <div class="col-md-6 mb-3">
                        <h6>Social Integration</h6>
                        <hr/>

                        <div style="height: calc(100vh - 250px);overflow: auto;overflow-x: hidden">
                                                            <h6>Facebook</h6>
                                <div class="custom-control custom-checkbox ">
                                    <input type="hidden" name="facebook" value="0"/>
                                    <input type="checkbox"  value="1" name="facebook"  class="custom-control-input" id="customCheck8">
                                    <label class="custom-control-label" for="customCheck8">Enable</label>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="custom-control custom-checkbox ">
                                            <input type="hidden" name="facebook_post" value="0"/>
                                            <input type="checkbox"  value="1" name="facebook_post"  class="custom-control-input" id="customCheck9">
                                            <label class="custom-control-label" for="customCheck9">Post</label>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="custom-control custom-checkbox ">
                                            <input type="hidden" name="livestream" value="0"/>
                                            <input type="checkbox" value="1" name="livestream"  class="custom-control-input" id="customCheckfacebook-livestream">
                                            <label class="custom-control-label" for="customCheckfacebook-livestream">Live stream</label>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
    <div class="custom-control custom-checkbox ">
        <input type="hidden" name="direct_message" value="0"/>
        <input type="checkbox"   value="1" name="direct_message"  class="custom-control-input" id="customCheckfacebook-direct-message">
        <label class="custom-control-label" for="customCheckfacebook-direct-message">Direct Message</label>
    </div>
</div>

                                </div>

                                <hr/>
                            
                                                            <h6>Instagram</h6>
                                <div class="custom-control custom-checkbox ">
                                    <input type="hidden" name="instagram" value="0"/>
                                    <input type="checkbox"  value="1" name="instagram"  class="custom-control-input" id="customCheck10">
                                    <label class="custom-control-label" for="customCheck10">Enable</label>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="custom-control custom-checkbox ">
                                            <input type="hidden" name="instagram_post" value="0"/>
                                            <input type="checkbox"  value="1" name="instagram_post"  class="custom-control-input" id="customCheck11">
                                            <label class="custom-control-label" for="customCheck11">Post</label>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="custom-control custom-checkbox ">
                                            <input type="hidden" name="analytics" value="0"/>
                                            <input type="checkbox"  value="1" name="analytics"  class="custom-control-input" id="customCheck12">
                                            <label class="custom-control-label" for="customCheck12">Analytics</label>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="custom-control custom-checkbox ">
                                            <input type="hidden" name="instagram_livestream" value="0"/>
                                            <input type="checkbox" value="1" name="instagram_livestream"  class="custom-control-input" id="customCheckinstagram-livestream">
                                            <label class="custom-control-label" for="customCheckinstagram-livestream">Live stream</label>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="custom-control custom-checkbox ">
                                            <input type="hidden" name="igtv" value="0"/>
                                            <input type="checkbox" value="1" name="igtv"  class="custom-control-input" id="customCheckinstagram-igtv">
                                            <label class="custom-control-label" for="customCheckinstagram-igtv">IGTV</label>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
    <div class="custom-control custom-checkbox ">
        <input type="hidden" name="instagram_tag" value="0"/>
        <input type="checkbox"   value="1" name="instagram_tag"  class="custom-control-input" id="customCheckinstagram-tag">
        <label class="custom-control-label" for="customCheckinstagram-tag">Tag People</label>
    </div>
</div>
<div class="col-md-6">
    <div class="custom-control custom-checkbox ">
        <input type="hidden" name="instagram_direct_message" value="0"/>
        <input type="checkbox"   value="1" name="instagram_direct_message"  class="custom-control-input" id="customCheckinstagram-direct-message">
        <label class="custom-control-label" for="customCheckinstagram-direct-message">Direct Message</label>
    </div>
</div>
                                </div>
                                <hr/>
                            

                                                            <h6>Twitter</h6>
                                <div class="custom-control custom-checkbox ">
                                    <input type="hidden" name="twitter" value="0"/>
                                    <input type="checkbox"  value="1" name="twitter"  class="custom-control-input" id="customCheck13">
                                    <label class="custom-control-label" for="customCheck13">Enable</label>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="custom-control custom-checkbox ">
                                            <input type="hidden" name="twitter_post" value="0"/>
                                            <input type="checkbox"  value="1" name="twitter_post"  class="custom-control-input" id="customCheck14">
                                            <label class="custom-control-label" for="customCheck14">Post</label>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
    <div class="custom-control custom-checkbox ">
        <input type="hidden" name="twitter_direct_message" value="0"/>
        <input type="checkbox"   value="1" name="twitter_direct_message"  class="custom-control-input" id="customChecktwitter-direct-message">
        <label class="custom-control-label" for="customChecktwitter-direct-message">Direct Message</label>
    </div>
</div>
                                </div>
                                <hr/>
                            
                                         
                            
                                                            <h6>LinkedIn</h6>
                                <div class="custom-control custom-checkbox ">
                                    <input type="hidden" name="linkedin" value="0"/>
                                    <input type="checkbox"  value="1" name="linkedin"  class="custom-control-input" id="customCheck19">
                                    <label class="custom-control-label" for="customCheck19">Enable</label>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="custom-control custom-checkbox ">
                                            <input type="hidden" name="linkedin_post" value="0"/>
                                            <input type="checkbox"  value="1" name="linkedin_post"  class="custom-control-input" id="customCheck20">
                                            <label class="custom-control-label" for="customCheck20">Post</label>
                                        </div>
                                    </div>

                                                                    </div>
                                <hr/>
                            



                                                        
                            
                                                         
                            
                                                        
                            
                                                          
                            
                               
                            
                                                            <h6>Tiktok</h6>
                                <div class="custom-control custom-checkbox ">
                                    <input type="hidden" name="tiktok" value="0"/>
                                    <input type="checkbox"  value="1" name="tiktok"  class="custom-control-input" id="customCheckdaily">
                                    <label class="custom-control-label" for="customCheckdaily">Enable</label>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="custom-control custom-checkbox ">
                                            <input type="hidden" name="tiktok_post" value="0"/>
                                            <input type="checkbox"  value="1" name="tiktok_post"  class="custom-control-input" id="customCheck-dailymotion">
                                            <label class="custom-control-label" for="customCheck-dailymotion">Post</label>
                                        </div>
                                    </div>

                                                                    </div>
                                <hr/>
                            


                                                            <h6>Youtube</h6>
                                <div class="custom-control custom-checkbox ">
                                    <input type="hidden" name="youtube" value="0"/>
                                    <input type="checkbox"  value="1" name="youtube"  class="custom-control-input" id="customCheck31">
                                    <label class="custom-control-label" for="customCheck31">Enable</label>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="custom-control custom-checkbox ">
                                            <input type="hidden" name="youtube_post" value="0"/>
                                            <input type="checkbox" value="1" name="youtube_post"  class="custom-control-input" id="customCheck32">
                                            <label class="custom-control-label" for="customCheck32">Post</label>
                                        </div>
                                    </div>

                                                                    </div>
                                <hr/>
                            
                        </div>
						</div>






				













						<button class="btn btn-primary" type="submit">save</button>
					</form>
				</div>
			</div>
		
			
		
		</div>
	</div>
</div>
@endsection

@section('script')
<script src="{{asset('assets/js/form-validation-custom.js')}}"></script>
@endsection