@extends('layouts.default')
@section('title')
    Meet Team
@endsection
@push('css')
    <style>
        .site-header--sticky:not(.mobile-sticky-enable) {
            background:#fff;
        }
        .btn.btn-xl{
            color:#fff;
        }
        .l4-content-img-group .img-1 {
            position: absolute;
            left: -5%;
            top: -35%;
            z-index: 1;
            width: 60% !important;
        }
    </style>
@endpush

@section('content')

    <!-- contentThree Area -->
    <div class="bg-dark-cloud pt-28 pt-md-32 pt-lg-30 pt-xl-33 pb-15 pb-md-19 pb-lg-34">
        <div class="container">
            <div class="row align-items-center justify-content-center">
                <!-- Left Image -->
                <div class="col-xl-6 col-lg-5 col-md-8 col-xs-11">
                    <!-- content img start -->
                    <div class="l4-content-img-group">
                        <div class="img-1" style="width:75% !important;">
                            <img class="w-100" style="margin-top: -180px;" src="{{asset('images/office team work.png')}}" alt="" data-aos="fade-right" data-aos-duration="300" data-aos-delay="200" data-aos-once="true">
                        </div>
{{--                        <div class="img-2">--}}
{{--                            <img class="w-100" src="{{asset('images/office team work.png')}}" alt="" data-aos="fade-left" data-aos-duration="300" data-aos-delay="400" data-aos-once="true">--}}
{{--                        </div>--}}
{{--                        <div class="img-3">--}}
{{--                            <img class="w-100" src="https://uxtheme.net/demos/finity/image/home-4/png/l4-card-3.png" alt="" data-aos="fade-up" data-aos-duration="300" data-aos-delay="600" data-aos-once="true">--}}
{{--                        </div>--}}
                    </div>
                </div>
                <!-- End Left Image -->
                <!-- Right Image -->
                <div class="col-xl-5 offset-lg-1 col-lg-6 col-md-8 mt-n13 mt-md-n8 mt-lg-n0">
                    <div class="pt-30 pt-lg-0 mt-lg-0 dark-mode-texts" data-aos="fade-left" data-aos-duration="500" data-aos-once="true">
                        <h6 class="font-size-3 text-dodger-blue-1 text-uppercase mb-9 letter-spacing-normal">We value teamwork</h6>
                        <h2 class="font-size-10 mb-8 letter-spacing-n83 pr-xs-22 pr-sm-18 pr-md-0">The best experience offered by the best team</h2>
                        <p class="font-size-7 mb-0">Minimize time consuming distractions, level up your team’s productivity by letting socialday.io handle your social media. Our focus is always on finding the best people to work with.</p>
                        <div class="mt-12">
                            <a class="btn btn-sunset btn-xl h-55 rounded-5 font-weight-normal" href="{{route('newRegister')}}">Start 14 Days trial</a>
                        </div>
                    </div>
                </div>
                <!-- End Right Image -->
            </div>
        </div>
    </div>
    <!-- End contentThree Area -->
    <!-- teamMember Area -->
    <div class="pt-14 pt-md-18 pt-lg-24 pb-15 pb-md-19 pb-lg-25">
        <div class="container">
            <!-- section title -->
            <div class="row justify-content-center">
                <div class="col-12">
                    <div class="text-center mb-12 mb-lg-19" data-aos="fade-up" data-aos-duration="600" data-aos-once="true">
                        <h2 class="font-size-10 mb-8 letter-spacing-n83">Team Behind Our Success</h2>
                    </div>
                </div>
            </div>
            <!-- section title -->
            <div class="row justify-content-center">
                <!-- Single Team -->
                <div class="col-lg-4 col-sm-6 col-xs-8">
                    <div class="text-center mb-10 mb-lg-15 mx-xl-9 mx-lg-0 mx-md-9" data-aos="fade-up" data-aos-duration="600" data-aos-once="true">
                        <div class="mb-8 rounded-10">
                            <img class="w-100" style="border-radius: 10px;
    object-fit: cover;" src="/Frederik Frifeldt.png" alt="">
                        </div>
                        <div class="content">
                            <h4 class="font-size-7 mb-1">Frederik Frifeldt</h4>
                            <p class="font-size-5 font-weight-normal mb-0">CEO & Founder</p>
                        </div>
                    </div>
                </div>
                <!-- Single Team -->
                <!-- Single Team -->
                <div class="col-lg-4 col-sm-6 col-xs-8">
                    <div class="text-center mb-10 mb-lg-15 mx-xl-9 mx-lg-0 mx-md-9" data-aos="fade-up" data-aos-duration="600" data-aos-delay="300" data-aos-once="true">
                        <div class="mb-8 rounded-10">
                            <img style="border-radius: 10px;
    object-fit: cover;" class="w-100" src="/Silas Storgaard.png" alt="">
                        </div>
                        <div class="content">
                            <h4 class="font-size-7 mb-1">Silas Storgaard</h4>
                            <p class="font-size-5 font-weight-normal mb-0">CPO & Partner</p>
                        </div>
                    </div>
                </div>
                <!-- Single Team -->
                <!-- Single Team -->
                <div class="col-lg-4 col-sm-6 col-xs-8">
                    <div class="text-center mb-10 mb-lg-15 mx-xl-9 mx-lg-0 mx-md-9" data-aos="fade-up" data-aos-duration="600" data-aos-delay="500" data-aos-once="true">
                        <div class="mb-8 rounded-10">
                            <img class="w-100" style="border-radius: 10px;object-fit: cover;" src="{{asset('images/Dylan Bastved.jpg')}}" alt="">
                        </div>
                        <div class="content">
                            <h4 class="font-size-7 mb-1">Dylan Bastved</h4>
                            <p class="font-size-5 font-weight-normal mb-0">CFO & Partner</p>
                        </div>
                    </div>
                </div>
                <!-- Single Team -->
            </div>
            <div class="mt-lg-8 text-center" data-aos="fade-up" data-aos-duration="600" data-aos-once="true">
                <a class="btn btn-outline-dark-cloud darkmode-btn btn-xl h-55 rounded-5" href="{{url('/career')}}">Join our team</a>
            </div>
        </div>
    </div>
    <!-- End teamMember Area -->

    <!-- testiMonials Area -->
    <div style="justify-content: space-evenly !important;" class="brand-logo-small d-flex align-items-center justify-content-center justify-content-lg-between flex-wrap">
        <!-- Single Brand -->
        <div class="single-brand-logo opacity-5 mx-5 my-6">
            <img class="light-version-logo aos-init aos-animate" src="http://localhost:8000/images/Client_1.png" style="width: auto;height: 50px;" alt="" data-aos="fade-in" data-aos-once="true">
            <img class="dark-version-logo default-logo aos-init aos-animate" src="http://localhost:8000/images/Client_1.png" style=" height: 50px;" alt="" data-aos="fade-in" data-aos-once="true">
        </div>
        <!-- Single Brand -->
        <div class="single-brand-logo opacity-5 mx-5 my-6">
            <img class="light-version-logo aos-init aos-animate" src="http://localhost:8000/images/Client_2.png" style="width: auto;height: 50px;" alt="" data-aos="fade-in" data-aos-duration="300" data-aos-delay="300" data-aos-once="true">
            <img class="dark-version-logo default-logo aos-init aos-animate" src="http://localhost:8000/images/Client_2.png" style="width: auto;height: 50px;" alt="" data-aos="fade-in" data-aos-duration="300" data-aos-delay="300" data-aos-once="true">
        </div>
        <!-- Single Brand -->
        <div class="single-brand-logo opacity-5 mx-5 my-6">
            <img class="light-version-logo aos-init aos-animate" src="http://localhost:8000/images/Client_3.png" style="width: auto;height: 50px;" alt="" data-aos="fade-in" data-aos-duration="600" data-aos-delay="500" data-aos-once="true">
            <img class="dark-version-logo default-logo aos-init aos-animate" src="http://localhost:8000/images/Client_3.png" style="width: auto;height: 50px;" alt="" data-aos="fade-in" data-aos-duration="600" data-aos-delay="500" data-aos-once="true">
        </div>
        <!-- Single Brand -->
        <div class="single-brand-logo opacity-5 mx-5 my-6">
            <img class="light-version-logo aos-init aos-animate" src="http://localhost:8000/images/Client_4.png" style="width: auto;height: 50px;" alt="" data-aos="fade-in" data-aos-duration="900" data-aos-delay="700" data-aos-once="true">
            <img class="dark-version-logo default-logo aos-init aos-animate" src="http://localhost:8000/images/Client_4.png" style="width: auto;height: 50px;" alt="" data-aos="fade-in" data-aos-duration="900" data-aos-delay="700" data-aos-once="true">
        </div>
        <!-- Single Brand -->
        <div class="single-brand-logo opacity-5 mx-5 my-6">
            <img class="light-version-logo aos-init aos-animate" src="http://localhost:8000/images/Client_5.png" style="width: auto;height: 50px;" alt="" data-aos="fade-in" data-aos-duration="1200" data-aos-delay="700" data-aos-once="true">
            <img class="dark-version-logo default-logo aos-init aos-animate" src="http://localhost:8000/images/Client_5.png" style="width: auto;height: 50px;" alt="" data-aos="fade-in" data-aos-duration="1200" data-aos-delay="700" data-aos-once="true">
        </div>
    </div>
    <!-- End testiMonials Area -->

{{--    <!-- pricingTable Area -->--}}
{{--    <div class="pt-13 pt-md-17 pt-lg-25 pb-8 pb-md-11 pb-lg-22 position-relative">--}}
{{--        <div class="container">--}}
{{--            <!-- Section Title -->--}}
{{--            <div class="mb-13 mb-lg-13">--}}
{{--                <div class="row align-items-center justify-content-center">--}}
{{--                    <div class="col-xl-5 col-lg-6 col-md-10">--}}
{{--                        <div class="mb-10 mb-lg-0 text-center text-lg-left aos-init aos-animate" data-aos-duration="800" data-aos="fade-right" data-aos-once="true">--}}
{{--                            <h2 class="font-size-10 pr-md-10 pr-xl-0 mb-0 letter-spacing-n83">Choose the right plan for your biz</h2>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div class="col-xl-7 col-lg-6 col-md-6 text-center text-md-right">--}}
{{--                        <div class="btn-section aos-init aos-animate" data-aos="fade-left" data-aos-duration="500" data-aos-once="true">--}}
{{--                            <a href="javascript:" class="btn-toggle-square active mx-3 price-deck-trigger rounded-10 bg-golden-yellow">--}}
{{--                                <span data-pricing-trigger="" data-target="#table-price-value" data-value="yearly" class="active text-break">Yearly</span>--}}
{{--                                <span data-pricing-trigger="" data-target="#table-price-value" data-value="monthly" class="text-break">Monthly</span>--}}
{{--                            </a>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--            <!-- End Section Title -->--}}
{{--            <!-- Table Main Body -->--}}
{{--            <div class="table-body" id="table-price-value" data-pricing-dynamic="" data-value-active="yearly">--}}
{{--                <div class="row justify-content-center">--}}
{{--                    <!-- Single Table -->--}}
{{--                    <div class="col-lg-12 col-sm-6">--}}
{{--                        <div class="border-top pt-10 pb-8 aos-init aos-animate" data-aos="fade-up" data-aos-duration="800" data-aos-delay="500" data-aos-once="true">--}}
{{--                            <div class="row">--}}
{{--                                <div class="col-lg-3">--}}
{{--                                    <div class="mb-5 mb-lg-0">--}}
{{--                                        <h3 class="font-size-7">Free</h3>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <div class="col-lg-3">--}}
{{--                                    <div class="mb-5 mb-lg-0">--}}
{{--                                        <ul class="list-unstyled font-size-6">--}}
{{--                                            <li class="heading-default-color mb-4">1 Social set</li>--}}
{{--                                            <li class="heading-default-color mb-4">1 user</li>--}}
{{--                                            <li class="heading-default-color mb-4">Simple features</li>--}}
{{--                                        </ul>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <div class="col-lg-3">--}}
{{--                                    <div class="pr-lg-15 pr-xl-10 mb-5 mb-lg-0">--}}
{{--                                        <h2 class="mb-0 font-size-11 font-weight-medium">€ <span class="dynamic-value" data-active="0" data-monthly="0" data-yearly="0"></span></h2>--}}
{{--                                        <p class="mb-0 font-size-5 pr-xl-22">Free</p>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <div class="col-lg-3">--}}
{{--                                    <div class="mb-5 mb-lg-0 mt-4 text-left text-lg-right">--}}
{{--                                        <a class="btn btn-outline-gray-1 btn-2 border-width-2 rounded-5 gr-hover-bg-blue-3 heading-default-color" href="{{route('newRegister')}}">Sign--}}
{{--                                            Up--}}
{{--                                            For--}}
{{--                                            Free</a>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <!-- Single Table End -->--}}
{{--                    <!-- Single Table -->--}}
{{--                    <div class="col-lg-12 col-sm-6">--}}
{{--                        <div class="border-top pt-10 pb-8 aos-init aos-animate" data-aos="fade-up" data-aos-duration="800" data-aos-delay="500" data-aos-once="true">--}}
{{--                            <div class="row">--}}
{{--                                <div class="col-lg-3">--}}
{{--                                    <div class="mb-5 mb-lg-0">--}}
{{--                                        <h3 class="font-size-7">Basic</h3>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <div class="col-lg-3">--}}
{{--                                    <div class="">--}}
{{--                                        <ul class="list-unstyled font-size-6">--}}
{{--                                            <li class="heading-default-color mb-4">1 Social set</li>--}}
{{--                                            <li class="heading-default-color mb-4">3 users</li>--}}
{{--                                            <li class="heading-default-color mb-4">Basic features</li>--}}
{{--                                        </ul>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <div class="col-lg-3">--}}
{{--                                    <div class="pr-lg-15 pr-xl-10 mb-5 mb-lg-0">--}}
{{--                                        <h2 class="mb-0 font-size-11 font-weight-medium">€ <span class="dynamic-value" data-active="15" data-monthly="15" data-yearly="12,5"></span></h2>--}}
{{--                                        <p class="mb-0 font-size-5 pr-xl-22"> Pr. social set pr. month</p>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <div class="col-lg-3">--}}
{{--                                    <div class="mb-5 mb-lg-0 mt-4 text-left text-lg-right">--}}
{{--                                        <a class="btn btn-outline-gray-1 btn-2 border-width-2 rounded-5 gr-hover-bg-blue-3 heading-default-color" href="{{route('newRegister')}}">Start--}}
{{--                                            14 Days--}}
{{--                                            Free--}}
{{--                                            Trial</a>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <!-- Single Table End -->--}}
{{--                    <!-- Single Table -->--}}
{{--                    <div class="col-lg-12 col-sm-6">--}}
{{--                        <div class="border-top pt-10 pb-8 aos-init aos-animate" data-aos="fade-up" data-aos-duration="800" data-aos-delay="800" data-aos-once="true">--}}
{{--                            <div class="row">--}}
{{--                                <div class="col-lg-3">--}}
{{--                                    <div class="mb-5 mb-lg-0">--}}
{{--                                        <h3 class="font-size-7">Pro</h3>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <div class="col-lg-3">--}}
{{--                                    <div class="">--}}
{{--                                        <ul class="list-unstyled font-size-6">--}}
{{--                                            <li class="heading-default-color mb-4">1 Social set</li>--}}
{{--                                            <li class="heading-default-color mb-4">5 users</li>--}}
{{--                                            <li class="heading-default-color mb-4">Unlimited features</li>--}}
{{--                                        </ul>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <div class="col-lg-3">--}}
{{--                                    <div class="pr-lg-15 pr-xl-10 mb-5 mb-lg-0">--}}
{{--                                        <h2 class="mb-0 font-size-11 font-weight-medium">€ <span class="dynamic-value" data-active="40" data-monthly="40" data-yearly="30"></span></h2>--}}
{{--                                        <p class="mb-0 font-size-5 pr-xl-22">--}}
{{--                                            <span></span>Pr. social set pr. month--}}
{{--                                        </p>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <div class="col-lg-3">--}}
{{--                                    <div class="mb-5 mb-lg-0 mt-4 text-left text-lg-right">--}}
{{--                                        <a class="btn btn-outline-gray-1 btn-2 border-width-2 rounded-5 gr-hover-bg-blue-3 heading-default-color" href="{{route('newRegister')}}">--}}
{{--                                            Start 14 Days Free Trial--}}
{{--                                        </a>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <!-- Single Table End -->--}}
{{--                </div>--}}
{{--            </div>--}}
{{--            <!-- Table Main Body -->--}}
{{--        </div>--}}
{{--        <div class="pricing-bottom-shape d-none d-sm-block">--}}
{{--            <img src="http://planzer.io/image/svg/footer-shape.svg" alt="" data-aos="fade-left" data-aos-delay="500" data-aos-once="true" class="aos-init">--}}
{{--        </div>--}}
{{--    </div>--}}
{{--    <!-- End pricingTable Area -->--}}
@endsection


@push('js')

@endpush
