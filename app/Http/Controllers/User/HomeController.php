<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Testimonial;
use App\Traits\General;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class HomeController extends Controller
{
    use General;
    public function index(){
        return view('userPortal.index');
    }

    public function registerUserData(Request $request){
        return back();
    }

    public function homePage(){
        $testimonial = Testimonial::all();
        return view('frontend.dashboard.new_home',[
            'testimonial' => $testimonial
        ]);
    }

    public function about(){
        $testimonial = Testimonial::all();
        return view('frontend.about.index',[
            'testimonial' => $testimonial
        ]);
    }

    public function policies(){
        return view('frontend.policy.index');
    }

    public function pricing(){
        return view('frontend.pricing.index');
    }

    public function newLogin(){
        return view('auth.new_login');
    }

    public function newRegister(){
        return view('auth.new_register');
    }

    public function terms(){
        return view('frontend.terms.index');
    }

    public function faqs(){
        return view('frontend.faqs.index');
    }

    public function meetTeams(){
        return view('frontend.meetTeams.index');
    }

    public function career(){
        return view('frontend.career.index');
    }

    public function contactUs(){
        return view('frontend.contactUs.contact');
    }

    public function sendContactEmail(Request $request) {
        // dd($request->all());
        $this->sendEmail('leet@usama.live',$request->all(),'email-templates.contactUsAdmin','planzer.io');
        // $this->sendEmail($request->email,$request->all(),'email-templates.contactUs','planzer.io');

        return back()->with('success_message','We will contact you soon .. !');
    }

    public function plus(){
        return view('frontend.plus.index');
    }

    public function error(){
        return view('frontend.error.404');
    }

    public function bookDemo() {
        return view('frontend.bookDemo.index');
    }
}
