<?php

namespace App\Http\Controllers\Social;

use App\Http\Controllers\Controller;
use App\Models\SocialAccount;
use App\Models\SocialAccountManager;
use App\Traits\General;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class InstagramController extends Controller
{
    use General;
    public $ig;
    public $username;
    public $password;
    public $proxy;
    public $security_code;
    public $verification_code;


    public function uploadVideo(Request $request){
        dd($request->all());
    }

    public function index() {
        $insta = SocialAccountManager::where('social_network','=',SocialAccount::INSTAGRAM)->first();
        return view('social.insta.create',[
            'insta' => $insta
        ]);
    }

    public function getUsername($pid){
        $account = SocialAccountManager::where('pid','=',$pid)->first();
        $this->username = addslashes($account->username);
        return $account->username;
    }

    public function getUserPassword(){
        $account = SocialAccount::where('username','=',$this->username)->first();
        $this->password = addslashes($account->password);
        return $account->password;
    }

    public function refreshInstaAccount($pid){
        $set_id = session()->get('set_id');
        $this->getUsername($pid);
        $this->getUserPassword();
        $this->connect();
        $response = $this->login();
        if($response['status'] == "success") {
            try {
                $user = $this->ig->account->getCurrentUser()->getUser();
                $avatar = $user->getProfilePicUrl();
                $data = [
                    'set_id' => $set_id,
                    'ids' => ids(),
                    'social_network' => SocialAccount::INSTAGRAM,
                    'category' => 'profile',
                    'login_type' => 2,
                    'can_post' => 1,
                    'team_id' => 1,
                    'pid' => $user->getPk(),
                    'name' => $user->getFullName() ? $user->getFullName() : $user->getUsername(),
                    'username' => $user->getUsername(),
                    'token' => bcrypt($this->password),
                    'avatar' => $avatar,
                    'url' => 'https://instagram.com/' . $user->getUsername(),
                    'data' => NULL,
                    'proxy' => isset($this->proxy) ? $this->proxy : 'null',
                    'status' => 1,
                    'user_id' => isset(Auth::user()->id) ? Auth::user()->id : Auth::guard('admin')->user()->id
                ];
                if($this->checkInstaAccountExists($user->getPk() , $set_id)) {
                    SocialAccountManager::updateAccount($data);
                    return back()->with('info_message','Your Instagram Account Refresh Successfully......!');
                } else {
                    return back()->with('warning_message','Please Attach Your Instagram Account......!');
                }
            } catch (\Exception $e) {
                $response = [
                    "status" => "error",
                    "message" => __("Please login on instagram to pass checkpoint")
                ];
                return back()->with('error_message','something went wrong');
            }
        }
    }

    public function connectToInsta(Request $request){
        $this->username = addslashes($request->username);
        $this->password = addslashes($request->password);
        $this->proxy = $request->proxy;
        $this->security_code = $request->security_code;
        $this->verification_code = $request->verification_code;
        $this->connect();
        $response = $this->login();
        if($response['status'] == "success") {
            try {
                $user = $this->ig->account->getCurrentUser()->getUser();
                $avatar = save_img($user->getProfilePicUrl(), 'uploads/insta/avatar');
                $user_id = Auth::user()->id;
                $data = [
                    'ids' => ids(),
                    'social_network' => 'instagram',
                    'category' => 'profile',
                    'login_type' => 2,
                    'can_post' => 1,
                    'team_id' => 1,
                    'pid' => $user->getPk(),
                    'name' => $user->getFullName() ? $user->getFullName() : $user->getUsername(),
                    'username' => $user->getUsername(),
                    'token' => bcrypt($this->password),
                    'avatar' => $avatar,
                    'url' => 'https://instagram.com/' . $user->getUsername(),
                    'data' => 'NULL',
                    'proxy' => isset($this->proxy) ? $this->proxy : 'null',
                    'status' => 1,
                    'user_id' => $user_id
                ];
                if($this->checkInstaAccountExists($user->getPk())) {
                    SocialAccountManager::updateAccount($data);
                    SocialAccount::updateUserCredentials($this->username,$this->password,SocialAccount::INSTAGRAM);
                    return collect([
                        'status' => true,
                        'message' => 'Your Instagram Account Already Added......!'
                    ]);
                } else {
                    SocialAccountManager::createAccount($data);
                    SocialAccount::saveUserCredentials($this->username,$this->password,SocialAccount::INSTAGRAM);
                    return collect([
                        'status' => true,
                        'message' => 'Instagram Added Successfully......!'
                    ]);
                }
            } catch (\Exception $e) {
                return collect([
                    'status' => false,
                    'error_message' => $e->getMessage(),
                    'message' => __("Please login on instagram to pass checkpoint")
                ]);
            }
        } else {
            return collect([
                'status' => false,
                'message' => $response['message']
            ]);
        }
    }

    public function checkInstaAccountExists($pid , $set_id){
        return SocialAccountManager::checkInstagramAccountExistsAgainstComingUser($pid , $set_id);
    }

    public function connect(){
        $this->ig = new \InstagramAPI\Instagram(false, false);

        $this->ig->setVerifySSL(false);

        if($this->proxy != "") {
            $this->ig->setProxy($this->proxy);
        }
    }

    public function login(){

        try {
            $response = $this->ig->login($this->username, $this->password);
            return $this->check_2fa($response);

        } catch (\InstagramAPI\Exception\ChallengeRequiredException $e) {
            $challenge = $e->getResponse()->getChallenge();
            if(is_array($challenge)){
                $apiPath = $challenge['api_path'];
            }else{
                $apiPath = $challenge->getApiPath();
            }
        } catch (\InstagramAPI\Exception\CheckpointRequiredException $e) {
            return [
                "status" => "error",
                "message" => __("Please login on instagram to pass checkpoint")
            ];
        } catch (\InstagramAPI\Exception\AccountDisabledException $e) {
            return [
                "status" => "error",
                "message" => __("Your account has been disabled for violating instagram terms")
            ];
        } catch (\InstagramAPI\Exception\SentryBlockException $e) {
            return [
                "status" => "error",
                "message" => __("Your account has been banned from instagram api for spam behaviour or otherwise abusing")
            ];
        } catch (\InstagramAPI\Exception\IncorrectPasswordException $e) {
            return [
                "status" => "error",
                "message" => __("The password you entered is incorrect please try again")
            ];
        } catch (\InstagramAPI\Exception\InstagramException $e) {
            if ($e->hasResponse()) {
                if($e->getResponse()->getMessage() == "consent_required"){
                    return [
                        "status" => "error",
                        "message" => __("Go to instagram.com login with this account and then try to add this account again")
                    ];
                }
                return [
                    "status" => "error",
                    "message" => $e->getResponse()->getMessage()
                ];
            } else {
                $message = explode(":", $e->getMessage(), 2);
                $message = explode(" (", end($message));
                return [
                    "status" => "error",
                    "message" => $message[0]
                ];
            }
        } catch (\Exception $e) {
            return [
                "status" => "error",
                "message" => __("Oops, something went wrong please try again later")
            ];
        }
    }

    public function check_2fa($response){
        if (!is_null($response) && $response->isTwoFactorRequired()) {
            $phone_number = $response->getTwoFactorInfo()->getObfuscatedPhoneNumber();
            $twofa = $response->getTwoFactorInfo()->getTwoFactorIdentifier();
            _ss($this->username."_twofa", $twofa);

            return [
                "status"   => "error",
                "message"  => sprintf( __("Enter the 6 digit code we sent to the number ending in %s"), $phone_number),
                "callback" => '<script type="text/javascript">Instagram_profiles.open_verification_form();</script>'
            ];

        }

        return [
            "status" => "success",
            "message" => __("Success")
        ];
    }

    public function confirm_2fa(){
        $twofa = _s($this->username."_twofa");
        try {

            $this->ig->finishTwoFactorLogin($this->username, $this->password,  $twofa, $this->verification_code);

            return [
                "status" => "success",
                "message" => __("Success")
            ];

        } catch (\InstagramAPI\Exception\ChallengeRequiredException $e) {

            $apiPath = $e->getResponse()->getChallenge()->getApiPath();

        } catch (\InstagramAPI\Exception\CheckpointRequiredException $e) {
            return [
                "status" => "error",
                "message" => __("Please login on instagram to pass checkpoint")
            ];

        } catch (\InstagramAPI\Exception\AccountDisabledException $e) {

            return [
                "status" => "error",
                "message" => __("Your account has been disabled for violating instagram terms")
            ];

        } catch (\InstagramAPI\Exception\SentryBlockException $e) {

            return [
                "status" => "error",
                "message" => __("Your account has been banned from instagram api for spam behaviour or otherwise abusing")
            ];

        } catch (\InstagramAPI\Exception\IncorrectPasswordException $e) {

            return [
                "status" => "error",
                "message" => __("The password you entered is incorrect, please try again")
            ];

        } catch (\InstagramAPI\Exception\InstagramException $e) {

            if ($e->hasResponse()) {

                if($e->getResponse()->getMessage() == "consent_required"){
                    return [
                        "status" => "error",
                        "message" => __("Go to instagram.com login with this account and then try to add this account again")
                    ];
                }

                return [
                    "status" => "error",
                    "message" => $e->getResponse()->getMessage()
                ];

            } else {

                $message = explode(":", $e->getMessage(), 2);
                return [
                    "status" => "error",
                    "message" => end($message)
                ];
            }

        } catch (\Exception $e) {

            return [
                "status" => "error",
                "message" => __("Oops, something went wrong please try again later")
            ];

        }
    }

    public function postToInstaViw(){
        $account = SocialAccountManager::where('social_network','=',SocialAccount::INSTAGRAM)->first();
        return view('social.insta.postToInsta', [
            'account' => $account
        ]);
    }

    public function postToInstaGram(Request $request) {
        $socialAccount = SocialAccount::where('social_type','=',SocialAccount::INSTAGRAM)->first();
        $caption = $this->create_caption($request->description);
        $this->ig = new \InstagramAPI\Instagram(false, false);
        $params['caption'] = $caption;
        try {
            $this->ig->login($socialAccount->username , $socialAccount->password);
        } catch (\Exception $e) {
            echo 'Something went wrong: ' . $e->getMessage() . "\n";
            exit(0);
        }
        /**
         * post to instagram using instagram sdk
        **/
        try {
            if($request->type == 'media') {
                if($request->file_type == 'image') {
                    $image = $this->uploadFile('uploads/users', $request['files']);
                    $file = public_path() . '/' . $image;
                    $img = new \InstagramAPI\Media\Photo\InstagramPhoto($file_path, [
                        "targetFeed" => \InstagramAPI\Constants::FEED_TIMELINE,
                        "operation" => \InstagramAPI\Media\InstagramMedia::CROP
                    ]);
                    $response = $this->ig->timeline->uploadPhoto($img->getFile(), $params);
                } else {
                    $video = $this->uploadVideoFile('uploads/users', $request['files']);
                    $file = public_path() . '/' . $video;
                    $response = $this->ig->timeline->uploadVideo($file, $params);
                }
                if(isset($request->description) && $request->description != ""){
                    $comment = $this->create_caption($request->description);
                    $this->ig->media->comment($response->getMedia()->getPk(), $comment);
                }
                return collect([
                    'status' => true,
                    'data' => [],
                    'message' => 'Post Added successfully............!'
                ]);
            } else if($request->type == 'story') {
                if($request->file_type == 'image') {
                    $image = $this->uploadFile('uploads/users', $request['files']);
                    $file = public_path() . '/' . $image;
                    $img = new \InstagramAPI\Media\Photo\InstagramPhoto($file, [
                        "targetFeed" => \InstagramAPI\Constants::FEED_TIMELINE_ALBUM,
                        "operation" => \InstagramAPI\Media\InstagramMedia::CROP,
                    ]);
                    $this->ig->story->uploadPhoto($img->getFile(), $params);
                } else {
                    $video = $this->uploadVideoFile('uploads/users', $request['files']);
                    $file = public_path() . '/' . $video;
                    $this->ig->story->uploadVideo($file,$params);
                }
                return collect([
                    'status' => true,
                    'data' => [],
                    'message' => 'Story Added successfully............!'
                ]);
            } else if($request->type == 'album') {
                $carousels = array();
                foreach ($request['files'] as $key => $media) {
                    if($request->file_type == 'image') {
                        $image = $this->uploadFile('uploads/users', $media);
                        $img = new \InstagramAPI\Media\Photo\InstagramPhoto($image, [
                            "targetFeed" => \InstagramAPI\Constants::FEED_TIMELINE_ALBUM,
                            "operation" => \InstagramAPI\Media\InstagramMedia::CROP,
                        ]);
                        $carousels[] = [
                            'type' => 'photo',
                            'file' => $img->getFile()
                        ];
                    } else {
                        $video = $this->uploadVideoFile('uploads/users', $request['files']);
                        $file = public_path() . '/' . $video;
                        $carousels[] = [
                            'type' => 'video',
                            'file' => $file
                        ];
                    }
                }
                $this->ig->timeline->uploadAlbum($carousels, $params);
                return collect([
                    'status' => true,
                    'data' => [],
                    'message' => 'Album Added successfully............!'
                ]);
            }
        } catch (\Exception $e) {
            echo 'Something went wrong: ' . $e->getMessage() . "\n";
        }
    }

    public function getFFConfig() {
        $ffmpeg = trim(shell_exec('which ffmpeg'));
        $ffprobe = trim(shell_exec('which ffprobe'));
        if (!empty($ffmpeg) && !empty($ffprobe)) {
            $config = [
                'ffmpeg.binaries'  => $ffmpeg,
                'ffprobe.binaries' => $ffprobe,
            ];
            return $config;
        }
        if (stripos(PHP_OS, 'win') === 0) {
            // code for windows
            $config = [
                'ffmpeg.binaries'  => resource_path('ffmpeg/windows/ffmpeg.exe'),
                'ffprobe.binaries' => resource_path('ffmpeg/windows/ffprobe.exe'),
            ];
        } elseif (stripos(PHP_OS, 'darwin') === 0) {
            // code for OS X
            $config = [
                'ffmpeg.binaries'  => resource_path('ffmpeg/darwin/ffmpeg'),
                'ffprobe.binaries' => resource_path('ffmpeg/darwin/ffprobe'),
            ];
        } elseif (stripos(PHP_OS, 'linux') === 0) {
            // code for Linux
            $config = [
                'ffmpeg.binaries'  => resource_path('ffmpeg/linux/ffmpeg'),
                'ffprobe.binaries' => resource_path('ffmpeg/linux/ffprobe'),
            ];
        }
        return $config;
    }

    public function resizeVideo($input) {
        $path_data = pathinfo($input);
        $output_dir = public_path('encodes/' .pathinfo($path_data['dirname'], PATHINFO_BASENAME ) . DIRECTORY_SEPARATOR . $path_data['filename']);
        \File::makeDirectory($output_dir, 0775, true, true);
        $ffmpeg_config = self::getFFConfig();

        //cut the video
        $cut = $ffmpeg_config['ffmpeg.binaries']." -y -threads 2 -ss 00:00:00 -t 00:00:59 -i $input -vcodec copy -acodec copy $output_dir/cut.mp4";
        exec($cut);

        //crop
        $cut = $ffmpeg_config['ffmpeg.binaries'].' -y -i '.$output_dir.'/cut.mp4 -vf "scale=\'min(640,iw)\':min\'(640,ih)\':force_original_aspect_ratio=decrease,pad=640:640:(ow-iw)/2:(oh-ih)/2" -strict -2 '.$output_dir.'/cropped-square.mp4';
        exec($cut);

        //transcode
        $cut = $ffmpeg_config['ffmpeg.binaries']." -y -i $output_dir/cropped-square.mp4 -vcodec libx264 -r 30 -vb 1024k -minrate 1024k -maxrate 1024k -bufsize 1024k -acodec aac -b:a 128k -ar 44100 -strict experimental -qscale 0 $output_dir/output-mpeg4.mp4";
        exec($cut);

        $ffmpeg_path = dirname($ffmpeg_config['ffmpeg.binaries']);
        putenv('PATH=$PATH:'.$ffmpeg_path);
        return "$output_dir/output-mpeg4.mp4";

    }

    public function create_caption($caption){
        $caption = preg_replace("/\r\n\r\n/", "?.??.?", $caption);
        $caption = preg_replace("/\r\n/", "?.?", $caption);
        $caption = str_replace("?.? ?.?", "?.?", $caption);
        $caption = str_replace(" ?.?", "?.?", $caption);
        $caption = str_replace("?.? ", "?.?", $caption);
        $caption = str_replace("?.??.?", "\n\n", $caption);
        $caption = str_replace("?.?", "\n", $caption);
        return $caption;
    }
}
