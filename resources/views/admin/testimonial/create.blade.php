@extends('layouts.simple.master')
@section('title', 'Testimonial')

@section('css')
@endsection

@section('style')
@endsection

@section('breadcrumb-title')
    <h3>Add New Testimonial</h3>
@endsection

@section('breadcrumb-items')
    <li class="breadcrumb-item">Company</li>
    <li class="breadcrumb-item active">Create Company</li>
@endsection

@section('content')
    <br>
    <div>
        @if (session('success_message'))
            <div class="alert alert-success" role="alert">
                {{ session('success_message') }}
            </div>
        @endif
        @if (Session::has('error'))
            <div class="alert alert-danger alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>{!! Session('error') !!}</strong>
            </div>
        @endif
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">

                    <div class="card-body">
                        <div class="row" style="display:flex; align-items:center; justify-content: space-between;">
                            <h3>Create Testimonial</h3>
                        </div>
                        <br>
                        <form class="needs-validation" enctype="multipart/form-data" method="POST" action="{{route('addTestimonial')}}">
                            @csrf
                            <div class="row">
                                <div class="col-md-6 mb-3">
                                    <label for="first_name">Name</label>
                                    <input  class="form-control"  name="name" id="" type="text" placeholder="Enter Name" required="">
                                </div>
                                <div class="col-md-6 mb-3">
                                    <label for="last_name">Image</label>
                                    <input  class="form-control" name="file" type="file" required="">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 mb-3">
                                    <label for="first_name">Description</label>
                                    <textarea  class="form-control"  name="description"></textarea>
                                </div>
                                <div class="col-md-6 mb-3">
                                    <label for="last_name">Title</label>
                                    <input  class="form-control" name="title" type="text" required="">
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <button type="submit" class="btn btn-primary">Save</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="{{asset('assets/js/form-validation-custom.js')}}"></script>
@endsection
