<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSocialAccountManagersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('social_account_managers', function (Blueprint $table) {
            $table->id();
            $table->longText('ids');
            $table->string('social_network');
            $table->string('category');
            $table->string('team_id');
            $table->string('login_type');
            $table->string('can_post');
            $table->longText('pid');
            $table->longText('name');
            $table->longText('username');
            $table->longText('token');
            $table->longText('avatar');
            $table->longText('url');
            $table->longText('data');
            $table->longText('proxy');
            $table->longText('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('social_account_managers');
    }
}
