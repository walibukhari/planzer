<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use HasFactory, Notifiable;

    const USER_TYPE = 0;


     public function Profile_Data() {
        return $this->hasOne(Profile::class, 'user_id','id');
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'type'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function profile(){
        return $this->hasOne(Profile::class,'user_id','id');
    }

    public static function createUser($request){
        return self::create([
            'name' => $request['first_name'] . ' ' . $request['last_name'],
            'email' => $request['email'],
            'password' => bcrypt($request['password']),
            'type' => self::USER_TYPE,
        ]);
    }

    public static function updateUser($user_id,$request){
        return self::where('user_id','=',$user_id)->udpate([
            'name' => $request['name'],
            'email' => $request['email'],
            'password' => self::USER_TYPE,
        ]);
    }
}
