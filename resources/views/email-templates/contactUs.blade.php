<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Cuba admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities.">
    <meta name="keywords" content="admin template, Cuba admin template, dashboard template, flat admin template, responsive admin template, web app">
    <meta name="author" content="pixelstrap">
    <link rel="icon" href="{{asset('assets/images/favicon.png')}}" type="image/x-icon">
    <link rel="shortcut icon" href="{{asset('assets/images/favicon.png')}}" type="image/x-icon">
    <title>Cuba - Premium Admin Template</title>
    <link href="https://fonts.googleapis.com/css?family=Work+Sans:100,200,300,400,500,600,700,800,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <style type="text/css">
      body{
      text-align: center;
      margin: 0 auto;
      width: 650px;
      font-family: work-Sans, sans-serif;
      background-color: #f6f7fb;
      display: block;
      }
      ul{
      margin:0;
      padding: 0;
      }
      li{
      display: inline-block;
      text-decoration: unset;
      }
      a{
      text-decoration: none;
      }
      h5{
      margin:10px;
      color:#777;
      }
      .text-center{
      text-align: center
      }
      .main-bg-light{
      background-color: #fafafa;
      }
      .title{
      color: #444444;
      font-size: 22px;
      font-weight: bold;
      margin-top: 20px;
      margin-bottom: 0;
      padding-bottom: 0;
      text-transform: capitalize;
      display: inline-block;
      line-height: 1;
      }
      .menu{
      width:100%;
      }
      .menu li a{
      text-transform: capitalize;
      color:#444;
      font-size:16px;
      margin-right:15px
      }
      .main-logo{
      padding: 10px 20px;
      }
      .product-box .product {
      border:1px solid #ddd;
      text-align: center;
      position: relative;
      margin: 0 15px;
      }
      .product-info {
      margin-top: 15px;
      }
      .product-info h6 {
      line-height: 1;
      margin-bottom: 0;
      padding-bottom: 5px;
      font-size: 14px;
      font-family: "Open Sans", sans-serif;
      color: #777;
      margin-top: 0;
      }
      .product-info h4 {
      font-size: 16px;
      color: #444;
      font-weight: 700;
      margin-bottom: 0;
      margin-top: 5px;
      padding-bottom: 5px;
      line-height: 1;
      }
      .add-with-banner > td{
      padding:0 15px;
      }
      .footer-social-icon tr td img{
      margin-left:5px;
      margin-right:5px;
      }
    </style>
  </head>
  <body style="margin: 20px auto;">
    <table align="center" border="0" cellpadding="0" cellspacing="0" style="background-color: #fff; -webkit-box-shadow: 0px 0px 14px -4px rgba(0, 0, 0, 0.2705882353);box-shadow: 0px 0px 14px -4px rgba(0, 0, 0, 0.2705882353);">
      <tbody>
        <tr>
          <td>
            <h1 class="title" style="text-align: center; width:100%;">Contact Us</h1>
            <h5 style="text-align: center;">Thanks for contacting us we will back you shortly ... !</h5>
            <table class="main-bg-light text-center" align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="margin-top:30px;">
              <tbody>
                <tr>
                  <td style="padding: 30px;">
                    <div>
                      <h4 class="title" style="margin:0;text-align: center;">Follow us</h4>
                    </div>
                    <table class="footer-social-icon" border="0" cellpadding="0" cellspacing="0" align="center" style="margin-top:20px;">
                      <tbody>
                        <tr>
                          <td><a href="#"><img src="{{asset('assets/images/email-template/facebook.png')}}" alt=""></a></td>
                          <td><a href="#"><img src="{{asset('assets/images/email-template/youtube.png')}}" alt=""></a></td>
                          <td><a href="#"><img src="{{asset('assets/images/email-template/twitter.png')}}" alt=""></a></td>
                          <td><a href="#"><img src="{{asset('assets/images/email-template/gplus.png')}}" alt=""></a></td>
                          <td><a href="#"><img src="{{asset('assets/images/email-template/linkedin.png')}}" alt=""></a></td>
                          <td><a href="#"><img src="{{asset('assets/images/email-template/pinterest.png')}}" alt=""></a></td>
                        </tr>
                      </tbody>
                    </table>
                    <div style="border-top: 1px solid #ddd; margin: 20px auto 0;"></div>
                    <table border="0" cellpadding="0" cellspacing="0" width="100%" style="margin: 20px auto 0;">
                      <tbody>
                        <tr>
                          <td><a href="#" style="font-size:13px">Want to change how you receive these emails?</a></td>
                        </tr>
                        <tr>
                          <td>
                            <p style="font-size:13px; margin:0;">2018 - 19 Copy Right by SocialDay</p>
                          </td>
                        </tr>
                        <tr>
                          <td><a href="#" style="font-size:13px; margin:0;text-decoration: underline;">Unsubscribe</a></td>
                        </tr>
                      </tbody>
                    </table>
                  </td>
                </tr>
              </tbody>
            </table>
          </td>
        </tr>
      </tbody>
    </table>
  </body>
</html>
