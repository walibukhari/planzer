<div class='u--page'>
    <section class='tPr--planchart'>
        <div class="pt-23">
            <div class="container">
                <!-- Section Padding -->
                <div class="row justify-content-center">
                    <div class="col-xl-7 col-lg-8 col-md-11">
                        <div class="text-center mb-10 mb-lg-23" data-aos="fade-up" data-aos-duration="500" data-aos-once="true">
                            <h2 class="font-size-11 mb-7">Pricing & Plan</h2>
                            <p class="font-size-7 mb-0">Our focus is always on finding the
                                best people to work with. Our bar is high, but you look ready
                                to take on the challenge.</p>
                        </div>
                    </div>
                </div>
                <!-- End Section Padding -->
            </div>
        </div>
        <div class='o--container--responsive o--container--responsive--lg'>
            <section class='tPF--plans__cont' id='yearly'>
                <div class='tPF--alt--plan tPf--yearly u--p__0'>
                    <div class='tPF--alt--plan__item tPF--alt--plan__item--calculator'>
                        <h2 class='tPF--alt--plan__title'>Customize Plan</h2>
                        <div class='tPF--planCustomizations'>
                            <div class="tPF--planToggle">
                                <div class="tPF--planToggle__option tPF--planToggle__option--yearly tPF--planToggle__option--active">Yearly</div>
                                <div class="tPF--planToggle__option tPF--planToggle__option--monthly">Monthly</div>
                            </div>
                            <div class='tPF--planCustomizationsContainer__item'>
                                <p class='tPF--alt--bottomCol--toolTip u--p__t__lg'>
                                    Additional Social Sets
{{--                                    <i class='i-help'></i>--}}
{{--                                    <span class='alt--toolTip__text'>--}}
{{--A Social Set includes 1 Instagram, 1 Facebook, 1 Twitter, and 1 Pinterest profile.--}}
{{--</span>--}}
                                </p>
                                <div class='tPF--counter'>
                                    <a class='tPF--counter__minus tPF--counter__minus--yearlySocialSets'></a>
                                    <input class='tPF--counter__input tPF--counter__input--yearlySocialSets' id='tPF--counter__input--yearlySocialSets' type='text' value='0'>
                                    <a class='tPF--counter__plus tPF--counter__plus--yearlySocialSets'></a>
                                </div>
                            </div>
                            <div class='tPF--planCustomizationsContainer__item'>
                                <p class='u--p__t__lg'>Additional Users</p>
                                <div class='tPF--counter'>
                                    <a class='tPF--counter__minus tPF--counter__minus--yearlyTeam'></a>
                                    <input class='tPF--counter__input tPF--counter__input--yearlyTeam' id='tPF--counter__input--yearlyTeam' type='text' value='0'>
                                    <a class='tPF--counter__plus tPF--counter__plus--yearlyTeam'></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class='qa--plan__item--free tPF--alt--plan__item tPF--alt--plan__item--free'>
                        <div class='tPF--alt--mpText__mobile u--hide--l'>

                        </div>
                        <h2 style="color: #22b0fc !important; text-align: center;" class='tPF--alt--plan__title u--text--bold'>
                            Free
                        </h2>

                        <div class='tPF--alt--plan__price'>
                            <div class="customFlex">
                                <sup class='tPF--alt--plan__priceCurrency'>
                                    €
                                </sup>
                                <span class='qa--alt--plan__price tPF--alt--plan__priceValue tPF--alt--plan__priceValue--freeYearly'>0</span>
                            </div>
                            <span style="color:#000 !important;" class='tPF--alt--plan__time u--text--light u--m__t__xs'>
free forever
</span>
                            <span class='tPF--alt--plan__priceDuration'>

</span>

                            <p class='tPF--alt--plan__planIncludesTitle'>
{{--                                Includes--}}
                            </p>
                            <span class='tPF--alt--plan__planSummary'>
1 Social Set
<br> 1 User
</span>
                            <p style="font-weight:normal !important;font-size:15px !important;" class='tPF--alt--plan__planAddonsTitle'>
                                Simple features
                            </p>
                            <br>
                            <div class="pt-7 pt-lg-17">
                                <a class="btn btn-blue-3 btn-2 rounded-5" href="/register">Start 14 days free trial</a>
                            </div>
                        </div>
                    </div>
                    <div class='qa--plan__item--starter tPF--alt--plan__item tPF--alt--plan__item--starter'>
                        <div class='tPF--alt--mpText__mobile u--hide--l'>

                        </div>
                        <h2 style="color: #22b0fc !important; text-align: center;" class='tPF--alt--plan__title u--text--bold'>
                            Basic
                        </h2>
                        <div class='tPF--alt--plan__price'>
                            <div class="customFlex">
                                <sup class='tPF--alt--plan__priceCurrency'>
                                    €
                                </sup>
                                <span class='qa--alt--plan__price tPF--alt--plan__priceValue tPF--alt--plan__priceValue--starterYearly'></span>
                            </div>
                            <span style="color:#000 !important;" class='tPF--alt--plan__time u--text--light u--m__t__xs'>
first 14 days free
</span>
                            <span class='tPF--alt--plan__priceDuration'>

</span>

                            <p class='tPF--alt--plan__planIncludesTitle'>
{{--                                Includes--}}
                            </p>
                            <span class='tPF--alt--plan__planSummary'>
1 Social Set
<br> 3 Users
</span>
                            <p style="font-weight:normal !important;font-size:15px !important;" class='tPF--alt--plan__planAddonsTitle'>
                                Basic features
                            </p>
                            <br>
                            <div class="pt-7 pt-lg-17">
                                <a class="btn btn-blue-3 btn-2 rounded-5" href="/register">Start 14 days free trial</a>
                            </div>
                        </div>
                    </div>
                    <div class='qa--plan__item--growth tPF--alt--plan__item tPF--alt--plan__item--growth'>
                        <h2 style="color: #22b0fc !important; text-align: center;" class='tPF--alt--plan__title u--text--bold'>
                            Pro
                        </h2>

                        <div class='tPF--alt--plan__price'>
                            <div class="customFlex">
                                <sup class='tPF--alt--plan__priceCurrency'>
                                    €
                                </sup>
                                <span class='qa--alt--plan__price tPF--alt--plan__priceValue tPF--alt--plan__priceValue--growthYearly'>13</span>
                            </div>
                            <span style="color:#000 !important;" class='tPF--alt--plan__time u--text--light u--m__t__xs'>
first 14 days free
</span>
                            <span class='tPF--alt--plan__priceDuration'>

</span>

                            <p class='tPF--alt--plan__planIncludesTitle'>
{{--                                Includes--}}
                            </p>
                            <span class='tPF--alt--plan__planSummary'>
1 Social Set
<br>5 Users
</span>
                            <p style="font-weight:normal !important;font-size:15px !important;" class='tPF--alt--plan__planAddonsTitle'>
                                Unlimited features
                            </p>
                            <br>
                            <div class="pt-7 pt-lg-17">
                                <a class="btn btn-blue-3 btn-2 rounded-5" href="/register">Start 14 days free trial</a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class='tPF--plans__cont' id='monthly'>
                <div class='tPF--alt--plan tPf--monthly u--p__0'>
                    <div class='tPF--alt--plan__item tPF--alt--plan__item--calculator'>
                        <h2 class='tPF--alt--plan__title'>Customize Plan</h2>
                        <div class='tPF--planCustomizations'>
                            <div class='tPF--planToggle'>
                                <div class='tPF--planToggle__option tPF--planToggle__option--yearly'>Yearly</div>
                                <div class='tPF--planToggle__option tPF--planToggle__option--monthly tPF--planToggle__option--active'>Monthly</div>
                            </div>
                            <div class='tPF--planCustomizationsContainer__item'>
                                <p class='tPF--alt--bottomCol--toolTip u--p__t__lg'>
                                    Additional Social Sets
{{--                                    <i class='i-help'></i>--}}
{{--                                    <span class='alt--toolTip__text'>--}}
{{--A Social Set includes 1 Instagram, 1 Facebook, 1 Twitter, and 1 Pinterest profile.--}}
{{--</span>--}}
                                </p>
                                <div class='tPF--counter'>
                                    <a class='tPF--counter__minus tPF--counter__minus--monthlySocialSets'></a>
                                    <input class='tPF--counter__input tPF--counter__input--monthlySocialSets' id='tPF--counter__input--monthlySocialSets' type='text' value='0'>
                                    <a class='tPF--counter__plus tPF--counter__plus--monthlySocialSets'></a>
                                </div>
                            </div>
                            <div class='tPF--planCustomizationsContainer__item'>
                                <p class='u--p__t__lg'>Additional Users</p>
                                <div class='tPF--counter'>
                                    <a class='tPF--counter__minus tPF--counter__minus--monthlyTeam'></a>
                                    <input class='tPF--counter__input tPF--counter__input--monthlyTeam' id='tPF--counter__input--monthlyTeam' type='text' value='0'>
                                    <a class='tPF--counter__plus tPF--counter__plus--monthlyTeam'></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class='qa--plan__item--free tPF--alt--plan__item tPF--alt--plan__item--free'>
                        <h2 style="color: #22b0fc !important; text-align: center;" class='tPF--alt--plan__title u--text--bold'>
                            Free
                        </h2>

                        <div class='tPF--alt--plan__price'>
                            <div class="customFlex">
                                <sup class='tPF--alt--plan__priceCurrency'>
                                    €
                                </sup>
                                <span class='qa--alt--plan__price tPF--alt--plan__priceValue tPF--alt--plan__priceValue--freeMonthly u--text--bold'>0</span>
                            </div>
                            <span style="color:#000 !important;" class='tPF--alt--plan__time u--text--light u--m__t__xs'>
free forever
</span>
                            <span class='tPF--alt--plan__priceDuration'>

</span>

                            <p class='tPF--alt--plan__planIncludesTitle'>
{{--                                Includes--}}
                            </p>
                            <span class='tPF--alt--plan__planSummary'>
                                1 Social Set
                                   <br>
                                    1 User
                                </span>
                            <p style="font-weight:normal !important;font-size:15px !important;" class='tPF--alt--plan__planAddonsTitle'>
                                Simple features
                            </p>
                            <br>
                            <div class="pt-7 pt-lg-17">
                                <a class="btn btn-blue-3 btn-2 rounded-5" href="/register">Start 14 days free trial</a>
                            </div>
                        </div>
                    </div>
                    <div class='qa--plan__item--starter tPF--alt--plan__item tPF--alt--plan__item--starter'>
                        <h2 style="color: #22b0fc !important; text-align: center;" class='tPF--alt--plan__title u--text--bold'>
                            Basic
                        </h2>

                        <div class='tPF--alt--plan__price'>
                            <div class="customFlex">
                                <sup class='tPF--alt--plan__priceCurrency'>
                                    €
                                </sup>
                                <span class='qa--alt--plan__price tPF--alt--plan__priceValue tPF--alt--plan__priceValue--starterMonthly u--text--bold'>9</span>
                            </div>
                            <span style="color:#000 !important;" class='tPF--alt--plan__time u--text--light u--m__t__xs'>
first 14 days free
</span>
                            <span class='tPF--alt--plan__priceDuration'>

</span>
                            <p class='tPF--alt--plan__planIncludesTitle'>
{{--                                Includes--}}
                            </p>
                            <span class='tPF--alt--plan__planSummary'>
1 Social Set
<br>
3 Users
</span>
                            <p style="font-weight:normal !important;font-size:15px !important;" class='tPF--alt--plan__planAddonsTitle'>
                                Basic features
                            </p>
                            <br>
                            <div class="pt-7 pt-lg-17">
                                <a class="btn btn-blue-3 btn-2 rounded-5" href="/register">Start 14 days free trial</a>
                            </div>
                        </div>
                    </div>
                    <div class='qa--plan__item--growth tPF--alt--plan__item tPF--alt--plan__item--growth'>
                        <h2 style="color: #22b0fc !important; text-align: center;" class='tPF--alt--plan__title u--text--bold'>
                            Pro
                        </h2>

                        <div class='tPF--alt--plan__price'>
                            <div class="customFlex">
                                <sup class='tPF--alt--plan__priceCurrency'>
                                    €
                                </sup>
                                <span class='qa--alt--plan__price tPF--alt--plan__priceValue tPF--alt--plan__priceValue--growthMonthly u--text--bold'>15</span>
                            </div>
                            <span style="color:#000 !important;" class='tPF--alt--plan__time u--text--light u--m__t__xs'>
first 14 days free
</span>
                            <span class='tPF--alt--plan__priceDuration'>

</span>
                            <p class='tPF--alt--plan__planIncludesTitle'>
{{--                                Includes--}}
                            </p>
                            <span class='tPF--alt--plan__planSummary'>
1 Social Set

<br>
5 Users
</span>
                            <p style="font-weight:normal !important;font-size:15px !important;" class='tPF--alt--plan__planAddonsTitle'>
                                Unlimited features
                            </p>
                            <br>
                            <div class="pt-7 pt-lg-17">
                                <a class="btn btn-blue-3 btn-2 rounded-5" href="/register">Start 14 days free trial</a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </section>
    <script>
        function goToCheckoutPage(plan, isMonthly){

            var additionalUsers = 0;
            var additionalSocialSets = 0;
            var additionalOptionsString = "";

            if(isMonthly){
                additionalUsers = document.getElementById('tPF--counter__input--monthlyTeam').value ;
                additionalSocialSets = document.getElementById('tPF--counter__input--monthlySocialSets').value ;
            }else{
                additionalUsers = document.getElementById('tPF--counter__input--yearlyTeam').value ;
                additionalSocialSets = document.getElementById('tPF--counter__input--yearlySocialSets').value ;
            }

            additionalOptionsString = `additionalSocialSetCount=${additionalSocialSets}&additionalTeamMemberCount=${additionalUsers}`

            window.open(`https://app.later.com/plans/${plan}?${isMonthly? "" : "isMonthly=false&"}${additionalOptionsString}`);

        }

        $(window).on('scroll', function() {
            var scrolled = $(this).scrollTop();
            $('.tPF--alt--sticky_subnav').removeClass('shadow').filter(function() {
                return scrolled >= $(this).offset().top-65;
            }).addClass('shadow');
        });



        $(window).ready(function(){
            // Yearly
            var starterYearly = window.OPTIMIZELY_VAR_STARTERYEARLY || 12.5;
            var growthYearly = window.OPTIMIZELY_VAR_GROWTHYEARLY || 30;
            var advancedYearly = window.OPTIMIZELY_VAR_ADVANCEDYEARLY || 33.33;
            // Initial Value
            var socialSetValue = 5;
            var teamMemberValue = 5;
            //Final Value
            var finalSocialSetValueStarter = 0;
            var finalSocialSetValueGrowth = 0;
            var finalSocialSetValueAdvanced = 0;
            var finalTeamMemberValue = 0;
            //Display Value on Load
            showPriceOnLoad ("isYearly", starterYearly, growthYearly, advancedYearly, "tPF--alt--plan__priceValue--");

            //Social Sets Yearly
            //Minus
            $('.tPF--counter__minus--yearlySocialSets').click(function () {
                var $input = $(this).parent().find('input.tPF--counter__input--yearlySocialSets');
                var count = parseInt($input.val()) - 1;
                count = count < 0 ? 0 : count;
                $input.val(count);
                $input.change();
                //Final Social Sets Value
                finalSocialSetValueStarter  = parseFloat($input.val()) * parseFloat(starterYearly);
                finalSocialSetValueGrowth  = parseFloat($input.val()) * parseFloat(growthYearly);
                finalSocialSetValueAdvanced  = parseFloat($input.val()) * parseFloat(advancedYearly);
                finalYearlyValue();
            });
            //Plus
            $('.tPF--counter__plus--yearlySocialSets').click(function () {
                var $input = $(this).parent().find('input.tPF--counter__input--yearlySocialSets');
                var count = parseInt($input.val()) + 1;
                count = count > 30 ? 30 : count;
                $input.val(count);
                $input.change();
                //Final Social Sets Value
                finalSocialSetValueStarter  = parseFloat($input.val()) * parseFloat(starterYearly);
                finalSocialSetValueGrowth  = parseFloat($input.val()) * parseFloat(growthYearly);
                finalSocialSetValueAdvanced  = parseFloat($input.val()) * parseFloat(advancedYearly);
                finalYearlyValue();
            });

            //Team Members Yearly
            //Minus
            $('.tPF--counter__minus--yearlyTeam').click(function () {
                var $input = $(this).parent().find('input.tPF--counter__input--yearlyTeam');
                var count = parseInt($input.val()) - 1;
                count = count < 0 ? 0 : count;
                $input.val(count);
                $input.change();
                //Final Team Member Value
                finalTeamMemberValue = parseFloat($input.val()) * parseFloat(teamMemberValue);
                finalYearlyValue();
            });
            //Plus
            $('.tPF--counter__plus--yearlyTeam').click(function () {
                var $input = $(this).parent().find('input.tPF--counter__input--yearlyTeam');
                var count = parseInt($input.val()) + 1;
                count = count > 15 ? 15 : count;
                $input.val(count);
                $input.change();
                //Final Team Member Value
                finalTeamMemberValue = parseFloat($input.val()) * parseFloat(teamMemberValue);
                finalYearlyValue();
            });

            function finalYearlyValue() {
                var updatedStarterYearly = finalSocialSetValueStarter + finalTeamMemberValue + starterYearly;
                $('.tPF--alt--plan__priceValue--starterYearly').text(updatedStarterYearly.toFixed(1));
                var updatedGrowthYearly = finalSocialSetValueGrowth + finalTeamMemberValue + growthYearly;
                $('.tPF--alt--plan__priceValue--growthYearly').text(updatedGrowthYearly);
                var updatedAdvancedYearly = finalSocialSetValueAdvanced + finalTeamMemberValue + advancedYearly;
                $('.tPF--alt--plan__priceValue--advancedYearly').text(updatedAdvancedYearly.toFixed(1));
            }


            // Monthly
            var starterMonthly = window.OPTIMIZELY_VAR_STARTERMONTHLY || 15;
            var growthMonthly = window.OPTIMIZELY_VAR_GROWTHMONTHLY || 40;
            var advancedMonthly = window.OPTIMIZELY_VAR_ADVANCEDMONTHLY || 40;
            // Initial Value
            var socialSetValue = 5;
            var teamMemberValue = 5;
            //Final Value
            var finalSocialSetValueMonthlyStarter = 0;
            var finalSocialSetValueMonthlyGrowth = 0;
            var finalSocialSetValueMonthlyAdvanced = 0;

            var finalSocialSetValueMonthlyStarter__yearly = 0;
            var finalSocialSetValueMonthlyGrowth__yearly = 0;
            var finalSocialSetValueMonthlyAdvanced__yearly = 0;
            var finalTeamMemberValueMonthly = 0;

            //Display Value on Load
            showPriceOnLoad ("isMonthly", starterMonthly, growthMonthly, advancedMonthly, "tPF--alt--plan__priceValue--");

            //Social Sets Monthly
            //Minus
            $('.tPF--counter__minus--monthlySocialSets').click(function () {
                var $input = $(this).parent().find('input.tPF--counter__input--monthlySocialSets');
                var count = parseInt($input.val()) - 1;
                count = count < 0 ? 0 : count;
                $input.val(count);
                $input.change();
                //Final Social Sets Value
                finalSocialSetValueMonthlyStarter  = parseFloat($input.val()) * parseFloat(starterMonthly);
                finalSocialSetValueMonthlyGrowth  = parseFloat($input.val()) * parseFloat(growthMonthly);
                finalSocialSetValueMonthlyAdvanced  = parseFloat($input.val()) * parseFloat(advancedMonthly);

                finalSocialSetValueMonthlyStarter__yearly  = parseFloat($input.val()) * parseFloat(starterYearly);
                finalSocialSetValueMonthlyGrowth__yearly  = parseFloat($input.val()) * parseFloat(growthYearly);
                finalSocialSetValueMonthlyAdvanced__yearly  = parseFloat($input.val()) * parseFloat(advancedYearly);
                finalMonthlyValue();
                updatedYearlyAnnotationForMonthlyCards();
            });
            //Plus
            $('.tPF--counter__plus--monthlySocialSets').click(function () {
                var $input = $(this).parent().find('input.tPF--counter__input--monthlySocialSets');
                var count = parseInt($input.val()) + 1;
                count = count > 30 ? 30 : count;
                $input.val(count);
                $input.change();
                //Final Social Sets Value
                finalSocialSetValueMonthlyStarter  = parseFloat($input.val()) * parseFloat(starterMonthly);
                finalSocialSetValueMonthlyGrowth  = parseFloat($input.val()) * parseFloat(growthMonthly);
                finalSocialSetValueMonthlyAdvanced  = parseFloat($input.val()) * parseFloat(advancedMonthly);

                finalSocialSetValueMonthlyStarter__yearly  = parseFloat($input.val()) * parseFloat(starterYearly);
                finalSocialSetValueMonthlyGrowth__yearly  = parseFloat($input.val()) * parseFloat(growthYearly);
                finalSocialSetValueMonthlyAdvanced__yearly  = parseFloat($input.val()) * parseFloat(advancedYearly);
                finalMonthlyValue();
                updatedYearlyAnnotationForMonthlyCards();
            });

            //Team Members Monthly
            //Minus
            $('.tPF--counter__minus--monthlyTeam').click(function () {
                var $input = $(this).parent().find('input.tPF--counter__input--monthlyTeam');
                var count = parseInt($input.val()) - 1;
                count = count < 0 ? 0 : count;
                $input.val(count);
                $input.change();
                //Final Team Member Value
                finalTeamMemberValueMonthly = parseFloat($input.val()) * parseFloat(teamMemberValue);
                console.log(finalTeamMemberValueMonthly);
                finalMonthlyValue();
                updatedYearlyAnnotationForMonthlyCards();
            });
            //Plus
            $('.tPF--counter__plus--monthlyTeam').click(function () {
                var $input = $(this).parent().find('input.tPF--counter__input--monthlyTeam');
                var count = parseInt($input.val()) + 1;
                count = count > 15 ? 15 : count;
                $input.val(count);
                $input.change();
                //Final Team Member Value
                finalTeamMemberValueMonthly = parseFloat($input.val()) * parseFloat(teamMemberValue);
                console.log(finalTeamMemberValueMonthly);
                finalMonthlyValue();
                updatedYearlyAnnotationForMonthlyCards();
            });

            //Display on load
            function showPriceOnLoad(isMonthly = "false", plan1, plan2, plan3, className = "tPF--alt--plan__priceValue--") {
                if(isMonthly === "isMonthly"){
                    $('.' + className + "starterMonthly").text(plan1);
                    $('.' + className + "growthMonthly").text(plan2);
                    $('.' + className + "advancedMonthly").text(plan3);
                }
                else{
                    $('.' + className + "starterYearly").text(plan1.toFixed(1));
                    $('.' + className + "growthYearly").text(plan2);
                    $('.' + className + "advancedYearly").text(plan3.toFixed(1));
                }
            }

            function finalMonthlyValue() {
                var updatedStarterMonthly = finalSocialSetValueMonthlyStarter + finalTeamMemberValueMonthly + starterMonthly;
                $('.tPF--alt--plan__priceValue--starterMonthly').text(updatedStarterMonthly);
                var updatedGrowthMonthly = finalSocialSetValueMonthlyGrowth + finalTeamMemberValueMonthly + growthMonthly;
                $('.tPF--alt--plan__priceValue--growthMonthly').text(updatedGrowthMonthly);
                var updatedAdvancedMonthly = finalSocialSetValueMonthlyAdvanced + finalTeamMemberValueMonthly + advancedMonthly;
                $('.tPF--alt--plan__priceValue--advancedMonthly').text(updatedAdvancedMonthly);
            }

            function updatedYearlyAnnotationForMonthlyCards(){
                let updatedStarterYearly = finalSocialSetValueMonthlyStarter__yearly + finalTeamMemberValueMonthly + starterYearly;
                var updatedGrowthYearly = finalSocialSetValueMonthlyGrowth__yearly + finalTeamMemberValueMonthly + growthYearly;
                var updatedAdvancedYearly = finalSocialSetValueMonthlyAdvanced__yearly + finalTeamMemberValueMonthly + advancedYearly;
                $('.tPF--alt--plan__billed--starterYearly').text(updatedStarterYearly.toFixed(2));
                $('.tPF--alt--plan__billed--growthYearly').text(updatedGrowthYearly.toFixed(2));
                $('.tPF--alt--plan__billed--advancedYearly').text(updatedAdvancedYearly.toFixed(2));
            }
            updatedYearlyAnnotationForMonthlyCards();


            /**
             * Adding eventlisters to the yearlynthly toggle
             */

            function setTablePrices(starterPrice, growthPrice, advancedPrice){
                $('.qa--plan__featureName--extraSocialSets > .qa--plan__featureValue--starter').text(starterPrice);
                $('.qa--plan__featureName--extraSocialSets > .qa--plan__featureValue--growth').text(growthPrice);
                $('.qa--plan__featureName--extraSocialSets > .qa--plan__featureValue--advanced').text(advancedPrice);
            }

            // Sets all pricing information based on what the payment toggle is set to i.e. Yearly, or Monthly
            function togglePaymentPlan(){
                var loadedPricing = $("#yearly").css("display") !== "none" ? "Yearly" : "Monthly";

                if(loadedPricing === "Yearly"){
                    setTablePrices("$" + starterYearly.toFixed(2) + " each", "$" + growthYearly.toFixed(2) + " each", "$" + advancedYearly.toFixed(2) + " each")
                    document.querySelector(".qa--plan__featureName--numberOfUsersIncluded").style.display = "none";
                    document.querySelector(".qa--plan__featureName--numberOfUsersIncludedYearly").style.display = "flex";
                }else if(loadedPricing === "Monthly"){
                    setTablePrices("$" + starterMonthly.toFixed(2) + " each", "$" + growthMonthly.toFixed(2) + " each", "$" + advancedMonthly.toFixed(2) + " each")
                    document.querySelector(".qa--plan__featureName--numberOfUsersIncludedYearly").style.display = "none";
                    document.querySelector(".qa--plan__featureName--numberOfUsersIncluded").style.display = "flex";
                }
            }



            togglePaymentPlan()
            $(".tPF--planToggle").click(function(){
                togglePaymentPlan();
            });

        });

        //Toogle


        jQuery('.tPF--planToggle__option--yearly').on("click", function() {
            jQuery('#yearly').toggle();
            jQuery('#monthly').toggle();
        });

        jQuery('.tPF--planToggle__option--monthly').on("click", function() {
            jQuery('#yearly').toggle();
            jQuery('#monthly').toggle();
        });
    </script>
</div>
