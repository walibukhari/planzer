<?php

namespace App\Http\Controllers\Social;

use App\Http\Controllers\Controller;
use App\Models\SocialAccount;
use App\Models\SocialAccountManager;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class YouTubeController extends Controller
{
    private $client;
    private $youtube;
    protected $googleLiveBroadcastSnippet;
    protected $googleLiveBroadcastStatus;
    protected $googleYoutubeLiveBroadcast;
    protected $googleYoutubeLiveStreamSnippet;
    protected $googleYoutubeCdnSettings;
    protected $googleYoutubeLiveStream;
    protected $googleYoutubeVideoRecordingDetails;

    public function __construct() {
        $this->client = new \Google_Client();
        $this->client->setAccessType("offline");
        $this->client->setApprovalPrompt("force");
        $this->client->setApplicationName('YouTube Tools');
        $this->client->setRedirectUri(url("user/accounts/youtube"));
        $this->client->setClientId("722339962436-n5nf4n3qor3nvg2aq0t9g6hsi6m0aodm.apps.googleusercontent.com");
        $this->client->setClientSecret("pxv4nPs2nAVdFNIp_kEdAH7w");
        $this->client->setDeveloperKey("AIzaSyDk7Mv5iUxRaWhUya1uJsCTdQnDmnaTt0E");
        $this->client->setScopes(array('https://www.googleapis.com/auth/youtube.readonly', 'https://www.googleapis.com/auth/youtube', 'https://www.googleapis.com/auth/youtube', 'https://www.googleapis.com/auth/youtube.force-ssl', 'https://www.googleapis.com/auth/userinfo.email'));

        $this->youtube = new \Google_Service_YouTube($this->client);
        $this->googleLiveBroadcastSnippet = new \Google_Service_YouTube_LiveBroadcastSnippet;
        $this->googleLiveBroadcastStatus = new \Google_Service_YouTube_LiveBroadcastStatus;
        $this->googleYoutubeLiveBroadcast = new \Google_Service_YouTube_LiveBroadcast;
        $this->googleYoutubeLiveStreamSnippet = new \Google_Service_YouTube_LiveStreamSnippet;
        $this->googleYoutubeCdnSettings = new \Google_Service_YouTube_CdnSettings;
        $this->googleYoutubeLiveStream = new \Google_Service_YouTube_LiveStream;
        $this->googleYoutubeVideoRecordingDetails = new \Google_Service_YouTube_VideoRecordingDetails;
    }

    public function loginUrl(){
        $url = $this->client->createAuthUrl();
        return redirect($url);
    }

    public function Youtube(){
        $youtube = SocialAccountManager::where('social_network','=',SocialAccount::YOU_TUBE)->where('user_id','=',Auth::user()->id)->first();
        return view('social.youtube.youtube',[
            'youtube' => $youtube
        ]);
    }

    public function getToken($request){
        try {
            if($code = $request->input('code')){
                $this->client->authenticate($code);
                $oauth2 = new \Google_Service_Oauth2($this->client);
                $token = $this->client->getAccessToken();
                $this->client->setAccessToken($token);
                return $token;
            }else{
                $request->redirect(url("accounts/youtube", array('auth' => true)));
            }

        } catch (\Exception $e) {
            $request->redirect(url("accounts/youtube", array('auth' => true)));
        }
    }

    public function setToken($token){
        $this->client->setAccessToken(perfectUnserialize($token));
    }

    public function getChannel(){
        try {
            $part = 'brandingSettings,status,id,snippet,contentDetails,contentOwnerDetails,statistics';
            $params = array(
                'mine' => true
            );
            return $this->youtube->channels->listChannels($part, $params);
        } catch (\Exception $e) {
            return false;
        }
    }

    public function accountYoutube(Request $request){
        try {
            $token = $this->getToken($request);
            $userData = $this->getCurrentUser($token['access_token']);
            $getUserName = explode('@', $userData->email);
            $fullname = $getUserName[0];
            $data = [
                'ids' => ids(),
                'social_network' => SocialAccount::YOU_TUBE,
                'category' => 'profile',
                'pid' => $userData->id,
                'user_id' => Auth::user()->id,
                'can_post' => 1,
                'name' => isset($userData->name) ? $userData->name : '',
                'username' => $fullname,
                'token' => $token['access_token'],
                'avatar' => $userData->picture,
                'url' => NULL,
                'data' => NULL,
                'proxy' => NULL,
                'status' => 200,
                'access_token' => $token['access_token'],
            ];
            if (SocialAccountManager::where('pid', '=', $userData->id)->orwhere('user_id','=',Auth::user()->id)->exists()) {
                SocialAccountManager::updateAccount($data);
                return redirect()->route('Youtube')->with('success_message','Youtube Already Connected .... !');
            } else {
                SocialAccountManager::createAccount($data);
                return redirect()->route('Youtube')->with('success_message','Congrats Youtube Connected Successfully .... !');
            }
        } catch(\Exception $e) {
//            dump($e->getMessage());
            return redirect()->route('Youtube')->with('error_message',$e->getMessage());
        }
    }

    public function getCurrentUser($token){
        try {
            $oauth = new \Google_Service_Oauth2($this->client);
            $userinfo = $oauth->userinfo->get();
            return $userinfo;
        } catch (\Exception $e) {
            return false;
        }
    }

    public function process( $text )
    {
        $text = specialcharDecode($text);
        return preg_replace_callback(
            '/\{(((?>[^\{\}]+)|(?R))*)\}/x',
            array( $this, 'replace' ),
            $text
        );
    }

    public function replace( $text )
    {
        $text = $this -> process( $text[1] );
        $parts = explode( '|', $text );
        return $parts[ array_rand( $parts ) ];
    }

    public function post($post, $account) {
        $account = '';
        dd('get Account');
        $this->accountId = $account['id'];
        $this->setToken($account['access_token']);
        $postData = perfectUnserialize($post['type_data']);
        $caption    = @$this->process($postData['text']);
        $title = ($postData['title']) ? @$this->process($postData['title']) : '';
        $tags = @$this->process($postData['youtube_tags']);
        $category = (int)@$this->process($postData['category']);
        $emojione = new \Emojione\Client(new \Emojione\Ruleset());
        $caption = $emojione->shortnameToUnicode($caption);
        try {
            $videoFile = base_path($postData['media'][0]);
            $snippet = new \Google_Service_YouTube_VideoSnippet();
            $snippet->setTitle($title);
            $snippet->setDescription($caption);
            if($tags != ""){
                $tags = explode(",", $tags);
                $snippet->setTags($tags);
            }

            if($category != 0){
                $snippet->setCategoryId($category);
            }

            $status = new \Google_Service_YouTube_VideoStatus();
            $status->privacyStatus = "public";

            $video = new \Google_Service_YouTube_Video();
            $video->setSnippet($snippet);
            $video->setStatus($status);

            // Specify the size of each chunk of data, in bytes. Set a higher value for
            // reliable connection as fewer chunks lead to faster uploads. Set a lower
            // value for better recovery on less reliable connections.
            $chunkSizeBytes = 1 * 1024 * 1024;

            // Setting the defer flag to true tells the client to return a request which can be called
            // with ->execute(); instead of making the API call immediately.
            $this->client->setDefer(true);

            // Create a request for the API's videos.insert method to create and upload the video.
            $insertRequest = $this->youtube->videos->insert("status,snippet", $video);

            // Create a MediaFileUpload object for resumable uploads.
            $media = new \Google_Http_MediaFileUpload(
                $this->client,
                $insertRequest,
                'video/*',
                null,
                true,
                $chunkSizeBytes
            );
            $media->setFileSize(filesize($videoFile));


            // Read the media file and upload it chunk by chunk.
            $status = false;
            $handle = fopen($videoFile, "rb");
            while (!$status && !feof($handle)) {
                $chunk = fread($handle, $chunkSizeBytes);
                $status = $media->nextChunk($chunk);
            }

            fclose($handle);

            // If you want to make other calls after the file upload, set setDefer back to false
            $this->client->setDefer(false);
            /* set result */
            return true;

        } catch (\Google_Service_Exception $e) {
            /* set result */
            return false;
        } catch (\Google_Exception $e) {
            /* set result */
            return false;
        }
    }

    public function index(){
        dd(123);
    }
}
