<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('packages', function (Blueprint $table) {
            $table->id();            
            $table->String('name');
            $table->String('description');         
            $table->float('storage');
            $table->float('file_size');
            $table->float('price_monthly');
            $table->float('price_anually');
            $table->Integer('no_of_account');
            $table->Integer('sort');
            $table->Integer('team_users');


             //file pickers start from here
            $table->Integer('google_drive');
            $table->Integer('dropbox');
            $table->Integer('one_drive');
            $table->Integer('photo');
            $table->Integer('video');
            $table->Integer('watermark');
            $table->Integer('image_editor');
            $table->Integer('drafts');
            $table->Integer('rss');
            $table->Integer('automations');
            $table->Integer('bulk_schedule');

            //social  facebook
            $table->Integer('facebook');
            $table->Integer('facebook_post');
            $table->Integer('livestream');
            $table->Integer('direct_message');

            //instagram
            $table->Integer('instagram');
            $table->Integer('instagram_post');
            $table->Integer('analytics');
            $table->Integer('instagram_livestream');
            $table->Integer('igtv');
            $table->Integer('instagram_tag');
            $table->Integer('instagram_direct_message');

            //twitter
            $table->Integer('twitter');
            $table->Integer('twitter_post');
            $table->Integer('twitter_direct_message');
          

            //linkedin
            $table->Integer('linkedin');
            $table->Integer('linkedin_post');
          

            //dailymotion
            $table->Integer('tiktok');
            $table->Integer('tiktok_post');
            

            //youtube
            $table->Integer('youtube');
            $table->Integer('youtube_post');



            $table->Integer('created_by')->nullable();
            $table->String('status')->nullable();
            $table->Integer('is_default')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('packages');
    }
}
