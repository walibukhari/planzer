@extends('layouts.default')
@section('title')
    About
@endsection
@push('css')
    <style type="text/css">
        .play-btn {
            width: 80px;
            height: 80px;
            background: radial-gradient( rgba(38, 44, 244, 0.8) 60%, rgba(38, 44, 244, 1) 62%);
            border-radius: 50%;
            position: relative;
            display: block;
            margin: 100px auto;
            box-shadow: 0px 0px 25px 3px rgba(38, 44, 244, 0.8);
        }

        /* triangle */
        .play-btn::after {
            content: "";
            position: absolute;
            left: 55%;
            top: 48%;
            -webkit-transform: translateX(-30%) translateY(-40%);
            transform: translateX(-30%) translateY(-40%);
            transform-origin: center center;
            width: 37px;
            height: 20px;
            border-top: 10px solid transparent;
            border-bottom: 10px solid transparent;
            border-left: 17px solid #fff;
            z-index: 100;
            -webkit-transition: all 400ms cubic-bezier(0.55, 0.055, 0.675, 0.19);
            transition: all 400ms cubic-bezier(0.55, 0.055, 0.675, 0.19);
        }

        /* pulse wave */
        .play-btn:before {
            content: "";
            position: absolute;
            width: 130%;
            height: 130%;
            -webkit-animation-delay: 0s;
            animation-delay: 0s;
            -webkit-animation: pulsate1 2s;
            animation: pulsate1 2s;
            -webkit-animation-direction: forwards;
            animation-direction: forwards;
            -webkit-animation-iteration-count: infinite;
            animation-iteration-count: infinite;
            -webkit-animation-timing-function: steps;
            animation-timing-function: steps;
            opacity: 1;
            border-radius: 50%;
            border: 5px solid rgba(255, 255, 255, .75);
            top: -14%;
            left: -14%;
            background: rgba(198, 16, 0, 0);
        }

        @-webkit-keyframes pulsate1 {
            0% {
                -webkit-transform: scale(0.6);
                transform: scale(0.6);
                opacity: 1;
                box-shadow: inset 0px 0px 25px 3px rgba(255, 255, 255, 0.75), 0px 0px 25px 10px rgba(255, 255, 255, 0.75);
            }
            100% {
                -webkit-transform: scale(1);
                transform: scale(1);
                opacity: 0;
                box-shadow: none;

            }
        }
        @keyframes pulsate1 {
            0% {
                -webkit-transform: scale(0.6);
                transform: scale(0.6);
                opacity: 1;
                box-shadow: inset 0px 0px 25px 3px rgba(255, 255, 255, 0.75), 0px 0px 25px 10px rgba(255, 255, 255, 0.75);
            }
            100% {
                -webkit-transform: scale(1, 1);
                transform: scale(1);
                opacity: 0;
                box-shadow: none;

            }
        }
    </style>
    <style type="text/css">
        @font-face {
            font-weight: 400;
            font-style:  normal;
            font-family: 'Circular-Loom';

            src: url('https://cdn.loom.com/assets/fonts/circular/CircularXXWeb-Book-cd7d2bcec649b1243839a15d5eb8f0a3.woff2') format('woff2');
        }

        @font-face {
            font-weight: 500;
            font-style:  normal;
            font-family: 'Circular-Loom';

            src: url('https://cdn.loom.com/assets/fonts/circular/CircularXXWeb-Medium-d74eac43c78bd5852478998ce63dceb3.woff2') format('woff2');
        }

        @font-face {
            font-weight: 700;
            font-style:  normal;
            font-family: 'Circular-Loom';

            src: url('https://cdn.loom.com/assets/fonts/circular/CircularXXWeb-Bold-83b8ceaf77f49c7cffa44107561909e4.woff2') format('woff2');
        }

        @font-face {
            font-weight: 900;
            font-style:  normal;
            font-family: 'Circular-Loom';

            src: url('https://cdn.loom.com/assets/fonts/circular/CircularXXWeb-Black-bf067ecb8aa777ceb6df7d72226febca.woff2') format('woff2');
        }
    </style>
    <style>
        .btn-sunset{
            background: #235AF6;
            border-color: #235AF6;
        }
        .btn-sunset:hover {
            color: #fff;
            background: #235AF6;
            border-color: #235AF6;
        }
        .play-btn{
            cursor: pointer;
        }
    </style>
@endpush

@section('content')

    <!-- heroFive Area -->
    <div class="position-relative pt-22 pt-lg-31 pb-13 pb-lg-25">
        <div class="container">
            <div class="row align-items-center justify-content-center">
                <!-- Hero Content -->
                <div class="col-lg-7 col-md-10 col-sm-12 order-2 order-lg-1">
                    <div class="mt-8 mt-lg-0 mr-xl-13" data-aos="fade-right" data-aos-duration="600" data-aos-once="true">
                        <h1 class="font-size-12 mb-9">Revolutionary management</h1>
                        <p class="font-size-8 mb-lg-13 pr-xs-10 pr-sm-10 pr-md-18 pr-lg-8 pr-xl-15">
                            We have created a platform to make it easier for businesses to schedule, manage, engage and measure their social accounts and grow their audience.
                        </p>
                        <div class="d-flex align-items-center py-6 mr-xl-15">
{{--                            <div class="mr-7">--}}
{{--                                <a class="play-btn" data-toggle="modal" data-target="#myModal"></a>--}}
{{--                            </div>--}}
                            <div class="mt-0">
                                    <a href="/book-demo" class="btn btn btn-sunset btn-medium rounded-5 font-size-3">Book demo</a>
{{--                                <h4 class="font-size-8 mb-0">Book demo</h4>--}}
{{--                                <p class="font-size-6 mb-0">Watch latest trending features</p>--}}
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Hero Content -->
                <!-- Hero Image -->
                <div class="col-lg-5 col-md-7 col-xs-8 order-1 order-lg-2">
                    <div class="pt-lg-3" data-aos="flip-right" data-aos-duration="600" data-aos-once="true">
                        <img class="w-100" src="{{asset('images/About 1.png')}}" alt="" >
                    </div>
                </div>
                <!-- End Hero Image -->
            </div>
        </div>
    </div>
    <!-- End heroFive Area -->
    <!-- contentOne Area -->
    <div class="bg-default-5 pt-13 pt-md-19 pt-lg-0 pb-14 pb-md-18 pb-lg-26 overflow-hidden">
        <br><br><br><br>
        <div class="container">
            <!-- Section title -->
            <div class="row align-items-center justify-content-center justify-content-lg-between">
                <div class="col-xl-6 col-lg-5 col-md-6 col-xs-8" data-aos="fade-bottom" data-aos-duration="600" data-aos-once="true">
                    <div class="l4-image-group mt-lg-n18 mt-xl-n20 mb-8 mb-lg-0 ml-xl-n15 mr-xl-19">
                        <img class="w-100" src="{{asset('images/Cool content pictures.png')}}" alt="" style="margin-top:180px;">
                    </div>
                </div>
                <div class="col-xl-6 col-lg-7 col-md-8 pt-lg-23 pb-lg-18" data-aos="fade-right" data-aos-duration="600" data-aos-once="true">
                    <h3 class="font-size-11 mb-0 mb-10 pr-xs-18 pr-md-0">Plan the social week in minutes</h3>
                    <p class="font-size-7 mb-0 pr-xs-15 pr-md-0 pl-xl-2 mt-7 mt-lg-0 pr-lg-18">
                        With our social platform you can schedule all your posts, stories and videos across all our platforms within minutes. Spend less time on posting on social channels and more time to grow your business.
                    </p>
                </div>
            </div>
            <!-- End Section title -->
            <!-- Services -->
            <div class="pt-lg-9">
                <div class="row justify-content-center">
                    <div class="col-lg-4 col-md-6 col-xs-8">
                        <div class="pt-10 pt-lg-18" data-aos="fade-up" data-aos-duration="600" data-aos-once="true">
                            <div class="circle-60 bg-sunset text-white font-size-7">
                                <img src="{{asset('images/Time.png')}}" style="width:65px;" />
                            </div>
                            <div class="mt-9">
                                <h4 class="font-size-8 mb-6">Social scheduling</h4>
                                <p class="font-size-5 mb-0 pr-xl-8">
                                    You can start scheduling social media posts on all channels in minutes.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-xs-8">
                        <div class="pt-10 pt-lg-18" data-aos="fade-up" data-aos-duration="600" data-aos-delay="300" data-aos-once="true">
                            <div class="circle-60 bg-bluewood text-white font-size-7">
                                <img src="{{asset('images/Preveiw feed.png')}}" style="width:65px;" />
                            </div>
                            <div class="mt-9">
                                <h4 class="font-size-8 mb-6">Preview feeds</h4>
                                <p class="font-size-5 mb-0 pr-xl-8">
                                    Preview your feed on different medias before you post to create a beautiful feed.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-xs-8">
                        <div class="pt-10 pt-lg-18" data-aos="fade-up" data-aos-duration="600" data-aos-delay="600" data-aos-once="true">
                            <div class="circle-60 bg-buttercup text-white font-size-7">
                                <img src="{{asset('images/Nofitication.png')}}" style="width:65px;" />
                            </div>
                            <div class="mt-9">
                                <h4 class="font-size-8 mb-6">Notification free</h4>
                                <p class="font-size-5 mb-0 pr-xl-8">
                                    No notifications required! Auto Publish is available for business profiles.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Services -->
        </div>
    </div>
    <!-- End contentOne Area -->
    <!-- contentTwo Area -->
    <div class="bg-pattens-blue pt-15 pt-md-19 pt-lg-30 pb-15 pb-md-19 pb-lg-32">
        <div class="container">
            <div class="row align-items-center justify-content-center">
                <div class="col-xl-6 col-lg-7 col-md-9 order-2 order-lg-1">
                    <div class="mt-13 mt-lg-0 pr pr-lg-0" data-aos="fade-right" data-aos-duration="500" data-aos-once="true">
{{--                        <h6 class="font-size-3 text-dodger-blue-1 text-uppercase mb-8 letter-spacing-5">Automated</h6>--}}
                        <h2 class="font-size-10 mb-8 letter-spacing-n83 pr-sm-10 pr-md-0 text-dark-cloud">
                            Automated online engagement
                        </h2>
                        <p class="font-size-7 mb-0 pr-sm-16 pr-md-15 pr-lg-19 pr-xl-20 text-stone">
                            Our platform is an advanced social media tool that handles all of your social media activities om complete auto pilot. Our tool can handle likes, comments, messages, follows and much more.
                        </p>
                        <div class="mt-12">
                            <a class="btn btn-sunset btn-xl h-55 rounded-5 font-weight-normal" href="{{route('newRegister')}}">Start 14 Days trial</a>
                        </div>
                    </div>
                </div>
                <!-- Right Image -->
                <div class="col-xl-6 col-lg-5 col-md-7 col-xs-9 order-1 order-lg-2">
                    <div class="l5-content-2 ml-lg-10 ml-xl-14 rounded-10">
                        <img class="w-100" src="{{asset('images/heart icon.png')}}" alt="" data-aos="fade-left" data-aos-duration="500" data-aos-once="true">
{{--                        <div class="image-card w-fit-content bg-white-op8 rounded-15 d-flex align-items-center shadow-11 px-8 pt-7 pb-7" data-aos-duration="500" data-aos="zoom-in" data-aos-once="true">--}}
{{--                            <div class="mr-6 circle-59 bg-dodger-blue-1-op1 text-dodger-blue-1 font-size-7">--}}
{{--                                <i class="fas fa-bell"></i>--}}
{{--                            </div>--}}
{{--                            <div class="content">--}}
{{--                                <h4 class="font-size-6 text-dark-cloud mb-0">Wireless Next Gen</h4>--}}
{{--                                <p class="font-size-5 text-stone mb-0">You’ve received a new notification</p>--}}
{{--                            </div>--}}
{{--                        </div>--}}
                    </div>
                </div>
                <!-- End Right Image -->
            </div>
        </div>
    </div>
    <!-- End contentTwo Area -->
    <!-- contentThree Area -->

    <!-- white section start -->
    <div class="bg-pattens-blue pt-15 pt-md-19 pt-lg-30 pb-15 pb-md-19 pb-lg-32" style="background:#fff !important;">
        <div class="container position-static">
            <div class="row align-items-center justify-content-center position-static">
                <div class="col-xl-6 col-lg-5 col-md-9 position-static">
                    <img src="/images/office team work.png" style="    width: 500px !important;" alt="" data-aos="zoom-in" data-aos-duration="600" data-aos-once="true" class="w-100 pb-6 pb-sm-9 rounded-bottom-10 aos-init aos-animate">
                </div>
                <div class="col-xl-6 col-lg-7 col-md-9">
                    <h2 class="font-size-10 mb-8 letter-spacing-n83 pr-xs-25 pr-sm-15 pr-md-15 pr-lg-0 aos-init aos-animate"
                        data-aos="fade-up" data-aos-duration="600" data-aos-delay="300" data-aos-once="true">We made teamwork easy</h2>
                    <p class="font-size-7 mb-0 aos-init aos-animate" data-aos="fade-up" data-aos-delay="500" data-aos-once="true">
                        Our platform makes it easy to manage your social media team with tools as goals, task board and the social media calendar.
                    </p>
                </div>
            </div>
        </div>
    </div>
    <!-- white section end -->
    <!-- light blue section start -->
    <div class="bg-pattens-blue pt-15 pt-md-19 pt-lg-30 pb-15 pb-md-19 pb-lg-32">
        <div class="container position-static">
            <div class="row align-items-center justify-content-center position-static">
                <div class="col-xl-6 col-lg-5 col-md-9 position-static">
                    <img src="/images/new.png" style="width:354px!important;" alt="" data-aos="zoom-in" data-aos-duration="600" data-aos-delay="200" data-aos-once="true" class="w-100 pb-6 pb-sm-9 rounded-10 aos-init aos-animate">
                </div>
                <div class="col-xl-6 col-lg-7 col-md-9">
{{--                    <h6 class="font-size-3 text-white text-uppercase mb-10 letter-spacing-5 aos-init aos-animate" data-aos="fade-up" data-aos-duration="600" data-aos-once="true" style="color:#000 !important;">Connect your socials</h6>--}}
                    <h2 class="font-size-10 mb-8 letter-spacing-n83 pr-xs-25 pr-sm-15 pr-md-15 pr-lg-0 aos-init aos-animate" data-aos="fade-up" data-aos-duration="600" data-aos-delay="300" data-aos-once="true">Integration based lead generation </h2>
                    <p class="font-size-7 mb-0 aos-init aos-animate" data-aos="fade-up" data-aos-delay="500" data-aos-once="true">
                        Integrate a wide range of CRM systems and setup the perfect pipeline to integrate the relevant leads for your business.
                    </p>
                    <div class="mt-12" data-aos="fade-up" data-aos-delay="700" data-aos-once="true">
                        <a class="btn btn-sunset btn-xl h-55 rounded-5" href="{{route('newRegister')}}">Start 14 Days trial</a>
                    </div>
                    <br>
                </div>
            </div>
        </div>
    </div>
    <!-- lightblue section -->


    <div class="bg-dark-cloud pt-15 pt-md-19 pt-lg-33 pt-xl-0 pb-15 pb-md-19 pb-lg-31 pb-xl-0 position-relative overflow-hidden" style="padding: 245px 0px !important; ">
        <div class="container position-static">
            <div class="row align-items-center justify-content-center position-static">
                <!-- Right Image -->
                <div class="col-xl-6 col-lg-5 col-md-9 position-static">
                    <!-- content-2 start -->
                    <div class="l5-content-gallery-img">
                        <!-- content img start -->
                        <div class="d-xs-flex ml-lg-15 mx-xl-n3">
                            <div class="d-flex flex-column px-3 px-sm-6 w-100 mr-xl-14">
                                <!-- single image -->
                                <img src="{{asset('images/About 6.png')}}" alt="" data-aos="zoom-in" data-aos-duration="600" data-aos-delay="300" data-aos-once="true" class="w-100 rounded-top-10">
                                <!-- single image end -->
                            </div>
                        </div>
                        <!-- abs-content end -->
                    </div>
                    <!-- content-2 end -->
                </div>
                <!-- End Right Image -->
                <div class="col-xl-6 col-lg-7 col-md-9">
                    <div class="dark-mode-texts mt-10 mt-lg-0 pr-xs-15 pr-sm-14 pr-md-13 pr-lg-0 pl-lg-15">
                        <br>
                        <br>
{{--                        <h6 class="font-size-3 text-white text-uppercase mb-10 letter-spacing-5" data-aos="fade-up" data-aos-duration="600" data-aos-once="true">Keep track of your results</h6>--}}
                        <h2 class="font-size-10 mb-8 letter-spacing-n83  pr-xs-25 pr-sm-15 pr-md-15 pr-lg-0" data-aos="fade-up" data-aos-duration="600" data-aos-delay="300" data-aos-once="true">
{{--                            Advanced--}}
                            Measurement across social media
                        </h2>
                        <p class="font-size-7 mb-0" data-aos="fade-up" data-aos-delay="500" data-aos-once="true">
                            Keep track of how your posts and campaigns are during across your social media accounts. Setup social media business goals, and watch your business grow on social media.
                        </p>
                        <br>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End contentThree Area -->
    <!-- testiMonials Area -->
    <div class="pt-4 pt-md-9 pt-lg-14 pb-15 pb-md-19 pb-lg-26 border-top border-default-color">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-7 col-lg-9 col-md-10" data-aos="zoom-in" data-aos-duration="500" data-aos-once="true">
                    <div class="l5-review-slider mt-13 mt-lg-21 mx-xl-0 mx-lg-10">
                        <!-- Single Review -->
                        @foreach($testimonial as $testies)
                            <div class="single-review text-center focus-reset">
                                <div class="mb-12 circle-265 mx-auto">
                                    <img
                                        style="border-radius:100%;width:100%;height:270px;object-fit: cover;"
                                        src="/{{$testies->image}}"
                                        alt=""
                                    />
                                </div>
                                <p class="font-size-9 font-weight-bold heading-default-color">{{$testies->description}}</p>
                                <div class="mt-11">
                                    <h5 class="font-size-7 mb-0">{{$testies->name}}</h5>
                                    <p class="font-size-5">{{$testies->title}}</p>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <!-- End Single Review -->
                </div>
            </div>
            <!-- l5-slider-dots -->
            <div class="row justify-content-center">
                <div class="col-xl-4 col-lg-5 col-md-6 col-sm-8 col-xs-9" data-aos="zoom-in" data-aos-duration="500" data-aos-once="true">
                    <div class="l5-slider-dots text-center mt-10 mx-xs-4 mx-md-0 mx-xl-6">
                        @foreach($testimonial as $testies)
                            <div class="single-img circle-50 mx-3 hover-pointer">
                                <img
                                    style="width:46px;height:46px;border-radius: 100px;object-fit:cover;"
                                    src="/{{$testies->image}}"
                                    alt=""
                                />
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
            <!-- End l5-slider-dots -->
        </div>
    </div>
    <!-- End testiMonials Area -->
    <!-- Music Area -->
{{--    <div class="bg-default-5 pt-13 pt-md-18 pt-lg-24 pb-9 pb-md-13 pb-lg-28">--}}
{{--        <div class="container">--}}
            <!-- Section Title -->
{{--            <div class="row justify-content-center">--}}
{{--                <div class="col-xl-7 col-lg-8 col-md-9">--}}
{{--                    <div class="text-center mb-lg-5" data-aos="fade-in" data-aos-duration="500" data-aos-once="true">--}}
{{--                        <h2 class="font-size-10 letter-spacing-n83 mb-6">Get what you want</h2>--}}
{{--                        <p class="font-size-7 mb-0">Sophisticated styling with exceptional comfort. Super-soft,--}}
{{--                            pressure-relieving earpads in foamed urethane.</p>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
            <!-- End Section Title -->
{{--            <div class="pt-13 pt-lg-16">--}}
{{--                <div class="row justify-content-center px-xl-0">--}}
{{--                    <!-- single card -->--}}
{{--                    <div class="col-xl-3 col-lg-4 col-md-5 col-sm-7 col-xs-8" data-aos="fade-in" data-aos-duration="500" data-aos-once="true">--}}
{{--                        <div class="bg-white rounded-15 text-center px-8 py-11 gr-hover-3 mb-9 border border-default-color">--}}
{{--                            <div class="mb-14">--}}
{{--                                <img class="w-100" src="https://uxtheme.net/demos/finity/image/home-5/png/music-headphone.png" alt="">--}}
{{--                            </div>--}}
{{--                            <span class="badge bg-dodger-blue-1-op1 text-dodger-blue-1 rounded-40 px-5 py-2 line-height-reset min-w-63 font-size-5 d-inline-block mb-7">$99</span>--}}
{{--                            <h5 class="text-bunker font-size-6 line-height-reset mb-7 line-height-26">Wireless Headphone with Noise Cancellation</h5>--}}
{{--                            <a class="btn btn-sunset rounded-5 min-w-144 h-55" href="#">Buy now</a>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <!-- end single card -->--}}
{{--                    <!-- single card -->--}}
{{--                    <div class="col-xl-3 col-lg-4 col-md-5 col-sm-7 col-xs-8" data-aos="fade-in" data-aos-duration="600" data-aos-delay="300" data-aos-once="true">--}}
{{--                        <div class="bg-white rounded-15 text-center px-8 py-11 gr-hover-3 mb-9 border border-default-color">--}}
{{--                            <div class="mb-14">--}}
{{--                                <img class="w-100" src="https://uxtheme.net/demos/finity/image/home-5/png/music-headphone-3.png" alt="">--}}
{{--                            </div>--}}
{{--                            <span class="badge bg-dodger-blue-1-op1 text-dodger-blue-1 rounded-40 px-5 py-2 line-height-reset min-w-63 font-size-5 d-inline-block mb-7">$99</span>--}}
{{--                            <h5 class="text-bunker font-size-6 line-height-reset mb-7 line-height-26">Wireless Headphone with Noise Cancellation</h5>--}}
{{--                            <a class="btn btn-sunset rounded-5 min-w-144 h-55" href="#">Buy now</a>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <!-- end single card -->--}}
{{--                    <!-- single card -->--}}
{{--                    <div class="col-xl-3 col-lg-4 col-md-5 col-sm-7 col-xs-8" data-aos="fade-in" data-aos-duration="600" data-aos-delay="500" data-aos-once="true">--}}
{{--                        <div class="bg-white rounded-15 text-center px-8 py-11 gr-hover-3 mb-9 border border-default-color">--}}
{{--                            <div class="mb-14">--}}
{{--                                <img class="w-100" src="https://uxtheme.net/demos/finity/image/home-5/png/music-headphone-2.png" alt="">--}}
{{--                            </div>--}}
{{--                            <span class="badge bg-dodger-blue-1-op1 text-dodger-blue-1 rounded-40 px-5 py-2 line-height-reset min-w-63 font-size-5 d-inline-block mb-7">$99</span>--}}
{{--                            <h5 class="text-bunker font-size-6 line-height-reset mb-7 line-height-26">Wireless Headphone with Noise Cancellation</h5>--}}
{{--                            <a class="btn btn-sunset rounded-5 min-w-144 h-55" href="#">Buy now</a>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <!-- end single card -->--}}
{{--                    <!-- single card -->--}}
{{--                    <div class="col-xl-3 col-lg-4 col-md-5 col-sm-7 col-xs-8" data-aos="fade-in" data-aos-duration="600" data-aos-delay="700" data-aos-once="true">--}}
{{--                        <div class="bg-white rounded-15 text-center px-8 py-11 gr-hover-3 mb-9 border border-default-color">--}}
{{--                            <div class="mb-14">--}}
{{--                                <img class="w-100" src="https://uxtheme.net/demos/finity/image/home-5/png/music-headphone-4.png" alt="">--}}
{{--                            </div>--}}
{{--                            <span class="badge bg-dodger-blue-1-op1 text-dodger-blue-1 rounded-40 px-5 py-2 line-height-reset min-w-63 font-size-5 d-inline-block mb-7">$99</span>--}}
{{--                            <h5 class="text-bunker font-size-6 line-height-reset mb-7 line-height-26">Wireless Headphone with Noise Cancellation</h5>--}}
{{--                            <a class="btn btn-sunset rounded-5 min-w-144 h-55" href="#">Buy now</a>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <!-- end single card -->--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
    <!-- End Music Area -->
    <!-- prcing area about page -->
    <div class="pt-13 pt-md-17 pt-lg-25 pb-8 pb-md-11 pb-lg-22 position-relative">
        <div class="container">
            <!-- Section Title -->
            <div class="mb-13 mb-lg-13">
                <div class="row align-items-center justify-content-center">
                    <div class="col-xl-5 col-lg-6 col-md-10">
                        <div class="mb-10 mb-lg-0 text-center text-lg-left aos-init aos-animate" data-aos-duration="800" data-aos="fade-right" data-aos-once="true">
                            <h2 class="font-size-10 pr-md-10 pr-xl-0 mb-0 letter-spacing-n83">Choose the right plan for your biz</h2>
                        </div>
                    </div>
                    <div class="col-xl-7 col-lg-6 col-md-6 text-center text-md-right">
                        <div class="btn-section aos-init aos-animate" data-aos="fade-left" data-aos-duration="500" data-aos-once="true">
                            <a href="javascript:" class="btn-toggle-square active mx-3 price-deck-trigger rounded-10 bg-golden-yellow">
                                <span data-pricing-trigger="" data-target="#table-price-value" data-value="yearly" class="text-break">Yearly</span>
                                <span data-pricing-trigger="" data-target="#table-price-value" data-value="monthly" class="text-break active">Monthly</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Section Title -->
            <!-- Table Main Body -->
            <div class="table-body" id="table-price-value" data-pricing-dynamic="" data-value-active="monthly">
                <div class="row justify-content-center">
                    <!-- Single Table -->
                    <div class="col-lg-12 col-sm-6">
                        <div class="border-top pt-10 pb-8 aos-init aos-animate" data-aos="fade-up" data-aos-duration="800" data-aos-delay="500" data-aos-once="true">
                            <div class="row">
                                <div class="col-lg-3">
                                    <div class="mb-5 mb-lg-0">
                                        <h3 class="font-size-7">Free</h3>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="mb-5 mb-lg-0">
                                        <ul class="list-unstyled font-size-6">
                                            <li class="heading-default-color mb-4">1 Social set</li>
                                            <li class="heading-default-color mb-4">1 user</li>
                                            <li class="heading-default-color mb-4">Simple features</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="pr-lg-15 pr-xl-10 mb-5 mb-lg-0">
                                        <h2 class="mb-0 font-size-11 font-weight-medium">€ <span class="dynamic-value" data-active="0" data-monthly="0" data-yearly="0"></span></h2>
                                        <p class="mb-0 font-size-5 pr-xl-22">Free</p>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="mb-5 mb-lg-0 mt-4 text-left text-lg-right">
                                        <a class="btn btn-outline-gray-1 btn-2 border-width-2 rounded-5 gr-hover-bg-blue-3 heading-default-color" href="/register">Sign
                                            Up
                                            For
                                            Free</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Single Table End -->
                    <!-- Single Table -->
                    <div class="col-lg-12 col-sm-6">
                        <div class="border-top pt-10 pb-8 aos-init aos-animate" data-aos="fade-up" data-aos-duration="800" data-aos-delay="500" data-aos-once="true">
                            <div class="row">
                                <div class="col-lg-3">
                                    <div class="mb-5 mb-lg-0">
                                        <h3 class="font-size-7">Basic</h3>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="">
                                        <ul class="list-unstyled font-size-6">
                                            <li class="heading-default-color mb-4">1 Social set</li>
                                            <li class="heading-default-color mb-4">3 users</li>
                                            <li class="heading-default-color mb-4">Basic features</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="pr-lg-15 pr-xl-10 mb-5 mb-lg-0">
                                        <h2 class="mb-0 font-size-11 font-weight-medium">€ <span class="dynamic-value" data-active="15" data-monthly="15" data-yearly="12,5"></span></h2>
                                        <p class="mb-0 font-size-5 pr-xl-22"> Pr. social set pr. month</p>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="mb-5 mb-lg-0 mt-4 text-left text-lg-right">
                                        <a class="btn btn-outline-gray-1 btn-2 border-width-2 rounded-5 gr-hover-bg-blue-3 heading-default-color" href="/register">Start
                                            14 Days
                                            Free
                                            Trial</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Single Table End -->
                    <!-- Single Table -->
                    <div class="col-lg-12 col-sm-6">
                        <div class="border-top pt-10 pb-8 aos-init aos-animate" data-aos="fade-up" data-aos-duration="800" data-aos-delay="800" data-aos-once="true">
                            <div class="row">
                                <div class="col-lg-3">
                                    <div class="mb-5 mb-lg-0">
                                        <h3 class="font-size-7">Pro</h3>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="">
                                        <ul class="list-unstyled font-size-6">
                                            <li class="heading-default-color mb-4">1 Social set</li>
                                            <li class="heading-default-color mb-4">5 users</li>
                                            <li class="heading-default-color mb-4">Unlimited features</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="pr-lg-15 pr-xl-10 mb-5 mb-lg-0">
                                        <h2 class="mb-0 font-size-11 font-weight-medium">€ <span class="dynamic-value" data-active="40" data-monthly="40" data-yearly="30"></span></h2>
                                        <p class="mb-0 font-size-5 pr-xl-22">
                                            <span></span>Pr. social set pr. month
                                        </p>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="mb-5 mb-lg-0 mt-4 text-left text-lg-right">
                                        <a class="btn btn-outline-gray-1 btn-2 border-width-2 rounded-5 gr-hover-bg-blue-3 heading-default-color" href="/register">Start
                                            14 Days
                                            Free
                                            Trial</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Single Table End -->
                </div>
            </div>
            <!-- Table Main Body -->
        </div>
        <div class="pricing-bottom-shape d-none d-sm-block">
            <img src="http://planzer.io/image/svg/footer-shape.svg" alt="" data-aos="fade-left" data-aos-delay="500" data-aos-once="true" class="aos-init aos-animate">
        </div>
    </div>
    <!-- end pricing area about page -->
    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog" style="max-width:90%;width:100%;">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-body" style="padding:0px;height:450px;">
                    <iframe width="100%" style="padding:0px;border-radius:8px;border:0px;" height="450" src="https://www.youtube.com/embed/LWZ7iytIA6k">
                    </iframe>
                </div>
            </div>

        </div>
    </div>
@endsection


@push('js')

@endpush
