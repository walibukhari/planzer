<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Company;
use Illuminate\Http\Request;

class CompanyController extends Controller
{
    public function companyDetail(){
        return view('admin.company.company_detail');
    }

    public function createCompany(){
        return view('admin.company.create');
    }

    public function saveCompany(Request  $request){
        Company::createCompany($request->all());
        return back()->with('success_message','Company Created Successfully');
    }

    public function companiesList(){
        $companies = Company::all();
        return view('admin.company.list',[
            'companies' => $companies
        ]);
    }

    public function editCompany($id){
        $company = Company::where('id','=',$id)->first();
        return view('admin.company.edit',[
            'company' => $company
        ]);
    }

    public function deleteCompany($id){
        Company::where('id','=',$id)->delete();
        return back()->with('success_message','Company Deleted Successfully');
    }

    public function updateCompany($id , Request  $request){
        Company::updateCompany($id , $request->all());
        return back()->with('success_message','Company Updated Successfully');
    }

    public function companyUsers(){
        return view('admin.company.users');
    }
}
