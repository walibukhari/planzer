@extends('layouts.default')
@section('title')
    Error
@endsection
@push('css')

@endpush

@section('content')

    <div class="pt-24 pt-md-26 pb-15 pt-lg-33 pb-md-19 pb-lg-25">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-5 col-lg-6 col-md-8 col-sm-11">
                    <div class="text-center">
                        <div class="mb-8 mb-lg-16" data-aos="fade-up" data-aos-duration="500" data-aos-once="true">
                            <img class="w-100 w-xs-auto" src="https://uxtheme.net/demos/finity/image/inner-page/png/404-error-img.png" alt="">
                        </div>
                        <h1 class="font-size-13 mb-8" data-aos="fade-up" data-aos-duration="500" data-aos-once="true">404 Error</h1>
                        <p class="font-size-7 mb-0" data-aos="fade-up" data-aos-duration="500" data-aos-once="true">The page you are looking for is not available or doesn’t belong to this website!</p>
                        <div class="">
                            <div class="pt-11 max-w-322 mx-auto" data-aos="fade-up" data-aos-duration="500" data-aos-once="true">
                                <a class="btn btn-bali-gray w-100 rounded-5 text-white" href="#">Go back to home</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('js')

@endpush
