<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Profile extends Model
{
    use HasFactory;


    const TYPE_NORMAL_ADMIN = 0;
    const TYPE_SUPER_ADMIN = 1;

    protected $fillable = [
        'user_id',
        'first_name',
        'last_name',
        'city',
        'state',
        'country',
        'phone_number',
        'address',
        'postal_code',
        'company',
        'timezone',
        'image',
        'type'
    ];

    public static function createUserProfile($user,$request,$type){
        return self::create([
            'user_id' => $user->id,
            'first_name' => isset($request['first_name']) ? $request['first_name'] : '',
            'last_name' => isset($request['last_name']) ? $request['last_name'] : '',
            'city' => isset($request['city']) ? $request['city'] : '',
            'state' => isset($request['state']) ? $request['state'] : '',
            'country' => isset($request['country']) ? $request['country'] : '',
            'address' => isset($request['address']) ? $request['address'] : '',
            'postal_code' => isset($request['postal_code']) ? $request['postal_code'] : '',
            'company' => isset($request['company']) ? $request['company'] : '',
            'timezone' => isset($request['timezone']) ? $request['timezone'] : '',
            'phone_number' => isset($request['phone_number']) ? $request['phone_number'] : '',
            'type' => $type
        ]);
    }

    public static function updateUserProfile($user_id,$request,$type){
        return self::where('user_id','=',$user_id)->where('type','=',$type)->update([
            'first_name' => $request['first_name'],
            'last_name' => $request['last_name'],
            'city' => $request['city'],
            'state' => $request['state'],
            'country' => $request['country'],
            'address' => $request['address'],
            'postal_code' => $request['postal_code'],
            'company' => $request['company'],
            'timezone' => $request['timezone'],
            'phone_number' => isset($request['phone_number']) ? $request['phone_number'] : '',
            'type' => $type
        ]);
    }
}
