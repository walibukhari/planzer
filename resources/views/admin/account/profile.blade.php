@extends('layouts.simple.master')
@section('title', 'Account Profile')

@section('css')
@endsection

@section('style')
    <style>
        .custom-box-text{
            margin-left: 15px;
        }
        .flex-box{
            display:flex;
            align-items:center;
        }
        .btn-flex{

        }
        .customClose{
            background: #c0c0c0 !important;
            padding: 2px 10px !important;
            position: relative !important;
            top: 10px !important;
            right: 20px;
            border-radius: 6px !important;
        }
    </style>
@endsection

@section('breadcrumb-title')
    <h3>Update Profile</h3>
@endsection

@section('breadcrumb-items')
    <li class="breadcrumb-item">Account Profile</li>
    <li class="breadcrumb-item active">Edit Profile</li>
@endsection

@section('content')
    <div>
        @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
        @endif
        @if (session('password_status'))
            <div class="alert alert-success" role="alert">
                {{ session('password_status') }}
            </div>
        @endif
        @if (Session::has('error'))
            <div class="alert alert-danger alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>{!! Session('error') !!}</strong>
            </div>
        @endif
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">

                    <div class="card-body">
                            <form class="needs-validation" novalidate="" method="POSt" action="{{route('account.profile.update',[$profile->id])}}">
                            @csrf
                            <div class="row">
                                <h3>Profile Photo</h3>
                            </div>
                            <br>
                            <div class="row">
                                <div class="flex-box">
                                    <img src="/{{isset($profile->profile) ? $profile->profile->image : ''}}" onerror="this.src='{{asset('assets/images/user/1.jpg')}}'" id="displayImage" style="border-radius:120px;width:120px; height:120px;" />
                                    <div class="custom-box-text">
                                        <input type="file" name="image" onchange="onChangeImage()" id="uploadImage" style="display:none;" />
                                        <h4>Upload Photo...</h4>
                                        <p>Photo should be at least 300px * 300px</p>
                                        <div class="btn-flex">
                                            <button type="button" onclick="uploadUserImage()" class="btn btn-outline-primary">Upload Photo</button>
                                            <input type="file" accept="image/*;capture=camera" id="openCameraFile" style="display:none;">
                                            <button type="button" class="btn btn-outline-info" onclick="openCamera()">Take A Photo</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <br>
                            <br>
                            <div class="row">

                                <div class="col-md-6 mb-3">
                                    <label for="first_name">First Name</label>
                                    <input class="form-control" value="{{ isset($profile->profile->first_name) ? $profile->profile->first_name : '' }}" name="first_name" id="" type="text" placeholder="First Name" >
                                    <div class="invalid-feedback">Please provide a valid state.</div>
                                </div>
                                <div class="col-md-6 mb-3">
                                    <label for="last_name">Last Name</label>
                                    <input class="form-control" value="{{ isset($profile->profile->last_name) ? $profile->profile->last_name : '' }}" id="" name="last_name" type="text" placeholder="Last Name" >
                                    <div class="invalid-feedback">Please provide a Country</div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 mb-3">
                                    <label for="validationCustom01">Username</label>
                                    <input id="name" type="text" class="form-control " name="name" value="{{ isset($profile->name) ? $profile->name : '' }}" required autocomplete="name" autofocus>
                                    <div class="valid-feedback">Looks good!</div>
                                </div>
                                <div class="col-md-6 mb-3">
                                    <label for="address">Email</label>
                                    <input class="form-control" value="{{isset($profile->email) ? $profile->email : '' }}" id="email" name="email" type="text" placeholder="Email" required="">
                                    <div class="invalid-feedback">Please provide a Address.</div>
                                </div>

                            </div>
                            <div class="row">


                                <div class="col-md-6 mb-3">
                                    <label for="mobile">Password</label>
                                    <input class="form-control" name="password" id="" type="password" placeholder="password" required="">
                                    <div class="invalid-feedback">Please provide a Mobile No.</div>
                                </div>
                                <div class="col-md-6 mb-3">
                                    <label for="company">Company Name</label>
                                    <input class="form-control" value="{{ isset($profile->profile->company) ? $profile->profile->company : '' }}" name="company" id="" type="text" placeholder="Company Name" >
                                    <div class="invalid-feedback">Please provide a company name</div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 mb-3">
                                    <label for="state">State</label>
                                    <input class="form-control"  value="{{ isset($profile->profile->state) ? $profile->profile->state : '' }}" name="state" id="" type="text" placeholder="State" required="">
                                    <div class="invalid-feedback">Please provide a valid state.</div>
                                </div>
                                <div class="col-md-6 mb-3">
                                    <label for="country">Country</label>
                                    <input class="form-control" value="{{ isset($profile->profile->country) ? $profile->profile->country : '' }}" id="" name="country" type="text" placeholder="Country" required="">
                                    <div class="invalid-feedback">Please provide a Country</div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 mb-3">
                                    <label for="city">City</label>
                                    <input class="form-control" name="city" id="" type="text" placeholder="City" value="{{ isset($profile->profile->city) ? $profile->profile->city : '' }}" required="">
                                    <div class="invalid-feedback">Please provide a valid city.</div>
                                </div>
                                <div class="col-md-6 mb-3">
                                    <label for="postal_code">Postal Code</label>
                                    <input class="form-control" name="postal_code" id="" type="text" placeholder="City" value="{{ isset($profile->profile->postal_code) ? $profile->profile->postal_code : '' }}" required="">
                                    <div class="invalid-feedback">Please provide a valid Postal Code.</div>
                                </div>

                            </div>
                            <div class="row">

                                <div class="col-md-6 mb-3">
                                    <label for="address">Address</label>
                                    <input class="form-control" value="{{isset($profile->profile->address) ? $profile->profile->address : '' }}" id="" name="address" type="text" placeholder="Address" required="">
                                    <div class="invalid-feedback">Please provide a Address.</div>
                                </div>

                                <div class="col-md-6 mb-3">
                                    <label for="vat_no">Timezone</label>
                                    <select class="form-control" name="timezone">
                                        <option value="" selected="" disabled="">Select One</option>
                                        @foreach($tzlist as $tzone )
                                            <option value="{{$tzone}}" @if(isset($profile->profile) ? $profile->profile->timezone : '' == $tzone) selected @endif>{{$tzone}}</option>
                                        @endforeach
                                    </select>
                                    <div class="invalid-feedback">Please provide a Timezone</div>
                                </div>
                            </div>
                            <button class="btn btn-primary" type="submit">Edit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- The Modal -->
    <div class="modal" id="myModal">
        <div class="modal-dialog">
            <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Take A Photo</h4>
                    <button type="button" onclick="closeCamera()" class="close customClose" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body text-center">
                    <div id="camera" style="height:auto;width:auto; text-align:left;"></div>
                    <p id="snapShot"></p>
                </div>
                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline-primary" onclick="takeSnapShot()">Upload</button>
                </div>

            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="{{asset('assets/js/form-validation-custom.js')}}"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/webcamjs/1.0.25/webcam.js"></script>
    <script>

        function uploadUserImage(){
            $('#uploadImage').trigger('click');
        }

        function onChangeImage(){
            let image = $('#uploadImage')[0].files[0];
            console.log('image');
            console.log(image);
            var formData = new FormData();
            formData.append('_token', '{{csrf_token()}}');
            formData.append('image', image);
            $.ajax({
                url:'{{route('updateUserImage')}}',
                method:'POST',
                data:formData,
                processData: false,
                contentType:false,
                cache: false,

                success: function (response) {
                    console.log('response');
                    console.log(response);
                    if(response.status == true) {
                        toastr.success(response.message);
                        setTimeout(() => {
                            window.location.reload();
                        },1000);
                    } else {
                        toastr.error(response.message);
                        setTimeout(() => {
                            window.location.reload();
                        },1000);
                    }
                },
                error: function (error) {
                    console.log('error');
                    console.log(error);
                }
            })
        }

        function openCamera() {
            $('#myModal').modal('show');
            // CAMERA SETTINGS.
            Webcam.set({
                width: 466,
                height: 350,
                image_format: 'jpeg',
                jpeg_quality: 100
            });

            Webcam.attach('#camera');

            // SHOW THE SNAPSHOT.
            takeSnapShot = function () {
                $('#camera').hide();
                Webcam.snap(function (data_uri) {
                    document.getElementById('snapShot').innerHTML =
                        '<img src="' + data_uri + '" width="466px" height="350px" />';
                    Webcam.reset();
                    uploadImage(data_uri);
                });
            }
        }

        function dataURItoBlob(dataURI) {
            var byteString = atob(dataURI.split(',')[1]);
            var ab = new ArrayBuffer(byteString.length);
            var ia = new Uint8Array(ab);
            for (var i = 0; i < byteString.length; i++) {
                ia[i] = byteString.charCodeAt(i);
            }
            return new Blob([ab], { type: 'image/jpeg' , extension:'jpg' });
        }

        function uploadImage(image){
            var blob = dataURItoBlob(image);
            var formData = new FormData();
            formData.append('_token', '{{csrf_token()}}');
            formData.append('image', blob);
            $.ajax({
                url:'{{route('updateUserImage')}}',
                method:'POST',
                data:formData,
                processData: false,
                contentType:false,
                cache: false,

                success: function (response) {
                    console.log('response');
                    console.log(response);
                    if(response.status == true) {
                        toastr.success(response.message);
                        setTimeout(() => {
                            window.location.reload();
                        },1000);
                    } else {
                        toastr.error(response.message);
                        setTimeout(() => {
                            window.location.reload();
                        },1000);
                    }
                },
                error: function (error) {
                    console.log('error');
                    console.log(error);
                }
            })
        }

        function closeCamera(){
            Webcam.reset();
        }
    </script>
@endsection
