@extends('userPortal.layout.app')
@section('title', 'Instagram')

@section('css')
@endsection

@push('style')
    <style>
        .panel-default{
            background: #fff;
            padding: 20px;
        }
    </style>
@endpush

@section('breadcrumb-title')
    <h3>Instagram Setup</h3>
@endsection

@section('breadcrumb-items')
    <li class="breadcrumb-item">Users</li>
    <li class="breadcrumb-item active">Add New User</li>
@endsection

@section('content')
    <br>
    <div>
        @if (session('success_message'))
            <div class="alert alert-success" role="alert">
                {{ session('success_message') }}
            </div>
        @endif
        @if (session('info_message'))
            <div class="alert alert-info" role="alert">
                {{ session('info_message') }}
            </div>
        @endif
        @if (Session::has('error_message'))
            <div class="alert alert-danger alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>{!! Session('error_message') !!}</strong>
            </div>
        @endif
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">

                    <div class="card-body">
                        @if (isset($insta) && !is_null($insta))
                            <div class="alert alert-success" role="alert" style="background:#C13584;border:0px;">
                                <span>
                                    Instagram Connected......
                                </span>
                            </div>
                        @endif
                        <form class="needs-validation" novalidate="" method="POST" action="{{route('connectToInsta')}}">
                            @csrf

                            <div class="row">
                                <div class="col-md-6 mb-3">
                                    <label for="first_name">User Name</label>
                                    <input class="form-control" value="{{ @$insta->username }}" name="user_name" id="" type="text" placeholder="Username" required="">
                                    <div class="invalid-feedback">Please provide a valid state.</div>
                                </div>
                                <div class="col-md-6 mb-3">
                                    <label for="last_name">Password</label>
                                    <input class="form-control" value="{{ @$insta->password }}" id="" name="password" type="password" placeholder="Password" required="">
                                    <div class="invalid-feedback">Please provide a Country</div>
                                </div>
                            </div>
                            <button class="btn btn-primary" type="submit">
                                <i class="fa fa-save"></i>
                                save
                            </button>
                        </form>
                    </div>
                </div>



            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="{{asset('assets/js/form-validation-custom.js')}}"></script>
@endsection
