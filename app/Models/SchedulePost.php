<?php

namespace App\Models;

use App\Traits\General;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class SchedulePost extends Model
{
    use HasFactory , General;

    const POST_SCHEDULE = 1;
    const SCHEDULE_POST_POSTED = 2;

    protected $fillable = [
        'user_id' ,
        'schedule_at' ,
        'minutes' ,
        'description' ,
        'location' ,
        'lat' ,
        'lng' ,
        'account_set_id' ,
        'file_type' ,
        'counter' ,
        'type' ,
        'schedule_at' ,
        'social_type' ,
        'images' ,
        'files' ,
        'status' ,
        'social_network'
    ];

    public static function uploadFile($path,$file){
        if($file) {
            $name = self::generateRandomString().'.png';
            $file->move($path,$name);
            return $path.'/'.$name;
        }
    }
    public static function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public static function schedulePostCreate($data , $social_network) {
        $minutes = self::getScheduleDayMinutes($data['schedule_at']);
        $counter = $data['counter'];
        $arr1 = [];
        $arr2 = [];
        for($i = 0; $i < $counter; $i++) {
            $media = $data["files-".$i];
            $media1 = $data["images-".$i];
            $media = self::uploadFile('uploads/schedule/post',$media);
            $arr1[] = $media;
            $media1 = self::uploadFile('uploads/schedule/post',$media1);
            $arr2[] = $media1;
        }
        return self::create([
            'user_id' => Auth::user()->id,
            'schedule_at' => $data['schedule_at'],
            'minutes' => $minutes,
            'description' => $data['description'],
            'location' => $data['location'],
            'lat' => $data['lat'],
            'lng' => $data['lng'],
            'account_set_id' => $data['account_set_id'],
            'file_type' => $data['file_type'],
            'counter' => $data['counter'],
            'type' => $data['type'],
            'social_network' => $social_network,
            'social_type' => $data['social_type'],
            'images' => \GuzzleHttp\json_encode($arr1),
            'files'=> \GuzzleHttp\json_encode($arr2),
            'status' => SchedulePost::POST_SCHEDULE,
        ]);
    }

    public static function getScheduleDayMinutes($date) {
        $post_date = $date;
        $date = Carbon::parse($post_date);
        $now = Carbon::now();
        $diff = $now->diffInMinutes($date);
        return $diff;
    }
}
