<?php

namespace App\Traits;


use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;
use function RingCentral\Psr7\str;

trait General
{
    function randomStringGenerator(){
        $seed = str_split('abcdefghijklmnopqrstuvwxyz'
            .'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
            .'0123456789!@#$&*');
        // and any other characters
        shuffle($seed);
        // probably optional since array_is randomized; this may be redundant
        $rand = '';
        foreach (array_rand($seed, 5) as $k) $rand .= $seed[$k];
        return strtoupper($rand);
    }

    function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function uploadFile($path,$file){
        if($file) {
            $name = self::generateRandomString().'.png';
            $file->move($path,$name);
            return $path.'/'.$name;
        }
    }

    public function uploadVideoFile($path,$file){
        if($file) {
            $filename = $file->getClientOriginalName();
            $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
            $name = $timestamp. '-' .$file->getClientOriginalName().'.mp4';
            $file->move($path,$name);
            return $path.'/'.$filename;
        }
    }

    public function sendEmail($receiverEmail , $array , $view , $msg){
        try {
            $senderEmail = env('MAIL_USERNAME');
            $senderName  ='planzer.com';
            $UserEmail = $receiverEmail;
            Mail::send($view, array('data' => $array), function ($message) use ($senderEmail, $senderName , $UserEmail,$msg) {
                $message->from($senderEmail, $senderName);
                $message->to($UserEmail)
                    ->subject($msg);
            });
        } catch (\Exception $e) {
            return $e->getMessage();
        }
        return true;
    }
}
