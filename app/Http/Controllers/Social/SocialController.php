<?php

namespace App\Http\Controllers\Social;

use App\Http\Controllers\Controller;
use App\Http\Controllers\SchedulePostController;
use App\Http\Requests\SocialSetRequest;
use App\Models\Media;
use App\Models\SchedulePost;
use App\Models\SocialAccountManager;
use App\Models\SocialSets;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class SocialController extends Controller
{

    public function socialSetsData($id) {
        $sets = SocialAccountManager::where('set_id','=',$id)->get();
        return collect([
            'status' => true,
            'data' => $sets
        ]);
    }

    public function getScheduleDates() {
        return SchedulePost::all();
    }

    public function connectedAccounts($set_id){
        $social_set = SocialSets::where('id','=',$set_id)->first();
        $accounts = SocialAccountManager::where('set_id','=',$set_id)->get();
        return collect([
            'status' => true,
            'data' => $accounts,
            'social_set' => $social_set
        ]);
    }

    public function updatePostSets(Request  $request){
        SocialSets::updateSocialSets($request->all());
        return back()->with('success_message','Set Update Successfully');
    }

    public function getSocialSets() {
        return SocialSets::where('user_id','=',Auth::user()->id)->get();
    }

    public function deleteImage($id){
        Media::where('id','=',$id)->delete();
        return back()->with('success_message','Media Image Deleted .. !');
    }

    public function index () {
        return view('social.index');
    }

    public function editSocialMedia($id) {
        $data = SocialSets::where('id','=',$id)->first();
        $accounts = json_decode($data->social_account);
        $allSocialSets = SocialSets::where('user_id','=',Auth::user()->id)->get();
        return view('social.add_sets',[
            'socialSets' => $data,
            'allSocialSets' => $allSocialSets,
            'accounts' => $accounts
        ]);
    }

    public function socialMediaDelete($id) {
        SocialSets::where('id','=',$id)->delete();
        return redirect()->back()->with('success_message','Set Deleted Successfully');
    }

    public function postMediaImages(Request $request){
        $count = (int)$request->length;
        for($i = 0; $i < $count; $i++) {
            if($request->hasFile('file' . $i)) {
                $file = $request->file('file' . $i);
                $filename = $request->file('file' . $i)->getClientOriginalName();
                $path = 'uploads/';
                $saveFile = url('/').'/'.$path.''.$filename;
                $file->move($path , $filename);
                Media::create([
                    'user_id' => Auth::user()->id,
                    'file' => $saveFile
                ]);
            }
        }
        $media = Media::where('user_id','=',Auth::user()->id)->get();
        return collect([
            'status' => true,
            'data' => $media
        ]);
    }

    public function addPostSets(SocialSetRequest  $request){
        try {
            DB::beginTransaction();

            SocialSets::createSocialSets($request->all());

            DB::commit();
            return response()->json([
                'status' => true,
                'message' => 'Set Added Successfully'
            ]);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'status' => false,
                'message' => $e->getMessage()
            ]);
        }
    }

    public function updateSets(Request  $request){
        try {
            DB::beginTransaction();
            SocialSets::updateSocialSets($request->all());

            DB::commit();
            return response()->json([
                'status' => true,
                'message' => 'Set Update Successfully'
            ]);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'status' => false,
                'message' => $e->getMessage()
            ]);
        }
    }

    public function addSocialSets() {
        $allSocialSets = SocialSets::where('user_id','=',Auth::user()->id)->get();
        return view('social.add_sets',[
            'allSocialSets' => $allSocialSets
        ]);
    }

    public function postToSocialMedia () {
        $media = Media::where('user_id','=',Auth::user()->id)->get();
        $allSocialSets = SocialSets::where('user_id','=',Auth::user()->id)->get();
        return view('social.main', [
            'allSocialSets' => $allSocialSets,
            'media' => $media
        ]);
    }

    public function postSendSocialMedia(Request $request) {
        if(isset($request->schedule_at)) {
            $socialSet = SocialAccountManager::where('set_id','=',$request->account_set_id)->get();
            foreach ($socialSet as $account) {
                $SchedulePostController = new SchedulePostController();
                $SchedulePostController->schedulePost($request , $account->social_network);
            }
            return collect([
                'status' => true,
                'message' => 'Post Schedule Successfully ... !',
                'schedule' => true,
                'schedule_at' => $request->schedule_at
            ]);
        } else {
            $check = $this->getAccountsAndPostToPlatform($request->account_set_id , $request->all());
            return $check;
        }
    }

    public function getAccountsAndPostToPlatform($accountId ,$request){
        $socialSet = SocialAccountManager::where('set_id','=',$accountId)->get();
        $linked_in = '';
        $twiiter = '';
        foreach ($socialSet as $account) {
            if($account->social_network == 'twitter') {
                $twitter = new TwitterController();
                $twitter = $twitter->postToTwitter($request);
                $status = $twitter['status'];
                $twiiter = $status;
            }
            if($account->social_network == 'link_den') {
                $linkdedin = new LinkDinController();
                $linkdedin = $linkdedin->postLinkedIn($request);
                $status = $linkdedin['status'];
                $linked_in = $status;
            }
        }
        if(isset($linked_in) && isset($twiiter)) {
            return collect([
                'status' => true,
                'data' => 'Posted Post Successfully ... !'
            ]);
        } else {
            return collect([
                'status' => false,
                'data' => 'Something went wrong  ... !'
            ]);
        }
    }
}
