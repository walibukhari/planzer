@extends('layouts.authentication.master')
@section('title', 'Login')

@section('css')
@endsection

@section('style')
@endsection


@section('content')
<div class="container-fluid">
   <div class="row">
      <div class="col-12">
         <div class="login-card">
            <div>
               <div><a class="logo" href="{{ route('index') }}">
                <img class="img-fluid for-dark" src="{{asset('assets/images/logo/logo_dark.png')}}" alt="looginpage"></a></div>
               <div class="login-main">
                 <form method="POST" class="" action="{{ route('postAdmin') }}">
                        @csrf
                     <h4>Sign in to account</h4>
                     <p>Enter your email & password to login</p>
                     <div class="form-group">
                        <label class="col-form-label">{{ __('E-Mail Address') }}</label>
                        <input id="email" type="email"  class="form-control" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus placeholder="Test@gmail.com">
                     </div>
                     <div class="form-group">
                        <label class="col-form-label">{{ __('Password') }}</label>
                        <input id="password" type="password" class="form-control" name="password" required autocomplete="current-password" placeholder="*********">
                     </div>
                     <button type="submit" class="btn btn-primary btn-block" type="submit"> {{ __('Login') }}</button>
                 </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection

@section('script')
@endsection
