@extends('layouts.authentication.master')
@section('title', 'Login')

@section('css')
@endsection

@section('style')
@endsection


@section('content')
<div class="container-fluid">
   <div class="row">
      <div class="col-12">
         <div class="login-card">
            <div>
               <div><a class="logo" href="{{ route('index') }}">
                <!-- <img class="img-fluid for-light" src="{{asset('assets/images/logo/logoo.png')}}" alt="looginpage"> -->
                <img class="img-fluid for-dark" src="{{asset('assets/images/logo/logo_dark.png')}}" alt="looginpage"></a>
               </div>
                @if(session()->has('error'))
                    <div class="row">
                        <div class="col-12">
                            <div class="alert alert-danger">
                                {{session()->get('error')}}
                            </div>
                        </div>
                    </div>
                @endif
               <div class="login-main">
                 <form method="POST" class="" action="{{ route('loginPost') }}">
                        @csrf
                     <h4>Sign in to account</h4>
                     <p>Enter your email & password to login</p>
                     <div class="form-group">
                        <label class="col-form-label">{{ __('E-Mail Address') }}</label>
                        <input id="email" type="email"  class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus placeholder="Test@gmail.com">
                         @error('email')
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                            </span>
                         @enderror
                     </div>
                     <div class="form-group">
                        <label class="col-form-label">{{ __('Password') }}</label>
                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password" placeholder="*********">
                           @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                        <div class="show-hide">
                            <!-- <span class="show">                         </span></div> -->
                     </div>
                     <div class="form-group mb-0">
                        <div class="checkbox p-0">
                           <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                           <label class="text-muted" for="checkbox1">{{ __('Remember Me') }}</label>
                        </div>
                        <!-- <a class="link" href="{{ route('forget-password') }}">Forgot password?</a> -->
                        <button class="btn btn-primary btn-block" type="submit"> {{ __('Login') }}</button>
 @if (Route::has('password.request'))
                                   <!--  <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a> -->
                                @endif
                         <!-- <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Login') }}
                                </button>


                            </div>
                        </div> -->
                     </div>
                    <!--  <h6 class="text-muted mt-4 or">Or Sign in with</h6>
                     <div class="social mt-4">
                        <div class="btn-showcase"><a class="btn btn-light" href="https://www.linkedin.com/login" target="_blank"><i class="txt-linkedin" data-feather="linkedin"></i> LinkedIn </a><a class="btn btn-light" href="https://twitter.com/login?lang=en" target="_blank"><i class="txt-twitter" data-feather="twitter"></i>twitter</a><a class="btn btn-light" href="https://www.facebook.com/" target="_blank"><i class="txt-fb" data-feather="facebook"></i>facebook</a></div>
                     </div> -->
                     <!-- <p class="mt-4 mb-0">Don't have account?<a class="ml-2" href="{{  route('sign-up') }}">Create Account</a></p> -->
                  </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection

@section('script')
@endsection
