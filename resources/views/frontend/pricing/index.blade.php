@extends('layouts.default')
@section('title')
    Pricing
@endsection
@push('css')
    <style>
        .tPF--alt--plan__item--free {
            margin-left: 0 !important;
            border-radius: 10px;
        }
        .qa--plan__item--starter {
            border-radius: 10px;
        }
        .tPF--alt--mpText__mobile {
            border-radius: 10px;
        }
        .tPF--alt--plan__item {
            border-radius: 10px !important;
        }
        .customHeight {
            height:699px;
        }
        .customSet{
            position: absolute;
            bottom: 35px;
        }
        .tPr--planchart{
            margin-top:150px;
        }
        .tPF--planToggle__option.tPF--planToggle__option--active {
            background: #FCD166 !important;
        }
        .tPF--planToggle {
            border: 1px solid #FCD166;
        }
        .tPF--planToggle__option.tPF--planToggle__option--active {
            background: #0544de !important;
        }
        .tPF--counter__minus, .tPF--counter__plus {
            background: #0544de !important;
        }
        .pt-lg-17, .py-lg-17 {
            padding-top: 12px !important;
            text-align: center;
        }
        .customFlex {
            text-align: center;
            color: #000;
        }
        .tPF--alt--plan__planSummary {
            line-height: 35px !important;
            text-align: center;
            color: #000 !important;
        }
        .tPF--alt--plan__priceCurrency {
            top: -6px !important;
            font-size: 1.64rem;
        }
        .u--m__t__xs {
            width: 100% !important;
            text-align: center !important;
            margin-top: 0.5rem !important;
            color:#000 !important;
        }
        .tPF--alt--plan__planIncludesTitle, .tPF--alt--plan__planAddonsTitle {
            font-size: 15px !important;
            font-family: "Rubik", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif;
            color: #323b43;
            font-weight: normal;
            margin: 22px 0 2px;
            text-align: center;
        }
    </style>

    <link crossorigin='anonymous' href='https://pro.fontawesome.com/releases/v5.14.0/css/all.css' integrity='sha384-VhBcF/php0Z/P5ZxlxaEx1GwqTQVIBu4G4giRWxTKOCjTxsPFETUDdVL5B6vYvOt' rel='stylesheet'>
    <link href="{{asset('css/pricing.css')}}" rel="stylesheet" />
    <script src='https://code.jquery.com/jquery-latest.js'></script>
@endpush

@section('content')
{{--    @include('frontend.pricing.oldDesign')--}}
    @include('frontend.pricing.newDesign')
@endsection

@push('js')
    <script>
        function toggleCookieBanner(){
            var element = document.getElementById("o--cookieBanner");
            var newDisplayValue = element.style.display == "none" ? "flex" : "none";
            element.style.display = newDisplayValue;
        }

        function showCookieBanner(){
            document.getElementById("o--cookieBanner").style.display = "flex";
        }
        function hideCookieBanner(){
            document.getElementById("o--cookieBanner").style.display = "none";
            setCookie("cookieConsent", "true", 365);
        }

        document.addEventListener("DOMContentLoaded", function(){
            let cookieConsent = getCookie("cookieConsent");
            if (cookieConsent != "true"){
                showCookieBanner();
            }else{
                hideCookieBanner();
            }
        });
    </script>
{{--    <script src="https://d2ekejdl2j2kuy.cloudfront.net/javascripts/all-c709c5cc.js"></script>--}}

{{--    <script>--}}
{{--        function incrementData() {--}}
{{--            let y2 = parseInt($('#y2').html());--}}
{{--            let y1 = parseInt($('#y1').html());--}}
{{--            let y3 = parseInt($('#y3').html());--}}
{{--            console.log('y2 Y1 Y3');--}}
{{--            console.log(y2);--}}
{{--            console.log(y3);--}}
{{--            console.log(y1);--}}
{{--            document.getElementById('tPF--counter__input--yearlySocialSets').stepUp();--}}
{{--        }--}}
{{--        function decrementData() {--}}
{{--            let y2 = parseInt($('#y2').html());--}}
{{--            let y1 = parseInt($('#y1').html());--}}
{{--            let y3 = parseInt($('#y3').html());--}}
{{--            console.log('y2 Y1 Y3');--}}
{{--            console.log(y2);--}}
{{--            console.log(y3);--}}
{{--            console.log(y1);--}}
{{--            document.getElementById('tPF--counter__input--yearlySocialSets').stepDown();--}}
{{--        }--}}
{{--        function changeValue(type){--}}
{{--        if(type == 'y') {--}}
{{--            $('#y3').show();--}}
{{--            $('#y2').show();--}}
{{--            $('#m3').hide();--}}
{{--            $('#m2').hide();--}}
{{--            $('#y1').show();--}}
{{--            $('#m1').hide();--}}
{{--            $('.yP3').show();--}}
{{--            $('.mP3').hide();--}}
{{--        }--}}
{{--        if(type == 'm'){--}}
{{--            $('#m3').show();--}}
{{--            $('#y3').hide();--}}
{{--            $('#m2').show();--}}
{{--            $('#y2').hide();--}}
{{--            $('#m1').show();--}}
{{--            $('#y1').hide();--}}
{{--            $('.yP3').hide();--}}
{{--            $('.mP3').show();--}}
{{--        }--}}
{{--        // if(pack == 'standard') {--}}
{{--        //     if(type == 'y') {--}}
{{--        //         $('#y2').show();--}}
{{--        //         $('#m2').hide();--}}
{{--        //     } else {--}}
{{--        //         $('#m2').show();--}}
{{--        //         $('#y2').hide();--}}
{{--        //     }--}}
{{--        // }--}}
{{--        // if(pack == 'basic') {--}}
{{--        //     if(type == 'y') {--}}
{{--        //         $('#y1').show();--}}
{{--        //         $('#m1').hide();--}}
{{--        //     } else {--}}
{{--        //         $('#m1').show();--}}
{{--        //         $('#y1').hide();--}}
{{--        //     }--}}
{{--        // }--}}
{{--    }--}}
{{--    </script>--}}
@endpush
