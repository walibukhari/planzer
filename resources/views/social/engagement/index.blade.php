

@extends('userPortal.layout.app')
@section('title', 'Engagement')

@section('css')
@endsection

@push('style')
    <style>
        .panel-default{
            background: #fff;
            padding: 20px;
            margin-bottom: 30px;
            border-radius: 15px;
        }
        .form-tabs{
            padding-left: 40px;
        }
        .form-tabs li a{
            color: #b3b7bb !important;
            font-size: 15px;
            padding:0px 20px;
            padding-bottom: 10px;


        }
        .form-tabs .active{
            border-bottom: 3px solid #1dc116 !important;
        }
        .form-tabs .active a{
            color: #1dc116 !important;
        }
        .avatar-img{
            margin-top: 5px;
            padding: 5px;
            margin-left: 18px;
            width: 15%;
            border-radius: 15px;
        }
        .card-heading{
            padding-top: 15px;
            padding-left: 5px;
        }
        .card-img-top{
            border-radius: 15px;
            margin: 15px;
            width: 90%;
            height: 275px;
        }
        .card-body-link{
            padding-left: 15%;
            font-size: 15px;
        }
        .card .card-body{
            padding: 10px !important;
            padding-bottom: 20px !important;
        }
        .card-body-link p{
            padding-left: 5px;
        }
        .socialaccount img{
            height:110px; display:block; margin:0 auto;margin-top:20px;
        }
        .socialaccount .title{
            text-align:center; font-weight:bold; font-size: 22px;margin:15px 0px;
        }
        .socialaccount:hover{
            box-shadow: 1px 1px 18px 9px #00000025;
            cursor: pointer;
        }
        .socialaccount .verified{
            position: absolute;
            top: 0;
            color: green;
            font-size:25px;
            margin: 10px;
            padding: 0px;

        }
        .socialaccount .unverified{
            position: absolute;
            top: 0;
            color: red;
            font-size:25px;
            margin: 10px;
            padding: 0px;

        }
    </style>
@endpush

@section('breadcrumb-title')
    <h3>Facebook Setup</h3>
@endsection

@section('breadcrumb-items')
    <li class="breadcrumb-item">Users</li>
    <li class="breadcrumb-item active">Add New User</li>
@endsection

@section('content')





    <div class="m-5">

        <div class="for-light">
            <div class="feed-account-head-light for-light">
                <h2>Select Account</h2>

            </div>
        </div>

        <div class="for-dark">
            <div class="feed-account-head-dark for-dark">
                <h2>Select Account</h2>

            </div>
        </div>



        <div class="row card panel-default" style="margin-top: 20px;
            width: 100% !important;
            margin: 0 auto;
            margin: 20px auto;">
            <ul class="form-tabs row">
                @foreach ($socialsets as $set )
                    <li class="tabitem @if($loop->first) active @endif" tabtoggle="socialset-{{$set->id}}"><a href="#">{{$set->name}}</a></li>
                @endforeach

            </ul>
        </div>

        @foreach ($socialsets as $set)

            @php
                $account_status  = App\Models\SocialAccountManager::where('set_id' , $set->id)->get();
                // dd($account_status);
            @endphp

            <div id="socialset-{{$set->id}}" class="tabbox row ml-auto mr-auto">
                @php
                    $social_accounts = json_decode($set->social_account);
                @endphp
                {{-- {{dd($social_accounts)}} --}}
                @foreach($social_accounts as $account)
                <div class="col-lg-4 col-md-6 col-sm-12   ">
                    @if ($account === 'linked_in')


                        <div class="card socialaccount">
                            <img src="{{url('/')}}/assets/icons/linkedin.png" alt="" style="">
                            <div class="title"  >LinkedIn </div>
                            @forelse ($account_status as $status )
                            {{-- {{$status->social_network}} --}}


                                @if ($status->social_network == 'link_den' )
                                    <div class="verified"><i class="fa fa-check-circle"></i></div>
                                    <a href="{{url('/')}}/user/engagement/feeds/{{$set->id}}/link_den" class="btn btn-success">Feeds</a>
                                @else
                                    <div class="unverified"><i class="fa fa-ban"></i></div>
                                    <button class="btn btn-secondary">Account Not Connected</button>

                                @endif

                            @empty
                                <div class="unverified"><i class="fa fa-ban"></i></div>
                                <button class="btn btn-secondary">Account Not Connected</button>
                            @endforelse

                        </div>


                    @else
                        <div class="card socialaccount">
                            <img src="{{url('/')}}/assets/icons/{{$account}}.png" alt="" >
                            <div class="title"  >{{ucwords($account)}}</div>
                            @forelse ($account_status as $status )
                            {{-- {{$status->social_network}} --}}
                                @if ($status->social_network === $account && $status->social_network != null )
                                    <div class="verified"><i class="fa fa-check-circle"></i></div>
                                    <a href="{{url('/')}}/user/engagement/feeds/{{$set->id}}/{{$account}}" class="btn btn-success">Feeds</a>

                                @else
                                    <div class="unverified"><i class="fa fa-ban"></i></div>
                                <button class="btn btn-secondary">Account Not Connected</button>


                                @endif
                            @empty
                                <div class="unverified"><i class="fa fa-ban"></i></div>
                                <button class="btn btn-secondary">Account Not Connected</button>


                            @endforelse
                        </div>

                        {{-- {{dd($account_status)}} --}}





                    @endif
                </div>
                @endforeach




            </div>

        @endforeach



    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

    <script defer>
        $(document).ready(function(){
            $('.tabbox').hide();
            $('.tabbox').first().show();

            $('.tabitem').click(function(){
                var toggleid = $(this).attr('tabtoggle');

                $('.tabitem').removeClass('active');
                $(this).addClass('active');

                $(`.tabbox`).hide();
                $(`#${toggleid}`).show();

            });
        });
    </script>



@endsection

@push('script')

    <script src="https://use.fontawesome.com/0e55b1dfb4.js"></script>
    <script src="{{asset('assets/js/form-validation-custom.js')}}"></script>

@endpush
