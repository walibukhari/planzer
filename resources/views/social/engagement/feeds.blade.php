

@extends('userPortal.layout.app')
@section('title', 'Engagement')

@section('css')
@endsection

@push('style')
    <style>
        .panel-default{
            background: #fff;
            padding: 20px;
            margin-bottom: 30px;
            border-radius: 15px;
        }
        .form-tabs{
            padding-left: 40px;
        }
        .form-tabs li a{
            color: #b3b7bb !important;
            font-size: 15px;
            padding:0px 20px;
            padding-bottom: 10px;


        }
        .form-tabs .active{
            border-bottom: 3px solid #1dc116 !important;
        }
        .form-tabs .active a{
            color: #1dc116 !important;
        }
        .avatar-img{
            margin-top: 5px;
            padding: 5px;
            margin-left: 18px;
            width: 15%;
            border-radius: 15px;
        }
        .card-heading{
            padding-top: 15px;
            padding-left: 5px;
        }
        .card-img-top{
            border-radius: 15px;
            margin: 15px;
            width: 90%;
            height: 275px;
        }
        .card-body-link{
            padding-left: 15%;
            font-size: 15px;
        }
        .card .card-body{
            padding: 10px !important;
            padding-bottom: 20px !important;
        }
        .card-body-link p{
            padding-left: 5px;
        }
    </style>
@endpush

@section('breadcrumb-title')
    <h3>Facebook Setup</h3>
@endsection

@section('breadcrumb-items')
    <li class="breadcrumb-item">Users</li>
    <li class="breadcrumb-item active">Add New User</li>
@endsection

@section('content')





    <div class="m-5">


        <div class="for-light">
            <div class="feed-account-head-light for-light">
                <div class="profile">
                    <img src="{{$account->avatar}}" alt="">
                </div>
                <div class="detail">
                    <div class="name">{{$account->name}}</div>
                    <div class="information">
                        <div class="tab">
                            <i class="fa fa-th-large"></i> 23 Posts
                        </div>
                        <div class="tab">
                            <i class="fa fa-users"></i> 23 Following
                        </div>
                        <div class="tab">
                            <i class="fa fa-users"></i> 23 Followers
                        </div>
                    </div>
                    <div class="bios">
                        lorem ipsum dolor sit amet, consectet lorem ipsum dolor sit amet

                    </div>
                    <div class="weblink"> <i class="fa fa-globe"></i> www.abc.com</div>
                </div>
            </div>
        </div>

        <div class="for-dark">
            <div class="feed-account-head-dark ">
                <div class="profile">
                    <img src="https://image.freepik.com/free-vector/profile-icon-male-avatar-hipster-man-wear-headphones_48369-8728.jpg" alt="">
                </div>
                <div class="detail">
                    <div class="name">JhonnyDepp</div>
                    <div class="information">
                        <div class="tab">
                            <i class="fa fa-th-large"></i> 23 Posts
                        </div>
                        <div class="tab">
                            <i class="fa fa-users"></i> 23 Following
                        </div>
                        <div class="tab">
                            <i class="fa fa-users"></i> 23 Followers
                        </div>
                    </div>
                    <div class="bios">
                        lorem ipsum dolor sit amet, consectet lorem ipsum dolor sit amet

                    </div>
                    <div class="weblink"> <i class="fa fa-globe"></i> www.abc.com</div>
                </div>
            </div>

        </div>



        <div class="row card panel-default" style="margin-top: 20px;
            width: 100% !important;
            margin: 0 auto;
            margin: 20px auto;">
            <ul class="form-tabs row">
                <li class="tabitem active" tabtoggle="home"><a href="#">Home</a></li>
                <li class="tabitem " tabtoggle="feed"><a href="#">Feed</a></li>
                <li class="tabitem " tabtoggle="followers"><a href="#">Followers</a></li>
                <li class="tabitem " tabtoggle="following"><a href="#">Following</a></li>
                <li class="tabitem " tabtoggle="tagged"><a href="#">Tagged</a></li>
                <li class="tabitem " tabtoggle="saved"><a href="#">Saved</a></li>
                <li class="tabitem " tabtoggle="stories"><a href="#">Stories</a></li>
                <li class="tabitem " tabtoggle="achived-stories"><a href="#">Archived Stories</a></li>
                <li class="tabitem " tabtoggle="search"><a href="#">Search</a></li>
            </ul>
        </div>
        <div id="home" class="tabbox row ml-auto mr-auto">
            <div class="col-lg-4 col-md-6 col-sm-12   ">
                <div class="card">
                    <div class="row" style="padding:10px;">
                        <img class="avatar-img" src="https://image.freepik.com/free-vector/profile-icon-male-avatar-hipster-man-wear-headphones_48369-8728.jpg">
                        <h5 class="card-heading">Breathembrella</h5>
                    </div>
                    <img class="card-img-top" src="https://graphberry.imgix.net/t/leon-psd-agency-template-thumb.png?auto=compress,format&w=360&q=80" alt="Card image cap">
                    <div class="card-body row">
                        <a class="row card-body-link">
                            <i class="fa fa-heart-o" aria-hidden="true"></i>
                            <p>1.2K</p>
                        </a>
                        <a class="row card-body-link">
                            <i class="fa fa-commenting-o" aria-hidden="true"></i>
                            <p>425</p>
                        </a>
                        <a class="row card-body-link">
                            <i class="fa fa-bookmark-o" aria-hidden="true"></i>
                            <p>13</p>
                        </a>
                        <a class="row card-body-link">
                            <i class="fa fa-paper-plane-o" aria-hidden="true"></i>
                            <p>35</p>
                        </a>
                    </div>
                </div>
            </div>

            <div class="col-lg-4 col-md-6 col-sm-12   ">
                <div class="card">
                    <div class="row" style="padding:10px;">
                        <img class="avatar-img" src="https://image.freepik.com/free-vector/profile-icon-male-avatar-hipster-man-wear-headphones_48369-8728.jpg">
                        <h5 class="card-heading">Breathembrella</h5>
                    </div>
                    <img class="card-img-top" src="https://graphberry.imgix.net/t/leon-psd-agency-template-thumb.png?auto=compress,format&w=360&q=80" alt="Card image cap">
                    <div class="card-body row">
                        <a class="row card-body-link">
                            <i class="fa fa-heart-o" aria-hidden="true"></i>
                            <p>1.2K</p>
                        </a>
                        <a class="row card-body-link">
                            <i class="fa fa-commenting-o" aria-hidden="true"></i>
                            <p>425</p>
                        </a>
                        <a class="row card-body-link">
                            <i class="fa fa-bookmark-o" aria-hidden="true"></i>
                            <p>13</p>
                        </a>
                        <a class="row card-body-link">
                            <i class="fa fa-paper-plane-o" aria-hidden="true"></i>
                            <p>35</p>
                        </a>
                    </div>
                </div>
            </div>

            <div class="col-lg-4 col-md-6 col-sm-12   ">
                <div class="card">
                    <div class="row" style="padding:10px;">
                        <img class="avatar-img" src="https://image.freepik.com/free-vector/profile-icon-male-avatar-hipster-man-wear-headphones_48369-8728.jpg">
                        <h5 class="card-heading">Breathembrella</h5>
                    </div>
                    <img class="card-img-top" src="https://graphberry.imgix.net/t/leon-psd-agency-template-thumb.png?auto=compress,format&w=360&q=80" alt="Card image cap">
                    <div class="card-body row">
                        <a class="row card-body-link">
                            <i class="fa fa-heart-o" aria-hidden="true"></i>
                            <p>1.2K</p>
                        </a>
                        <a class="row card-body-link">
                            <i class="fa fa-commenting-o" aria-hidden="true"></i>
                            <p>425</p>
                        </a>
                        <a class="row card-body-link">
                            <i class="fa fa-bookmark-o" aria-hidden="true"></i>
                            <p>13</p>
                        </a>
                        <a class="row card-body-link">
                            <i class="fa fa-paper-plane-o" aria-hidden="true"></i>
                            <p>35</p>
                        </a>
                    </div>
                </div>
            </div>

            <div class="col-lg-4 col-md-6 col-sm-12   ">
                <div class="card">
                    <div class="row" style="padding:10px;">
                        <img class="avatar-img" src="https://image.freepik.com/free-vector/profile-icon-male-avatar-hipster-man-wear-headphones_48369-8728.jpg">
                        <h5 class="card-heading">Breathembrella</h5>
                    </div>
                    <img class="card-img-top" src="https://graphberry.imgix.net/t/leon-psd-agency-template-thumb.png?auto=compress,format&w=360&q=80" alt="Card image cap">
                    <div class="card-body row">
                        <a class="row card-body-link">
                            <i class="fa fa-heart-o" aria-hidden="true"></i>
                            <p>1.2K</p>
                        </a>
                        <a class="row card-body-link">
                            <i class="fa fa-commenting-o" aria-hidden="true"></i>
                            <p>425</p>
                        </a>
                        <a class="row card-body-link">
                            <i class="fa fa-bookmark-o" aria-hidden="true"></i>
                            <p>13</p>
                        </a>
                        <a class="row card-body-link">
                            <i class="fa fa-paper-plane-o" aria-hidden="true"></i>
                            <p>35</p>
                        </a>
                    </div>
                </div>
            </div>

            <div class="col-lg-4 col-md-6 col-sm-12   ">
                <div class="card">
                    <div class="row" style="padding:10px;">
                        <img class="avatar-img" src="https://image.freepik.com/free-vector/profile-icon-male-avatar-hipster-man-wear-headphones_48369-8728.jpg">
                        <h5 class="card-heading">Breathembrella</h5>
                    </div>
                    <img class="card-img-top" src="https://graphberry.imgix.net/t/leon-psd-agency-template-thumb.png?auto=compress,format&w=360&q=80" alt="Card image cap">
                    <div class="card-body row">
                        <a class="row card-body-link">
                            <i class="fa fa-heart-o" aria-hidden="true"></i>
                            <p>1.2K</p>
                        </a>
                        <a class="row card-body-link">
                            <i class="fa fa-commenting-o" aria-hidden="true"></i>
                            <p>425</p>
                        </a>
                        <a class="row card-body-link">
                            <i class="fa fa-bookmark-o" aria-hidden="true"></i>
                            <p>13</p>
                        </a>
                        <a class="row card-body-link">
                            <i class="fa fa-paper-plane-o" aria-hidden="true"></i>
                            <p>35</p>
                        </a>
                    </div>
                </div>
            </div>

            <div class="col-lg-4 col-md-6 col-sm-12   ">
                <div class="card">
                    <div class="row" style="padding:10px;">
                        <img class="avatar-img" src="https://image.freepik.com/free-vector/profile-icon-male-avatar-hipster-man-wear-headphones_48369-8728.jpg">
                        <h5 class="card-heading">Breathembrella</h5>
                    </div>
                    <img class="card-img-top" src="https://graphberry.imgix.net/t/leon-psd-agency-template-thumb.png?auto=compress,format&w=360&q=80" alt="Card image cap">
                    <div class="card-body row">
                        <a class="row card-body-link">
                            <i class="fa fa-heart-o" aria-hidden="true"></i>
                            <p>1.2K</p>
                        </a>
                        <a class="row card-body-link">
                            <i class="fa fa-commenting-o" aria-hidden="true"></i>
                            <p>425</p>
                        </a>
                        <a class="row card-body-link">
                            <i class="fa fa-bookmark-o" aria-hidden="true"></i>
                            <p>13</p>
                        </a>
                        <a class="row card-body-link">
                            <i class="fa fa-paper-plane-o" aria-hidden="true"></i>
                            <p>35</p>
                        </a>
                    </div>
                </div>
            </div>



        </div>

        <div id="feed" class="tabbox row ml-auto mr-auto">
            <p style="text-align:center;">Feed</p>
        </div>


        <div id="followers" class="tabbox row ml-auto mr-auto">
            <p style="text-align:center;">followers</p>
        </div>
        <div id="following" class="tabbox row ml-auto mr-auto">
            <p style="text-align:center;">following</p>
        </div>
        <div id="tagged" class="tabbox row ml-auto mr-auto">
            <p style="text-align:center;">tagged</p>
        </div>
        <div id="saved" class="tabbox row ml-auto mr-auto">
            <p style="text-align:center;">saved</p>
        </div>

    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

    <script defer>
        $(document).ready(function(){
            $('.tabbox').hide();
            $('.tabbox').first().show();

            $('.tabitem').click(function(){
                var toggleid = $(this).attr('tabtoggle');

                $('.tabitem').removeClass('active');
                $(this).addClass('active');

                $(`.tabbox`).hide();
                $(`#${toggleid}`).show();

            });
        });
    </script>



@endsection

@push('script')

    <script src="https://use.fontawesome.com/0e55b1dfb4.js"></script>
    <script src="{{asset('assets/js/form-validation-custom.js')}}"></script>

@endpush
