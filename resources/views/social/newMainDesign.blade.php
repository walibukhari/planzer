
<link rel="stylesheet" href="{{url('/')}}/assets/css/socialpreview.css">

<style>
    .note {
        width: 500px;
        margin: 50px auto;
        font-size: 1.1em;
        color: #333;
        text-align: justify;
    }
    #drop-area {
        border: 2px dashed #ccc;
        border-radius: 20px;
        width: 480px;
        margin: 20px auto;
        padding: 20px;
    }
    #drop-area.highlight {
        border-color: purple;
    }
    p {
        margin-top: 0;
    }
    .my-form {
        margin-bottom: 10px;
    }
    #gallery {
        margin-top: 10px;
    }
    #gallery img {
        width: 50px;
        margin-bottom: 10px;
        margin-right: 10px;
        vertical-align: middle;
        height:50px;
    }
    .button {
        display: block;
        padding: 10px;
        width: 100%;
        text-align: center;
        color: white;
        background: gray;
        cursor: pointer;
        border-radius: 5px;
        border: 1px solid #ccc;
    }
    .button:hover {
        background: #ddd;
    }
    #fileElem {
        display: none;
    }
    #gallery .imageholder {
        width: fit-content;
        height: fit-content;
        float: left;
    }
    #gallery .imageholder .removeimage  {
        color: gray;
        width:fit-content;
        background-color:black;
        border-radius: 50%;
        transform: translate(-9px, 12px);
    }

    #gallery .imageholder .removeimage:hover  {
        color: white;

    }
    #gallery .imageholder .removeimage i  {
        margin: 0px;
        padding: 5px;

    }
</style>


<div class="container" id="container">
    <div class="row">
        <div id="mid-section-1" class="col-md-4" style="padding: 0px;">
            <div class="md-4Main">
                <div class="section-heading">
                    <h3 class="customH3">Media</h3>
                    <span>
                        <input type="file" multiple style="display: none;" id="selectFiles"
                               onchange="getImages(event)" />
                        <i class="fa fa-desktop" style="cursor:pointer; margin:0px;" onclick="uploadImages()"
                           aria-hidden="true"></i>
                        <i class="fa fa-link" style="margin:0px;" aria-hidden="true"></i>
                    </span>
                </div>
                <div class="section-body">
                    <div class="row" id="imagesMedia" data-simplebar>
                        @foreach($media as $file)
                            <div class="col-md-6 remove-{{$file->id}}" id="customImages">
                                <i class="fa fa-trash" onclick="window.location.href='{{route('deleteImage',[$file->id])}}'"
                                   style="color: red;cursor: pointer;font-size: 19px;position: absolute;top: 15px;left: 19px;"
                                   aria-hidden="true"></i>
                                <img draggable="true" id="drag1" data-id="{{$file->id}}" ondragstart="drag(event)" src="{{$file->file}}"
                                     style="width:100%;" />
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
        <div id="mid-section" class="col-md-4">
            <div class="md-4Main">
                <div class="section-heading">
                    <h3 class="customH3">New Post</h3>
                    <span>
                        <a href="/authentication/login" style="width:200px;display:flex;justify-content:flex-end;"
                           data-original-title="" title="">
                            <img class="img-fluid for-light " style="max-width:40%;margin-top:-12px;"
                                 src="http://localhost:8000/assets/images/logo/planzer_white.png" alt=""
                                 data-original-title="" title="">
                            <img class="img-fluid for-dark" style="max-width:40%;margin-top:-12px;"
                                 src="http://localhost:8000/assets/images/logo/planzer_dark.png" alt=""
                                 data-original-title="" title=""></a>
                    </span>
                </div>
                <div class="section-body">
                    <div class="row">
                        {{-- <div class="input-field" id="media-section-card">
                            <div class="input-images-2" style="padding-top: .5rem;">
                            </div>
                        </div> --}}
                        <div id="drop-area">
                            <form class="my-form" >
                                <p>Upload multiple files with the file dialog or by dragging and dropping images onto the dashed region</p>
                                <input type="file" id="fileElem" multiple accept="image/*" onchange="handleFiles(this.files)">
                                <label class="button" for="fileElem">Select some files</label>
                            </form>
                            <progress id="progress-bar" style="display:none;" max=100 value=0></progress>
                            <div id="gallery" ></div>
                        </div>
                    </div>
                    <br>
                    <br>
                    <div class="row">
                        <div class="col-md-6" style="padding: 0px;" onclick="sectionSelected('a')">
                            <div id="caption-area" class="section-selected-a">
                                Caption
                            </div>
                        </div>
                        <div class="col-md-6" style="padding: 0px;" onclick="sectionSelected('b')">
                            <div id="comment-area" class="section-selected-b">
                                First Comment
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="comment-post-section">
                            <div data-emojiarea="" style="width:100%;" data-type="unicode" data-global-picker="false">
                                <div class="emoji-button">😄</div>
                                <textarea name="description" class="form-control" placeholder="write caption ..."
                                          id="input1" onkeydown="removeWords()" onkeypress="getTextData()"
                                          rows="5"></textarea>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="customCheck1" data-original-title=""
                                   title="">
                            <label class="custom-control-label" for="customCheck1" onclick="scheduleTime()">
                                <span class="whiteClass">Schudule</span>
                            </label>
                        </div>
                    </div>
                    <br>
                    <div class="row" id="scheduleTime" style="display:none;">
                        <div class="col-md-12" style="padding-left:0px;padding-right:2px;">
                            <input type="date" class="form-control"
                                   style="background: rgb(60, 63, 78);color: #fff;border: 1px solid rgb(60, 63, 78);"
                                   id="schedule_at" name="schedule_at" />
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <span class="whiteClass">Advanced setting</span>
                    </div>
                    <br>
                    <div class="row">
                        <div class="location-section">
                            <i class="fa fa-map-marker" aria-hidden="true"></i>
                            <input type="text" class="form-control pac-target-input" id="pac-input" name="location"
                                   placeholder="Add Location...." data-original-title="" title="" autocomplete="off">
                            <div id="map" style="display: none; height: 300px; position: relative; overflow: hidden;">
                                <div
                                    style="height: 100%; width: 100%; position: absolute; top: 0px; left: 0px; background-color: rgb(229, 227, 223);">
                                    <div style="overflow: hidden;"></div>
                                    <div class="gm-style"
                                         style="position: absolute; z-index: 0; left: 0px; top: 0px; height: 100%; width: 100%; padding: 0px; border-width: 0px; margin: 0px;">
                                        <div tabindex="0" aria-label="Map" aria-roledescription="map" role="group"
                                             style="position: absolute; z-index: 0; left: 0px; top: 0px; height: 100%; width: 100%; padding: 0px; border-width: 0px; margin: 0px; cursor: url(&quot;https://maps.gstatic.com/mapfiles/openhand_8_8.cur&quot;), default; touch-action: pan-x pan-y;">
                                            <div
                                                style="z-index: 1; position: absolute; left: 50%; top: 50%; width: 100%;">
                                                <div
                                                    style="position: absolute; left: 0px; top: 0px; z-index: 100; width: 100%;">
                                                    <div style="position: absolute; left: 0px; top: 0px; z-index: 0;">
                                                    </div>
                                                </div>
                                                <div
                                                    style="position: absolute; left: 0px; top: 0px; z-index: 101; width: 100%;">
                                                </div>
                                                <div
                                                    style="position: absolute; left: 0px; top: 0px; z-index: 102; width: 100%;">
                                                </div>
                                                <div
                                                    style="position: absolute; left: 0px; top: 0px; z-index: 103; width: 100%;">
                                                    <div style="position: absolute; left: 0px; top: 0px; z-index: -1;">
                                                    </div>
                                                </div>
                                                <div style="position: absolute; left: 0px; top: 0px; z-index: 0;"></div>
                                            </div>
                                            <div class="gm-style-pbc"
                                                 style="z-index: 2; position: absolute; height: 100%; width: 100%; padding: 0px; border-width: 0px; margin: 0px; left: 0px; top: 0px; opacity: 0;">
                                                <p class="gm-style-pbt"></p>
                                            </div>
                                            <div
                                                style="z-index: 3; position: absolute; height: 100%; width: 100%; padding: 0px; border-width: 0px; margin: 0px; left: 0px; top: 0px; touch-action: pan-x pan-y;">
                                                <div
                                                    style="z-index: 4; position: absolute; left: 50%; top: 50%; width: 100%;">
                                                    <div
                                                        style="position: absolute; left: 0px; top: 0px; z-index: 104; width: 100%;">
                                                    </div>
                                                    <div
                                                        style="position: absolute; left: 0px; top: 0px; z-index: 105; width: 100%;">
                                                    </div>
                                                    <div
                                                        style="position: absolute; left: 0px; top: 0px; z-index: 106; width: 100%;">
                                                    </div>
                                                    <div
                                                        style="position: absolute; left: 0px; top: 0px; z-index: 107; width: 100%;">
                                                    </div>
                                                </div>
                                            </div>
                                        </div><iframe aria-hidden="true" frameborder="0" tabindex="-1"
                                                      style="z-index: -1; position: absolute; width: 100%; height: 100%; top: 0px; left: 0px; border: none;"></iframe>
                                        <div
                                            style="pointer-events: none; width: 100%; height: 100%; box-sizing: border-box; position: absolute; z-index: 1000002; opacity: 0; border: 2px solid rgb(26, 115, 232);">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" id="lat" name="lat" value="" data-original-title="" title="">
                            <input type="hidden" id="lng" name="lng" value="" data-original-title="" title="">
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <span class="whiteClass">Select Accounts</span>
                    </div>
                    <input type="hidden" name="type" value="media" />
                    <input type="hidden" name="file_type" value="image" />
                    <div class="row">
                        <select class="form-control" name="account_set_id" onchange="selectedSets()" id="account_set_id">
                            <option selected>select set</option>
                            @foreach($allSocialSets as $accounts)
                                <option value="{{$accounts->id}}">{{$accounts->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <br>
                    <div class="row">
                        <ul id="setul">

                        </ul>
                    </div>
                    <br>
                    <div class="row">
                        <button class="post-now-btn" onclick="postToSocialMedia()">Post Now</button>
                    </div>
                </div>
            </div>
        </div>
        <div id="mid-section-3" class="col-md-4" style="padding: 0px;">
            <div class="preview-holder">
                <div class="header">
                    <div class="brand">
                        <img class=" for-light " style=""
                             src="http://localhost:8000/assets/images/logo/planzer_white.png" alt=""
                             data-original-title="" title="">
                        <img class=" for-dark" style=""
                             src="http://localhost:8000/assets/images/logo/planzer_dark.png" alt=""
                             data-original-title="" title="">
                    </div>
                    <div class="social-switch-select">
                        <select name="social-switch-select" onchange="socialpreview()" class="select optionselection" id="">
                            <option value="instagram"> Instagram</option>
                            <option value="facebook">Facebook</option>
                            <option value="twitter">Twitter</option>
                            <option value="youtube">Youtube</option>
                            <option value="linkedin">LinkedIn</option>
                            <option value="tiktok">Tiktok</option>
                        </select>
                        <select name="posttype" onchange="socialpreview()" class="select" id="" style="display:none;">
                            <option value="post">Post</option>
                            <option value="story">Story</option>
                            <option value="album">Album</option>
                        </select>
                    </div>
                </div>

                {{-- forlight --}}
                @include('social.socialpreview.forlight')


                {{-- fordark --}}
                {{-- @include('social.socialpreview.fordark') --}}

            </div>
        </div>
    </div>
</div>



<script>
    // ************************ Drag and drop ***************** //
    let dropArea = document.getElementById("drop-area")

// Prevent default drag behaviors
    ;['dragenter', 'dragover', 'dragleave', 'drop'].forEach(eventName => {
        dropArea.addEventListener(eventName, preventDefaults, false)
        document.body.addEventListener(eventName, preventDefaults, false)
    })

// Highlight drop area when item is dragged over it
    ;['dragenter', 'dragover'].forEach(eventName => {
        dropArea.addEventListener(eventName, highlight, false)
    })

    ;['dragleave', 'drop'].forEach(eventName => {
        dropArea.addEventListener(eventName, unhighlight, false)
    })

    // Handle dropped files
    dropArea.addEventListener('drop', handleDrop, false)

    function preventDefaults (e) {
        e.preventDefault()
        e.stopPropagation()
    }

    function highlight(e) {
        dropArea.classList.add('highlight')
    }

    function unhighlight(e) {
        dropArea.classList.remove('active')
    }

    function handleDrop(e) {
        var dt = e.dataTransfer
        var files = dt.files

        //   console.log(sessionStorage.getItem('dimage'));
        $image = sessionStorage.getItem('dimage');
        if($image){
            // setsocialpreviewimage($image);
            // $('#gallery').append(`<img class="post-image-selection" src="${$image}">`);
            sessionStorage.removeItem('dimage');


            fetch(`${$image}`)
                .then(res => res.blob()) // Gets the response and returns it as a blob
                .then(blob => {
                    // Here's where you get access to the blob
                    // And you can use it for whatever you want
                    // Like calling ref().put(blob)

                    // Here, I use it to make an image appear on the page
                    let objectURL = URL.createObjectURL(blob);
                    let myImage = new Image();
                    myImage.src = objectURL;
                    // document.getElementById('myImg').appendChild(myImage)

                    var reader = new FileReader();
                    reader.onload = function(event){
                        setsocialpreviewimage(event.target.result);
                        $('#gallery').append(`
                    <div class="imageholder">
                        <div class="removeimage" ><i class="fa fa-times"></i></div>
                        <img class="post-image-selection" src="${event.target.result}">
                    </div>

                `);
                    }; // data url!
                    var source = reader.readAsDataURL(blob);

                });


        }
        else{
            handleFiles(files)
        }


    }

    let uploadProgress = []
    let progressBar = document.getElementById('progress-bar')

    function initializeProgress(numFiles) {
        progressBar.value = 0
        uploadProgress = []

        for(let i = numFiles; i > 0; i--) {
            uploadProgress.push(0)
        }
    }

    function updateProgress(fileNumber, percent) {
        uploadProgress[fileNumber] = percent
        let total = uploadProgress.reduce((tot, curr) => tot + curr, 0) / uploadProgress.length
        console.debug('update', fileNumber, percent, total)
        progressBar.value = total
    }

    function handleFiles(files) {
        files = [...files]
        initializeProgress(files.length)
//   files.forEach(uploadFile)
        files.forEach(previewFile)
    }

    function GetPostFiles(){
        var files = [];
        $('body').find('.post-image-selection').each(function(){
            var file = $(this).attr('src');
            files.push(dataURLtoFile(file,makeid(10)));

        });
        return files;
    }

    function makeid(length) {
        var result           = [];
        var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        var charactersLength = characters.length;
        for ( var i = 0; i < length; i++ ) {
            result.push(characters.charAt(Math.floor(Math.random() *
                charactersLength)));
        }
        return result.join('')+'.png';
    }

    function dataURLtoFile(dataurl, filename) {

        var arr = dataurl.split(','),
            mime = arr[0].match(/:(.*?);/)[1],
            bstr = atob(arr[1]),
            n = bstr.length,
            u8arr = new Uint8Array(n);

        while(n--){
            u8arr[n] = bstr.charCodeAt(n);
        }

        return new File([u8arr], filename, {type:mime});
    }

    function previewFile(file) {
        let reader = new FileReader()
        reader.readAsDataURL(file)
        reader.onloadend = function() {
            let img = document.createElement('img');
            img.src = reader.result;
            setsocialpreviewimage(reader.result);
            // document.getElementById('gallery').appendChild(img);
            $('#gallery').append(`
        <div class="imageholder">
            <div class="removeimage" ><i class="fa fa-times"></i></div>
            <img class="post-image-selection" src="${event.target.result}">
        </div>

    `);

        }
    }

    $(document).ready(function(){
        $('body').on('click','.post-image-selection',function(){
            var image = $(this).attr('src');
            setsocialpreviewimage(image);
        });
        $('body').on('click','.removeimage',function(){
            $(this).parent().remove();
        });

    });

    // function uploadFile(file, i) {
    //   var url = 'https://api.cloudinary.com/v1_1/joezimim007/image/upload'
    //   var xhr = new XMLHttpRequest()
    //   var formData = new FormData()
    //   xhr.open('POST', url, true)
    //   xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest')

    //   // Update progress (can be used to show progress indicator)
    //   xhr.upload.addEventListener("progress", function(e) {
    //     updateProgress(i, (e.loaded * 100.0 / e.total) || 100)
    //   })

    //   xhr.addEventListener('readystatechange', function(e) {
    //     if (xhr.readyState == 4 && xhr.status == 200) {
    //       updateProgress(i, 100) // <- Add this
    //     }
    //     else if (xhr.readyState == 4 && xhr.status != 200) {
    //       // Error. Inform the user
    //     }
    //   })

    //   formData.append('upload_preset', 'ujpu6gyk')
    //   formData.append('file', file)
    //   xhr.send(formData)
    // }
</script>
