<?php

namespace App\Jobs;

use App\Http\Controllers\SchedulePostController;
use App\Http\Controllers\Social\LinkDinController;
use App\Http\Controllers\Social\TwitterController;
use App\Models\SchedulePost;
use App\Models\SocialAccountManager;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Http\Request;

class SocialMediaPostScheduler implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $schedule = '';

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($schedule)
    {
        \Log::info('SocialMediaPostScheduler __construct running ... !');
        \Log::info($schedule);
        $this->schedule = $schedule;
        foreach ($this->schedule as $post) {
            if(Carbon::parse($post->schedule_at)->format('d-m-y') == Carbon::now()->format('d-m-y')) {
                \Log::info('Posting Date Matched Request Data Going To Post Data On Platform .... !');
                $account = SocialAccountManager::where('set_id','=',$post->account_set_id)->first();
                $data = [
                    'schedule_type' => 1,
                    'user_id' => $post['user_id'],
                    'minutes' => $post['minutes'],
                    'description' => $post['description'],
                    'location' => $post['location'],
                    'lat' => $post['lat'],
                    'lng' => $post['lng'],
                    'account_set_id' => $post['account_set_id'],
                    'file_type' => $post['file_type'],
                    'counter' => $post['counter'],
                    'type' => $post['type'],
                    'schedule_at' => $post['schedule_at'],
                    'social_type' => $post['social_type'],
                    'social_network' => $post['social_network'],
                    'images' => \GuzzleHttp\json_decode($post['images']),
                    'files' => \GuzzleHttp\json_decode($post['files']),
                    'status' => $post['status'],
                ];
                if($account->social_network == 'twitter') {
                    \Log::info('going to post to twitter ... !');
                    \Log::info($data);
                    $twitter = new TwitterController();
                    $twitter = $twitter->postToTwitter($data);
                    $status = $twitter['status'];
                    $twiiter = $status;
                    \Log::info('post to twitter successfully ... !');
                }
                if($account->social_network == 'link_den') {
                    session()->put('scheduled',1);
                    \Log::info('going to post to linkedin ... !');
                    $linkdedin = new LinkDinController();
                    $linkdedin = $linkdedin->postLinkedIn($data);
                    $status = $linkdedin['status'];
                    $linked_in = $status;
                    \Log::info('post to linkedIn successfully ... !');
                }
                \Log::info('going to update status on schedule post database ... !');
                $post->update([
                    'status' => SchedulePost::SCHEDULE_POST_POSTED,
                ]);
            }
        }
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        \Log::info('handler function calling ... ');
    }
}
