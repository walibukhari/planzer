<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Package;
use Auth;

class PackageController extends Controller
{
    public function create_new()
    {
        return view('packages.index');
    }

    public function store(Request $request)
    {
        if ($request->package_id) {
            $update_package = Package::where('id',$request->package_id)->first();
            $update_package->name                     = $request->name;
            $update_package->description              = $request->description;
            $update_package->storage                  = $request->storage;
            $update_package->file_size                = $request->file_size;
            $update_package->price_monthly            = $request->price_monthly;
            $update_package->price_anually            = $request->price_anually;
            $update_package->no_of_account            = $request->no_of_account;
            $update_package->sort                     = $request->sort;
            $update_package->team_users               = $request->team_users;
            $update_package->google_drive             = $request->google_drive;
            $update_package->dropbox                  = $request->dropbox;
            $update_package->one_drive                = $request->one_drive;
            $update_package->photo                    = $request->photo;
            $update_package->video                    = $request->video;
            $update_package->watermark                = $request->watermark;
            $update_package->image_editor             = $request->image_editor;
            $update_package->drafts                   = $request->drafts;
            $update_package->rss                      = $request->rss;
            $update_package->automations              = $request->automations;
            $update_package->bulk_schedule            = $request->bulk_schedule;

            $update_package->facebook                 = $request->facebook;
            $update_package->facebook_post            = $request->facebook_post;
            $update_package->livestream               = $request->livestream;
            $update_package->direct_message           = $request->direct_message;

            $update_package->instagram                = $request->instagram;
            $update_package->instagram_post           = $request->instagram_post;
            $update_package->analytics                = $request->analytics;
            $update_package->instagram_livestream     = $request->instagram_livestream;
            $update_package->igtv                     = $request->igtv;
            $update_package->instagram_tag            = $request->instagram_tag;
            $update_package->instagram_direct_message = $request->instagram_direct_message;

            $update_package->twitter                  = $request->twitter;
            $update_package->twitter_post             = $request->twitter_post;
            $update_package->twitter_direct_message   = $request->twitter_direct_message;


            $update_package->linkedin                 = $request->linkedin;
            $update_package->linkedin_post            = $request->linkedin_post;


            $update_package->tiktok                   = $request->tiktok;
            $update_package->tiktok_post              = $request->tiktok_post;

            $update_package->youtube                  = $request->youtube;
            $update_package->youtube_post             = $request->youtube_post;

            $update_package->status                   = $request->status;
            $update_package->is_default               = $request->is_default;
            // $package->created_by               = Auth()->id;
            $update_package->save();
            return back()->withStatus(__('Package Updated successfully.'));
        }

        $package =  new Package();
        $package->name                     = $request->name;
        $package->description              = $request->description;
        $package->storage                  = $request->storage;
        $package->file_size                = $request->file_size;
        $package->price_monthly            = $request->price_monthly;
        $package->price_anually            = $request->price_anually;
        $package->no_of_account            = $request->no_of_account;
        $package->sort                     = $request->sort;
        $package->team_users               = $request->team_users;
        $package->google_drive             = $request->google_drive;
        $package->dropbox                  = $request->dropbox;
        $package->one_drive                = $request->one_drive;
        $package->photo                    = $request->photo;
        $package->video                    = $request->video;
        $package->watermark                = $request->watermark;
        $package->image_editor             = $request->image_editor;
        $package->drafts                   = $request->drafts;
        $package->rss                      = $request->rss;
        $package->automations              = $request->automations;
        $package->bulk_schedule            = $request->bulk_schedule;

        $package->facebook                 = $request->facebook;
        $package->facebook_post            = $request->facebook_post;
        $package->livestream               = $request->livestream;
        $package->direct_message           = $request->direct_message;

        $package->instagram                = $request->instagram;
        $package->instagram_post           = $request->instagram_post;
        $package->analytics                = $request->analytics;
        $package->instagram_livestream     = $request->instagram_livestream;
        $package->igtv                     = $request->igtv;
        $package->instagram_tag            = $request->instagram_tag;
        $package->instagram_direct_message = $request->instagram_direct_message;

        $package->twitter                  = $request->twitter;
        $package->twitter_post             = $request->twitter_post;
        $package->twitter_direct_message   = $request->twitter_direct_message;


        $package->linkedin                 = $request->linkedin;
        $package->linkedin_post            = $request->linkedin_post;

        $package->tiktok                   = $request->tiktok;
        $package->tiktok_post              = $request->tiktok_post;

        $package->youtube                  = $request->youtube;
        $package->youtube_post             = $request->youtube_post;

        $package->status                   = 1;
        $package->is_default               = 1;
        // $package->created_by               = Auth()->id;
        $package->save();
        return back()->withStatus(__('Package Saved successfully.'));
    }

    public function all_packages()
    {
        $data['counter'] = 1;
        $data['package'] = Package::all();
        return view('packages.list',$data);
    }

    public function edit_package($id)
    {
        $data['package'] = Package::find($id);
        return view('packages.edit',$data);
    }

    public function delete_package($id)
    {
        Package::where('id',$id)->delete();
        return back()->withStatus(__('Package Deleted'));
    }
}
